-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 23, 2020 at 02:30 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spiritpedia`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `alias` varchar(300) NOT NULL,
  `short_description` varchar(2000) NOT NULL,
  `description` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `banner_image` varchar(200) NOT NULL,
  `banner_text` varchar(200) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `meta_title` varchar(200) NOT NULL,
  `meta_keyword` varchar(1000) NOT NULL,
  `meta_description` varchar(1000) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `title`, `alias`, `short_description`, `description`, `banner_image`, `banner_text`, `menu_id`, `image`, `meta_title`, `meta_keyword`, `meta_description`, `status`, `created_at`, `sort`) VALUES
(1, 'About Spiritpedia', 'About-Spiritpedia', 'Whisky or whiskey is a type of distilled alcoholic beverage made from fermented grain mash. Various grains (which may be malted) are used for different varieties, including barley, corn, rye, and wheat. Whisky is typically aged in wooden casks, generally made of charred white oak.', '<p>Who We Are?</p>\r\n\r\n<p><strong>Whisky</strong>&nbsp;or&nbsp;<strong>whiskey</strong>&nbsp;is a type of&nbsp;<a href=\"https://en.wikipedia.org/wiki/Distilled_beverage\">distilled alcoholic beverage</a>&nbsp;made from&nbsp;<a href=\"https://en.wikipedia.org/wiki/Fermentation_in_food_processing\">fermented</a>&nbsp;<a href=\"https://en.wikipedia.org/wiki/Grain\">grain</a>&nbsp;<a href=\"https://en.wikipedia.org/wiki/Mashing\">mash</a>. Various grains (which may be&nbsp;<a href=\"https://en.wikipedia.org/wiki/Malting_process\">malted</a>) are used for different varieties, including&nbsp;<a href=\"https://en.wikipedia.org/wiki/Barley\">barley</a>,&nbsp;<a href=\"https://en.wikipedia.org/wiki/Maize\">corn</a>,&nbsp;<a href=\"https://en.wikipedia.org/wiki/Rye\">rye</a>, and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Wheat\">wheat</a>. Whisky is typically&nbsp;<a href=\"https://en.wikipedia.org/wiki/Aging_(food)\">aged</a>&nbsp;in wooden&nbsp;<a href=\"https://en.wikipedia.org/wiki/Cask\">casks</a>, generally made of charred&nbsp;<a href=\"https://en.wikipedia.org/wiki/White_oak\">white oak</a>.</p>\r\n\r\n<p>Whisky is a strictly regulated spirit worldwide with many classes and types. The typical unifying characteristics of the different classes and types are the fermentation of grains, distillation, and aging in wooden barrels.</p>\r\n', 'http://localhost:8888/spirit/backend/web/about-us/imageedit_3_3936675731.png', 'About Spiritpedia', 0, '', '', '', '', 1, '2020-06-18 10:58:56', 0);

-- --------------------------------------------------------

--
-- Table structure for table `address_book`
--

CREATE TABLE `address_book` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(500) NOT NULL,
  `address_flag` int(2) NOT NULL,
  `mobile_no` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `state` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `postal_code` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `default_address` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address_book`
--

INSERT INTO `address_book` (`id`, `user_id`, `name`, `address`, `address_flag`, `mobile_no`, `email`, `state`, `city`, `postal_code`, `status`, `created_at`, `default_address`) VALUES
(1, 7, 'Abhilash Dhone', 'office no. 305, venture building, bhusari colony, Kothrud', 2, '9856235689', 'abhilash@xceedtech.in', 15, 30, 411038, 1, '2019-10-15 07:07:16', 0),
(2, 7, 'Ketaki Tankhiwale', 'office no. 305, venture building, bhusari colony, Kothrud', 2, '9856235689', 'ketaki@xceedtech.in', 15, 30, 411038, 0, '2019-10-15 08:49:42', 0),
(3, 7, 'Abhilah', 'Hshsbshd', 1, '9764648787', 'Deadpoet8@gmail.com', 1, 1, 979464, 1, '2019-10-16 09:19:06', 1),
(4, 7, 'Dhanashree supare', 'Hshsbshd sjsbsbd\n', 1, '9794548484', 'Deadpoet8@gmail.com', 7, 7, 948464, 0, '2019-10-16 09:21:42', 0),
(5, 7, 'Dhanashree supare', 'Kothrud venture building, bhusary colony pad road kothrud', 1, '3467848764', 'Deadpoet8@gmail.com', 1, 1, 976464, 1, '2019-10-17 08:29:05', 0);

-- --------------------------------------------------------

--
-- Table structure for table `blog_master`
--

CREATE TABLE `blog_master` (
  `id` int(30) NOT NULL,
  `title` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `author` varchar(150) NOT NULL,
  `short_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) NOT NULL,
  `banner_image` varchar(100) NOT NULL,
  `banner_text` varchar(500) NOT NULL,
  `meta_title` varchar(200) NOT NULL,
  `meta_description` varchar(1000) NOT NULL,
  `meta_keyword` varchar(1000) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1,
  `sort` int(2) NOT NULL,
  `created_at` date NOT NULL,
  `page_heading` varchar(500) NOT NULL,
  `blog_date` date NOT NULL,
  `category` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_master`
--

INSERT INTO `blog_master` (`id`, `title`, `alias`, `author`, `short_description`, `description`, `image`, `banner_image`, `banner_text`, `meta_title`, `meta_description`, `meta_keyword`, `status`, `sort`, `created_at`, `page_heading`, `blog_date`, `category`) VALUES
(1, 'Whiskey Maturation in different types of oak casks.', 'whiskey-maturation-in-different-types-of-oak-casks-', 'Bhushan Palkar', '<p>Before storing the whisky, the barrels are charred. This creates a layer of charcoal on the inside which filters the spirit and removes the undesirable flavours. It would not be wrong to state that the environment outside also matters which includes temperature.</p>\r\n', '<p>When Glenlivet is freshly distilled it is clear in colour and tastes cereal-rich and slightly smoky. But fifty years later of being stored in an oak barrel it develops a deep amber colour, the smoky aroma is gone and there are new caramel and chocolate characteristics. During maturation, whisky is largely influenced by the environment inside the barrel. Law has made it simpler by laying down that all casks have to be made of oak. Bourbon must be aged in previously unused casks whereas Scotch whisky should be held in barrels which were previously used to store sherry or bourbon. Before storing the whisky, the barrels are charred. This creates a layer of charcoal on the inside which filters the spirit and removes the undesirable flavours. It would not be wrong to state that the environment outside also matters which includes temperature. The various types of whisky casks are Gorda, Madeira Drum, Port Pipe, Butt, Puncheon, Barrique, Hogshead, ASB, Quarter Cask and Blood Tub. They differ in capacity. It is important to note that smaller the cask the more contact the whisky has with the wood. Interestingly, in the olden days, it was accidentally discovered that whisky matures in oak casks. Wooden barrels were merely used as vessels to store freshly produced distilled spirit. A lot of spirits was consumed before it matured into something like what we drink today. Nevertheless, a few casks were left laying for long. One can only imagine the delight of the Scotsmen who accidentally discovered the magical effects of wood on whisky. Soon drinkers developed a preference for matured whisky following which the process of oak cask maturation gained ground.</p>\r\n', 'http://admin.spiritpedia.xceedtech.in/images/blog/House Party.jpg', 'http://admin.spiritpedia.xceedtech.in/images/blog/Chivas regal 12.jpg', 'test', '', '', '', 1, 0, '2019-11-19', '', '2019-11-18', '');

-- --------------------------------------------------------

--
-- Table structure for table `bookmark_master`
--

CREATE TABLE `bookmark_master` (
  `id` int(30) NOT NULL,
  `thread_id` int(30) NOT NULL,
  `added_date` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '1-active, 0-inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cart_master`
--

CREATE TABLE `cart_master` (
  `id` int(30) NOT NULL,
  `user_id` int(30) NOT NULL,
  `product_id` int(30) NOT NULL,
  `price` int(30) NOT NULL,
  `quantity` int(30) NOT NULL,
  `added_date` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '1-active, 0-inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `stateID` int(11) NOT NULL,
  `city` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `stateID`, `city`) VALUES
(1, 32, 'North and Middle Andaman'),
(2, 32, 'South Andaman'),
(3, 32, 'Nicobar'),
(4, 1, 'Adilabad'),
(5, 1, 'Anantapur'),
(6, 1, 'Chittoor'),
(7, 1, 'East Godavari'),
(8, 1, 'Guntur'),
(9, 1, 'Hyderabad'),
(10, 1, 'Kadapa'),
(11, 1, 'Karimnagar'),
(12, 1, 'Khammam'),
(13, 1, 'Krishna'),
(14, 1, 'Kurnool'),
(15, 1, 'Mahbubnagar'),
(16, 1, 'Medak'),
(17, 1, 'Nalgonda'),
(18, 1, 'Nellore'),
(19, 1, 'Nizamabad'),
(20, 1, 'Prakasam'),
(21, 1, 'Rangareddi'),
(22, 1, 'Srikakulam'),
(23, 1, 'Vishakhapatnam'),
(24, 1, 'Vizianagaram'),
(25, 1, 'Warangal'),
(26, 1, 'West Godavari'),
(27, 3, 'Anjaw'),
(28, 3, 'Changlang'),
(29, 3, 'East Kameng'),
(30, 3, 'Lohit'),
(31, 3, 'Lower Subansiri'),
(32, 3, 'Papum Pare'),
(33, 3, 'Tirap'),
(34, 3, 'Dibang Valley'),
(35, 3, 'Upper Subansiri'),
(36, 3, 'West Kameng'),
(37, 2, 'Barpeta'),
(38, 2, 'Bongaigaon'),
(39, 2, 'Cachar'),
(40, 2, 'Darrang'),
(41, 2, 'Dhemaji'),
(42, 2, 'Dhubri'),
(43, 2, 'Dibrugarh'),
(44, 2, 'Goalpara'),
(45, 2, 'Golaghat'),
(46, 2, 'Hailakandi'),
(47, 2, 'Jorhat'),
(48, 2, 'Karbi Anglong'),
(49, 2, 'Karimganj'),
(50, 2, 'Kokrajhar'),
(51, 2, 'Lakhimpur'),
(52, 2, 'Marigaon'),
(53, 2, 'Nagaon'),
(54, 2, 'Nalbari'),
(55, 2, 'North Cachar Hills'),
(56, 2, 'Sibsagar'),
(57, 2, 'Sonitpur'),
(58, 2, 'Tinsukia'),
(59, 4, 'Araria'),
(60, 4, 'Aurangabad'),
(61, 4, 'Banka'),
(62, 4, 'Begusarai'),
(63, 4, 'Bhagalpur'),
(64, 4, 'Bhojpur'),
(65, 4, 'Buxar'),
(66, 4, 'Darbhanga'),
(67, 4, 'Purba Champaran'),
(68, 4, 'Gaya'),
(69, 4, 'Gopalganj'),
(70, 4, 'Jamui'),
(71, 4, 'Jehanabad'),
(72, 4, 'Khagaria'),
(73, 4, 'Kishanganj'),
(74, 4, 'Kaimur'),
(75, 4, 'Katihar'),
(76, 4, 'Lakhisarai'),
(77, 4, 'Madhubani'),
(78, 4, 'Munger'),
(79, 4, 'Madhepura'),
(80, 4, 'Muzaffarpur'),
(81, 4, 'Nalanda'),
(82, 4, 'Nawada'),
(83, 4, 'Patna'),
(84, 4, 'Purnia'),
(85, 4, 'Rohtas'),
(86, 4, 'Saharsa'),
(87, 4, 'Samastipur'),
(88, 4, 'Sheohar'),
(89, 4, 'Sheikhpura'),
(90, 4, 'Saran'),
(91, 4, 'Sitamarhi'),
(92, 4, 'Supaul'),
(93, 4, 'Siwan'),
(94, 4, 'Vaishali'),
(95, 4, 'Pashchim Champaran'),
(96, 36, 'Bastar'),
(97, 36, 'Bilaspur'),
(98, 36, 'Dantewada'),
(99, 36, 'Dhamtari'),
(100, 36, 'Durg'),
(101, 36, 'Jashpur'),
(102, 36, 'Janjgir-Champa'),
(103, 36, 'Korba'),
(104, 36, 'Koriya'),
(105, 36, 'Kanker'),
(106, 36, 'Kawardha'),
(107, 36, 'Mahasamund'),
(108, 36, 'Raigarh'),
(109, 36, 'Rajnandgaon'),
(110, 36, 'Raipur'),
(111, 36, 'Surguja'),
(112, 29, 'Diu'),
(113, 29, 'Daman'),
(114, 25, 'Central Delhi'),
(115, 25, 'East Delhi'),
(116, 25, 'New Delhi'),
(117, 25, 'North Delhi'),
(118, 25, 'North East Delhi'),
(119, 25, 'North West Delhi'),
(120, 25, 'South Delhi'),
(121, 25, 'South West Delhi'),
(122, 25, 'West Delhi'),
(123, 26, 'North Goa'),
(124, 26, 'South Goa'),
(125, 5, 'Ahmedabad'),
(126, 5, 'Amreli District'),
(127, 5, 'Anand'),
(128, 5, 'Banaskantha'),
(129, 5, 'Bharuch'),
(130, 5, 'Bhavnagar'),
(131, 5, 'Dahod'),
(132, 5, 'The Dangs'),
(133, 5, 'Gandhinagar'),
(134, 5, 'Jamnagar'),
(135, 5, 'Junagadh'),
(136, 5, 'Kutch'),
(137, 5, 'Kheda'),
(138, 5, 'Mehsana'),
(139, 5, 'Narmada'),
(140, 5, 'Navsari'),
(141, 5, 'Patan'),
(142, 5, 'Panchmahal'),
(143, 5, 'Porbandar'),
(144, 5, 'Rajkot'),
(145, 5, 'Sabarkantha'),
(146, 5, 'Surendranagar'),
(147, 5, 'Surat'),
(148, 5, 'Vadodara'),
(149, 5, 'Valsad'),
(150, 6, 'Ambala'),
(151, 6, 'Bhiwani'),
(152, 6, 'Faridabad'),
(153, 6, 'Fatehabad'),
(154, 6, 'Gurgaon'),
(155, 6, 'Hissar'),
(156, 6, 'Jhajjar'),
(157, 6, 'Jind'),
(158, 6, 'Karnal'),
(159, 6, 'Kaithal'),
(160, 6, 'Kurukshetra'),
(161, 6, 'Mahendragarh'),
(162, 6, 'Mewat'),
(163, 6, 'Panchkula'),
(164, 6, 'Panipat'),
(165, 6, 'Rewari'),
(166, 6, 'Rohtak'),
(167, 6, 'Sirsa'),
(168, 6, 'Sonepat'),
(169, 6, 'Yamuna Nagar'),
(170, 6, 'Palwal'),
(171, 7, 'Bilaspur'),
(172, 7, 'Chamba'),
(173, 7, 'Hamirpur'),
(174, 7, 'Kangra'),
(175, 7, 'Kinnaur'),
(176, 7, 'Kulu'),
(177, 7, 'Lahaul and Spiti'),
(178, 7, 'Mandi'),
(179, 7, 'Shimla'),
(180, 7, 'Sirmaur'),
(181, 7, 'Solan'),
(182, 7, 'Una'),
(183, 8, 'Anantnag'),
(184, 8, 'Badgam'),
(185, 8, 'Bandipore'),
(186, 8, 'Baramula'),
(187, 8, 'Doda'),
(188, 8, 'Jammu'),
(189, 8, 'Kargil'),
(190, 8, 'Kathua'),
(191, 8, 'Kupwara'),
(192, 8, 'Leh'),
(193, 8, 'Poonch'),
(194, 8, 'Pulwama'),
(195, 8, 'Rajauri'),
(196, 8, 'Srinagar'),
(197, 8, 'Samba'),
(198, 8, 'Udhampur'),
(199, 34, 'Bokaro'),
(200, 34, 'Chatra'),
(201, 34, 'Deoghar'),
(202, 34, 'Dhanbad'),
(203, 34, 'Dumka'),
(204, 34, 'Purba Singhbhum'),
(205, 34, 'Garhwa'),
(206, 34, 'Giridih'),
(207, 34, 'Godda'),
(208, 34, 'Gumla'),
(209, 34, 'Hazaribagh'),
(210, 34, 'Koderma'),
(211, 34, 'Lohardaga'),
(212, 34, 'Pakur'),
(213, 34, 'Palamu'),
(214, 34, 'Ranchi'),
(215, 34, 'Sahibganj'),
(216, 34, 'Seraikela and Kharsawan'),
(217, 34, 'Pashchim Singhbhum'),
(218, 34, 'Ramgarh'),
(219, 9, 'Bidar'),
(220, 9, 'Belgaum'),
(221, 9, 'Bijapur'),
(222, 9, 'Bagalkot'),
(223, 9, 'Bellary'),
(224, 9, 'Bangalore Rural District'),
(225, 9, 'Bangalore Urban District'),
(226, 9, 'Chamarajnagar'),
(227, 9, 'Chikmagalur'),
(228, 9, 'Chitradurga'),
(229, 9, 'Davanagere'),
(230, 9, 'Dharwad'),
(231, 9, 'Dakshina Kannada'),
(232, 9, 'Gadag'),
(233, 9, 'Gulbarga'),
(234, 9, 'Hassan'),
(235, 9, 'Haveri District'),
(236, 9, 'Kodagu'),
(237, 9, 'Kolar'),
(238, 9, 'Koppal'),
(239, 9, 'Mandya'),
(240, 9, 'Mysore'),
(241, 9, 'Raichur'),
(242, 9, 'Shimoga'),
(243, 9, 'Tumkur'),
(244, 9, 'Udupi'),
(245, 9, 'Uttara Kannada'),
(246, 9, 'Ramanagara'),
(247, 9, 'Chikballapur'),
(248, 9, 'Yadagiri'),
(249, 10, 'Alappuzha'),
(250, 10, 'Ernakulam'),
(251, 10, 'Idukki'),
(252, 10, 'Kollam'),
(253, 10, 'Kannur'),
(254, 10, 'Kasaragod'),
(255, 10, 'Kottayam'),
(256, 10, 'Kozhikode'),
(257, 10, 'Malappuram'),
(258, 10, 'Palakkad'),
(259, 10, 'Pathanamthitta'),
(260, 10, 'Thrissur'),
(261, 10, 'Thiruvananthapuram'),
(262, 10, 'Wayanad'),
(263, 11, 'Alirajpur'),
(264, 11, 'Anuppur'),
(265, 11, 'Ashok Nagar'),
(266, 11, 'Balaghat'),
(267, 11, 'Barwani'),
(268, 11, 'Betul'),
(269, 11, 'Bhind'),
(270, 11, 'Bhopal'),
(271, 11, 'Burhanpur'),
(272, 11, 'Chhatarpur'),
(273, 11, 'Chhindwara'),
(274, 11, 'Damoh'),
(275, 11, 'Datia'),
(276, 11, 'Dewas'),
(277, 11, 'Dhar'),
(278, 11, 'Dindori'),
(279, 11, 'Guna'),
(280, 11, 'Gwalior'),
(281, 11, 'Harda'),
(282, 11, 'Hoshangabad'),
(283, 11, 'Indore'),
(284, 11, 'Jabalpur'),
(285, 11, 'Jhabua'),
(286, 11, 'Katni'),
(287, 11, 'Khandwa'),
(288, 11, 'Khargone'),
(289, 11, 'Mandla'),
(290, 11, 'Mandsaur'),
(291, 11, 'Morena'),
(292, 11, 'Narsinghpur'),
(293, 11, 'Neemuch'),
(294, 11, 'Panna'),
(295, 11, 'Rewa'),
(296, 11, 'Rajgarh'),
(297, 11, 'Ratlam'),
(298, 11, 'Raisen'),
(299, 11, 'Sagar'),
(300, 11, 'Satna'),
(301, 11, 'Sehore'),
(302, 11, 'Seoni'),
(303, 11, 'Shahdol'),
(304, 11, 'Shajapur'),
(305, 11, 'Sheopur'),
(306, 11, 'Shivpuri'),
(307, 11, 'Sidhi'),
(308, 11, 'Singrauli'),
(309, 11, 'Tikamgarh'),
(310, 11, 'Ujjain'),
(311, 11, 'Umaria'),
(312, 11, 'Vidisha'),
(313, 12, 'Ahmednagar'),
(314, 12, 'Akola'),
(315, 12, 'Amrawati'),
(316, 12, 'Aurangabad'),
(317, 12, 'Bhandara'),
(318, 12, 'Beed'),
(319, 12, 'Buldhana'),
(320, 12, 'Chandrapur'),
(321, 12, 'Dhule'),
(322, 12, 'Gadchiroli'),
(323, 12, 'Gondiya'),
(324, 12, 'ngoli'),
(325, 12, 'Jalgaon'),
(326, 12, 'Jalna'),
(327, 12, 'Kolhapur'),
(328, 12, 'Latur'),
(329, 12, 'Mumbai City'),
(330, 12, 'Mumbai suburban'),
(331, 12, 'Nandurbar'),
(332, 12, 'Nanded'),
(333, 12, 'Nagpur'),
(334, 12, 'Nashik'),
(335, 12, 'Osmanabad'),
(336, 12, 'Parbhani'),
(337, 12, 'Pune'),
(338, 12, 'Raigad'),
(339, 12, 'Ratnagiri'),
(340, 12, 'Sindhudurg'),
(341, 12, 'Sangli'),
(342, 12, 'Solapur'),
(343, 12, 'Satara'),
(344, 12, 'Thane'),
(345, 12, 'Wardha'),
(346, 12, 'Washim'),
(347, 12, 'Yavatmal'),
(348, 13, 'Bishnupur'),
(349, 13, 'Churachandpur'),
(350, 13, 'Chandel'),
(351, 13, 'Imphal East'),
(352, 13, 'Senapati'),
(353, 13, 'Tamenglong'),
(354, 13, 'Thoubal'),
(355, 13, 'Ukhrul'),
(356, 13, 'Imphal West'),
(357, 14, 'East Garo Hills'),
(358, 14, 'Khasi Hills'),
(359, 14, 'Jaintia Hills'),
(360, 14, 'Ri-Bhoi'),
(361, 14, 'South Garo Hills'),
(362, 14, 'West Garo Hills'),
(363, 14, 'West Khasi Hills'),
(364, 15, 'Aizawl'),
(365, 15, 'Champhai'),
(366, 15, 'Kolasib'),
(367, 15, 'Lawngtlai'),
(368, 15, 'Lunglei'),
(369, 15, 'Mamit'),
(370, 15, 'Saiha'),
(371, 15, 'Serchhip'),
(372, 16, 'Dimapur'),
(373, 16, 'Kohima'),
(374, 16, 'Mokokchung'),
(375, 16, 'Mon'),
(376, 16, 'Phek'),
(377, 16, 'Tuensang'),
(378, 16, 'Wokha'),
(379, 16, 'Zunheboto'),
(380, 17, 'Angul'),
(381, 17, 'Boudh'),
(382, 17, 'Bhadrak'),
(383, 17, 'Bolangir'),
(384, 17, 'Bargarh'),
(385, 17, 'Baleswar'),
(386, 17, 'Cuttack'),
(387, 17, 'Debagarh'),
(388, 17, 'Dhenkanal'),
(389, 17, 'Ganjam'),
(390, 17, 'Gajapati'),
(391, 17, 'Jharsuguda'),
(392, 17, 'Jajapur'),
(393, 17, 'Jagatsinghpur'),
(394, 17, 'Khordha'),
(395, 17, 'Kendujhar'),
(396, 17, 'Kalahandi'),
(397, 17, 'Kandhamal'),
(398, 17, 'Koraput'),
(399, 17, 'Kendrapara'),
(400, 17, 'Malkangiri'),
(401, 17, 'Mayurbhanj'),
(402, 17, 'Nabarangpur'),
(403, 17, 'Nuapada'),
(404, 17, 'Nayagarh'),
(405, 17, 'Puri'),
(406, 17, 'Rayagada'),
(407, 17, 'Sambalpur'),
(408, 17, 'Subarnapur'),
(409, 17, 'Sundargarh'),
(410, 27, 'Karaikal'),
(411, 27, 'Mahe'),
(412, 27, 'Puducherry'),
(413, 27, 'Yanam'),
(414, 18, 'Amritsar'),
(415, 18, 'Bathinda'),
(416, 18, 'Firozpur'),
(417, 18, 'Faridkot'),
(418, 18, 'Fatehgarh Sahib'),
(419, 18, 'Gurdaspur'),
(420, 18, 'Hoshiarpur'),
(421, 18, 'Jalandhar'),
(422, 18, 'Kapurthala'),
(423, 18, 'Ludhiana'),
(424, 18, 'Mansa'),
(425, 18, 'Moga'),
(426, 18, 'Mukatsar'),
(427, 18, 'Nawan Shehar'),
(428, 18, 'Patiala'),
(429, 18, 'Rupnagar'),
(430, 18, 'Sangrur'),
(431, 19, 'Ajmer'),
(432, 19, 'Alwar'),
(433, 19, 'Bikaner'),
(434, 19, 'Barmer'),
(435, 19, 'Banswara'),
(436, 19, 'Bharatpur'),
(437, 19, 'Baran'),
(438, 19, 'Bundi'),
(439, 19, 'Bhilwara'),
(440, 19, 'Churu'),
(441, 19, 'Chittorgarh'),
(442, 19, 'Dausa'),
(443, 19, 'Dholpur'),
(444, 19, 'Dungapur'),
(445, 19, 'Ganganagar'),
(446, 19, 'Hanumangarh'),
(447, 19, 'Juhnjhunun'),
(448, 19, 'Jalore'),
(449, 19, 'Jodhpur'),
(450, 19, 'Jaipur'),
(451, 19, 'Jaisalmer'),
(452, 19, 'Jhalawar'),
(453, 19, 'Karauli'),
(454, 19, 'Kota'),
(455, 19, 'Nagaur'),
(456, 19, 'Pali'),
(457, 19, 'Pratapgarh'),
(458, 19, 'Rajsamand'),
(459, 19, 'Sikar'),
(460, 19, 'Sawai Madhopur'),
(461, 19, 'Sirohi'),
(462, 19, 'Tonk'),
(463, 19, 'Udaipur'),
(464, 20, 'East Sikkim'),
(465, 20, 'North Sikkim'),
(466, 20, 'South Sikkim'),
(467, 20, 'West Sikkim'),
(468, 21, 'Ariyalur'),
(469, 21, 'Chennai'),
(470, 21, 'Coimbatore'),
(471, 21, 'Cuddalore'),
(472, 21, 'Dharmapuri'),
(473, 21, 'Dindigul'),
(474, 21, 'Erode'),
(475, 21, 'Kanchipuram'),
(476, 21, 'Kanyakumari'),
(477, 21, 'Karur'),
(478, 21, 'Madurai'),
(479, 21, 'Nagapattinam'),
(480, 21, 'The Nilgiris'),
(481, 21, 'Namakkal'),
(482, 21, 'Perambalur'),
(483, 21, 'Pudukkottai'),
(484, 21, 'Ramanathapuram'),
(485, 21, 'Salem'),
(486, 21, 'Sivagangai'),
(487, 21, 'Tiruppur'),
(488, 21, 'Tiruchirappalli'),
(489, 21, 'Theni'),
(490, 21, 'Tirunelveli'),
(491, 21, 'Thanjavur'),
(492, 21, 'Thoothukudi'),
(493, 21, 'Thiruvallur'),
(494, 21, 'Thiruvarur'),
(495, 21, 'Tiruvannamalai'),
(496, 21, 'Vellore'),
(497, 21, 'Villupuram'),
(498, 22, 'Dhalai'),
(499, 22, 'North Tripura'),
(500, 22, 'South Tripura'),
(501, 22, 'West Tripura'),
(502, 33, 'Almora'),
(503, 33, 'Bageshwar'),
(504, 33, 'Chamoli'),
(505, 33, 'Champawat'),
(506, 33, 'Dehradun'),
(507, 33, 'Haridwar'),
(508, 33, 'Nainital'),
(509, 33, 'Pauri Garhwal'),
(510, 33, 'Pithoragharh'),
(511, 33, 'Rudraprayag'),
(512, 33, 'Tehri Garhwal'),
(513, 33, 'Udham Singh Nagar'),
(514, 33, 'Uttarkashi'),
(515, 23, 'Agra'),
(516, 23, 'Allahabad'),
(517, 23, 'Aligarh'),
(518, 23, 'Ambedkar Nagar'),
(519, 23, 'Auraiya'),
(520, 23, 'Azamgarh'),
(521, 23, 'Barabanki'),
(522, 23, 'Badaun'),
(523, 23, 'Bagpat'),
(524, 23, 'Bahraich'),
(525, 23, 'Bijnor'),
(526, 23, 'Ballia'),
(527, 23, 'Banda'),
(528, 23, 'Balrampur'),
(529, 23, 'Bareilly'),
(530, 23, 'Basti'),
(531, 23, 'Bulandshahr'),
(532, 23, 'Chandauli'),
(533, 23, 'Chitrakoot'),
(534, 23, 'Deoria'),
(535, 23, 'Etah'),
(536, 23, 'Kanshiram Nagar'),
(537, 23, 'Etawah'),
(538, 23, 'Firozabad'),
(539, 23, 'Farrukhabad'),
(540, 23, 'Fatehpur'),
(541, 23, 'Faizabad'),
(542, 23, 'Gautam Buddha Nagar'),
(543, 23, 'Gonda'),
(544, 23, 'Ghazipur'),
(545, 23, 'Gorkakhpur'),
(546, 23, 'Ghaziabad'),
(547, 23, 'Hamirpur'),
(548, 23, 'Hardoi'),
(549, 23, 'Mahamaya Nagar'),
(550, 23, 'Jhansi'),
(551, 23, 'Jalaun'),
(552, 23, 'Jyotiba Phule Nagar'),
(553, 23, 'Jaunpur District'),
(554, 23, 'Kanpur Dehat'),
(555, 23, 'Kannauj'),
(556, 23, 'Kanpur Nagar'),
(557, 23, 'Kaushambi'),
(558, 23, 'Kushinagar'),
(559, 23, 'Lalitpur'),
(560, 23, 'Lakhimpur Kheri'),
(561, 23, 'Lucknow'),
(562, 23, 'Mau'),
(563, 23, 'Meerut'),
(564, 23, 'Maharajganj'),
(565, 23, 'Mahoba'),
(566, 23, 'Mirzapur'),
(567, 23, 'Moradabad'),
(568, 23, 'Mainpuri'),
(569, 23, 'Mathura'),
(570, 23, 'Muzaffarnagar'),
(571, 23, 'Pilibhit'),
(572, 23, 'Pratapgarh'),
(573, 23, 'Rampur'),
(574, 23, 'Rae Bareli'),
(575, 23, 'Saharanpur'),
(576, 23, 'Sitapur'),
(577, 23, 'Shahjahanpur'),
(578, 23, 'Sant Kabir Nagar'),
(579, 23, 'Siddharthnagar'),
(580, 23, 'Sonbhadra'),
(581, 23, 'Sant Ravidas Nagar'),
(582, 23, 'Sultanpur'),
(583, 23, 'Shravasti'),
(584, 23, 'Unnao'),
(585, 23, 'Varanasi'),
(586, 24, 'Birbhum'),
(587, 24, 'Bankura'),
(588, 24, 'Bardhaman'),
(589, 24, 'Darjeeling'),
(590, 24, 'Dakshin Dinajpur'),
(591, 24, 'Hooghly'),
(592, 24, 'Howrah'),
(593, 24, 'Jalpaiguri'),
(594, 24, 'Cooch Behar'),
(595, 24, 'Kolkata'),
(596, 24, 'Malda'),
(597, 24, 'Midnapore'),
(598, 24, 'Murshidabad'),
(599, 24, 'Nadia'),
(600, 24, 'North 24 Parganas'),
(601, 24, 'South 24 Parganas'),
(602, 24, 'Purulia'),
(603, 24, 'Uttar Dinajpur'),
(604, 12, 'Mandangad');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `code` varchar(5) NOT NULL,
  `country` varchar(100) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `code`, `country`, `status`) VALUES
(1, 'AF', 'Afghanistan', 0),
(2, 'AL', 'Albania', 0),
(3, 'DZ', 'Algeria', 0),
(4, 'DS', 'American Samoa', 0),
(5, 'AD', 'Andorra', 0),
(6, 'AO', 'Angola', 0),
(7, 'AI', 'Anguilla', 0),
(8, 'AQ', 'Antarctica', 0),
(9, 'AG', 'Antigua and Barbuda', 0),
(10, 'AR', 'Argentina', 0),
(11, 'AM', 'Armenia', 0),
(12, 'AW', 'Aruba', 0),
(13, 'AU', 'Australia', 0),
(14, 'AT', 'Austria', 0),
(15, 'AZ', 'Azerbaijan', 0),
(16, 'BS', 'Bahamas', 0),
(17, 'BH', 'Bahrain', 0),
(18, 'BD', 'Bangladesh', 0),
(19, 'BB', 'Barbados', 0),
(20, 'BY', 'Belarus', 0),
(21, 'BE', 'Belgium', 0),
(22, 'BZ', 'Belize', 0),
(23, 'BJ', 'Benin', 0),
(24, 'BM', 'Bermuda', 0),
(25, 'BT', 'Bhutan', 0),
(26, 'BO', 'Bolivia', 0),
(27, 'BA', 'Bosnia and Herzegovina', 0),
(28, 'BW', 'Botswana', 0),
(29, 'BV', 'Bouvet Island', 0),
(30, 'BR', 'Brazil', 0),
(31, 'IO', 'British Indian Ocean Territory', 0),
(32, 'BN', 'Brunei Darussalam', 0),
(33, 'BG', 'Bulgaria', 0),
(34, 'BF', 'Burkina Faso', 0),
(35, 'BI', 'Burundi', 0),
(36, 'KH', 'Cambodia', 0),
(37, 'CM', 'Cameroon', 0),
(38, 'CA', 'Canada', 0),
(39, 'CV', 'Cape Verde', 0),
(40, 'KY', 'Cayman Islands', 0),
(41, 'CF', 'Central African Republic', 0),
(42, 'TD', 'Chad', 0),
(43, 'CL', 'Chile', 0),
(44, 'CN', 'China', 0),
(45, 'CX', 'Christmas Island', 0),
(46, 'CC', 'Cocos (Keeling) Islands', 0),
(47, 'CO', 'Colombia', 0),
(48, 'KM', 'Comoros', 0),
(49, 'CD', 'Democratic Republic of the Congo', 0),
(50, 'CG', 'Republic of Congo', 0),
(51, 'CK', 'Cook Islands', 0),
(52, 'CR', 'Costa Rica', 0),
(53, 'HR', 'Croatia (Hrvatska)', 0),
(54, 'CU', 'Cuba', 0),
(55, 'CY', 'Cyprus', 0),
(56, 'CZ', 'Czech Republic', 0),
(57, 'DK', 'Denmark', 0),
(58, 'DJ', 'Djibouti', 0),
(59, 'DM', 'Dominica', 0),
(60, 'DO', 'Dominican Republic', 0),
(61, 'TP', 'East Timor', 0),
(62, 'EC', 'Ecuador', 0),
(63, 'EG', 'Egypt', 0),
(64, 'SV', 'El Salvador', 0),
(65, 'GQ', 'Equatorial Guinea', 0),
(66, 'ER', 'Eritrea', 0),
(67, 'EE', 'Estonia', 0),
(68, 'ET', 'Ethiopia', 0),
(69, 'FK', 'Falkland Islands (Malvinas)', 0),
(70, 'FO', 'Faroe Islands', 0),
(71, 'FJ', 'Fiji', 0),
(72, 'FI', 'Finland', 0),
(73, 'FR', 'France', 0),
(74, 'FX', 'France, Metropolitan', 0),
(75, 'GF', 'French Guiana', 0),
(76, 'PF', 'French Polynesia', 0),
(77, 'TF', 'French Southern Territories', 0),
(78, 'GA', 'Gabon', 0),
(79, 'GM', 'Gambia', 0),
(80, 'GE', 'Georgia', 0),
(81, 'DE', 'Germany', 0),
(82, 'GH', 'Ghana', 0),
(83, 'GI', 'Gibraltar', 0),
(84, 'GK', 'Guernsey', 0),
(85, 'GR', 'Greece', 0),
(86, 'GL', 'Greenland', 0),
(87, 'GD', 'Grenada', 0),
(88, 'GP', 'Guadeloupe', 0),
(89, 'GU', 'Guam', 0),
(90, 'GT', 'Guatemala', 0),
(91, 'GN', 'Guinea', 0),
(92, 'GW', 'Guinea-Bissau', 0),
(93, 'GY', 'Guyana', 0),
(94, 'HT', 'Haiti', 0),
(95, 'HM', 'Heard and Mc Donald Islands', 0),
(96, 'HN', 'Honduras', 0),
(97, 'HK', 'Hong Kong', 0),
(98, 'HU', 'Hungary', 0),
(99, 'IS', 'Iceland', 0),
(100, 'IN', 'India', 1),
(101, 'IM', 'Isle of Man', 0),
(102, 'ID', 'Indonesia', 0),
(103, 'IR', 'Iran (Islamic Republic of)', 0),
(104, 'IQ', 'Iraq', 0),
(105, 'IE', 'Ireland', 0),
(106, 'IL', 'Israel', 0),
(107, 'IT', 'Italy', 0),
(108, 'CI', 'Ivory Coast', 0),
(109, 'JE', 'Jersey', 0),
(110, 'JM', 'Jamaica', 0),
(111, 'JP', 'Japan', 0),
(112, 'JO', 'Jordan', 0),
(113, 'KZ', 'Kazakhstan', 0),
(114, 'KE', 'Kenya', 0),
(115, 'KI', 'Kiribati', 0),
(116, 'KP', 'Korea, Democratic People\'s Republic of', 0),
(117, 'KR', 'Korea, Republic of', 0),
(118, 'XK', 'Kosovo', 0),
(119, 'KW', 'Kuwait', 0),
(120, 'KG', 'Kyrgyzstan', 0),
(121, 'LA', 'Lao People\'s Democratic Republic', 0),
(122, 'LV', 'Latvia', 0),
(123, 'LB', 'Lebanon', 0),
(124, 'LS', 'Lesotho', 0),
(125, 'LR', 'Liberia', 0),
(126, 'LY', 'Libyan Arab Jamahiriya', 0),
(127, 'LI', 'Liechtenstein', 0),
(128, 'LT', 'Lithuania', 0),
(129, 'LU', 'Luxembourg', 0),
(130, 'MO', 'Macau', 0),
(131, 'MK', 'North Macedonia', 0),
(132, 'MG', 'Madagascar', 0),
(133, 'MW', 'Malawi', 0),
(134, 'MY', 'Malaysia', 0),
(135, 'MV', 'Maldives', 0),
(136, 'ML', 'Mali', 0),
(137, 'MT', 'Malta', 0),
(138, 'MH', 'Marshall Islands', 0),
(139, 'MQ', 'Martinique', 0),
(140, 'MR', 'Mauritania', 0),
(141, 'MU', 'Mauritius', 0),
(142, 'TY', 'Mayotte', 0),
(143, 'MX', 'Mexico', 0),
(144, 'FM', 'Micronesia, Federated States of', 0),
(145, 'MD', 'Moldova, Republic of', 0),
(146, 'MC', 'Monaco', 0),
(147, 'MN', 'Mongolia', 0),
(148, 'ME', 'Montenegro', 0),
(149, 'MS', 'Montserrat', 0),
(150, 'MA', 'Morocco', 0),
(151, 'MZ', 'Mozambique', 0),
(152, 'MM', 'Myanmar', 0),
(153, 'NA', 'Namibia', 0),
(154, 'NR', 'Nauru', 0),
(155, 'NP', 'Nepal', 0),
(156, 'NL', 'Netherlands', 0),
(157, 'AN', 'Netherlands Antilles', 0),
(158, 'NC', 'New Caledonia', 0),
(159, 'NZ', 'New Zealand', 0),
(160, 'NI', 'Nicaragua', 0),
(161, 'NE', 'Niger', 0),
(162, 'NG', 'Nigeria', 0),
(163, 'NU', 'Niue', 0),
(164, 'NF', 'Norfolk Island', 0),
(165, 'MP', 'Northern Mariana Islands', 0),
(166, 'NO', 'Norway', 0),
(167, 'OM', 'Oman', 0),
(168, 'PK', 'Pakistan', 0),
(169, 'PW', 'Palau', 0),
(170, 'PS', 'Palestine', 0),
(171, 'PA', 'Panama', 0),
(172, 'PG', 'Papua New Guinea', 0),
(173, 'PY', 'Paraguay', 0),
(174, 'PE', 'Peru', 0),
(175, 'PH', 'Philippines', 0),
(176, 'PN', 'Pitcairn', 0),
(177, 'PL', 'Poland', 0),
(178, 'PT', 'Portugal', 0),
(179, 'PR', 'Puerto Rico', 0),
(180, 'QA', 'Qatar', 0),
(181, 'RE', 'Reunion', 0),
(182, 'RO', 'Romania', 0),
(183, 'RU', 'Russian Federation', 0),
(184, 'RW', 'Rwanda', 0),
(185, 'KN', 'Saint Kitts and Nevis', 0),
(186, 'LC', 'Saint Lucia', 0),
(187, 'VC', 'Saint Vincent and the Grenadines', 0),
(188, 'WS', 'Samoa', 0),
(189, 'SM', 'San Marino', 0),
(190, 'ST', 'Sao Tome and Principe', 0),
(191, 'SA', 'Saudi Arabia', 0),
(192, 'SN', 'Senegal', 0),
(193, 'RS', 'Serbia', 0),
(194, 'SC', 'Seychelles', 0),
(195, 'SL', 'Sierra Leone', 0),
(196, 'SG', 'Singapore', 0),
(197, 'SK', 'Slovakia', 0),
(198, 'SI', 'Slovenia', 0),
(199, 'SB', 'Solomon Islands', 0),
(200, 'SO', 'Somalia', 0),
(201, 'ZA', 'South Africa', 0),
(202, 'GS', 'South Georgia South Sandwich Islands', 0),
(203, 'SS', 'South Sudan', 0),
(204, 'ES', 'Spain', 0),
(205, 'LK', 'Sri Lanka', 0),
(206, 'SH', 'St. Helena', 0),
(207, 'PM', 'St. Pierre and Miquelon', 0),
(208, 'SD', 'Sudan', 0),
(209, 'SR', 'Suriname', 0),
(210, 'SJ', 'Svalbard and Jan Mayen Islands', 0),
(211, 'SZ', 'Swaziland', 0),
(212, 'SE', 'Sweden', 0),
(213, 'CH', 'Switzerland', 0),
(214, 'SY', 'Syrian Arab Republic', 0),
(215, 'TW', 'Taiwan', 0),
(216, 'TJ', 'Tajikistan', 0),
(217, 'TZ', 'Tanzania, United Republic of', 0),
(218, 'TH', 'Thailand', 0),
(219, 'TG', 'Togo', 0),
(220, 'TK', 'Tokelau', 0),
(221, 'TO', 'Tonga', 0),
(222, 'TT', 'Trinidad and Tobago', 0),
(223, 'TN', 'Tunisia', 0),
(224, 'TR', 'Turkey', 0),
(225, 'TM', 'Turkmenistan', 0),
(226, 'TC', 'Turks and Caicos Islands', 0),
(227, 'TV', 'Tuvalu', 0),
(228, 'UG', 'Uganda', 0),
(229, 'UA', 'Ukraine', 0),
(230, 'AE', 'United Arab Emirates', 0),
(231, 'GB', 'United Kingdom', 0),
(232, 'US', 'United States', 0),
(233, 'UM', 'United States minor outlying islands', 0),
(234, 'UY', 'Uruguay', 0),
(235, 'UZ', 'Uzbekistan', 0),
(236, 'VU', 'Vanuatu', 0),
(237, 'VA', 'Vatican City State', 0),
(238, 'VE', 'Venezuela', 0),
(239, 'VN', 'Vietnam', 0),
(240, 'VG', 'Virgin Islands (British)', 0),
(241, 'VI', 'Virgin Islands (U.S.)', 0),
(242, 'WF', 'Wallis and Futuna Islands', 0),
(243, 'EH', 'Western Sahara', 0),
(244, 'YE', 'Yemen', 0),
(245, 'ZM', 'Zambia', 0),
(246, 'ZW', 'Zimbabwe', 0);

-- --------------------------------------------------------

--
-- Table structure for table `distillery_master`
--

CREATE TABLE `distillery_master` (
  `id` int(30) NOT NULL,
  `distillery_name` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `established_year` int(4) NOT NULL,
  `owner` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `contact_no` int(15) NOT NULL,
  `website` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `image_path` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `added_at` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '1-active, 0-inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `distillery_master`
--

INSERT INTO `distillery_master` (`id`, `distillery_name`, `description`, `country`, `established_year`, `owner`, `address`, `contact_no`, `website`, `image_path`, `added_at`, `status`) VALUES
(6, 'Allt-a-Bhainne [alt a.vain]', '<p>Allt-a-Bhainne is a Scotch whisky distillery located in the Speyside region of Scotland. It is a young malt whisky which is located near the well-known whisky town of Dufftown. This whisky was meant to serve as one of the leading malt whiskies in the Chivas Regal blends. The distillery does not have its own original bottling facilities but there are independent bottlings that carry the name of the distillery. Its taste resembles Speyside whisky and has a lot of character but is not as rough and strong as other Highland and Islay malts. The distillery has to its credit an active production record of four million litres of pure alcohol every year.</p>\r\n', '247', 0, 'Chivas Brothers Ltd (Pernod Ricard)', 'India', 0, 'aberfeldy.com', 'http://admin.spiritpedia.xceedtech.in/images/distillery/Allt-a-Bhainne.jfif', '0000-00-00 00:00:00', 1),
(7, 'Aberlour [ah.bur.lower]', '<p>Aberlour is situated in Scotland&rsquo;s Speyside whisky region which is at the confluence of the rivers Lour and Spey near Ben Rinnes. The picturesque scenery and the age-old oak trees leave signs of its long heritage. The crystal-clear spring from the Lour valley is the lifeblood of Aberlour malt whisky. It is also blessed with plentiful rain and snow. The water passes through the mountains made of granite and hence only traces of elements are carried to the distillery rather than the minerals. Aberlour is indeed blessed by nature with its pure and simple resources. Aberlour has bagged success at the spirit rating competitions including awards at the San Francisco World Spirits Competitions.</p>\r\n', '247', 0, 'Chivas Brothers Ltd (Pernod Ricard)', 'Us', 0, 'aberfeldy.com', 'Aberfeldy.jpg', '0000-00-00 00:00:00', 1),
(8, 'Aberfeldy [ah.bur.fell.dee]', '<p>Aberfeldy is situated in the centre of Scotland, five miles east of Loch (lake) Tay. It gets its water supply from the freshwater stream Pitilie Burn which runs alongside the distillery. Aberfeldy is the only distillery in Scotland to use the water from the stream. The distillery faced a major setback during World War I when it had to be closed due to shortage of supplies of barley and other food materials. In the year 1998, Bacardi bought John Dewar &amp; Sons from Diageo at &pound;1,150 million. The distillery has recorded output of around 3.4 million litres of spirit every year.</p>\r\n', '247', 0, 'John Dewar & Sons (Bacardi)', '', 0, '', 'http://admin.spiritpedia.xceedtech.in/images/distillery/15804513058.jpg', '0000-00-00 00:00:00', 1),
(9, 'Ardbeg [ard.beg]', '<p>Ardbeg is a very old Scotch whisky distillery on the south coast of the Isle of Islay. The Isle of Islay is an island with quite a few water sources and hence Ardbeg distillery gets its water supply from two main water supplies &ndash; the Loch (lake) Airigh Nam Beist and the Loch Uigeadail. The distillery has been producing whisky since 1798 but began commercial production in 1815. It is heavily peated malt with the sweetness of the chocolate and vanilla. As compared to the other malts, Ardbeg does not focus on sea and salt tastes but rather on the aromas of the spices, malt and sweet flavours.</p>\r\n', '247', 0, 'The Glenmorangie Co (Moet Hennessy)', '', 0, '', '', '0000-00-00 00:00:00', 1),
(10, 'Ardmore [ard.moor]', '<p>Ardmore is a single malt Scotch whisky distillery located in the village of Kennethmont, Aberdeenshire in Scotland. It is one of the oldest distilleries founded by Adam Teacher in 1898. Interestingly, at the beginning of the 18th century, there was another distillery Ardmore on the Isle of Islay. But the distillery had nothing to do with this Ardmore distillery except the name. It receives its water supply from the water source at Knockandy Hill. Ardmore serves as the backbone of Teacher&rsquo;s blended Scotch but is also released as a single malt.<br />\r\n&nbsp;</p>\r\n', '247', 0, 'Isle of Arran Distillers', '', 0, '', '', '0000-00-00 00:00:00', 1),
(11, 'Auchentoshan [ock.en.tosh.en]', '<p>Auchentoshan is a single malt whisky distillery found in the western region of Scotland. Auchentoshan, translates &lsquo;corner of the field&rsquo;, is also known as &lsquo;Glasgow&rsquo;s Malt Whisky&rsquo; due to its proximity to Glasgow. It is coined &lsquo;breakfast whisky&rsquo; as it is sweet and delicate in nature. The distillery is located at the foot of the Old Kilpatrick Hills which is on the outskirts of Clyde River in Scotland. What makes Auchentoshan different from other brands is that it is the only distillery practising 100 per cent triple distillation as opposed to most Scottish malt whiskies that are distilled twice. This triple distillation softens the flavour and body of the final product.<br />\r\n&nbsp;</p>\r\n', '247', 0, 'Morrison Bowmore (Suntory)', '', 0, '', '', '0000-00-00 00:00:00', 1),
(12, 'Auchroisk [ar.thrusk]', '<p>Auchroisk is a single malt distillery in Banffshire in the Speyside of Scotland. It is an industrial malt distillery used by Justerini &amp; Brooks to produce their blended Scotch whisky. The water from Dorie&rsquo;s well in Banffshire was compared to check if it was suitable for the single malt whisky production. And when the quality was found adequate the production began which currently stands at 3.4 million litres of alcohol every year. This water gives the spirit a light, fresh and citrusy taste. Auchroisk has been renamed a couple of times -- once in 1986 when it was rebranded &lsquo;The Singleton&rsquo; as it was perceived too difficult for consumers outside Scotland, then in 2001 back to Auchroisk when a bottling was released under the name &lsquo;Auchroisk 10 Year Old Flora &amp; Fauna&rsquo; and again in 2008 the name was changed to &lsquo;The Singleton of Auchroisk&rsquo;. The distillery does a lot of mixture between American and European oaks to change the tastes &ndash; to create a little more spiciness, they use the European oak whereas to create a calm and fresh blend, the American oak casks are used.<br />\r\n&nbsp;</p>\r\n', '247', 0, 'Diageo', '', 0, '', '', '0000-00-00 00:00:00', 1),
(13, 'Aultmore [ault.moor]', '<p>Aultmore is a distillery in Keith Banffshire which produces an eponymous single malt whisky. In the early years, the distillery was originally powered by a waterwheel and was soon adapted to use the steam engine which could be operated day and night. It was used for over seven decades. In 1969 the steam engine was decommissioned when electricity was introduced. This steam engine is now on display at the distillery site. Aultmore has witnessed many disasters including the closures during the World Wars. Surviving all odds, today it stands solitary and has become Dewar&rsquo;s white label blend. It is sometimes dubbed as &lsquo;rarest of Speyside&rsquo;.</p>\r\n', '247', 0, 'John Dewar & Sons (Bacardi)', '', 0, '', '', '0000-00-00 00:00:00', 1),
(14, 'Balblair [bal.blair]', '<p>Balblair is a Scotch whisky distillery located in Edderton in Ross-shire in Scotland. Although it is said that the distillery was founded in 1790 by John Ross, there are unofficial records of distilling that dates back to 1749 since back in those days distilling in the Highlands was illegal and hence the records got &lsquo;distilled&rsquo;. But it is believed that the business picked up when the railway opened near the distillery which gave the Ross family the opportunity to sell their whisky to England and other parts of the world. The distillery has been receiving pure water supply from Ault Dearg burn for centuries. This uniqueness of the distillery &ndash; its location, its water source, it&#39;s still shape and its wood policy &ndash; determines the taste which is intriguingly complex and satisfying. Balblair single malt has characteristics of apricots, oranges, spices, floral notes and green apples. It is often combined with freshness and taste of grass or hay.</p>\r\n', '247', 0, 'Invers House Distillers (Thai Beverages plc)', '', 0, '', '', '0000-00-00 00:00:00', 1),
(15, 'Balmenach [bal.may.nack]', '<p>Balmenach distillery is located near the village of Cromdale on the banks of the River Spey. It is one of the most traditional distilleries and hence it is classified as an &lsquo;old-style&rsquo; whisky. It receives its water supply from different water sources from the Cromdale Hills. It is a rather fruity and not peated whisky which is very typical for a Speyside distillery. The distillery is capable of producing nearly 1.8 million litres every year. Gin is also a part of the production of Balmenach. It&rsquo;s sherry-matured malt goes to blends including Johnnie Walker. There is no official bottling of Balmenach single malt. Over the years it is sold under the name Deerstalker.</p>\r\n', '247', 0, 'Inver House Distillers (Thai Beverages plc)', '', 0, '', '', '0000-00-00 00:00:00', 1),
(16, 'Balvenie [bal.ven.ee]', '<p>Balvenie is a single malt Scotch whisky distillery in Dufftown, Scotland. It is one of the most famous names in the world of whisky and is capable of producing over 5.5 million litres every year. It is described as &lsquo;the complete distillery&rsquo; as every process of its production takes place on the site itself &ndash; right from growing the barley on the land adjoining the distillery building to having an active malting floor and making casks in their own cooperage. The name Balvenie means &lsquo;village of luck&rsquo;. Most of its warehouses are kept traditionally flat and are made of old grey stone. This coupled with the Scottish Highland climate makes the whisky mature slowly thereby making it very smooth.</p>\r\n', '247', 0, 'William Grant & Sons', '', 0, '', '', '0000-00-00 00:00:00', 1),
(17, 'Ben Nevis [ben.nev.iss]', '<p>Ben Nevis is a distillery at Lochy Bridge in Fort William in Scotland. It is at the base of Ben Nevis, the highest mountain in the British Isles. Being a coastal distillery in the Western Highlands, Ben Nevis draws its water from Allt a&rsquo;Mhuilinn which originates from two pools &ndash; Coire Leis and Coire na&rsquo;Ciste. Its malt whisky is rather strong and intense and it often leaves a spicy aroma. The production at Ben Nevis has increased a lot over the last few years &ndash; it rose from as low as five lakh litres per year to a high of 20 lakh litres annually. In the earlier times, the whisky would be shipped through the Caledonia canal over the Loch Ness; however, today everything gets transported by trucks. It uses wooden washback and brewer&rsquo;s yeast, making it the last distillery in Scotland that works with such an old practice of inducing fermentation. It has slow and steady distillation process giving it a rich, deep distillate which matures in ex-Sherry casks where the ripe and chewy texture is produced.&nbsp;</p>\r\n', '247', 0, 'Ben Nevis Distillery Ltd (Nikka, Asahi Breweries)', '', 0, '', '', '0000-00-00 00:00:00', 1),
(18, 'Benriach [ben.ree.ack]', '<p>Benriach is a single malt Scotch whisky distillery in the Speyside area of Scotland. It lies in the heart of Speyside between the Town Rothes and the city of Elgin. It is one of the traditional distilleries from Speyside. It procures water from the Burnside Springs. Its uniqueness lies in the skills the men use to craft the whisky, the ingredients used and the distinctive copper stills and high-quality barrels selected for maturation. Another unique feature that makes it stand apart from the other Spey Valley distilleries is the traditional floor maltings with their distinctive pagoda style chimneys that produce peated, malted barley enabling the distillery to get a unique taste of peat reek in a few of their special bottlings.</p>\r\n', '247', 0, 'BenRiach Distillery Company (Brown Forman)', '', 0, '', '', '0000-00-00 00:00:00', 1),
(19, 'Benrinnes [ben rin.ess]', '<p>Benrinnes is a malt whisky distillery located at the foot of the hill Ben Rinnes in Aberlour. It produces an eponymous whisky. Founded in 1826, it is still active. This distillery had employed a unique partial triple distillation process until 2007 which was abandoned and changed to a double distilling process. The water supply for this Scotch comes from the Rowan Tree Burn, the Scurran Burn and the Benrinnes Spring. It is a lighter Speyside whisky. Majority of the whisky distilled at Benrinnes goes to the blend industry and is used in Johnnie Walker and J&amp;B blended whiskies.&nbsp;</p>\r\n', '247', 0, 'Diageo', '', 0, '', '', '0000-00-00 00:00:00', 1),
(20, 'Benromach [ben.ro.mack]', '<p>Benromach is a Speyside distillery situated near Forres in Morayshire in Scotland. It receives its water supply from the Chapelton Springs in the Romach Hills beside Forres. It procures its malt from the big malting companies and uses only organic barley for their whisky. It is one of the smallest distilleries in Speyside. Despite being the smallest, it is the single malt that has made a big impact in the past few years, not only in terms of quality but also in volumes.<br />\r\n&nbsp;</p>\r\n', '247', 0, 'Gordon & MacPhail', '', 0, '', '', '0000-00-00 00:00:00', 1),
(21, 'Bladnoch [blad.nock]', '<p>Bladnoch distillery is a single malt Scotch whisky distillery located in south west region of Scotland. It is one of the six remaining lowland distilleries located at Bladnoch. It is situated on the banks of River Bladnoch. It can be characterized by its strong fruity favour as it gets matured in sherry casks. It is unusual for a lowland whisky to have double distillation and peated malt being used in the production. Although being rather smaller with not many independent bottlers to their range, Bladnoch still has a lot of variety to enjoy.</p>\r\n', '247', 0, 'David Prior', '', 0, '', '', '0000-00-00 00:00:00', 1),
(22, 'Blair Athol [blair ath.ull]', '<p>Blair Athol is a distillery located on the south edge of Pitlochry in Perthshire in Scotland. It is a very old distillery founded way back in 1798 by John Stewart and Robert Robertson under the name &lsquo;Aldour distillery&rsquo;. The distillery can be described as strong and voluminous and the whiskies here are matured mostly in sherry casks and are therefore sweet. The water to this distillery comes from Allt Dour Burn and the distillery registers about 2.5 million litres alcohol production every year. Blair Athol falls in the &lsquo;nutty-spicy&rsquo; category as it needs short fermentation period which gives it the nutty base. However, it is distillation that actually adds weight to the distillate.<br />\r\n&nbsp;</p>\r\n', '247', 0, 'Diageo', '', 0, '', '', '0000-00-00 00:00:00', 1),
(23, 'Bowmore [bow.moor]', '<p>Bowmore is a distillery on the Isle of Islay, an island of the Inner Herbides and it is one of the oldest in Scotland dating back to 1779. Like most of the other distilleries, Bowmore was also set up as a farming distillery. It is a typical Islay whisky with a heavy peat smoke note with a lot of sea and salt aromas. The speciality about Bowmore is the slight violet flower note which gets hidden behind the heavy sherry or wine aromas. But if it is tasted carefully it can be noticed. Sourcing its water supply from the River Laggan, it produces about two million litres of alcohol every year.</p>\r\n', '247', 0, 'Morrison Bowmore (Suntory)', '', 0, '', '', '0000-00-00 00:00:00', 1),
(24, 'Braeval [bre.vaal]', '<p>Braeval is a Scotch whisky distillery in Chapeltown, Banffshire in Scotland. The distillery was founded by Chivas Brothers Ltd and had to be closed in 2002 but was later reopened in 2008. The Braeval distillery does not have any distillery bottling but its Scotch can be got from independent bottlers. The distillery&rsquo;s character is light and partly sweet with common notes being floral and vanilla. The Chivas Regal is made from Braeval&rsquo;s spirit. The distillery accounts for about four million litres of pure alcohol production every year.&nbsp;</p>\r\n', '247', 0, 'Chivas Brothers (Pernod Ricard)', '', 0, '', '', '0000-00-00 00:00:00', 1),
(25, 'Bruichladdich [brook.lad.dee]', '<p>Bruichladdich is a distillery on the Rhinns of the isle of Islay in Scotland. It mainly produces single malt Scotch whisky but it also offers artisanal gin. It is one of the nine working distilleries on the island. Bruichladdich means corner of the beach or gentle slope of the sea. It is described as the heavily peated Islay Scotch whisky. The maturation in different casks gives it a lot of flavours and aromas in different bottlings. If the different vintages from the Bruichladdich distillery are tasted it will have all sorts of flavours of fruits and spices. The distillery has three brands &ndash; the normal Bruichladdich, the Port Charlotte and the Octomore which is a very heavy peated single malt Scotch whisky. The distillery receives its water from Bruichladdich Loch and the Octomore spring.</p>\r\n', '247', 0, 'Remy Cointreau', '', 0, '', '', '0000-00-00 00:00:00', 1),
(26, 'Bunnahabbain [buh.nah.hav.enn]', '<p>Bunnahabhain is a single malt distillery found near Port Askaig on the island of Islay, off the west coast of Scotland. Its taste varies greatly from other spirits and is one of the nine active distilleries on the island. Bunnahabhain means &lsquo;mouth of the river&rsquo; and it stands on the mouth of the Margadale River where it joins Bunnahabhain Bay on the north-east coast of the western isle of Islay. It is light Islay malt as it uses non-peated malted barley, water from the nearby natural spring that does not flow through any peat and because it takes only a narrow selection of the spirit for maturation during the second distillation. The company is trying to increase its production to 2.5 million litres capacity. Although it is popular in America, France and Holland, Bunnahabhain&rsquo;s main market is the UK. Distell International is working towards upgrading and transforming the site into a &lsquo;world-class whisky destination&rsquo;.</p>\r\n', '247', 0, 'Distell International Ltd', '', 0, '', '', '0000-00-00 00:00:00', 1),
(27, 'Caol Ila [cull eel.a]', '<p>Caol Ila is a Scotch whisky distillery near Port Askaig on the isle of Islay in Scotland. Built in 1846, it is the largest of the eight distilleries scattered across Islay on the west coast of Scotland. It is light in colour with a distinctive floral and peppery note which complements the traditional Islay&rsquo;s peaty flavor. It is used as an ingredient in Johnnie Walker and in Black Bottle. It receives pure spring water rising from limestone in nearby Lock nam Ban. It produces over three million litres of pure alcohol every year. It is a remarkable distillery as it can boast of consistent production since its inception, barring its brief closure during World War II when regulations were placed on the use of barley. Caol Ila has changed hands multiple times but every new owner has contributed to its expansion and development.</p>\r\n', '247', 0, 'Diageo', '', 0, '', '', '0000-00-00 00:00:00', 1),
(28, 'Cardhu [car.doo]', '<p>Cardhu is a Speyside distillery near Archiestown, Moray in Scotland. It was founded by whisky smuggler John Cumming and his wife Helen Cumming in 1824. The word Cardhu means &lsquo;black rock&rsquo;. The whisky has a warmth and cleanliness of taste which is described as &lsquo;silky&rsquo; and is used in some of Johnnie Walker blends. The distillery is located in the picturesque setting in the heart of Speyside which is also known as the &lsquo;whisky country&rsquo;. It is the only distillery that was started by a woman and it stands out from all the other distilleries in the area. The water source for Cardhu&rsquo;s production is wells that are situated in the Mannoch Hills and the water from the Lyne Burn. An annual production of 3.4 million litres is carried out at the Cardhu distillery.</p>\r\n', '247', 0, 'Diageo', '', 0, '', '', '0000-00-00 00:00:00', 1),
(29, 'Clynelish [cline.leash]', '<p>Clynelish is a distillery near Brora, Sutherland in the Highlands of Scotland. It is used as the base spirits for Johnnie Walker Gold Label. It was built in 1967 in the Northern Highlands opposite its earlier sister distillery which was set up by the first Duke of Sutherland. It was designed following the blueprint of the Caol Ila distillery on Islay. Clynelish has always produced a muscular but slightly peaty whisky. It produces a &lsquo;waxy&rsquo; new make spirit which is created in the most unusual fashion.&nbsp;</p>\r\n', '247', 0, 'Diageo', '', 0, '', '', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `distillery_type`
--

CREATE TABLE `distillery_type` (
  `id` int(10) NOT NULL,
  `distillery_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event_images`
--

CREATE TABLE `event_images` (
  `id` int(30) NOT NULL,
  `event_id` int(30) NOT NULL,
  `image_path` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL COMMENT '1-active, 0-inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event_master`
--

CREATE TABLE `event_master` (
  `id` int(30) NOT NULL,
  `event` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `city` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `location` varchar(250) COLLATE utf8_unicode_ci NOT NULL COMMENT 'google map coordinates',
  `terms_conditions` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `is_book` int(1) NOT NULL,
  `ticket_quantity` int(10) NOT NULL,
  `image` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `img_alt_txt` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '1-active, 0-inactive',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) NOT NULL,
  `meta_title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` varchar(3000) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(3000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faq_master`
--

CREATE TABLE `faq_master` (
  `id` int(30) NOT NULL,
  `question` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `answer` varchar(10000) COLLATE utf8_unicode_ci NOT NULL,
  `added_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '1-active, 0-inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `faq_master`
--

INSERT INTO `faq_master` (`id`, `question`, `answer`, `added_date`, `status`) VALUES
(1, 'What does \'whisky\' mean?', '<p>The term &#39;whisky&#39; comes from the Gaelic &#39;uisge beatha&#39; or &#39;usquebaugh&#39;, meaning water of life.</p>', '2019-04-18 02:23:48', 1),
(3, 'Which spelling is correct - whisky or whiskey?', '<p><span style=\"color:#ff0000\">&nbsp;of whisk(e)y is governed by law, not the word. Scotch Whisky and Irish Whiskey are often distinguished in spelling&#39;. &nbsp;American whiskey is usually spelt with an &#39;e&#39;, while English, Welsh, Japanese and most other world whiskies are not.</span></p>\r\n', '2019-04-18 02:59:01', 1),
(5, 'Is drinking whiskey good for you?', '<p>With&nbsp;<strong>good</strong>&nbsp;cholesterol comes,&nbsp;<strong>you</strong>&nbsp;guessed it, a&nbsp;<strong>healthy</strong>&nbsp;heart. The antioxidant boost from&nbsp;<strong>whiskey</strong>&nbsp;aids in coronary heart disease prevention. The phenolic compounds in&nbsp;<strong>whiskey</strong>&nbsp;are absorbed into the body easier than that of wine. This is great news for&nbsp;<strong>whiskey</strong>&nbsp;drinkers.Feb 6, 2019</p>', '2019-04-22 05:04:56', 1),
(6, 'Is drinking whiskey bad for you? testing', '<p>With good cholesterol comes,&nbsp;<strong>you</strong>&nbsp;guessed it, a healthy heart. The antioxidant boost from&nbsp;<strong>whiskey</strong>&nbsp;aids in coronary heart disease prevention. The phenolic compounds in&nbsp;<strong>whiskey</strong>&nbsp;are absorbed into the body easier than that of wine. This is great news for&nbsp;<strong>whiskey</strong>&nbsp;drinkers.</p>', '2019-04-22 06:06:03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

CREATE TABLE `favourites` (
  `id` int(10) NOT NULL,
  `type` int(1) NOT NULL COMMENT '1-spirits, 2- finger food, 3- mixer, 4-smoke, 5- music',
  `name` varchar(300) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='you can add list of spirits, smoke, mixer, finger foods, music';

--
-- Dumping data for table `favourites`
--

INSERT INTO `favourites` (`id`, `type`, `name`, `created_at`) VALUES
(1, 1, 'Beer', '2019-07-04 07:19:55'),
(2, 1, 'Wine', '2019-07-04 07:19:55'),
(3, 1, 'Whiskey', '2019-07-04 07:19:55'),
(4, 1, 'Rum', '2019-07-04 07:19:55'),
(5, 1, 'Cider', '2019-07-04 07:19:55'),
(6, 1, 'Vodka', '2019-07-04 07:19:55'),
(7, 1, 'Mild Beer', '2019-07-04 07:19:55'),
(8, 2, 'Kebab', '2019-07-04 13:06:02'),
(9, 2, 'Spiced Groundnuts', '2019-07-04 13:06:02'),
(10, 2, 'Potato Chips', '2019-07-04 13:07:04'),
(11, 2, 'Burgers', '2019-07-04 13:07:04'),
(12, 2, 'Pizzas', '2019-07-04 13:07:04'),
(13, 3, 'cola', '2019-07-04 13:09:53'),
(14, 3, 'Apple Juice', '2019-07-04 13:09:53'),
(15, 3, 'Orange Juice', '2019-07-04 13:09:53'),
(16, 3, 'Water', '2019-07-04 13:09:53'),
(17, 3, 'Lemonade', '2019-07-04 13:09:53'),
(18, 3, 'Soda', '2019-07-04 13:09:53'),
(19, 4, 'Cigars', '2019-07-04 13:18:19'),
(20, 4, 'Cigarettes', '2019-07-04 13:18:22'),
(21, 4, 'Kretek', '2019-07-04 13:18:26'),
(22, 4, 'Hookah', '2019-07-04 13:18:29'),
(23, 4, 'Light cigarettes', '2019-07-04 13:18:32'),
(24, 5, 'Pop', '2019-07-04 13:15:21'),
(25, 5, 'Funk', '2019-07-04 13:15:21'),
(26, 5, 'Kretek', '2019-07-04 13:15:21'),
(27, 5, 'Rock', '2019-07-04 13:15:21'),
(28, 5, 'Metal, progressive', '2019-07-04 13:15:21'),
(29, 5, 'Psychedelic', '2019-07-04 13:15:21');

--
-- Triggers `favourites`
--
DELIMITER $$
CREATE TRIGGER `favourite_spirit_date` BEFORE INSERT ON `favourites` FOR EACH ROW BEGIN
    IF (NEW.created_at IS NULL) THEN 
        SET NEW.created_at = now();
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `feedback_master`
--

CREATE TABLE `feedback_master` (
  `id` int(30) NOT NULL,
  `user_id` int(30) NOT NULL,
  `comment` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `added_date` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '1-active, 0-inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feed_comments`
--

CREATE TABLE `feed_comments` (
  `id` int(30) NOT NULL,
  `feed_id` int(30) NOT NULL,
  `user_id` int(30) NOT NULL,
  `comment` varchar(2000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `feed_comments`
--

INSERT INTO `feed_comments` (`id`, `feed_id`, `user_id`, `comment`) VALUES
(1, 1, 7, 'Test');

-- --------------------------------------------------------

--
-- Table structure for table `feed_likes`
--

CREATE TABLE `feed_likes` (
  `id` int(30) NOT NULL,
  `feed_id` int(30) NOT NULL,
  `user_id` int(30) NOT NULL,
  `likes` int(30) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feed_master`
--

CREATE TABLE `feed_master` (
  `id` int(30) NOT NULL,
  `user_id` int(30) NOT NULL,
  `question` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `views` int(30) NOT NULL,
  `likes` int(30) NOT NULL,
  `post_date` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '1-active, 0-inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `feed_master`
--

INSERT INTO `feed_master` (`id`, `user_id`, `question`, `views`, `likes`, `post_date`, `status`) VALUES
(1, 7, 'Why should I go for whisky?', 0, 0, '2019-07-10 00:00:00', 0),
(2, 7, 'Why should I go for Rum?', 0, 0, '2019-07-10 00:00:00', 0),
(3, 7, 'Why should I go for Vodka?', 0, 0, '2019-07-10 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `follower_master`
--

CREATE TABLE `follower_master` (
  `id` int(30) NOT NULL,
  `user_id` int(30) NOT NULL,
  `to_user_id` int(30) NOT NULL,
  `added_date` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '1-active, 0-inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `institute_master`
--

CREATE TABLE `institute_master` (
  `id` int(30) NOT NULL,
  `institute_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `course_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image_path` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `site_url` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(30) NOT NULL,
  `duration` int(30) NOT NULL COMMENT 'in months',
  `added_date` datetime NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '1-active, 0-inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `institute_master`
--

INSERT INTO `institute_master` (`id`, `institute_name`, `course_name`, `description`, `image_path`, `site_url`, `price`, `duration`, `added_date`, `status`) VALUES
(1, 'The Introduction to Whisky Certificate', '', '<p>This unique online SQA awarded Certificate explores the fundamentals of whisky.&nbsp; The Certificate offers our first tier of whisky education and is aimed at those new to whisky or those wishing to improve their knowledge to a Certificate level.&nbsp; The online format makes it globally accessible, flexible and a cost-effective way to access independent and comprehensive material and expertise.</p>\r\n\r\n<p>Modules covered include the History of Whisky, Pre-production (a look at the raw materials and the malting process), Batch Distillation, Maturation, Grain Distillation and Blending.</p>\r\n\r\n<p>Please note that we are upgrading our software and as such this course will not be available for access from 30th Sept until 19th October.&nbsp; We apologise for any inconvenience and thank you for your understanding - luckily our whisky knowledge is far superior to our technical knowledge!</p>\r\n\r\n<p>* In order to gain the SQA Certificate candidates are required to take a formal assessment. There is an additional cost of &pound;180 attached to this for the SQA registration fee and the cost of the assessment centre. The EWA has engaged with Prometric to provide and oversee the assessment venues and administration.&nbsp;</p>\r\n', 'http://admin.spiritpedia.xceedtech.in/images/institute/1546868093photo21.jpg', 'https://www.edinburghwhiskyacademy.com/whisky-courses/whisky-certificate', 0, 0, '2019-10-14 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `level_master`
--

CREATE TABLE `level_master` (
  `id` int(30) NOT NULL,
  `level` int(30) NOT NULL,
  `points` int(30) NOT NULL,
  `badge` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `added_date` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '1-active, 0-inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menu_master`
--

CREATE TABLE `menu_master` (
  `menu_id` int(30) NOT NULL,
  `menu_desc` varchar(500) NOT NULL,
  `menu_url` varchar(500) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `sequence_no` int(11) NOT NULL,
  `active` int(11) DEFAULT NULL,
  `menu_icon` varchar(50) NOT NULL,
  `is_applicable_for_teacher` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_master`
--

INSERT INTO `menu_master` (`menu_id`, `menu_desc`, `menu_url`, `parent_id`, `sequence_no`, `active`, `menu_icon`, `is_applicable_for_teacher`) VALUES
(3, 'Admin', 'javascript:void(0)', 0, 2, 1, 'fa-user', 0),
(4, 'Menu Master', 'MenuMaster/admin', 3, 1, 1, 'None', 0),
(5, 'Access Control', 'UserMenuAccessMaster/admin', 3, 2, 1, 'None', 0),
(6, 'Masters', 'javascript:void(0)', 0, 1, 1, 'fa-folder-open', 0),
(9, 'News', 'NewsMaster/admin', 0, 4, 1, 'fa-paste', 0),
(18, 'Product Master', 'ProductMaster/admin', 6, 2, 1, 'None', 0),
(19, 'Product Type', 'ProductType/admin', 6, 1, 1, 'None', 0),
(29, 'Users', 'UserMaster/admin', 3, 2, 1, 'None', 0),
(32, 'Information', 'javascript:void(0)', 0, 1, 1, 'fa-pencil-square', 0),
(33, 'About Us', 'AboutUs/admin', 32, 1, 1, 'None', 0),
(34, 'FAQ\'s', 'FaqMaster/admin', 32, 2, 1, 'None', 0),
(35, 'Mobile App', 'javascript:void(0)', 0, 1, 1, 'fa-phone-square', 0),
(36, 'Dashboard', 'MobileAppDashboard/admin', 35, 1, 1, 'None', 0),
(37, 'Types of whiskies', 'TypesOfWhiskies/admin', 32, 3, 1, 'None', 0),
(38, 'Options', 'OptionMaster/admin', 6, 3, 1, 'None', 0),
(39, 'Distillery', 'DistilleryMaster/admin', 0, 7, 1, 'fa-globe', 0),
(40, 'Blog', 'BlogMaster/admin', 0, 9, 1, 'fa-newspaper-o', 0),
(41, 'Whiskies Of The World', 'WhiskiesoftheworldMaster/admin', 0, 9, 1, 'fa-users', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mobile_app_dashboard`
--

CREATE TABLE `mobile_app_dashboard` (
  `id` int(10) NOT NULL,
  `section_heading` varchar(200) NOT NULL,
  `background_image` varchar(500) NOT NULL,
  `sequence` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobile_app_dashboard`
--

INSERT INTO `mobile_app_dashboard` (`id`, `section_heading`, `background_image`, `sequence`, `created_at`, `created_by`) VALUES
(1, 'Whiskypedia', 'http://admin.spiritpedia.xceedtech.in/images/dashboard/15617042071.jpg', 1, '2019-06-26 04:09:47', 1),
(2, 'Whiskey\'s of the world', 'http://admin.spiritpedia.xceedtech.in/images/dashboard/156170572022.jpg', 2, '2019-06-28 07:08:40', 1),
(3, 'Distilleries of the world', 'http://admin.spiritpedia.xceedtech.in/images/dashboard/15617057423.jpg', 3, '2019-06-28 07:09:02', 1),
(4, 'Events', 'http://admin.spiritpedia.xceedtech.in/images/dashboard/Whiskey Tasting.jpg', 4, '2019-06-28 07:09:24', 1),
(5, 'Offers', 'http://admin.spiritpedia.xceedtech.in/images/dashboard/Shop.jpg', 5, '2019-06-28 10:15:05', 1),
(6, 'Feeds', 'http://admin.spiritpedia.xceedtech.in/images/dashboard/Feed.jpg', 6, '2019-07-08 05:44:06', 1);

--
-- Triggers `mobile_app_dashboard`
--
DELIMITER $$
CREATE TRIGGER `dashboard_date` BEFORE INSERT ON `mobile_app_dashboard` FOR EACH ROW BEGIN
    IF (NEW.created_at IS NULL) THEN 
        SET NEW.created_at = now();
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `modules_list`
--

CREATE TABLE `modules_list` (
  `module_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `module_name` varchar(100) DEFAULT NULL,
  `controller` varchar(50) NOT NULL,
  `action` varchar(200) NOT NULL,
  `url` varchar(300) NOT NULL,
  `icon` varchar(25) NOT NULL,
  `is_active` tinyint(1) DEFAULT 1,
  `sequence` int(11) NOT NULL,
  `show_in_sidebar` int(1) NOT NULL,
  `group_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules_list`
--

INSERT INTO `modules_list` (`module_id`, `parent_id`, `module_name`, `controller`, `action`, `url`, `icon`, `is_active`, `sequence`, `show_in_sidebar`, `group_name`) VALUES
(1, 0, 'Master', '#', '#', '#', 'fa-folder-open', 1, 1, 1, ''),
(2, 1, 'Country', 'country', 'index', 'country/index', 'None', 1, 1, 1, 'country'),
(3, 1, 'Create Country', 'country', 'create', 'country/create', 'None', 1, 0, 0, 'country'),
(4, 1, 'Delete Country', 'countrry', 'delete', 'country/delete', 'None', 1, 0, 0, 'country'),
(5, 1, 'State', 'state', 'index', 'state/index', 'None', 1, 2, 1, 'state'),
(6, 1, 'Create State', 'state', 'create', 'state/create', 'None', 1, 0, 0, 'state'),
(7, 1, 'Delete State', 'state', 'delete', 'state/delete', 'None', 1, 0, 0, 'state'),
(8, 1, 'City', 'city', 'index', 'city/index', 'None', 1, 3, 1, 'city'),
(9, 1, 'Update City', 'city', 'update', 'city/update', 'None', 1, 0, 0, 'city'),
(10, 1, 'Delete City', 'city', 'delete', 'city/delete', 'None', 1, 0, 0, 'city'),
(11, 1, 'Company', 'company', 'index', 'company/index', 'None', 1, 4, 1, 'company'),
(12, 1, 'Update Company', 'company', 'update', 'company/update', 'None', 1, 0, 0, 'company'),
(13, 1, 'Delete Company', 'company', 'delete', 'company/delete', 'None', 1, 0, 0, 'company'),
(14, 1, 'Transporter Type', 'transporter-type', 'index', 'transporter-type/index', 'None', 1, 5, 1, 'transportertype'),
(15, 1, 'Update Transporter Type', 'transporter-type', 'update', 'transporter-type/update', 'None', 1, 0, 0, 'transportertype'),
(16, 1, 'Delete Transporter Type', 'transporter-type', 'delete', 'transporter-type/delete', 'None', 1, 0, 0, 'transportertype'),
(17, 1, 'Vehicle Ranges', 'vehicle-ranges', 'index', 'vehicle-ranges/index', 'None', 1, 6, 1, 'vehicleranges'),
(18, 1, 'Update Vehicle Ranges', 'vehicle-ranges', 'update', 'vehicle-ranges/update', 'None', 1, 0, 0, 'vehicleranges'),
(19, 1, 'Delete Vehicle Ranges', 'vehicle-ranges', 'delete', 'vehicle-ranges/delete', 'None', 1, 0, 0, 'vehicleranges'),
(20, 1, 'Vehicle Type', 'vehicle-type', 'index', 'vehicle-type/index', 'None', 1, 7, 1, 'vehicletype'),
(21, 1, 'Update Vehicle Type', 'vehicle-type', 'update', 'vehicle-type/update', 'None', 1, 0, 0, 'vehicletype'),
(22, 1, 'Delete Vehicle Type', 'vehicle-type', 'delete', 'vehicle-type/delete', 'None', 1, 0, 0, 'vehicletype'),
(23, 1, 'Vehicle Capacity', 'vehicle-capacity', 'index', 'vehicle-capacity/index', 'None', 1, 8, 1, 'vehiclecapacity'),
(24, 1, 'Update Vehicle Capacity', 'vehicle-capacity', 'update', 'vehicle-capacity/update', 'None', 1, 0, 0, 'vehiclecapacity'),
(25, 1, 'Delete Vehicle Capacity', 'vehicle-capacity', 'delete', 'vehicle-capacity/delete', 'None', 1, 0, 0, 'vehiclecapacity'),
(26, 1, 'Vehicle', 'vehicles', 'index', 'vehicles/index', 'None', 1, 9, 1, 'vehicle'),
(27, 1, 'Update Vehicle', 'vehicles', 'update', 'vehicles/update', 'None', 1, 0, 0, 'vehicle'),
(28, 1, 'Delete Vehicle', 'vehicles', 'delete', 'vehicles/delete', 'None', 1, 0, 0, 'vehicle'),
(29, 0, 'User', '#', '#', '#', 'fa-user-plus', 1, 2, 1, ''),
(30, 29, 'Transporter', 'transporter', 'index', 'transporter/index', 'None', 1, 1, 1, 'transporter'),
(31, 29, 'Transporter Create', 'transporter', 'create', 'transporter/create', 'None', 1, 0, 0, 'transporter'),
(32, 29, 'Transporter delete', 'transporter', 'delete', 'transporter/delete', 'None', 1, 0, 0, 'transporter'),
(33, 29, 'Consignee', 'consignee', 'index', 'consignee/index', 'None', 1, 2, 1, 'consignee'),
(34, 29, 'Create Consignee', 'consignee', 'create', 'consignee/create', 'None', 1, 0, 0, 'consignee'),
(35, 29, 'Update Consignee', 'consignee', 'update', 'consignee/update', 'None', 1, 0, 0, 'consignee'),
(36, 29, 'Delete Consignee', 'consignee', 'delete', 'consignee/delete', 'fa-user', 1, 0, 0, 'consignee'),
(37, 29, 'Consignor', 'consignor', 'index', 'consignor', 'None', 1, 0, 1, 'consignor'),
(38, 29, 'Create Consignor', 'consignor', 'create', 'consignor/delete', 'None', 1, 0, 0, 'consignor'),
(39, 29, 'Update Consignor', 'consignor', 'update', 'consignor/update', 'None', 1, 0, 0, 'consignor'),
(40, 29, 'Delete Consignor', 'consignor', 'delete', 'consignor/delete', 'None', 1, 0, 0, 'consignor'),
(41, 29, 'Driver', 'drivers', 'index', 'drivers/index', 'None', 1, 0, 1, 'driver'),
(42, 29, 'Create Driver', 'drivers', 'create', 'drivers/create', 'None', 1, 0, 0, 'driver'),
(43, 29, 'Update Driver', 'drivers', 'update', 'drivers/update', 'None', 1, 0, 0, 'driver'),
(44, 29, 'Delete Driver', 'drivers', 'delete', 'drivers/delete', 'None', 1, 0, 0, 'driver'),
(45, 0, 'Trip', 'trip-details', 'index', 'trip-details/index', 'fa-truck', 1, 3, 1, 'trip'),
(46, 1, 'Update Country', 'country', 'update', 'country/update', 'None', 1, 0, 0, 'country'),
(47, 0, 'Transporter Dropdown', 'drivers', 'transporter', 'drivers/transporter', 'None', 0, 0, 0, ''),
(48, 1, 'Create Vehicle', 'vehicles', 'create', 'vehicles/create', 'None', 1, 0, 0, 'vehicle'),
(49, 0, 'Admin', '#', '#', '#', 'fa fa-user', 1, 4, 1, ''),
(50, 49, 'Feature list', 'modules-list', 'index', 'modules-list/index', '', 1, 1, 1, 'featurelist'),
(51, 49, 'Create Feature list', 'modules-list', 'create', 'modules-list/create', 'None', 1, 0, 0, 'featurelist'),
(52, 49, 'Update Feature list', 'modules-list', 'update', 'modules-list/update', 'None', 1, 0, 0, 'featurelist'),
(53, 1, 'Add Company Admin', 'company', 'adduser', 'company/adduser', 'None', 0, 0, 0, 'company'),
(54, 1, 'Show Company Admins', 'company', 'allusers', 'company/allusers', 'None', 0, 0, 0, 'company'),
(55, 1, 'Update Company Admin', 'company', 'updateuser', 'company/updateuser', 'None', 0, 0, 0, 'company'),
(56, 29, 'Update Transporter', 'transporter', 'update', 'transporter/update', 'None', 1, 0, 0, 'transporter'),
(57, 29, 'Add Transporter Admin', 'transporter', 'adduser', 'transporter/adduser', 'None', 1, 0, 0, 'transporter'),
(58, 29, 'Show Transporter Admin', 'transporter', 'allusers', 'transporter/allusers', 'None', 1, 0, 0, 'transporter'),
(59, 29, 'Update Transporter Admin', 'transporter', 'updateuser', 'transporter/updateuser', 'None', 1, 0, 0, 'transporter'),
(60, 0, 'Create Trip', 'trip-details', 'create', 'trip-details/create', 'None', 1, 0, 0, 'tripdetails'),
(61, 0, 'Update Trip', 'trip-details', 'update', 'trip-details/update', 'None', 1, 0, 0, 'tripdetails'),
(62, 0, 'Send for Quote', 'trip-details', 'sendforquote', 'trip-details/sendforquote', 'None', 1, 0, 0, 'tripdetails'),
(63, 0, 'View Transporter\'s Responses', 'trip-details', 'viewresponse', 'trip-details/viewresponse', 'None', 1, 0, 0, 'tripdetails'),
(64, 0, 'View Trip summary', 'trip-details', 'view', 'transporter/view', 'None', 1, 0, 0, 'tripdetails'),
(65, 0, 'Get Consignnee and Consignor Address', 'trip-details', 'getaddress', 'trip-details/getaddress', 'None', 0, 0, 0, 'tripdetails'),
(66, 0, 'Allocate Drivers to Trp', 'trip-details', 'allocatedriver', 'trip-details/allocatedriver', 'None', 0, 0, 0, 'tripdetails'),
(67, 0, 'Get Driver list for dropdown', 'trip-details', 'getdriver', 'trip-details/getdriver', 'None', 0, 0, 0, ''),
(68, 0, 'Get Vehicle list for dropdown', 'trip-details', 'getvehicles', 'trip-details/getvehicles', 'None', 0, 0, 0, ''),
(69, 1, 'Create Company', 'company', 'create', 'company/create', 'None', 1, 0, 0, 'company'),
(70, 1, 'City dropdown for company', 'company', 'city', 'company/city', 'None', 0, 0, 0, ''),
(71, 29, 'Company Admin', 'company', 'userlisting', 'company/userlisting', 'None', 0, 1, 1, 'company'),
(72, 0, 'Show Trips', 'trip-details', 'showtrips', 'trip-details/showtrips', '', 1, 5, 1, 'tripdetails'),
(73, 0, 'Transporter- View trip details', 'trip-details', 'viewdetails', 'trip-details/admin', '', 1, 0, 0, 'tripdetails'),
(74, 0, 'Respond to Quote', 'trip-details', 'respondtoquote', 'trip-details/respondtoquote', '', 1, 0, 0, 'tripdetails'),
(75, 1, 'Department', 'department', 'index', 'department', 'None', 1, 4, 1, 'Department'),
(76, 1, 'Add Department', 'department', 'create', 'department/create', 'None', 1, 0, 0, 'Department'),
(77, 1, 'Update Department', 'department', 'update', 'department/update', 'None', 1, 0, 0, 'Department'),
(78, 1, 'Category', 'employee-category', 'index', 'employee-category', 'None', 1, 6, 1, 'Employee Category'),
(79, 1, 'Add Category', 'employee-category', 'create', 'employee-category/create', 'None', 1, 0, 0, 'Employee Category'),
(80, 1, 'Update Category', 'employee-category', 'update', 'employee-category/update', 'None', 1, 0, 0, 'Employee Category'),
(81, 1, 'Material', 'material-master', 'index', 'material-master', 'None', 1, 6, 1, 'Material'),
(82, 1, 'Add Material', 'material-master', 'create', 'material-master/create', 'None', 1, 0, 0, 'Material'),
(83, 1, 'Update Material', 'material-master', 'update', 'material-master/update', 'None', 1, 0, 0, 'Material'),
(84, 1, 'Employee', 'employee', 'index', 'employee', 'None', 1, 4, 1, 'Employee'),
(85, 1, 'Add Employee', 'employee', 'create', 'employee/create', 'None', 1, 0, 0, 'Employee'),
(86, 1, 'Update Employee', 'employee', 'update', 'employee/update', 'None', 1, 0, 0, 'Employee'),
(87, 1, 'Active/Inactive Employee', 'employee', 'delete', 'employee/delete', 'None', 1, 0, 0, 'Employee'),
(88, 1, 'User', 'users', 'index', 'users', 'None', 1, 5, 1, 'User'),
(89, 1, 'Add User', 'users', 'create', 'users/create', 'None', 1, 0, 0, 'User'),
(90, 1, 'Update User', 'users', 'update', 'users/update', 'None', 1, 0, 0, 'User'),
(91, 1, 'Active/Inactive User', 'users', 'changestatus', 'users/changestatus', 'None', 1, 0, 0, 'User'),
(92, 1, 'Role', 'role', 'index', 'role', 'None', 1, 1, 1, 'Role'),
(93, 1, 'Add Role', 'role', 'create', 'role/create', 'None', 1, 0, 0, 'Role'),
(94, 1, 'Update Role', 'role', 'update', 'role/update', 'None', 1, 0, 0, 'Role'),
(95, 1, 'View Role', 'role', 'view', 'role/view', 'None', 1, 0, 0, 'Role'),
(96, 1, 'Copy Role', 'role', 'copy', 'role/copy', 'None', 1, 0, 0, 'Role');

-- --------------------------------------------------------

--
-- Table structure for table `news_master`
--

CREATE TABLE `news_master` (
  `id` int(30) NOT NULL,
  `date` date NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `image_path` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `added_date` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '1-active, 0-inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `news_master`
--

INSERT INTO `news_master` (`id`, `date`, `title`, `description`, `image_path`, `added_date`, `status`) VALUES
(1, '2019-10-06', 'WHISKIES TO ENJOY AT A WEEKEND HOUSE PARTY', '<p>Throwing a house party has the potential to be the highlight of your social status, but planning a house party is a much more tedious activity that few people enjoy, and that is because very few people get it right.Familiarizing yourself with every whimsical guest request is the first step in the right direction and for that, you must know every kind of drinker that shows up to a house party.</p>\r\n\r\n<p>Setting the tone for your house party is governed by getting your alcohol shopping list right, and for that, we have you covered. Whether it&rsquo;s the guest that exclusively emphasizes on single malt whiskies, to the champagne diva, this guide will help you cover every alcohol related request.</p>\r\n\r\n<p>You can easily make your house party one of the buzziest nights of the year that will have people eagerly waiting for an invite for the next one! Let us get started on understanding every kind of house party guest that show up, so that you can walk up to the store with purpose and the decisive execution to define your identity of the perfect party planner.</p>\r\n\r\n<h2>The Drinking-To-Get-High People</h2>\r\n\r\n<p>Every party is crowded with people who are indulging in drinks to loosen up, and hit the dance floor as quickly as possible. Help these party people shake-a-leg with some fine Indian whisky brands such as&nbsp;<a href=\"https://thewhiskypedia.com/royal-stag-whisky\">Royal Stag</a>, or&nbsp;<a href=\"https://thewhiskypedia.com/blenders-pride-whisky\">Blenders Pride</a>, for a slightly more premium whisky brand. These whiskies deliver a quality product at a reasonable price and administer a buzz that will help your guests to hit the dance floor with an enviable energy!</p>\r\n', 'http://admin.spiritpedia.xceedtech.in/images/news/15710482081.jpg', '2019-10-14 00:00:00', 1),
(2, '2019-10-14', 'FACTS ABOUT SOME OF THE OLDEST AND MOST ENDURING SCOTCH WHISKY BRANDS', '<p>None can deny how Scotch whisky is an absolutely crucial and integral part of Scotland&rsquo;s history, and just how much it has shaped the country&rsquo;s identity for centuries. Scotch lovers have for years, undertaken pilgrimages to the beautiful country to visit and savour every aspect of this hugely interdependent history.</p>\r\n\r\n<p>The people, distilleries and brands that have played a pivotal role towards enriching this enormous tradition are revered all over Scotland, and for every Scotch lover in the world, this knowledge is gospel.</p>\r\n\r\n<p>Let us learn about some of the oldest and the most enduring&nbsp;<a href=\"https://thewhiskypedia.com/whisky-brands-by-region/scotch-whisky\">Scotch whisky brands</a>&nbsp;that have contributed immensely towards keeping the traditions of Scotch whisky making alive, popularized the spirit of the Gods all over the world, and strived for excellence no matter the odds.</p>\r\n\r\n<h2>The Glenlivet</h2>\r\n\r\n<p>The definitive Speyside distillery, The Glenlivet has been an institution unto itself for over two centuries since George Smith, founder and master distiller, established the brand in 1824.</p>\r\n\r\n<p>Back in the day, while the Speyside was dotted with illicit distilleries all over, George Smith too was running an illicit operation of his own. Perfecting the craft of distilling what is known today as &lsquo;the single malt that started it all&rsquo;, George Smith&rsquo;s whisky gained an infamous notoriety in the region.</p>\r\n\r\n<p>Tempting even King George IV himself who requested a dram of The Glenlivet when he visited the country of Scotland, the now iconic single malt brand was the first Speyside distillery to obtain a license to distil whisky. Currently, The Glenlivet Distillery is the oldest distillery in the parish of Glenlivet, and has survived threats of sabotage and destruction from rival distillers, both World Wars, the Great Depression, and the Prohibition too.</p>\r\n\r\n<p>Today, The Glenlivet whisky is considered', 'http://admin.spiritpedia.xceedtech.in/images/news/15710489332.jpg', '2019-10-14 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `option_master`
--

CREATE TABLE `option_master` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `option_master`
--

INSERT INTO `option_master` (`id`, `name`, `status`) VALUES
(1, 'size', 1);

-- --------------------------------------------------------

--
-- Table structure for table `option_values`
--

CREATE TABLE `option_values` (
  `id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `option_values`
--

INSERT INTO `option_values` (`id`, `option_id`, `option_value`) VALUES
(1, 1, 'S');

-- --------------------------------------------------------

--
-- Table structure for table `order_master`
--

CREATE TABLE `order_master` (
  `id` int(30) NOT NULL,
  `order_date` datetime NOT NULL,
  `user_id` int(30) NOT NULL,
  `product_id` int(30) NOT NULL,
  `product_amount` double NOT NULL,
  `delivery_amount` double NOT NULL,
  `total_amount` double NOT NULL,
  `payment_mode` int(250) NOT NULL,
  `fullname` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(750) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `pincode` int(6) NOT NULL,
  `city` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `order_status` int(1) NOT NULL COMMENT '1-placed,2-approved,3-deliver,4-reached',
  `order_remark` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `order_update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `specification` longtext COLLATE utf8_unicode_ci NOT NULL,
  `category` int(11) NOT NULL,
  `color` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `actual_price` double NOT NULL,
  `discounted_price` double NOT NULL,
  `discount` int(2) NOT NULL,
  `quantity` int(3) NOT NULL,
  `image` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `manufacturer` int(11) NOT NULL,
  `available_from` date NOT NULL,
  `available_to` date NOT NULL,
  `free_whiskypoints` int(11) NOT NULL,
  `price` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_master`
--

CREATE TABLE `product_master` (
  `id` int(30) NOT NULL,
  `product_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `product_type` int(30) NOT NULL,
  `quantity` int(30) NOT NULL,
  `image_path` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `retail_price` double NOT NULL,
  `price` double NOT NULL,
  `whisky_points` int(30) NOT NULL,
  `discount` int(30) NOT NULL COMMENT 'in %',
  `added_date` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '1-active, 0-inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_pedia`
--

CREATE TABLE `product_pedia` (
  `id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `keywards` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `product_char_style` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `region` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `product_category` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `product_distillery` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `distillery_description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_type`
--

CREATE TABLE `product_type` (
  `id` int(30) NOT NULL,
  `type` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL COMMENT '1-active, 0-inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

CREATE TABLE `recipes` (
  `id` int(11) NOT NULL,
  `name_of_cocktail` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ingredients` text COLLATE utf8_unicode_ci NOT NULL,
  `how_to_make` text COLLATE utf8_unicode_ci NOT NULL,
  `video_url` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `referal_master`
--

CREATE TABLE `referal_master` (
  `id` int(30) NOT NULL,
  `user_id` int(30) NOT NULL,
  `to_user_id` int(30) NOT NULL,
  `referal_code` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `used_at` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '1-success,0-failed'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `role_name` varchar(100) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `company_id`, `role_name`, `status`) VALUES
(1, 0, 'SuperAdmin', 1),
(2, 0, 'Company Admin', -1),
(3, 0, 'Transporter', -1),
(4, 0, 'Consignee', -1),
(5, 0, 'Consignor', -1),
(6, 6, 'Admin', 1),
(7, 6, 'New testing role -copy', 1),
(8, 6, 'Tester', 0),
(9, 6, 'Developer', 1),
(10, 9, 'Admin-ABHAY', 1);

-- --------------------------------------------------------

--
-- Table structure for table `role_module_permission`
--

CREATE TABLE `role_module_permission` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `module_id` int(11) DEFAULT NULL,
  `access` int(1) DEFAULT NULL,
  `added_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_module_permission`
--

INSERT INTO `role_module_permission` (`id`, `role_id`, `module_id`, `access`, `added_at`) VALUES
(1, 6, 3, 0, '2020-04-23 06:23:22'),
(2, 6, 4, 0, '2020-04-23 06:23:22'),
(3, 6, 6, 0, '2020-04-23 06:23:22'),
(4, 6, 7, 0, '2020-04-23 06:23:22'),
(5, 6, 9, 0, '2020-04-23 06:23:22'),
(6, 6, 10, 0, '2020-04-23 06:23:22'),
(7, 6, 12, 0, '2020-04-23 06:23:22'),
(8, 6, 13, 0, '2020-04-23 06:23:22'),
(9, 6, 15, 1, '2020-04-23 06:23:22'),
(10, 6, 16, 1, '2020-04-23 06:23:22'),
(11, 6, 18, 1, '2020-04-23 06:23:22'),
(12, 6, 19, 1, '2020-04-23 06:23:22'),
(13, 6, 21, 1, '2020-04-23 06:23:22'),
(14, 6, 22, 1, '2020-04-23 06:23:22'),
(15, 6, 24, 1, '2020-04-23 06:23:22'),
(16, 6, 25, 1, '2020-04-23 06:23:22'),
(17, 6, 27, 1, '2020-04-23 06:23:22'),
(18, 6, 28, 1, '2020-04-23 06:23:22'),
(19, 6, 31, 1, '2020-04-23 06:23:22'),
(20, 6, 32, 1, '2020-04-23 06:23:22'),
(21, 6, 34, 1, '2020-04-23 06:23:22'),
(22, 6, 35, 1, '2020-04-23 06:23:22'),
(23, 6, 36, 1, '2020-04-23 06:23:22'),
(24, 6, 37, 1, '2020-04-23 06:23:22'),
(25, 6, 38, 1, '2020-04-23 06:23:22'),
(26, 6, 39, 1, '2020-04-23 06:23:22'),
(27, 6, 40, 1, '2020-04-23 06:23:22'),
(28, 6, 41, 1, '2020-04-23 06:23:22'),
(29, 6, 42, 1, '2020-04-23 06:23:22'),
(30, 6, 43, 1, '2020-04-23 06:23:22'),
(31, 6, 44, 1, '2020-04-23 06:23:22'),
(32, 6, 46, 0, '2020-04-23 06:23:22'),
(33, 6, 48, 1, '2020-04-23 06:23:22'),
(34, 6, 53, 0, '2020-04-23 06:23:22'),
(35, 6, 54, 0, '2020-04-23 06:23:22'),
(36, 6, 55, 0, '2020-04-23 06:23:22'),
(37, 6, 56, 1, '2020-04-23 06:23:22'),
(38, 6, 57, 1, '2020-04-23 06:23:22'),
(39, 6, 58, 1, '2020-04-23 06:23:22'),
(40, 6, 59, 1, '2020-04-23 06:23:22'),
(41, 6, 69, 0, '2020-04-23 06:23:22'),
(42, 6, 76, 1, '2020-04-23 06:23:22'),
(43, 6, 77, 1, '2020-04-23 06:23:22'),
(44, 6, 79, 1, '2020-04-23 06:23:22'),
(45, 6, 80, 1, '2020-04-23 06:23:22'),
(46, 6, 82, 1, '2020-04-23 06:23:22'),
(47, 6, 83, 1, '2020-04-23 06:23:22'),
(48, 6, 85, 1, '2020-04-23 06:23:22'),
(49, 6, 86, 1, '2020-04-23 06:23:22'),
(50, 6, 87, 1, '2020-04-23 06:23:22'),
(51, 6, 89, 1, '2020-04-23 06:23:22'),
(52, 6, 90, 1, '2020-04-23 06:23:22'),
(53, 6, 91, 1, '2020-04-23 06:23:22'),
(54, 6, 93, 1, '2020-04-23 06:23:22'),
(55, 6, 94, 1, '2020-04-23 06:23:22'),
(56, 6, 95, 1, '2020-04-23 06:23:22'),
(57, 6, 96, 1, '2020-04-23 06:23:22'),
(58, 6, 2, 0, '2020-04-23 06:23:22'),
(59, 6, 30, 1, '2020-04-23 06:23:22'),
(60, 6, 71, 0, '2020-04-23 06:23:22'),
(61, 6, 92, 1, '2020-04-23 06:23:22'),
(62, 6, 5, 0, '2020-04-23 06:23:22'),
(63, 6, 33, 1, '2020-04-23 06:23:22'),
(64, 6, 8, 0, '2020-04-23 06:23:22'),
(65, 6, 45, 1, '2020-04-23 06:23:22'),
(66, 6, 11, 0, '2020-04-23 06:23:22'),
(67, 6, 75, 1, '2020-04-23 06:23:22'),
(68, 6, 84, 1, '2020-04-23 06:23:22'),
(69, 6, 14, 1, '2020-04-23 06:23:22'),
(70, 6, 88, 1, '2020-04-23 06:23:22'),
(71, 6, 17, 1, '2020-04-23 06:23:22'),
(72, 6, 78, 1, '2020-04-23 06:23:22'),
(73, 6, 81, 1, '2020-04-23 06:23:22'),
(74, 6, 20, 1, '2020-04-23 06:23:22'),
(75, 6, 23, 1, '2020-04-23 06:23:22'),
(76, 6, 26, 1, '2020-04-23 06:23:22'),
(77, 6, 51, 0, '2020-04-23 06:24:26'),
(78, 6, 52, 0, '2020-04-23 06:24:26'),
(79, 6, 50, 0, '2020-04-23 06:24:26'),
(80, 7, 3, 1, '2020-04-25 04:29:10'),
(81, 7, 4, 1, '2020-04-25 04:29:10'),
(82, 7, 6, 1, '2020-04-25 04:29:10'),
(83, 7, 7, 1, '2020-04-25 04:29:10'),
(84, 7, 9, 1, '2020-04-25 04:29:10'),
(85, 7, 10, 1, '2020-04-25 04:29:10'),
(86, 7, 12, 1, '2020-04-25 04:29:10'),
(87, 7, 13, 1, '2020-04-25 04:29:10'),
(88, 7, 15, 1, '2020-04-25 04:29:10'),
(89, 7, 16, 1, '2020-04-25 04:29:10'),
(90, 7, 18, 1, '2020-04-25 04:29:10'),
(91, 7, 19, 1, '2020-04-25 04:29:10'),
(92, 7, 21, 1, '2020-04-25 04:29:10'),
(93, 7, 22, 1, '2020-04-25 04:29:10'),
(94, 7, 24, 1, '2020-04-25 04:29:11'),
(95, 7, 25, 1, '2020-04-25 04:29:11'),
(96, 7, 27, 1, '2020-04-25 04:29:11'),
(97, 7, 28, 1, '2020-04-25 04:29:11'),
(98, 7, 31, 1, '2020-04-25 04:29:11'),
(99, 7, 32, 1, '2020-04-25 04:29:11'),
(100, 7, 34, 1, '2020-04-25 04:29:11'),
(101, 7, 35, 1, '2020-04-25 04:29:11'),
(102, 7, 36, 1, '2020-04-25 04:29:11'),
(103, 7, 37, 1, '2020-04-25 04:29:11'),
(104, 7, 38, 1, '2020-04-25 04:29:11'),
(105, 7, 39, 1, '2020-04-25 04:29:11'),
(106, 7, 40, 1, '2020-04-25 04:29:12'),
(107, 7, 41, 1, '2020-04-25 04:29:12'),
(108, 7, 42, 1, '2020-04-25 04:29:12'),
(109, 7, 43, 1, '2020-04-25 04:29:12'),
(110, 7, 44, 1, '2020-04-25 04:29:12'),
(111, 7, 46, 1, '2020-04-25 04:29:12'),
(112, 7, 48, 1, '2020-04-25 04:29:12'),
(113, 7, 53, 1, '2020-04-25 04:29:12'),
(114, 7, 54, 1, '2020-04-25 04:29:12'),
(115, 7, 55, 1, '2020-04-25 04:29:12'),
(116, 7, 56, 1, '2020-04-25 04:29:13'),
(117, 7, 57, 1, '2020-04-25 04:29:13'),
(118, 7, 58, 1, '2020-04-25 04:29:13'),
(119, 7, 59, 1, '2020-04-25 04:29:13'),
(120, 7, 69, 1, '2020-04-25 04:29:13'),
(121, 7, 76, 1, '2020-04-25 04:29:13'),
(122, 7, 77, 1, '2020-04-25 04:29:13'),
(123, 7, 79, 1, '2020-04-25 04:29:13'),
(124, 7, 80, 1, '2020-04-25 04:29:13'),
(125, 7, 82, 1, '2020-04-25 04:29:13'),
(126, 7, 83, 1, '2020-04-25 04:29:13'),
(127, 7, 85, 1, '2020-04-25 04:29:13'),
(128, 7, 86, 1, '2020-04-25 04:29:13'),
(129, 7, 87, 1, '2020-04-25 04:29:13'),
(130, 7, 89, 1, '2020-04-25 04:29:13'),
(131, 7, 90, 1, '2020-04-25 04:29:14'),
(132, 7, 91, 1, '2020-04-25 04:29:14'),
(133, 7, 93, 1, '2020-04-25 04:29:14'),
(134, 7, 94, 1, '2020-04-25 04:29:14'),
(135, 7, 95, 1, '2020-04-25 04:29:14'),
(136, 7, 96, 1, '2020-04-25 04:29:14'),
(137, 7, 2, 1, '2020-04-25 04:29:14'),
(138, 7, 30, 1, '2020-04-25 04:29:14'),
(139, 7, 71, 1, '2020-04-25 04:29:14'),
(140, 7, 92, 1, '2020-04-25 04:29:15'),
(141, 7, 5, 1, '2020-04-25 04:29:15'),
(142, 7, 33, 1, '2020-04-25 04:29:15'),
(143, 7, 8, 1, '2020-04-25 04:29:15'),
(144, 7, 45, 1, '2020-04-25 04:29:15'),
(145, 7, 11, 1, '2020-04-25 04:29:15'),
(146, 7, 75, 1, '2020-04-25 04:29:15'),
(147, 7, 84, 1, '2020-04-25 04:29:15'),
(148, 7, 14, 1, '2020-04-25 04:29:16'),
(149, 7, 88, 1, '2020-04-25 04:29:16'),
(150, 7, 17, 1, '2020-04-25 04:29:16'),
(151, 7, 78, 1, '2020-04-25 04:29:16'),
(152, 7, 81, 1, '2020-04-25 04:29:16'),
(153, 7, 20, 1, '2020-04-25 04:29:16'),
(154, 7, 23, 1, '2020-04-25 04:29:16'),
(155, 7, 26, 1, '2020-04-25 04:29:16'),
(156, 7, 51, 1, '2020-04-25 04:29:17'),
(157, 7, 52, 1, '2020-04-25 04:29:17'),
(158, 7, 50, 1, '2020-04-25 04:29:17'),
(159, 6, 60, 1, '2020-04-25 04:32:00'),
(160, 6, 61, 1, '2020-04-25 04:32:00'),
(161, 6, 62, 1, '2020-04-25 04:32:00'),
(162, 6, 63, 1, '2020-04-25 04:32:00'),
(163, 6, 64, 1, '2020-04-25 04:32:01'),
(164, 6, 73, 1, '2020-04-25 04:32:01'),
(165, 6, 74, 1, '2020-04-25 04:32:01'),
(166, 8, 15, 1, '2020-05-05 06:16:00'),
(167, 8, 16, 1, '2020-05-05 06:16:00'),
(168, 8, 18, 1, '2020-05-05 06:16:00'),
(169, 8, 19, 1, '2020-05-05 06:16:00'),
(170, 8, 21, 1, '2020-05-05 06:16:00'),
(171, 8, 22, 1, '2020-05-05 06:16:00'),
(172, 8, 24, 1, '2020-05-05 06:16:00'),
(173, 8, 25, 1, '2020-05-05 06:16:00'),
(174, 8, 27, 1, '2020-05-05 06:16:00'),
(175, 8, 28, 1, '2020-05-05 06:16:00'),
(176, 8, 31, 1, '2020-05-05 06:16:00'),
(177, 8, 32, 1, '2020-05-05 06:16:00'),
(178, 8, 41, 1, '2020-05-05 06:16:00'),
(179, 8, 42, 1, '2020-05-05 06:16:00'),
(180, 8, 43, 1, '2020-05-05 06:16:00'),
(181, 8, 44, 1, '2020-05-05 06:16:00'),
(182, 8, 48, 1, '2020-05-05 06:16:00'),
(183, 8, 56, 1, '2020-05-05 06:16:00'),
(184, 8, 57, 1, '2020-05-05 06:16:00'),
(185, 8, 58, 1, '2020-05-05 06:16:00'),
(186, 8, 59, 1, '2020-05-05 06:16:00'),
(187, 8, 76, 1, '2020-05-05 06:16:00'),
(188, 8, 77, 1, '2020-05-05 06:16:00'),
(189, 8, 79, 1, '2020-05-05 06:16:00'),
(190, 8, 80, 1, '2020-05-05 06:16:00'),
(191, 8, 82, 1, '2020-05-05 06:16:00'),
(192, 8, 83, 1, '2020-05-05 06:16:00'),
(193, 8, 89, 1, '2020-05-05 06:16:00'),
(194, 8, 90, 1, '2020-05-05 06:16:00'),
(195, 8, 91, 1, '2020-05-05 06:16:00'),
(196, 8, 30, 1, '2020-05-05 06:16:00'),
(197, 8, 75, 1, '2020-05-05 06:16:00'),
(198, 8, 14, 1, '2020-05-05 06:16:00'),
(199, 8, 88, 1, '2020-05-05 06:16:00'),
(200, 8, 17, 1, '2020-05-05 06:16:00'),
(201, 8, 78, 1, '2020-05-05 06:16:00'),
(202, 8, 81, 1, '2020-05-05 06:16:00'),
(203, 8, 20, 1, '2020-05-05 06:16:00'),
(204, 8, 23, 1, '2020-05-05 06:16:00'),
(205, 8, 26, 1, '2020-05-05 06:16:00'),
(206, 9, 9, 1, '2020-05-07 05:06:02'),
(207, 9, 10, 1, '2020-05-07 05:06:02'),
(208, 9, 12, 1, '2020-05-07 05:06:02'),
(209, 9, 13, 1, '2020-05-07 05:06:02'),
(210, 9, 15, 1, '2020-05-07 05:06:02'),
(211, 9, 16, 1, '2020-05-07 05:06:02'),
(212, 9, 18, 1, '2020-05-07 05:06:02'),
(213, 9, 19, 1, '2020-05-07 05:06:02'),
(214, 9, 21, 1, '2020-05-07 05:06:02'),
(215, 9, 22, 1, '2020-05-07 05:06:02'),
(216, 9, 24, 1, '2020-05-07 05:06:02'),
(217, 9, 25, 1, '2020-05-07 05:06:02'),
(218, 9, 27, 1, '2020-05-07 05:06:02'),
(219, 9, 28, 1, '2020-05-07 05:06:02'),
(220, 9, 31, 1, '2020-05-07 05:06:02'),
(221, 9, 32, 1, '2020-05-07 05:06:02'),
(222, 9, 34, 1, '2020-05-07 05:06:02'),
(223, 9, 35, 1, '2020-05-07 05:06:02'),
(224, 9, 36, 1, '2020-05-07 05:06:02'),
(225, 9, 37, 1, '2020-05-07 05:06:02'),
(226, 9, 38, 1, '2020-05-07 05:06:02'),
(227, 9, 39, 1, '2020-05-07 05:06:02'),
(228, 9, 40, 1, '2020-05-07 05:06:02'),
(229, 9, 41, 1, '2020-05-07 05:06:02'),
(230, 9, 42, 1, '2020-05-07 05:06:02'),
(231, 9, 43, 1, '2020-05-07 05:06:02'),
(232, 9, 44, 1, '2020-05-07 05:06:02'),
(233, 9, 48, 1, '2020-05-07 05:06:02'),
(234, 9, 53, 1, '2020-05-07 05:06:02'),
(235, 9, 54, 1, '2020-05-07 05:06:02'),
(236, 9, 55, 1, '2020-05-07 05:06:02'),
(237, 9, 56, 1, '2020-05-07 05:06:02'),
(238, 9, 57, 1, '2020-05-07 05:06:02'),
(239, 9, 58, 1, '2020-05-07 05:06:02'),
(240, 9, 59, 1, '2020-05-07 05:06:02'),
(241, 9, 60, 1, '2020-05-07 05:06:02'),
(242, 9, 61, 1, '2020-05-07 05:06:02'),
(243, 9, 62, 1, '2020-05-07 05:06:02'),
(244, 9, 63, 1, '2020-05-07 05:06:02'),
(245, 9, 64, 1, '2020-05-07 05:06:02'),
(246, 9, 69, 1, '2020-05-07 05:06:02'),
(247, 9, 73, 1, '2020-05-07 05:06:02'),
(248, 9, 74, 1, '2020-05-07 05:06:02'),
(249, 9, 76, 1, '2020-05-07 05:06:02'),
(250, 9, 77, 1, '2020-05-07 05:06:02'),
(251, 9, 79, 1, '2020-05-07 05:06:02'),
(252, 9, 80, 1, '2020-05-07 05:06:02'),
(253, 9, 82, 1, '2020-05-07 05:06:02'),
(254, 9, 83, 1, '2020-05-07 05:06:02'),
(255, 9, 85, 1, '2020-05-07 05:06:02'),
(256, 9, 86, 1, '2020-05-07 05:06:02'),
(257, 9, 87, 1, '2020-05-07 05:06:02'),
(258, 9, 93, 1, '2020-05-07 05:06:02'),
(259, 9, 94, 1, '2020-05-07 05:06:02'),
(260, 9, 95, 1, '2020-05-07 05:06:02'),
(261, 9, 96, 1, '2020-05-07 05:06:02'),
(262, 9, 30, 1, '2020-05-07 05:06:02'),
(263, 9, 71, 1, '2020-05-07 05:06:02'),
(264, 9, 92, 1, '2020-05-07 05:06:02'),
(265, 9, 33, 1, '2020-05-07 05:06:02'),
(266, 9, 8, 1, '2020-05-07 05:06:02'),
(267, 9, 45, 1, '2020-05-07 05:06:02'),
(268, 9, 11, 1, '2020-05-07 05:06:02'),
(269, 9, 75, 1, '2020-05-07 05:06:02'),
(270, 9, 84, 1, '2020-05-07 05:06:02'),
(271, 9, 14, 1, '2020-05-07 05:06:02'),
(272, 9, 72, 1, '2020-05-07 05:06:02'),
(273, 9, 17, 1, '2020-05-07 05:06:02'),
(274, 9, 78, 1, '2020-05-07 05:06:02'),
(275, 9, 81, 1, '2020-05-07 05:06:02'),
(276, 9, 20, 1, '2020-05-07 05:06:02'),
(277, 9, 23, 1, '2020-05-07 05:06:02'),
(278, 9, 26, 1, '2020-05-07 05:06:02'),
(279, 10, 3, 0, '2020-05-08 07:19:33'),
(280, 10, 4, 0, '2020-05-08 07:19:33'),
(281, 10, 6, 0, '2020-05-08 07:19:33'),
(282, 10, 7, 0, '2020-05-08 07:19:33'),
(283, 10, 9, 0, '2020-05-08 07:19:33'),
(284, 10, 10, 0, '2020-05-08 07:19:33'),
(285, 10, 12, 0, '2020-05-08 07:19:33'),
(286, 10, 13, 0, '2020-05-08 07:19:33'),
(287, 10, 15, 1, '2020-05-08 07:19:33'),
(288, 10, 16, 1, '2020-05-08 07:19:33'),
(289, 10, 18, 1, '2020-05-08 07:19:33'),
(290, 10, 19, 1, '2020-05-08 07:19:33'),
(291, 10, 21, 1, '2020-05-08 07:19:33'),
(292, 10, 22, 1, '2020-05-08 07:19:33'),
(293, 10, 24, 1, '2020-05-08 07:19:33'),
(294, 10, 25, 1, '2020-05-08 07:19:33'),
(295, 10, 27, 1, '2020-05-08 07:19:33'),
(296, 10, 28, 1, '2020-05-08 07:19:33'),
(297, 10, 31, 1, '2020-05-08 07:19:33'),
(298, 10, 32, 1, '2020-05-08 07:19:33'),
(299, 10, 34, 1, '2020-05-08 07:19:33'),
(300, 10, 35, 1, '2020-05-08 07:19:33'),
(301, 10, 36, 1, '2020-05-08 07:19:33'),
(302, 10, 37, 1, '2020-05-08 07:19:33'),
(303, 10, 38, 1, '2020-05-08 07:19:33'),
(304, 10, 39, 1, '2020-05-08 07:19:33'),
(305, 10, 40, 1, '2020-05-08 07:19:33'),
(306, 10, 41, 1, '2020-05-08 07:19:33'),
(307, 10, 42, 1, '2020-05-08 07:19:33'),
(308, 10, 43, 1, '2020-05-08 07:19:33'),
(309, 10, 44, 1, '2020-05-08 07:19:33'),
(310, 10, 46, 0, '2020-05-08 07:19:33'),
(311, 10, 48, 1, '2020-05-08 07:19:33'),
(312, 10, 53, 0, '2020-05-08 07:19:33'),
(313, 10, 54, 0, '2020-05-08 07:19:33'),
(314, 10, 55, 0, '2020-05-08 07:19:33'),
(315, 10, 56, 1, '2020-05-08 07:19:33'),
(316, 10, 57, 1, '2020-05-08 07:19:33'),
(317, 10, 58, 1, '2020-05-08 07:19:33'),
(318, 10, 59, 1, '2020-05-08 07:19:33'),
(319, 10, 69, 0, '2020-05-08 07:19:33'),
(320, 10, 76, 1, '2020-05-08 07:19:33'),
(321, 10, 77, 1, '2020-05-08 07:19:33'),
(322, 10, 79, 1, '2020-05-08 07:19:33'),
(323, 10, 80, 1, '2020-05-08 07:19:33'),
(324, 10, 82, 1, '2020-05-08 07:19:33'),
(325, 10, 83, 1, '2020-05-08 07:19:33'),
(326, 10, 85, 1, '2020-05-08 07:19:33'),
(327, 10, 86, 1, '2020-05-08 07:19:33'),
(328, 10, 87, 1, '2020-05-08 07:19:33'),
(329, 10, 89, 1, '2020-05-08 07:19:33'),
(330, 10, 90, 1, '2020-05-08 07:19:33'),
(331, 10, 91, 1, '2020-05-08 07:19:33'),
(332, 10, 93, 1, '2020-05-08 07:19:33'),
(333, 10, 94, 1, '2020-05-08 07:19:33'),
(334, 10, 95, 1, '2020-05-08 07:19:33'),
(335, 10, 96, 1, '2020-05-08 07:19:33'),
(336, 10, 2, 0, '2020-05-08 07:19:33'),
(337, 10, 30, 1, '2020-05-08 07:19:33'),
(338, 10, 71, 0, '2020-05-08 07:19:33'),
(339, 10, 92, 1, '2020-05-08 07:19:33'),
(340, 10, 5, 0, '2020-05-08 07:19:33'),
(341, 10, 33, 1, '2020-05-08 07:19:33'),
(342, 10, 8, 0, '2020-05-08 07:19:33'),
(343, 10, 45, 1, '2020-05-08 07:19:33'),
(344, 10, 11, 0, '2020-05-08 07:19:33'),
(345, 10, 75, 1, '2020-05-08 07:19:33'),
(346, 10, 84, 1, '2020-05-08 07:19:33'),
(347, 10, 14, 1, '2020-05-08 07:19:33'),
(348, 10, 88, 1, '2020-05-08 07:19:33'),
(349, 10, 17, 1, '2020-05-08 07:19:33'),
(350, 10, 78, 1, '2020-05-08 07:19:33'),
(351, 10, 81, 1, '2020-05-08 07:19:33'),
(352, 10, 20, 1, '2020-05-08 07:19:33'),
(353, 10, 23, 1, '2020-05-08 07:19:33'),
(354, 10, 26, 1, '2020-05-08 07:19:33'),
(355, 10, 51, 0, '2020-05-08 07:19:33'),
(356, 10, 52, 0, '2020-05-08 07:19:33'),
(357, 10, 50, 0, '2020-05-08 07:19:33'),
(358, 10, 60, 1, '2020-05-08 07:19:33'),
(359, 10, 61, 1, '2020-05-08 07:19:33'),
(360, 10, 62, 1, '2020-05-08 07:19:33'),
(361, 10, 63, 1, '2020-05-08 07:19:33'),
(362, 10, 64, 1, '2020-05-08 07:19:33'),
(363, 10, 73, 1, '2020-05-08 07:19:33'),
(364, 10, 74, 1, '2020-05-08 07:19:33');

-- --------------------------------------------------------

--
-- Table structure for table `spam_master`
--

CREATE TABLE `spam_master` (
  `id` int(30) NOT NULL,
  `thread_id` int(30) NOT NULL,
  `user_id` int(30) NOT NULL,
  `remark` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `added_date` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '1-active, 0-inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(11) NOT NULL,
  `countryID` int(11) NOT NULL,
  `state` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `countryID`, `state`) VALUES
(1, 100, 'ANDHRA PRADESH'),
(2, 100, 'ASSAM'),
(3, 100, 'ARUNACHAL PRADESH'),
(4, 100, 'BIHAR'),
(5, 100, 'GUJRAT'),
(6, 100, 'HARYANA'),
(7, 100, 'HIMACHAL PRADESH'),
(8, 100, 'JAMMU & KASHMIR'),
(9, 100, 'KARNATAKA'),
(10, 100, 'KERALA'),
(11, 100, 'MADHYA PRADESH'),
(12, 100, 'MAHARASHTRA'),
(13, 100, 'MANIPUR'),
(14, 100, 'MEGHALAYA'),
(15, 100, 'MIZORAM'),
(16, 100, 'NAGALAND'),
(17, 100, 'ORISSA'),
(18, 100, 'PUNJAB'),
(19, 100, 'RAJASTHAN'),
(20, 100, 'SIKKIM'),
(21, 100, 'TAMIL NADU'),
(22, 100, 'TRIPURA'),
(23, 100, 'UTTAR PRADESH'),
(24, 100, 'WEST BENGAL'),
(25, 100, 'DELHI'),
(26, 100, 'GOA'),
(27, 100, 'PONDICHERY'),
(28, 100, 'LAKSHDWEEP'),
(29, 100, 'DAMAN & DIU'),
(30, 100, 'DADRA & NAGAR'),
(31, 100, 'CHANDIGARH'),
(32, 100, 'ANDAMAN & NICOBAR'),
(33, 100, 'UTTARANCHAL'),
(34, 100, 'JHARKHAND'),
(35, 100, 'CHATTISGARH');

-- --------------------------------------------------------

--
-- Table structure for table `thread_master`
--

CREATE TABLE `thread_master` (
  `id` int(30) NOT NULL,
  `feed_id` int(30) NOT NULL,
  `user_id` int(30) NOT NULL,
  `answer` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `views` int(30) NOT NULL,
  `likes` int(30) NOT NULL,
  `post_date` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '1-active, 0-inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(30) NOT NULL,
  `user_id` int(30) NOT NULL,
  `to_user_id` int(30) NOT NULL,
  `transaction_date` datetime NOT NULL,
  `type` varchar(250) COLLATE utf8_unicode_ci NOT NULL COMMENT 'cr,dr',
  `amount` double NOT NULL,
  `closing_amount` double NOT NULL,
  `transaction_term` varchar(250) COLLATE utf8_unicode_ci NOT NULL COMMENT 'ORDER,REFER,VIDEO,ANSWER',
  `remark` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL COMMENT '1-success,0-failed'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `types_of_whiskies`
--

CREATE TABLE `types_of_whiskies` (
  `id` int(11) NOT NULL,
  `alias` varchar(500) DEFAULT NULL,
  `whisky_type` varchar(500) NOT NULL,
  `whisky_description` longtext NOT NULL,
  `image` varchar(500) NOT NULL,
  `meta_title` varchar(500) NOT NULL,
  `meta_keyword` varchar(3000) NOT NULL,
  `meta_description` varchar(3000) NOT NULL,
  `image_alt_text` varchar(200) NOT NULL,
  `banner_image` varchar(500) NOT NULL,
  `banner_alt_text` varchar(200) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` int(3) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `types_of_whiskies`
--

INSERT INTO `types_of_whiskies` (`id`, `alias`, `whisky_type`, `whisky_description`, `image`, `meta_title`, `meta_keyword`, `meta_description`, `image_alt_text`, `banner_image`, `banner_alt_text`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
(1, 'American-Whiskey', 'American Whiskey', '<p>American whiskey, as the name suggests, is produced in the United States of America and is prepared from a fermented mash of cereal grain.</p>\r\n\r\n<p><strong>The primary types of spirit that comprise American whiskey are;</strong></p>\r\n\r\n<p>Rye whiskey</p>\r\n\r\n<p>Bourbon whiskey</p>\r\n\r\n<p>Wheat whiskey</p>\r\n\r\n<p>Malt whiskey</p>\r\n\r\n<p>Rye malt whiskey</p>\r\n\r\n<p>Tennessee whiskey</p>\r\n\r\n<p>Corn whiskey</p>\r\n\r\n<p>A minimum mash of 51% of the named grains is contained in these products, for example, wheat whiskey must use 51% of wheat in its mash and other grains in smaller quantities. Similarly, a rye whiskey must use a 51% rye mash.</p>\r\n\r\n<p>Similarly,&nbsp;<a href=\"https://thewhiskypedia.com/whisky-brands-by-type/bourbon-whiskey\">Bourbon whiskey</a>&nbsp;is legally defined as the one obtained from a mash of 51% Corn, and small batches of other grains.</p>\r\n\r\n<p>Other variants that are included under this classification are blended whiskeys, spirit whiskeys, grain whiskeys and straight whiskey blends &ndash; all devoid of a preponderant grain specification.</p>\r\n\r\n<p>American Whiskey Regulations</p>\r\n\r\n<p>Title 27 of the U.S. Code of Federal Regulations governs the labelling and production of American whiskey. Internationally speaking, there are various countries that recognise and designate products like Tennessee whiskey and bourbon as American whiskeys. The laws overseeing the sale of American whiskey outside United States will be determined by the country in which it&rsquo;s being sold. In some countries, the regulations can be stricter than U.S. law.</p>\r\n\r\n<p>In Canada, products like Tennessee whiskey and bourbon should satisfy the American regulations concerning their manufacture and labelling. However, this is not required in some other countries as there is no uniformity concerning laws.</p>\r\n\r\n<p>Types of American Whiskey</p>\r\n\r\n<p>The following variants of American whiskey are listed in the U.S. Code of Federal Regulations:</p>\r\n\r\n<p>Rye whiskey &ndash; containing at least 51% rye</p>\r\n\r\n<p>Rye malt whiskey &ndash; consisting a minimum of 51% malted rye</p>\r\n\r\n<p>Wheat whiskey &ndash; should contain at least 51% wheat</p>\r\n\r\n<p>Malt whiskey &ndash; comprising at least 51% malted barley</p>\r\n\r\n<p>Bourbon whiskey &ndash; minimum 51% corn (maize)</p>\r\n\r\n<p>Corn whiskey &ndash; should comprise at least 80% corn</p>\r\n\r\n<p>All the above mentioned whiskey types should be distilled to a maximum of 80% alcohol by volume (160 U.S proof) to adequately retain the flavour of the original mash and to effectively deter the addition of caramel, colouring and other flavouring agents. All these products with the exception of corn whiskey should be mandatorily aged for a brief period in charred new oak barrels. However, Canadian law doesn&rsquo;t require American corn whiskey to be aged at all. Conversely, if it&rsquo;s aged then it should be in uncharred oak containers.</p>\r\n\r\n<p>Straight whiskey is distilled to a maximum of 80% alcohol by volume (160 proof) and should be aged for a minimum of two years with the initial alcohol concentration being 62.5% at the most. Any kind of different spirits, additives and colourings should strictly not be added to the blend. Whiskeys devoid of a grain type like corn, rye or bourbon should be distilled to a maximum of 95% alcohol by volume (190 proof) from a fermented mash of grains using such a technique that the original features of whiskey are retained.</p>\r\n\r\n<p>Here are brief descriptions of the various styles of American whiskey.</p>\r\n\r\n<p>Bourbon:</p>\r\n\r\n<p>Produced from a mash of 51%-80% corn and matured in unused charred white-oak barrels. It&rsquo;s considered to be sweeter than other types of American whiskey.</p>\r\n\r\n<p>Corn:</p>\r\n\r\n<p>It&rsquo;s mandatory that this variant contains a minimum of 80% corn and should be matured in uncharred oak barrels. Corn whiskey is inexpensive, aged for a short while and produced for immediate consumption.</p>\r\n\r\n<p>Rye:</p>\r\n\r\n<p>Comprises a minimum of 51% rye and matured in unused charred oak barrels rendering this whiskey spicy and ideal for old fashioned cocktails.</p>\r\n\r\n<p>Tennessee:</p>\r\n\r\n<p>This type should be produced in the American state of Tennessee from a mash of at least 51% corn and matured in unused charred oak containers. Maple charcoal is used to filter this whiskey type before bottling. Jack Daniels &ndash; a popular American whiskey brand is known for producing this type of whiskey.</p>\r\n\r\n<p>Wheat:</p>\r\n\r\n<p>The mash should contain a minimum of 51% wheat and be matured in unused charred white-oak containers to be termed as wheat whiskey. It&rsquo;s quite similar to bourbon except for the grain component and represents a softer style.</p>\r\n\r\n<p>White Dog:</p>\r\n\r\n<p>It&rsquo;s an American term used to designate whiskey that hasn&rsquo;t been aged and is also called white whiskey or moonshine. These types are bottled immediately upon distillation with added water.</p>\r\n\r\n<p>Other American</p>\r\n\r\n<p>This category includes single malt and many craft or micro-distillery offerings and blended and flavour infusions that cannot be classified in the above mentioned categories.</p>\r\n\r\n<p>Let&rsquo;s discuss the federal definitions of the following whiskey types:</p>\r\n\r\n<p>Blended whiskey:</p>\r\n\r\n<p>It&rsquo;s a mixture that comprises straight whiskey or a blend of straight whiskeys containing a minimum of 20 percent straight whiskey, either in combination or separately, neutral spirits or other whiskey. For a whiskey to be recognised with a specific grain type (i.e. wheat, malt, blended rye or bourbon), the straight whiskey should contain a minimum of 51% blend of that grain type.</p>\r\n\r\n<p>Light whiskey:</p>\r\n\r\n<p>Produced in the United States and aged in used or new uncharred oak containers. It&rsquo;s made at more than 80% alcohol by volume.</p>\r\n\r\n<p>Spirit whiskey:</p>\r\n\r\n<p>It&rsquo;s a blend of neutral spirits and a minimum 5% of certain stricter whiskey categories.</p>\r\n', 'http://localhost:8888/spirit/admin/types-of-whiskies/15919528491.jpg', '', '', '', '', '', '', 1, '2020-06-12 09:07:29', 1, NULL, NULL, NULL, NULL),
(2, NULL, 'Blended Scotch Whisky', '<p>The list of iconic Blended Scotch whiskies is a large one, and a significant chunk of the worldwide Scotch whisky sales happen to be blended Scotch whiskies. Not to take away anything from single malts but the rich depth of flavors and immensely complex drinks that blended Scotches are, we understand why they are in such huge demand.</p>\r\n\r\n<p>Brands such as Chivas Regal, Ballantine&rsquo;s, Johnnie Walker, 100 Pipers and many other Blended Scotch whisky makers have conquered bar shelves in countries throughout the globe.</p>\r\n\r\n<p>But what does a Blended Scotch whisky mean; and how are they different from single malts, and single grain whiskies? Are they the same as blended malt whiskies? Let us dispel all the rumors, clear all your doubts, and put you on the Scotch whisky enlightenment path so that the next time you down a few drams, you will have a treasure chests worth of knowledge to spread around.</p>\r\n\r\n<h2>Definition</h2>\r\n\r\n<p>A blended Scotch whisky is a blend of single malt, and single grain whiskies, a marriage of spirits, that was legally prohibited until the Spirits Act of 1860 relaxed these restrictions, and allowed whisky blenders and merchants to unlock new possibilities in the world of whisky making.</p>\r\n\r\n<p>Pioneers in the blended Scotch whisky world, the Chivas Brothers were one of the first whisky makers to seize this opportunity with both hands, and create some of their finest proprietary blends. Other blenders such as the Alec Walker and Alexander Walker II, beneficiaries and builders of the Johnnie Walker brand, soon followed suit.</p>\r\n\r\n<p>Over the years, the blended Scotch whisky market began to gain phenomenal traction, from which arose the need to legally define the parameters for a whisky to be called Scotch whisky, and thus, the parameters for a Blended Scotch whisky too, were laid out.</p>\r\n\r\n<h2>Legal Definition</h2>\r\n\r\n<p>In order for a whisky to earn the title of a Scotch whisky, it must be distilled, matured and bottled in the country of Scotland. Whether it&rsquo;s a single malt, or a single grain whisky, it must embark on its journey from malting to bottling in Scotland for a period no less than three years.</p>\r\n\r\n<p>Scotch whisky must be aged in Oak barrels and bottled at a minimum alcoholic strength of 40%, without the addition of any flavoring or coloring except food-grade caramel coloring.</p>\r\n\r\n<p>Now for Blended Scotch whiskies, they can be a mixture of any number of source whiskies, and there are no limitations on that. When a single malt whisky, and a single grain whisky are married together, the liquid is then legally referred to as a Blended Scotch whisky.</p>\r\n\r\n<p>Similarly, if only two or more single malt whiskies are blended together, it would be referred to as a &lsquo;Blended Malt Whisky&rsquo;; whereas a blend of two or more single grain whiskies would be known as a &lsquo;Blended Grain Whisky&rsquo;.</p>\r\n\r\n<h2>Period of Prominence</h2>\r\n\r\n<p>Misconceptions and flawed notions of the superiority of single malts were soon beginning to fade in the minds of consumers after they had a chance to savor some meticulously blended liquids that redefined what opulence meant.</p>\r\n\r\n<p>Expressions such as the Chivas Regal 25, often credited as the world&rsquo;s first luxury whisky, the Johnnie Walker core range of blended whiskies and others took the world by storm. They began to establish a loyal fan base for blended Scotch whiskies all over the world, inspiring more and more whisky makers to take up whisky blending and deliver rich, complex drinks.</p>\r\n\r\n<h2>Blended Scotch Whiskies Today</h2>\r\n\r\n<p>Chivas Regal, Ballantine&rsquo;s, Johnnie Walker, Teacher&rsquo;s, 100 Pipers and many other blended scotch whisky brands are some of the most voluminously sold whiskies worldwide, and in some countries, outnumber the sale of single malt whiskies altogether.</p>\r\n\r\n<p>New techniques, and many other whimsical changes in the blending process have allowed brands to experiment and present breathtaking expressions to the whisky lovers. Finishing the blended product in special casks, blending a number of whiskies from different region to retain their individual characteristics and other such activities have really diversified the blended Scotch whisky segment with drinks that are truly exquisite and unique in nature.</p>\r\n', 'http://admin.spiritpedia.xceedtech.in/images/whisky/15617199702.jpg', '', '', '', '', '', '', 1, '2019-06-28 05:36:10', 1, NULL, NULL, NULL, NULL),
(3, NULL, 'Bourbon Whiskey', '<p>Bourbon &ndash; a barrel-aged distilled spirit; it is a type of&nbsp;<a href=\"https://thewhiskypedia.com/whisky-brands-by-type/american-whiskey\">American whiskey&nbsp;</a>mainly produced from corn. The roots of this name can be traced back to the French Bourbon dynasty although the precise inspiration remains shrouded in mystery.</p>\r\n\r\n<p>show&nbsp;less</p>\r\n\r\n<p>However, many suggest that it&rsquo;s named after Bourbon County, Kentucky or Bourbon Street, New Orleans &ndash; both named after the dynasty. Historical records say that this popular drink has been distilled since the 18th century and the use of the term can be traced back to the 1820s.</p>\r\n\r\n<p><strong>Bourbon whiskey</strong>&nbsp;is traditionally emblematic of the American south (particularly Kentucky) although it can be manufactured anywhere in the United States. In 2014, bourbon made up about two thirds of the distilled spirits exported by America and the revenue obtained from domestic sales accounted approximately $2.7 billion.</p>\r\n\r\n<h2>Bourbon Whiskey History</h2>\r\n\r\n<p>Scottish, Irish and other European migrants introduced the process of distillation in Kentucky during the late 18th century. The origins of bourbon whiskey are not well documented and there are many presentable conflicting accounts, some being more credible. Some whiskey enthusiasts credit Elijah Craig - distiller and Baptist minister, as the inventor of bourbon. He is said to have pioneered aging whiskey in charred oak casks, which gives bourbon its reddish colour and distinct flavours.</p>\r\n\r\n<p>Others in Bourbon County claim that a man named Jacob Spears was the first to label his offering as bourbon whiskey. His home called Stone Castle on Clay-Kiser Road, spring house and warehouse still survive to this date.</p>\r\n\r\n<p>Both the popular accounts cannot be extensively verified with the Craig story being fictitious and the other one being obscure outside the county. Hence, the invention of bourbon cannot be attributed to a single inventor as there are multiple brains behind its invention.</p>\r\n\r\n<p>This can be confirmed by the fact that Europeans have been aging their whiskey in charred barrels for centuries before the conception of bourbon. However, farmers in Maryland and Pennsylvania were the first ones to conceive the idea of bourbon whiskey. This was formerly considered as a drink for poor American farmers before becoming popular among all classes of the society. The production was subsequently moved to Kentucky due to rising taxes and has become prominent there since then.</p>\r\n\r\n<h2>Bourbon Whiskey Legal Requirements</h2>\r\n\r\n<p><strong>Many would ask, &ldquo;What is bourbon whiskey?</strong></p>\r\n\r\n<p>&ldquo;Most trade agreements state that the name bourbon strictly applies to products made in the United States. Canadian and EU law also have the same definition although local regulations will apply as per the country where it&rsquo;s being sold. The Federal US Standards for consumption are as follows:</p>\r\n\r\n<ul>\r\n	<li>Bourbon should be made in the United States.</li>\r\n	<li>The grain concoction from which it&rsquo;s obtained should contain at least 51% corn.</li>\r\n	<li>The product (bourbon) should be matured in an unused charred oak cask.</li>\r\n	<li>Should be distilled to more than 160 U.S. proof (80% alcohol by volume).</li>\r\n	<li>Poured into the barrel for maturing at no more than 125 proof (62.5% alcohol by volume).</li>\r\n	<li>Bottling should be done at the standard 80 proof or more. (40% alcohol by volume)</li>\r\n</ul>\r\n\r\n<h2>Bourbon Whiskey Origin</h2>\r\n\r\n<p>As mentioned before, bourbon can be produced anywhere in the United States, but it&rsquo;s mostly produced in Kentucky. Limestone is abundantly found in Kentucky and is required for filtering bourbon. That&rsquo;s why 95% of all bourbon is produced in this American state with more than 5.3 million barrels being aged there too. This number exceeds the total population of Kentucky where also incidentally the annual bourbon festival is held each year in September. The most popular bourbon brands are based in Kentucky.</p>\r\n\r\n<h2>Bourbon Whiskey Production Process</h2>\r\n\r\n<p>The mash should contain at least 51% corn with the rest being any cereal grain. A mash bill comprising wheat instead of rye is called a wheated bourbon whiskey. The grounded grain is mixed with water and finally yeast is added to ferment the mash.</p>\r\n\r\n<p>The double distillation process is done using a pot still for modern bourbon production. The resultant spirit is called &lsquo;white dog&rsquo; and is matured in new charred oak barrels. Straight bourbon should be matured for at least 2 years although no such restrictions apply to blended bourbon. This whiskey should not be over-aged and maturity instead of the aging duration is crucial for both prime bourbons and their less expensive counterparts.</p>\r\n\r\n<h2>Typical Character and Style of Bourbon</h2>\r\n\r\n<ul>\r\n	<li>Vanilla</li>\r\n	<li>Toffee</li>\r\n	<li>Oak</li>\r\n	<li>Cinnamon</li>\r\n	<li>Dried fruit</li>\r\n</ul>\r\n\r\n<h2>Bourbon Whiskey Uses</h2>\r\n\r\n<p>Bourbon whiskey can be mixed with other drinks, taken neat or diluted with water, as per your preference.</p>\r\n\r\n<p>Some of the famous bourbon cocktails are Manhattan, the Whiskey Sour, the&nbsp;<a href=\"https://thewhiskypedia.com/whisky-cocktails/heck-it-bill-hecks-old-fashioned\">Old Fashioned</a>&nbsp;and the Mint Julep.</p>\r\n\r\n<p>A number of chefs have been known to incorporate bourbon whiskey into their recipes; it was historically used for medicinal purposes. It&rsquo;s also used in many confections like banana bourbon syrup for waffles, chocolate cake flavouring, pumpkin pie, desserts containing fruits like grilled peach sundaes served with salted bourbon caramel and brown sugar shortcake with warmed bourbon peaches and is also used as an optional ingredient in several American pies.</p>\r\n\r\n<p>Bourbon is a must try for whiskey lovers all over the world. Some of the world&rsquo;s most popular and bestselling bourbon brands are Jim Beam, Woodford Reserve, Maker&rsquo;s Mark, and more.</p>\r\n', 'http://admin.spiritpedia.xceedtech.in/images/whisky/15617200483.jpg', '', '', '', '', '', '', 1, '2019-06-28 05:37:28', 1, NULL, NULL, NULL, NULL),
(4, NULL, 'Canadian Whisky', '<p>A premium product from Canada,&nbsp;<strong>Canadian whisky</strong>&nbsp;is a blended multi-grain liquor with corn spirit as the predominant ingredient. But it&rsquo;s the use of rye in Canadian whiskies that incorporates a unique flavour into it.</p>\r\n\r\n<p>show&nbsp;less</p>\r\n\r\n<h2>Canadian Whisky History</h2>\r\n\r\n<p>Centuries ago, Canadian distillers added small quantities of rye to the whisky production, which instantly gained popularity among locals.</p>\r\n\r\n<p>The use of rye can be traced back to the time when eastern Canada soil and climate were not favourable for cereal crops. Only the rye crop could sustain the winters, hence, it was used in distilleries. Over the years, farmers found better land in the West which reduced the significance of rye.</p>\r\n\r\n<p>Whisky production in Canada dates back to the British rule in Canada. European and Canada settlers brought along with them distilling methods and technologies which were used in Canadian flour mills for distillation of excess grains. Their knowledge in distilling rye and wheat was used for the early Canadian whisky production where grains closest to decay were distilled with uncontrolled proofs. Before aging, these whiskies were sold in the market for consumption.</p>\r\n\r\n<p>However, distilling companies swerved to rum production, as Atlantic Canada&rsquo;s prevalent British sugar trade favoured rum. But this was not going to dampen Canadian whisky production, thanks to the efforts of Seagram, Molson and Walker.</p>\r\n\r\n<p>In 1801, John Molson together with his son, Thomas Molson, and his partner James Morton, established a whisky distillery in Montreal and Kingston. Napoleon wars witnessed reduced supplies of brandies and French wines to England. And Molson&rsquo;s distillery took advantage of this to become the first Canadian exporters of whisky.</p>\r\n\r\n<p>Molson&rsquo;s had new competitors when in 1837, Gooderham and Worts started whisky production apart from wheat milling in Toronto. And soon their production increased with a new distillery, which later came to be known as Distillery District. Likewise, in 1859 John Corby, alongside his gristmill operation, started whisky production. This defunct distillery and its surroundings today are known as Corbyville.</p>\r\n\r\n<p>In 1857, American J.P. Wiser migrated to Prescott to work in his uncle&rsquo;s distillery and later purchased the distillery with his success in rye whisky production. Another American Hiram walker moved to Windsor in 1858 to establish a flour mill and distillery. The year 1883 had a similar story, when Joseph Seagram progressed from being a worker at his father-in-law&rsquo;s flourmill and distillery to become its owner.</p>\r\n\r\n<p>American Civil War gave rise to export of Canadian whiskies. By then, Wiser and Walker had succeeded in aging whisky and could surpass the post-war tariffs. In 1890, Canada became the pioneer in establishing an aging law for whiskies with a minimum requirement of two years for aging. The temperance movement against alcohol consumption led to prohibition in 1916. All this and the storage charges accosted due to Aging Law for whisky added to the production woes. During prohibition, Detroit River was the hub of illegal Canadian whisky traffic to US and hence came to be known as &lsquo;the river of booze&rsquo;.</p>\r\n\r\n<p>Some distilleries produced industrial alcohol to support the nation&rsquo;s effort in World War II. But the whisky industry gained momentum after the war till 1980s as several distilleries sprung up nationwide. Later, the popularity for white spirits in American market, the surplus supply of Canadian whiskies resulted in longer aging of whiskies and their storage costs. This forced acquisitions of Canadian distilleries by international companies.</p>\r\n\r\n<h2>Canadian Whisky Characteristics</h2>\r\n\r\n<p>Corn and rye are the main characteristics of a Canadian whisky as the comparatively inexpensive corn with its high starch content is ideal for base whiskies. And though the Canadian whisky is also referred to as &ldquo;rye whisky&rdquo;, the authentic blending technique requires only a small amount of rye for the required flavouring.</p>\r\n\r\n<p><strong>Here is a list of popular Canadian whisky brands listed as per the distilleries:</strong></p>\r\n\r\n<ul>\r\n	<li>The Canadian whisky brands Alberta Premium, Windsor Canadian, Alberta Springs and Tangle Ridge, are produced by Alberta Distillers. A particular strain of yeast is used for fermenting the rye to prepare these Canadian whiskies.</li>\r\n	<li>Black Velvet Distillery produces Black Velvet, while Canadian Mist Distillery is known for their Canadian Mist. This distillery also produces Collingwood brand made from whisky aged for a year with toasted maple staves.</li>\r\n	<li>One of the top selling Canadian whiskies in the world, Crown Royal, is produced by Gimli Distillery. This distillery along with Valleyfield Distillery also produces Canadian 83 and VO.</li>\r\n	<li>Mainly using wheat in their base whiskies, Highwood Distillery delivers Centennial, Potter&rsquo;s, Ninety and Century brands.</li>\r\n	<li>Old Montreal Distillery and Kittling Ridge Distillery produce Sazerac and Fort Creek brands respectively.</li>\r\n	<li>And the 19th century Hiram Walker Distillery is known for the following Canadian whisky brands: &nbsp;Lot 40, Gooderham and Worts, Pike Creek, Corby&#39;s Royal Reserve Hiram Walker&#39;s Special Old and&nbsp;J.P. Wiser&rsquo;s brands. The distillery is also used for contract production of the popular brand, Canadian Club.</li>\r\n</ul>\r\n', 'http://admin.spiritpedia.xceedtech.in/images/whisky/15617201334.jpg', '', '', '', '', '', '', 1, '2019-06-28 05:38:53', 1, NULL, NULL, NULL, NULL),
(5, NULL, 'Indian Whisky', '<p><strong>Indian whisky</strong>&nbsp;is typically referred to as &lsquo;Indian-made foreign liquor&rsquo;, making it a separate category in the world of liquor just like Scotch whisky and American Bourbon.</p>\r\n\r\n<p>Many Indian whisky brands blend imported Scotch malts together with grain spirits or neutral spirits although the exact ratio differs from brand to brand. Sometimes, neutral spirits distilled from fermented molasses with hints of traditional malts - usually 10%-12%, are labelled as whisky in India.</p>\r\n\r\n<p>There is no strict definition of whisky in India and it is often broadly conceptualised. Unlike European whisky, their Indian counterparts are not distilled from cereals and not matured.</p>\r\n\r\n<p>This ensures the relatively cheaper prices for Indian whisky and blends, and even wider variety of choice for the Indian whisky drinker as compared to the more traditional and strict process driven whisky such as&nbsp;<a href=\"https://thewhiskypedia.com/whisky-brands-by-region/scotch-whisky\">Scotch whisky</a>,&nbsp;<a href=\"https://thewhiskypedia.com/whisky-brands-by-type/irish-whiskey\">Irish whiskey</a>&nbsp;and&nbsp;<a href=\"https://thewhiskypedia.com/whisky-brands-by-type/bourbon-whiskey\">Bourbon whiskey</a>.</p>\r\n\r\n<p>Liquor derived from molasses make up 90% of Indian whiskies although traditional whiskies are wholly blended from malt and specific grains. Regardless, Indian whisky is hugely popular among local whisky lovers and consumed in large amounts.</p>\r\n\r\n<h2>Indian Whisky History</h2>\r\n\r\n<p>Whisky was first introduced in India during the late 1820s by British colonists. The first brewery in India was established in Kasauli by an Englishman named Edward Dyer. This heralded the beginning of Indian whisky, which continues till date to be the most popular liquor in the country.</p>\r\n\r\n<p>The brewery was quickly shifted to Solan (near Shimla) due to the availability of abundant fresh spring water there. The former brewery in Kasauli was converted into India&rsquo;s first distillery, which is presently owned and operated by Mohan Meakin.</p>\r\n\r\n<p>The scarcity of grains which led to food shortages posed a major hurdle for whisky production. Grains are an important food source and hence it is generally avoided to use food grains for the production of alcoholic beverages. Factors like poverty and the ambivalent reputation of alcohol further compounded this issue. Utilising grain for alcohol production had become a controversial subject. Distillers gained access to better technology in the 1990s due to economic reforms that reduced import duties to approximately 35%.</p>\r\n\r\n<p>In 1982, Amrut Distilleries pioneered Indian whisky manufactured from grains unlike their predecessors. The Amrut brand soon gained prominence as a popular whisky brand in India.</p>\r\n\r\n<p>Amrut Distilleries sourced barley and molasses from farmers in Haryana, Rajasthan and Punjab. Subsequently in 1986, they introduced Prestige Blended Malt Whisky in Canteen Stores Department. The first batch of single malt whisky in India was ready in approximately 18 months.</p>\r\n\r\n<p>Back then, single malt whiskies were not popular in India and most Indians were oblivious to that concept. Which is why earlier, they were not bottled as single malt but were blended with spirits derived from sugarcane to produce MaQintosh Premium Whisky. The relative obscurity of alcoholic beverage industries in India made it difficult for blenders to adopt sophisticated distillation methods.</p>\r\n\r\n<p>On 24th of August, 2004, Amrut distilleries introduced Amrut &ndash; the first ever single malt whisky in India. The availability of aging malt at the distillery further facilitated the production of this premium Indian whisky.</p>\r\n\r\n<p>Formerly, Amrut aged its malt whisky for about a year before blending it. However, due to changing customer demands, the blends were produced with a lesser amount of malt whisky. The master blender at Amrut &ndash; Surinder Kumar, declared that, &ldquo;Due to climatic differences, one year of aging whisky in a barrel in India is equivalent to three years of ageing in Scotland.&rdquo;</p>\r\n\r\n<p>Another popular single malt variant in India is called Paul John Whisky. It is being manufactured since 2008 by John Distilleries, who previously blended the generic Indian Whisky.</p>\r\n\r\n<p>Another quality whisky brand in India is Paul John, manufactured by John Distilleries. It was launched in 2012 in London, and is made from malted Indian barley and imported Scottish peat.</p>\r\n\r\n<h2>Domestic Market</h2>\r\n\r\n<p>Indians consume the most amount of whisky globally by volume and the voluminous population of India is a significant contributing factor. The taxation of liquor here is complex with taxes being levied from both Central and State Governments.</p>\r\n\r\n<p>Import taxes are generally exorbitant making imported liquors unaffordable for majority of Indians. It makes Indian whiskies a viable alternative for the local populace, contributing to the popularity of locally manufactured &lsquo;Indian Whisky&rsquo;.</p>\r\n\r\n<p>The taxes imposed by state governments differ across different states which further complicates Indian whisky rates. Hence, there is no uniformity of liquor rates across the country. Whisky is the chief amongst Indian-made foreign liquors &ndash; accounting for almost 60% of this category in 2010.</p>\r\n\r\n<h2>Indian Whisky Taste</h2>\r\n\r\n<p>Indian whiskies generally have the following flavours: Honey, Oak, Cinnamon and Toast.</p>\r\n\r\n<h2>Major Manufacturers of Indian Whiskies and Spirits</h2>\r\n\r\n<ul>\r\n	<li>Seagram Manufacturing Ltd, a subsidiary of Pernod Ricard</li>\r\n	<li>United Spirits Limited (Bangalore, Karnataka)</li>\r\n	<li>Radico Khaitan (Rampur, Uttar Pradesh)</li>\r\n	<li>Modi Distillery (Modi Nagar, Uttar Pradesh)</li>\r\n	<li>Pincon Spirit Limited (Kolkata, West Bengal)</li>\r\n	<li>Khoday India Limited (Bangalore, Karnataka)</li>\r\n	<li>John Distilleries (Bangalore, Karnataka)</li>\r\n	<li>Diageo India Pvt Ltd. (Mumbai, Maharashtra)</li>\r\n	<li>Amrut Distilleries (Bangalore, Karnataka)</li>\r\n</ul>\r\n\r\n<p>These are some of the top whisky brands in India that have continued to satisfy the needs of the Indian whisky lover, as well as whisky seeking tourists from around the world.</p>\r\n', 'http://admin.spiritpedia.xceedtech.in/images/whisky/15617202695.jpg', '', '', '', '', '', '', 1, '2019-06-28 05:41:09', 1, NULL, NULL, NULL, NULL),
(6, 'mlk', 'jlk', '<p>mlk</p>\r\n', '', '', '', '', 'lk', '', '', 1, '2020-06-12 08:32:35', 0, NULL, NULL, NULL, NULL),
(7, 'Indian-Blended', 'Indian Blended', '<p>njkn</p>\r\n', '', '', '', '', '', '', '', 1, '2020-06-12 08:34:31', 0, NULL, NULL, NULL, NULL),
(8, 'Indian-Blended', 'Indian Blended', '<p>njkn</p>\r\n', 'http://localhost:8888/spirit/backend/web/types-of-whiskies/types-of-whiskies/15919510408.jpg', '', '', '', '', '', '', 1, '2020-06-12 08:37:20', 0, NULL, NULL, NULL, NULL),
(9, 'Indian-Blended', 'Indian Blended', '<p>dscac</p>\r\n', 'http://localhost:8888/spirit/backend/web/types-of-whiskies/types-of-whiskies/15919512379.jpg', '', '', '', 'lk', '', '', 1, '2020-06-12 08:40:37', 0, NULL, NULL, NULL, NULL);

--
-- Triggers `types_of_whiskies`
--
DELIMITER $$
CREATE TRIGGER `types_of_whisky_date` BEFORE INSERT ON `types_of_whiskies` FOR EACH ROW BEGIN
    IF (NEW.created_at IS NULL) THEN 
        SET NEW.created_at = now();
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(30) NOT NULL,
  `fullname` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `city` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `country` int(11) NOT NULL,
  `auth_key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `dob` date NOT NULL,
  `referal_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `home_address` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `office_address` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `added_at` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '1-active, 0-inactive, 2-block',
  `user_type` int(5) NOT NULL COMMENT '0:webuser, 1:adminuser',
  `facebook_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `google_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favorite_spirit` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favorite_mixer` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favorite_food` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favorite_smoke` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favorite_music` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_photo` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `followers` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fullname`, `username`, `mobile`, `email`, `password`, `gender`, `city`, `state`, `country`, `auth_key`, `password_reset_token`, `role_id`, `user_id`, `dob`, `referal_code`, `home_address`, `office_address`, `added_at`, `status`, `user_type`, `facebook_id`, `google_id`, `first_name`, `last_name`, `favorite_spirit`, `favorite_mixer`, `favorite_food`, `favorite_smoke`, `favorite_music`, `profile_photo`, `followers`) VALUES
(1, 'Ketaki Mudvikar', 'admin', '9823917308', 'ketaki@xceedtech.in', 'd033e22ae348aeb5660fc2140aec35850c4da997', '', 0, 0, 0, '', '', 0, 0, '0000-00-00', '', '', '', '0000-00-00 00:00:00', 1, 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_module_permission`
--

CREATE TABLE `user_module_permission` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `module_id` int(11) DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `access` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_module_permission`
--

INSERT INTO `user_module_permission` (`id`, `user_id`, `module_id`, `action_id`, `access`) VALUES
(1, 13, 1, 0, 1),
(2, 13, 3, 0, 1),
(3, 1, 1, 0, 0),
(4, 2, 4, 0, 0),
(5, 14, 3, 0, 1),
(6, 14, 4, 0, 1),
(7, 1, 2, 0, 1),
(8, 1, 46, 0, 1),
(9, 1, 3, 0, 1),
(10, 1, 4, 0, 1),
(11, 1, 6, 0, 1),
(12, 1, 7, 0, 1),
(13, 1, 9, 0, 1),
(14, 1, 10, 0, 1),
(15, 1, 12, 0, 1),
(16, 1, 13, 0, 1),
(17, 1, 15, 0, 1),
(18, 1, 16, 0, 1),
(19, 1, 18, 0, 1),
(20, 1, 19, 0, 1),
(21, 1, 21, 0, 1),
(22, 1, 22, 0, 1),
(23, 1, 24, 0, 1),
(24, 1, 25, 0, 1),
(25, 1, 27, 0, 1),
(26, 1, 28, 0, 1),
(27, 1, 31, 0, 1),
(28, 1, 32, 0, 1),
(29, 1, 34, 0, 1),
(30, 1, 35, 0, 1),
(31, 1, 36, 0, 1),
(32, 1, 37, 0, 1),
(33, 1, 38, 0, 1),
(34, 1, 39, 0, 1),
(35, 1, 40, 0, 1),
(36, 1, 42, 0, 1),
(37, 1, 43, 0, 1),
(38, 1, 44, 0, 1),
(39, 1, 30, 0, 1),
(40, 1, 5, 0, 1),
(41, 1, 33, 0, 1),
(42, 1, 8, 0, 1),
(43, 1, 45, 0, 1),
(44, 1, 11, 0, 1),
(45, 1, 41, 0, 0),
(46, 1, 14, 0, 1),
(47, 1, 17, 0, 1),
(48, 1, 20, 0, 1),
(49, 1, 23, 0, 1),
(50, 1, 26, 0, 1),
(51, 15, 31, 0, 1),
(52, 15, 45, 0, 1),
(53, 1, 47, 0, 1),
(54, 1, 48, 0, 1),
(55, 1, 50, 0, 0),
(56, 1, 51, 0, 0),
(57, 1, 52, 0, 0),
(58, 1, 53, 0, 1),
(59, 1, 54, 0, 1),
(60, 1, 55, 0, 1),
(61, 2, 15, 0, 1),
(62, 2, 16, 0, 1),
(63, 2, 18, 0, 1),
(64, 2, 19, 0, 1),
(65, 2, 21, 0, 1),
(66, 2, 22, 0, 1),
(67, 2, 24, 0, 1),
(68, 2, 25, 0, 1),
(69, 2, 27, 0, 1),
(70, 2, 28, 0, 1),
(71, 2, 31, 0, 1),
(72, 2, 32, 0, 1),
(73, 2, 34, 0, 1),
(74, 2, 35, 0, 1),
(75, 2, 36, 0, 1),
(76, 2, 37, 0, 1),
(77, 2, 38, 0, 1),
(78, 2, 39, 0, 1),
(79, 2, 40, 0, 1),
(80, 2, 42, 0, 1),
(81, 2, 43, 0, 1),
(82, 2, 44, 0, 1),
(83, 2, 47, 0, 0),
(84, 2, 48, 0, 1),
(85, 2, 53, 0, 1),
(86, 2, 54, 0, 1),
(87, 2, 55, 0, 1),
(88, 2, 30, 0, 1),
(89, 2, 33, 0, 1),
(90, 2, 45, 0, 1),
(91, 2, 14, 0, 1),
(92, 2, 17, 0, 1),
(93, 2, 20, 0, 1),
(94, 2, 23, 0, 1),
(95, 2, 26, 0, 1),
(96, 2, 56, 0, 1),
(97, 2, 57, 0, 1),
(98, 2, 58, 0, 1),
(99, 2, 59, 0, 1),
(100, 3, 34, 0, 1),
(101, 3, 35, 0, 1),
(102, 3, 36, 0, 1),
(103, 3, 37, 0, 1),
(104, 3, 38, 0, 1),
(105, 3, 39, 0, 1),
(106, 3, 40, 0, 1),
(107, 3, 42, 0, 1),
(108, 3, 43, 0, 1),
(109, 3, 44, 0, 1),
(110, 3, 47, 0, 0),
(111, 3, 33, 0, 1),
(112, 1, 56, 0, 1),
(113, 1, 57, 0, 1),
(114, 1, 58, 0, 1),
(115, 1, 59, 0, 1),
(116, 1, 60, 0, 1),
(117, 1, 61, 0, 1),
(118, 1, 62, 0, 1),
(119, 1, 63, 0, 1),
(120, 1, 64, 0, 1),
(121, 1, 65, 0, 1),
(122, 1, 66, 0, 1),
(123, 1, 67, 0, 1),
(124, 1, 68, 0, 1),
(125, 1, 69, 0, 1),
(126, 1, 70, 0, 1),
(127, 4, 31, 0, 1),
(128, 4, 32, 0, 1),
(129, 4, 34, 0, 1),
(130, 4, 35, 0, 1),
(131, 4, 36, 0, 1),
(132, 4, 37, 0, 1),
(133, 4, 38, 0, 1),
(134, 4, 39, 0, 1),
(135, 4, 40, 0, 1),
(136, 4, 42, 0, 1),
(137, 4, 43, 0, 1),
(138, 4, 44, 0, 1),
(139, 4, 47, 0, 0),
(140, 4, 56, 0, 1),
(141, 4, 57, 0, 1),
(142, 4, 58, 0, 1),
(143, 4, 59, 0, 1),
(144, 4, 60, 0, 1),
(145, 4, 61, 0, 1),
(146, 4, 62, 0, 1),
(147, 4, 63, 0, 1),
(148, 4, 64, 0, 1),
(149, 4, 65, 0, 0),
(150, 4, 66, 0, 0),
(151, 4, 67, 0, 0),
(152, 4, 68, 0, 0),
(153, 4, 30, 0, 1),
(154, 4, 33, 0, 1),
(155, 4, 45, 0, 0),
(156, 4, 71, 0, 1),
(157, 2, 71, 0, 1),
(158, 5, 3, 0, 0),
(159, 5, 4, 0, 0),
(160, 5, 6, 0, 0),
(161, 5, 7, 0, 0),
(162, 5, 9, 0, 0),
(163, 5, 10, 0, 0),
(164, 5, 12, 0, 0),
(165, 5, 13, 0, 0),
(166, 5, 15, 0, 1),
(167, 5, 16, 0, 1),
(168, 5, 18, 0, 1),
(169, 5, 19, 0, 1),
(170, 5, 21, 0, 1),
(171, 5, 22, 0, 1),
(172, 5, 24, 0, 1),
(173, 5, 25, 0, 1),
(174, 5, 27, 0, 1),
(175, 5, 28, 0, 1),
(176, 5, 31, 0, 1),
(177, 5, 32, 0, 1),
(178, 5, 34, 0, 1),
(179, 5, 35, 0, 1),
(180, 5, 36, 0, 1),
(181, 5, 37, 0, 1),
(182, 5, 38, 0, 1),
(183, 5, 39, 0, 1),
(184, 5, 40, 0, 1),
(185, 5, 42, 0, 1),
(186, 5, 43, 0, 1),
(187, 5, 44, 0, 1),
(188, 5, 46, 0, 0),
(189, 5, 48, 0, 1),
(190, 5, 53, 0, 0),
(191, 5, 54, 0, 0),
(192, 5, 55, 0, 0),
(193, 5, 56, 0, 1),
(194, 5, 57, 0, 1),
(195, 5, 58, 0, 1),
(196, 5, 59, 0, 1),
(197, 5, 60, 0, 0),
(198, 5, 61, 0, 0),
(199, 5, 62, 0, 0),
(200, 5, 63, 0, 0),
(201, 5, 64, 0, 0),
(202, 5, 65, 0, 0),
(203, 5, 66, 0, 0),
(204, 5, 67, 0, 0),
(205, 5, 68, 0, 0),
(206, 5, 69, 0, 0),
(207, 5, 2, 0, 0),
(208, 5, 30, 0, 1),
(209, 5, 71, 0, 1),
(210, 5, 5, 0, 0),
(211, 5, 33, 0, 1),
(212, 5, 8, 0, 0),
(213, 5, 45, 0, 0),
(214, 5, 11, 0, 0),
(215, 5, 14, 0, 1),
(216, 5, 17, 0, 1),
(217, 5, 20, 0, 1),
(218, 5, 23, 0, 1),
(219, 5, 26, 0, 1),
(220, 5, 41, 0, 1),
(221, 3, 73, 0, 0),
(222, 3, 74, 0, 1),
(223, 3, 72, 0, 0),
(224, 2, 41, 0, 1),
(225, 2, 60, 0, 1),
(226, 2, 61, 0, 1),
(227, 2, 62, 0, 1),
(228, 2, 63, 0, 1),
(229, 2, 64, 0, 1),
(230, 2, 73, 0, 1),
(231, 2, 74, 0, 1),
(232, 3, 41, 0, 1),
(233, 3, 64, 0, 1),
(234, 4, 27, 0, 1),
(235, 4, 28, 0, 1),
(236, 4, 41, 0, 1),
(237, 4, 48, 0, 1),
(238, 4, 73, 0, 1),
(239, 4, 74, 0, 1),
(240, 4, 72, 0, 1),
(241, 4, 26, 0, 1),
(242, 3, 27, 0, 1),
(243, 3, 28, 0, 1),
(244, 3, 48, 0, 1),
(245, 3, 26, 0, 1),
(246, 5, 73, 0, 1),
(247, 5, 74, 0, 1),
(248, 5, 72, 0, 1),
(249, 6, 64, 0, 1),
(250, 6, 74, 0, 1),
(251, 2, 12, 0, 1),
(252, 2, 13, 0, 1),
(253, 2, 51, 0, 1),
(254, 2, 52, 0, 1),
(255, 2, 69, 0, 1),
(256, 2, 76, 0, 1),
(257, 2, 77, 0, 1),
(258, 2, 79, 0, 1),
(259, 2, 80, 0, 1),
(260, 2, 82, 0, 1),
(261, 2, 83, 0, 1),
(262, 2, 85, 0, 1),
(263, 2, 86, 0, 1),
(264, 2, 87, 0, 1),
(265, 2, 89, 0, 1),
(266, 2, 90, 0, 1),
(267, 2, 91, 0, 1),
(268, 2, 93, 0, 1),
(269, 2, 94, 0, 1),
(270, 2, 95, 0, 1),
(271, 2, 96, 0, 1),
(272, 2, 50, 0, 1),
(273, 2, 92, 0, 1),
(274, 2, 11, 0, 1),
(275, 2, 75, 0, 1),
(276, 2, 84, 0, 1),
(277, 2, 88, 0, 1),
(278, 2, 78, 0, 1),
(279, 2, 81, 0, 1),
(280, 3, 15, 0, 1),
(281, 3, 16, 0, 1),
(282, 3, 18, 0, 1),
(283, 3, 19, 0, 1),
(284, 3, 21, 0, 1),
(285, 3, 22, 0, 1),
(286, 3, 24, 0, 1),
(287, 3, 25, 0, 1),
(288, 3, 31, 0, 1),
(289, 3, 32, 0, 1),
(290, 3, 56, 0, 1),
(291, 3, 57, 0, 1),
(292, 3, 58, 0, 1),
(293, 3, 59, 0, 1),
(294, 3, 60, 0, 1),
(295, 3, 61, 0, 1),
(296, 3, 62, 0, 1),
(297, 3, 63, 0, 1),
(298, 3, 76, 0, 1),
(299, 3, 77, 0, 1),
(300, 3, 79, 0, 1),
(301, 3, 80, 0, 1),
(302, 3, 82, 0, 1),
(303, 3, 83, 0, 1),
(304, 3, 85, 0, 1),
(305, 3, 86, 0, 1),
(306, 3, 87, 0, 1),
(307, 3, 89, 0, 1),
(308, 3, 90, 0, 1),
(309, 3, 91, 0, 1),
(310, 3, 93, 0, 1),
(311, 3, 94, 0, 1),
(312, 3, 95, 0, 1),
(313, 3, 96, 0, 1),
(314, 3, 30, 0, 1),
(315, 3, 92, 0, 1),
(316, 3, 45, 0, 1),
(317, 3, 75, 0, 1),
(318, 3, 84, 0, 1),
(319, 3, 14, 0, 1),
(320, 3, 88, 0, 1),
(321, 3, 17, 0, 1),
(322, 3, 78, 0, 1),
(323, 3, 81, 0, 1),
(324, 3, 20, 0, 1),
(325, 3, 23, 0, 1),
(326, 4, 3, NULL, 1),
(327, 4, 4, NULL, 1),
(328, 4, 6, NULL, 1),
(329, 4, 7, NULL, 1),
(330, 4, 9, NULL, 1),
(331, 4, 10, NULL, 1),
(332, 4, 12, NULL, 1),
(333, 4, 13, NULL, 1),
(334, 4, 15, NULL, 1),
(335, 4, 16, NULL, 1),
(336, 4, 18, NULL, 1),
(337, 4, 19, NULL, 1),
(338, 4, 21, NULL, 1),
(339, 4, 22, NULL, 1),
(340, 4, 24, NULL, 1),
(341, 4, 25, NULL, 1),
(342, 4, 46, NULL, 1),
(343, 4, 53, NULL, 1),
(344, 4, 54, NULL, 1),
(345, 4, 55, NULL, 1),
(346, 4, 69, NULL, 1),
(347, 4, 76, NULL, 1),
(348, 4, 77, NULL, 1),
(349, 4, 79, NULL, 1),
(350, 4, 80, NULL, 1),
(351, 4, 82, NULL, 1),
(352, 4, 83, NULL, 1),
(353, 4, 85, NULL, 1),
(354, 4, 86, NULL, 1),
(355, 4, 87, NULL, 1),
(356, 4, 89, NULL, 1),
(357, 4, 90, NULL, 1),
(358, 4, 91, NULL, 1),
(359, 4, 93, NULL, 1),
(360, 4, 94, NULL, 1),
(361, 4, 95, NULL, 1),
(362, 4, 96, NULL, 1),
(363, 4, 2, NULL, 1),
(364, 4, 92, NULL, 1),
(365, 4, 5, NULL, 1),
(366, 4, 8, NULL, 1),
(367, 4, 11, NULL, 1),
(368, 4, 75, NULL, 1),
(369, 4, 84, NULL, 1),
(370, 4, 14, NULL, 1),
(371, 4, 88, NULL, 1),
(372, 4, 17, NULL, 1),
(373, 4, 78, NULL, 1),
(374, 4, 81, NULL, 1),
(375, 4, 20, NULL, 1),
(376, 4, 23, NULL, 1),
(377, 5, 51, NULL, 1),
(378, 5, 52, NULL, 1),
(379, 5, 76, NULL, 1),
(380, 5, 77, NULL, 1),
(381, 5, 79, NULL, 1),
(382, 5, 80, NULL, 1),
(383, 5, 82, NULL, 1),
(384, 5, 83, NULL, 1),
(385, 5, 85, NULL, 1),
(386, 5, 86, NULL, 1),
(387, 5, 87, NULL, 1),
(388, 5, 89, NULL, 1),
(389, 5, 90, NULL, 1),
(390, 5, 91, NULL, 1),
(391, 5, 93, NULL, 1),
(392, 5, 94, NULL, 1),
(393, 5, 95, NULL, 1),
(394, 5, 96, NULL, 1),
(395, 5, 50, NULL, 1),
(396, 5, 92, NULL, 1),
(397, 5, 75, NULL, 1),
(398, 5, 84, NULL, 1),
(399, 5, 88, NULL, 1),
(400, 5, 78, NULL, 1),
(401, 5, 81, NULL, 1),
(402, 6, 15, NULL, 1),
(403, 6, 16, NULL, 1),
(404, 6, 18, NULL, 1),
(405, 6, 19, NULL, 1),
(406, 6, 21, NULL, 1),
(407, 6, 22, NULL, 1),
(408, 6, 24, NULL, 1),
(409, 6, 25, NULL, 1),
(410, 6, 27, NULL, 1),
(411, 6, 28, NULL, 1),
(412, 6, 31, NULL, 1),
(413, 6, 32, NULL, 1),
(414, 6, 34, NULL, 1),
(415, 6, 35, NULL, 1),
(416, 6, 36, NULL, 1),
(417, 6, 37, NULL, 1),
(418, 6, 38, NULL, 1),
(419, 6, 39, NULL, 1),
(420, 6, 40, NULL, 1),
(421, 6, 41, NULL, 1),
(422, 6, 42, NULL, 1),
(423, 6, 43, NULL, 1),
(424, 6, 44, NULL, 1),
(425, 6, 48, NULL, 1),
(426, 6, 56, NULL, 1),
(427, 6, 57, NULL, 1),
(428, 6, 58, NULL, 1),
(429, 6, 59, NULL, 1),
(430, 6, 73, NULL, 1),
(431, 6, 79, NULL, 1),
(432, 6, 80, NULL, 1),
(433, 6, 82, NULL, 1),
(434, 6, 83, NULL, 1),
(435, 6, 30, NULL, 1),
(436, 6, 33, NULL, 1),
(437, 6, 14, NULL, 1),
(438, 6, 72, NULL, 1),
(439, 6, 17, NULL, 1),
(440, 6, 78, NULL, 1),
(441, 6, 81, NULL, 1),
(442, 6, 20, NULL, 1),
(443, 6, 23, NULL, 1),
(444, 6, 26, NULL, 1),
(445, 7, 12, NULL, 1),
(446, 7, 13, NULL, 1),
(447, 7, 15, NULL, 1),
(448, 7, 16, NULL, 1),
(449, 7, 18, NULL, 1),
(450, 7, 19, NULL, 1),
(451, 7, 21, NULL, 1),
(452, 7, 22, NULL, 1),
(453, 7, 24, NULL, 1),
(454, 7, 25, NULL, 1),
(455, 7, 27, NULL, 1),
(456, 7, 28, NULL, 1),
(457, 7, 31, NULL, 1),
(458, 7, 32, NULL, 1),
(459, 7, 34, NULL, 1),
(460, 7, 35, NULL, 1),
(461, 7, 36, NULL, 1),
(462, 7, 37, NULL, 1),
(463, 7, 38, NULL, 1),
(464, 7, 39, NULL, 1),
(465, 7, 40, NULL, 1),
(466, 7, 41, NULL, 1),
(467, 7, 42, NULL, 1),
(468, 7, 43, NULL, 1),
(469, 7, 44, NULL, 1),
(470, 7, 48, NULL, 1),
(471, 7, 53, NULL, 1),
(472, 7, 54, NULL, 1),
(473, 7, 55, NULL, 1),
(474, 7, 56, NULL, 1),
(475, 7, 57, NULL, 1),
(476, 7, 58, NULL, 1),
(477, 7, 59, NULL, 1),
(478, 7, 60, NULL, 1),
(479, 7, 61, NULL, 1),
(480, 7, 62, NULL, 1),
(481, 7, 63, NULL, 1),
(482, 7, 64, NULL, 1),
(483, 7, 69, NULL, 1),
(484, 7, 73, NULL, 1),
(485, 7, 74, NULL, 1),
(486, 7, 79, NULL, 1),
(487, 7, 80, NULL, 1),
(488, 7, 82, NULL, 1),
(489, 7, 83, NULL, 1),
(490, 7, 89, NULL, 1),
(491, 7, 90, NULL, 1),
(492, 7, 91, NULL, 1),
(493, 7, 30, NULL, 1),
(494, 7, 71, NULL, 1),
(495, 7, 33, NULL, 1),
(496, 7, 45, NULL, 1),
(497, 7, 11, NULL, 1),
(498, 7, 14, NULL, 1),
(499, 7, 72, NULL, 1),
(500, 7, 88, NULL, 1),
(501, 7, 17, NULL, 1),
(502, 7, 78, NULL, 1),
(503, 7, 81, NULL, 1),
(504, 7, 20, NULL, 1),
(505, 7, 23, NULL, 1),
(506, 7, 26, NULL, 1),
(507, 8, 41, NULL, 1),
(508, 8, 43, NULL, 1),
(509, 8, 44, NULL, 1),
(510, 8, 45, NULL, 1),
(511, 9, 15, NULL, 1),
(512, 9, 16, NULL, 1),
(513, 9, 18, NULL, 1),
(514, 9, 19, NULL, 1),
(515, 9, 21, NULL, 1),
(516, 9, 22, NULL, 1),
(517, 9, 24, NULL, 1),
(518, 9, 25, NULL, 1),
(519, 9, 27, NULL, 1),
(520, 9, 28, NULL, 1),
(521, 9, 31, NULL, 1),
(522, 9, 32, NULL, 1),
(523, 9, 34, NULL, 1),
(524, 9, 35, NULL, 1),
(525, 9, 36, NULL, 1),
(526, 9, 37, NULL, 1),
(527, 9, 38, NULL, 1),
(528, 9, 39, NULL, 1),
(529, 9, 40, NULL, 1),
(530, 9, 41, NULL, 1),
(531, 9, 42, NULL, 1),
(532, 9, 43, NULL, 1),
(533, 9, 44, NULL, 1),
(534, 9, 48, NULL, 1),
(535, 9, 56, NULL, 1),
(536, 9, 57, NULL, 1),
(537, 9, 58, NULL, 1),
(538, 9, 59, NULL, 1),
(539, 9, 60, NULL, 1),
(540, 9, 61, NULL, 1),
(541, 9, 62, NULL, 1),
(542, 9, 63, NULL, 1),
(543, 9, 64, NULL, 1),
(544, 9, 73, NULL, 1),
(545, 9, 74, NULL, 1),
(546, 9, 76, NULL, 1),
(547, 9, 77, NULL, 1),
(548, 9, 79, NULL, 1),
(549, 9, 80, NULL, 1),
(550, 9, 82, NULL, 1),
(551, 9, 83, NULL, 1),
(552, 9, 85, NULL, 1),
(553, 9, 86, NULL, 1),
(554, 9, 87, NULL, 1),
(555, 9, 89, NULL, 1),
(556, 9, 90, NULL, 1),
(557, 9, 91, NULL, 1),
(558, 9, 93, NULL, 1),
(559, 9, 94, NULL, 1),
(560, 9, 95, NULL, 1),
(561, 9, 96, NULL, 1),
(562, 9, 30, NULL, 1),
(563, 9, 92, NULL, 1),
(564, 9, 33, NULL, 1),
(565, 9, 45, NULL, 1),
(566, 9, 75, NULL, 1),
(567, 9, 84, NULL, 1),
(568, 9, 14, NULL, 1),
(569, 9, 88, NULL, 1),
(570, 9, 17, NULL, 1),
(571, 9, 78, NULL, 1),
(572, 9, 81, NULL, 1),
(573, 9, 20, NULL, 1),
(574, 9, 23, NULL, 1),
(575, 9, 26, NULL, 1),
(576, 10, 9, NULL, 1),
(577, 10, 10, NULL, 1),
(578, 10, 12, NULL, 1),
(579, 10, 13, NULL, 1),
(580, 10, 15, NULL, 1),
(581, 10, 16, NULL, 1),
(582, 10, 18, NULL, 1),
(583, 10, 19, NULL, 1),
(584, 10, 21, NULL, 1),
(585, 10, 22, NULL, 1),
(586, 10, 24, NULL, 1),
(587, 10, 25, NULL, 1),
(588, 10, 27, NULL, 1),
(589, 10, 28, NULL, 1),
(590, 10, 31, NULL, 1),
(591, 10, 32, NULL, 1),
(592, 10, 34, NULL, 1),
(593, 10, 35, NULL, 1),
(594, 10, 36, NULL, 1),
(595, 10, 37, NULL, 1),
(596, 10, 38, NULL, 1),
(597, 10, 39, NULL, 1),
(598, 10, 40, NULL, 1),
(599, 10, 41, NULL, 1),
(600, 10, 42, NULL, 1),
(601, 10, 43, NULL, 1),
(602, 10, 44, NULL, 1),
(603, 10, 48, NULL, 1),
(604, 10, 56, NULL, 1),
(605, 10, 57, NULL, 1),
(606, 10, 58, NULL, 1),
(607, 10, 59, NULL, 1),
(608, 10, 60, NULL, 1),
(609, 10, 61, NULL, 1),
(610, 10, 62, NULL, 1),
(611, 10, 63, NULL, 1),
(612, 10, 64, NULL, 1),
(613, 10, 69, NULL, 1),
(614, 10, 73, NULL, 1),
(615, 10, 74, NULL, 1),
(616, 10, 76, NULL, 1),
(617, 10, 77, NULL, 1),
(618, 10, 79, NULL, 1),
(619, 10, 80, NULL, 1),
(620, 10, 82, NULL, 1),
(621, 10, 83, NULL, 1),
(622, 10, 85, NULL, 1),
(623, 10, 86, NULL, 1),
(624, 10, 87, NULL, 1),
(625, 10, 93, NULL, 1),
(626, 10, 94, NULL, 1),
(627, 10, 95, NULL, 1),
(628, 10, 96, NULL, 1),
(629, 10, 30, NULL, 1),
(630, 10, 92, NULL, 1),
(631, 10, 33, NULL, 1),
(632, 10, 8, NULL, 1),
(633, 10, 45, NULL, 1),
(634, 10, 11, NULL, 1),
(635, 10, 75, NULL, 1),
(636, 10, 84, NULL, 1),
(637, 10, 14, NULL, 1),
(638, 10, 72, NULL, 1),
(639, 10, 17, NULL, 1),
(640, 10, 78, NULL, 1),
(641, 10, 81, NULL, 1),
(642, 10, 20, NULL, 1),
(643, 10, 23, NULL, 1),
(644, 10, 26, NULL, 1),
(645, 11, 15, NULL, 1),
(646, 11, 16, NULL, 1),
(647, 11, 18, NULL, 1),
(648, 11, 19, NULL, 1),
(649, 11, 21, NULL, 1),
(650, 11, 22, NULL, 1),
(651, 11, 24, NULL, 1),
(652, 11, 25, NULL, 1),
(653, 11, 27, NULL, 1),
(654, 11, 28, NULL, 1),
(655, 11, 31, NULL, 1),
(656, 11, 32, NULL, 1),
(657, 11, 34, NULL, 1),
(658, 11, 35, NULL, 1),
(659, 11, 36, NULL, 1),
(660, 11, 37, NULL, 1),
(661, 11, 38, NULL, 1),
(662, 11, 39, NULL, 1),
(663, 11, 40, NULL, 1),
(664, 11, 41, NULL, 1),
(665, 11, 42, NULL, 1),
(666, 11, 43, NULL, 1),
(667, 11, 44, NULL, 1),
(668, 11, 48, NULL, 1),
(669, 11, 56, NULL, 1),
(670, 11, 57, NULL, 1),
(671, 11, 58, NULL, 1),
(672, 11, 59, NULL, 1),
(673, 11, 63, NULL, 1),
(674, 11, 64, NULL, 1),
(675, 11, 73, NULL, 1),
(676, 11, 74, NULL, 1),
(677, 11, 79, NULL, 1),
(678, 11, 80, NULL, 1),
(679, 11, 82, NULL, 1),
(680, 11, 83, NULL, 1),
(681, 11, 89, NULL, 1),
(682, 11, 90, NULL, 1),
(683, 11, 91, NULL, 1),
(684, 11, 93, NULL, 1),
(685, 11, 94, NULL, 1),
(686, 11, 95, NULL, 1),
(687, 11, 96, NULL, 1),
(688, 11, 30, NULL, 1),
(689, 11, 92, NULL, 1),
(690, 11, 33, NULL, 1),
(691, 11, 14, NULL, 1),
(692, 11, 72, NULL, 1),
(693, 11, 88, NULL, 1),
(694, 11, 17, NULL, 1),
(695, 11, 78, NULL, 1),
(696, 11, 81, NULL, 1),
(697, 11, 20, NULL, 1),
(698, 11, 23, NULL, 1),
(699, 11, 26, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_wallet`
--

CREATE TABLE `user_wallet` (
  `id` int(30) NOT NULL,
  `user_id` int(30) NOT NULL,
  `whisky_point` int(30) NOT NULL,
  `is_lock` int(1) NOT NULL COMMENT '1-lock,0-active',
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wallet_conversion`
--

CREATE TABLE `wallet_conversion` (
  `id` int(30) NOT NULL,
  `activity` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `points` int(30) NOT NULL,
  `added_date` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '1-active, 0-inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `whiskes_of_the_world`
--

CREATE TABLE `whiskes_of_the_world` (
  `id` int(11) NOT NULL,
  `whisky_type` int(11) NOT NULL,
  `distillery` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `nose` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `palate` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `finish` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1,
  `meta_title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` varchar(3000) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(3000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `whiskes_of_the_world`
--

INSERT INTO `whiskes_of_the_world` (`id`, `whisky_type`, `distillery`, `name`, `alias`, `description`, `nose`, `palate`, `finish`, `image`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `status`, `meta_title`, `meta_keyword`, `meta_description`) VALUES
(1, 1, 'American Whiskey', 'Evan Williams Cherry Whiskey', 'Evan-Williams-Cherry-Whiskey', '<p>Title 27 of the U.S. Code of Federal Regulations governs the labelling and production of American whiskey. Internationally speaking, there are various countries that recognise and designate products like Tennessee whiskey and bourbon as American whiskeys. The laws overseeing the sale of American whiskey outside United States will be determined by the country in which it&rsquo;s being sold. In some countries, the regulations can be stricter than U.S. law.</p>\r\n', 'Rich notes of cherry tarts, vanilla and ground almond', 'Smooth, buttery palate with an oily mouth-feel. Rich spices, jammy cherry, allspice, vanilla and charred oak.', 'Juicy, fruity finish. Soft spices.', 'http://admin.spiritpedia.xceedtech.in/images/whiskiesoftheworld/15710498813.jpg', '2020-06-16 12:23:00', 0, '2020-06-16 12:23:00', 1, NULL, 0, 1, '', '', ''),
(2, 2, 'Imperial Whiskey', 'Imperial Whiskey', 'Imperial-Whiskey', '<p>A minimum mash of 51% of the named grains is contained in these products, for example, wheat whiskey must use 51% of wheat in its mash and other grains in smaller quantities. Similarly, a rye whiskey must use a 51% rye mash.</p>\r\n', 'Rich notes of cherry tarts, vanilla and ground almond', 'Smooth, buttery palate with an oily mouth-feel. Rich spices, jammy cherry, allspice, vanilla and charred oak.', 'Juicy, fruity finish. Soft spices.', 'http://admin.spiritpedia.xceedtech.in/images/whiskiesoftheworld/15710482081.jpg', '2020-06-16 12:16:54', 0, '2020-06-16 12:16:54', 1, NULL, 0, 1, 'test', '', ''),
(3, 1, '', 'Seagram\'s 7 Crown With Honey Whiskey', '', '<h1>Seagram&#39;s 7 Crown With Honey Whiskey</h1>\r\n\r\n<p>Seagram&#39;s 7 Dark Honey is a liqueur from Seagrams made from their 7 Crown whiskey and, as the name suggests, dosed with honey, as well as spices and other sekrit ingredients. It&#39;s bottled at a hefty, for a liqueur, 35.5% and is a subtly flavoured addition to the Seagram&#39;s range</p>\r\n', '', '', '', 'http://admin.spiritpedia.xceedtech.in/images/whiskiesoftheworld/15801270943.png', '2020-01-27 06:41:34', 0, NULL, 0, NULL, 0, 1, '', '', ''),
(4, 1, '', 'Beam Rye', '', '<h1>Beam Rye</h1>\r\n\r\n<p>Use it in any classic or contemporary cocktail for a spicy, warm kick with a black pepper bite. Savor the spicy aroma with hints of vanilla and oak notes to round off the distinct flavor of a classic pre-Prohibition-style rye whiskey.​</p>\r\n', 'Bespoke of rye spiciness, with rich oaky smokiness', 'Spicy yet sweet, caramalised brown notes.', 'Short and sweet with prominent vanilla and maraschino.\r\n\r\n', 'http://admin.spiritpedia.xceedtech.in/images/whiskiesoftheworld/15801272214.png', '2020-01-27 06:43:41', 0, NULL, 0, NULL, 0, 1, '', '', ''),
(5, 1, '', 'Jack Daniels 1907 Whiskey', '', '<h1>Jack Daniels 1907 Whiskey</h1>\r\n\r\n<p>Jack Daniel&#39;s 1907 Tennessee Whiskey is charcoal mellowed and matured in the cooler areas of the barrel warehouse and bottled at 37% alcohol. Because the whiskey does not work its way as deeply into the barrel wood it has a light, slightly sweet taste profile. Jack Daniel&#39;s entrusted his famous distillery to his nephew, Lem Motlow in 1907. Lem soon introduced a white-labelled, lighter version of Jack&rsquo;s Whiskey. Jack Daniel&#39;s proudly continue this tradition with Jack Daniel&#39;s 1907.</p>\r\n', '', '', '', 'http://admin.spiritpedia.xceedtech.in/images/whiskiesoftheworld/15801272885.png', '2020-01-27 06:44:48', 0, NULL, 0, NULL, 0, 1, '', '', ''),
(6, 1, '', 'Gentleman Jack American Whisky', '', '<h1>Gentleman Jack American Whisky</h1>\r\n\r\n<p>Gentleman Jack is an American Tennessee Whiskey from the house of Jack Daniel&rsquo;s, one of the world&rsquo;s biggest and bestselling&nbsp;<a href=\"https://thewhiskypedia.com/whisky-brands-by-type/american-whiskey\">American whiskey brands</a>. Released in 1988, it has risen tremendously in terms of popularity in recent times.</p>\r\n\r\n<p>It is one of the few variety expressions offered by the Jack Daniel&rsquo;s brand, and uses the Jack Daniel&rsquo;s Old No. 7 as the fingerprint whiskey before undergoing a double charcoal mellowing process that offers it the distinct smoothness.</p>\r\n\r\n<h2>History</h2>\r\n\r\n<p>The history of Jack Daniel&rsquo;s begins in the county of Moore, city of Lynchburg, Tennessee in the year 1866, although some dispute exists regarding the official year of establishment with some claiming the Jack Daniel&rsquo;s brand wasn&rsquo;t established until 1875.</p>\r\n\r\n<p>Founder Jack Daniel established the brand after he spent his early years at the family farm of his mentor, Dan Call and learned the art and techniques of whisky making from Nearest Green, an enslaved man who worked at the Call farm.</p>\r\n\r\n<p>Over the years, Jack Daniel and Nearest Green continued to distill whisky at the Jack Daniel&rsquo;s Distillery as the company began using the now iconic square shaped bottles in 1897. By 1904, the Jack Daniel&rsquo;s Old No. 7 whiskey had begun to attain some popularity as it won the Gold medal at the World&rsquo;s Fair in St. Louis, Missouri.</p>\r\n\r\n<p>After Jack Daniel passed away due to a freak injury that would eventually take his life, his beloved nephew Lem Motlow, whom he had taken under his wing and had put in charge of bookkeeping at the distillery was put in charge of the business. Motlow would go on to head the business for the next 40 years, guiding the company through Prohibition and the Second World War.</p>\r\n\r\n<p>After Motlow&rsquo;s passing, he passed the reins of the company to his five children in 1947, as the company eventually underwent another change in ownership after it was acquired by the Brown-Forman Corporation in 1956.</p>\r\n\r\n<p>Jack Daniel&rsquo;s successfully transformed itself from a brand of whiskey to a cultural icon in the United States of America, and now occupies an important space in the country&rsquo;s history.&nbsp;<a href=\"https://thewhiskypedia.com/jack-daniels-whiskey/jack-daniels-old-no-7-whiskey\">The Old No. 7 is Jack Daniel&rsquo;s</a>&nbsp;oldest and most popular drink, although the company has released other special and limited edition bottlings from time to time. Master Distiller Jimmy Bedford is credited with creating the Gentleman Jack.</p>\r\n\r\n<p>Other Jack Daniel&rsquo;s whiskeys are Gentleman Jack,&nbsp;<a href=\"https://thewhiskypedia.com/jack-daniels-whiskey/jack-daniels-single-barrel-whiskey\">Single Barrel Collection</a>, and ; liqueurs such as&nbsp;<a href=\"https://thewhiskypedia.com/jack-daniels-whiskey/jack-daniels-tennessee-fire-whiskey\">Jack Daniel&rsquo;s Tennessee Fire</a>,&nbsp;<a href=\"https://thewhiskypedia.com/jack-daniels-whiskey/jack-daniels-tennessee-honey-whiskey\">Jack Daniel&rsquo;s Tennessee Honey</a>&nbsp;and limited edition releases such as Jack Daniel&rsquo;s Sinatra Select.</p>\r\n\r\n<h2>Making Process</h2>\r\n\r\n<p>The Gentleman Jack is prepared in the same way as the Jack Daniel&rsquo;s Old No. 7 beginning with a mash that is made up of 80% Corn, 12% Barley and 8% Rye. The mash is fermented, distilled and then prepared for charcoal filtration which is known as the Lincoln Country Process.</p>\r\n\r\n<p>The unique charcoal filtration process that Jack Daniel&rsquo;s whiskies undergo involved burning a stack of sugar maple pallets are sprayed with raw Jack Daniel&rsquo;s whiskey and set ablaze. The resulting charcoal pellets are then stacked up in a large drum, as the distilled spirit is then slowly dripped through the pellets over a period of 3-5 days.</p>\r\n\r\n<p>The process of preparing the Jack Daniel&rsquo;s Old No.7 stops here, although what sets the Gentleman Jack apart from other Jack Daniel&rsquo;s bottlings is that it undergoes this process twice. Once before it is filled into a barrel, and once after the maturation process is completed.</p>\r\n\r\n<p>This makes the Gentleman Jack an exceptionally smooth, and mellow. It was first released by the brand in 1988, and was one of the first other releases from the house of Jack Daniel&rsquo;s.</p>\r\n\r\n<h2>Alcohol Percentage</h2>\r\n\r\n<p>Gentleman Jack carries an alcohol strength of 40% ABV/80 US Proof, the standard limit for&nbsp;<a href=\"https://thewhiskypedia.com/whisky-brands-by-type/bourbon-whiskey\">American Bourbon whiskies</a>&nbsp;as specified by law although Jack Daniel&rsquo;s whiskies are classified as a Tennessee Whiskey.</p>\r\n\r\n<h2>Awards</h2>\r\n\r\n<p>Gentleman Jack has not won any major awards at whisky tasting competitions.</p>\r\n', 'Vanilla, caramel character. Clean & soft.', 'Balanced oak flavour with notes of caramel & vanilla.', 'Balanced oak flavour with notes of caramel & vanilla.\r\nwarm and short\r\n', 'http://admin.spiritpedia.xceedtech.in/images/whiskiesoftheworld/15801274216.jpg', '2020-01-27 06:47:01', 0, NULL, 0, NULL, 0, 1, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `whisky_pedia`
--

CREATE TABLE `whisky_pedia` (
  `id` int(10) NOT NULL,
  `product_title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(10000) COLLATE utf8_unicode_ci NOT NULL,
  `image_path` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `banner_image` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `abv` float NOT NULL,
  `added_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '1-active, 0-inactive',
  `meta_title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` varchar(3000) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(3000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `whisky_pedia`
--

INSERT INTO `whisky_pedia` (`id`, `product_title`, `alias`, `description`, `image_path`, `banner_image`, `abv`, `added_date`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `status`, `meta_title`, `meta_keyword`, `meta_description`) VALUES
(1, 'WHAT IS WHISKY?', 'WHAT-IS-WHISKY', '<p>To the world, whisky can be categorized as a&nbsp;&lsquo;distilled alcoholic beverage made from fermented grain mash&rsquo;.&nbsp;But to its drinker, it&rsquo;s so much more than that.</p>\r\n\r\n<p>When whisky lovers say, &lsquo;whisky&rsquo;, they mean the enriching conversation, the warmth that is consumed when good folks get together, the experience that puts a song in their hearts and laughter on their lips, and the warm glow of contentment in their eyes.</p>\r\n\r\n<p>Yes, that is what whisky is made of, to its drinkers:</p>\r\n\r\n<p>THE<br />\r\nWARMTH</p>\r\n\r\n<p>THE<br />\r\nCONVERSATION</p>\r\n\r\n<p>THE<br />\r\nSONG</p>\r\n\r\n<p>AND<br />\r\nTHE FIRE</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"What is Whisky\" src=\"https://thewhiskypedia.com/sites/default/files/whisky-meaning_0.jpg\" /></p>\r\n\r\n<p>Whisky originated from Scotland. It was introduced to the United States by Irish immigrants in 1700s,where it came to be spelled as whiskey. This elegant liquid gold is now produced all over the world.</p>\r\n\r\n<h2>What is Whiskey made from, and how?</h2>\r\n\r\n<p>The basic process of whiskey production follows a strictly regulated standard. Typically, a distilled spirit, whisky is made of fermented grain and matured in oak casks or barrels for a period of time. In the first step, the best quality grains (such like malted barley, rye, corn or wheat) are soaked in water, spread out on malting floors and left to germinate. The mixture, now called green malt is sent to the kiln for drying where it is provided heat at maximum 70 degrees Celsius. Next step is mashing, to extract the sugars, in which the dried malt is ground into a course flour, mixed with hot water in the mash tun (this sugary mixture is called wort) and stirred. After this comes the fermentation in which the wort is allowed to cool down to 20 degrees, and yeast is inserted inside to start the fermentation process. The last step is distillation wherein this fermented mix is distilled twice and then passed through spirit safe and allowed to mature.&nbsp;</p>\r\n', 'http://admin.spiritpedia.xceedtech.in/images/whiskypedia/Chivas regal 12.jpg', '', 0, '2019-10-17 04:19:23', 0, NULL, 0, NULL, 0, 1, '', '', ''),
(2, 'HOW IS WHISKY MADE?', 'HOW-IS-WHISKY-MADE', '<p>Whisky is not made or produced, it is carefully crafted to perfection. The principles of distillation have changed little over the last two centuries, and still follow the five step process.</p>\r\n\r\n<h2>Malting&nbsp;&nbsp;<strong>&nbsp;&nbsp;</strong></h2>\r\n\r\n<p>Barley contains starch, which converted to soluble sugars to make alcohol. For this, the barley undergoes germination, which is called &#39;malting&#39;. After this, the barley is soaked for two to three days in warm water, and then spread on the floor. When the barley starts to shoot, the germination is stopped by drying it in a kiln. Traditionally, peat is used to power the kiln. The type of peat used along with the length of drying in the peat smoke influences the flavour of the final spirit. The barley is now called &#39;malt&#39;, and this is ground down in a mill, with any husks removed.</p>\r\n\r\n<p><img alt=\"\" src=\"https://thewhiskypedia.com/sites/all/themes/bootstrap/images/whiskyImg.jpg\" /></p>\r\n\r\n<h2>Mashing&nbsp;&nbsp;&nbsp;<strong>&nbsp;</strong></h2>\r\n\r\n<p>The grounded malt, which is called &#39;grist&#39;, is added to warm water to begin the extraction of the soluble sugars. The character of water also influences the final spirit, as it may contain minerals from passing over granite, peat or other rocks. The liquid combination of malt and water is called the &#39;mash&#39;. It is then put into a large vessel called a mash tun and stirred for several hours. When the sugars in the malt dissolve and drawn off through the bottom of the mash tun, the resulting liquid is called &#39;wort&#39;. Any residue, such as husks, is called &#39;draff&#39;.</p>\r\n\r\n<p><img alt=\"\" src=\"https://thewhiskypedia.com/sites/all/themes/bootstrap/images/whiskyImg1.jpg\" /></p>\r\n\r\n<h2>Fermentation&nbsp;&nbsp;<strong>&nbsp;&nbsp;</strong></h2>\r\n\r\n<p>The wort is cooled and passed into large tanks called &#39;washbacks&#39;. These are traditionally made of wood. The yeast is added and the fermentation begins. The yeast turns the sugars into alcohol. As with the barley and water, the distiller carefully selects the strain of yeast as it also has a significant effect on the final flavour of the spirit. The fermentation normally takes around 48 hours to run its natural course, although some distilleries will let it go for longer so as to enrich the characteristics that they require. The liquid at this stage is called &#39;wash&#39; and is low in alcohol strength (between 5-10% ABV), like beer or ale.</p>\r\n', 'http://admin.spiritpedia.xceedtech.in/images/whiskypedia/How-Is-Whisky-made-_B.jpg', '', 0, '2019-10-17 06:31:16', 0, NULL, 0, NULL, 0, 1, '', '', ''),
(3, 'How to taste Whisky', 'How-to-taste-Whisky', '<h1>HOW TO TASTE WHISKY?</h1>\r\n\r\n<p>When it comes to tasting whisky, this golden elixir makes the glass feel like it has a lot of potential hidden in it. It makes the water taste better, and understands the share of the throat. The first sip communicates with you, asking how you feel about the gratifying presence of it in your mouth, and you reply to it by passing it slowly down your throat. Twelve years of nurturing, which is the least, does leave one&#39;s thirst quenched in form of this liquid sunshine. Delicious, oaky, crafted whisky brews deserve to be tasted like a true whisky connoisseur. Even if you&rsquo;re a whiskey beginner you can still learn the proper ways and etiquettes of how to taste whisky. It&rsquo;s nothing complicated but definitely elegant.</p>\r\n\r\n<h2>GLASS&nbsp;&nbsp;<strong>&nbsp;&nbsp;</strong></h2>\r\n\r\n<p>The first step in the art of tasting whisky involves choosing a glass that deserves the drink to be poured into it. Drinking whiskey is all about the experience since whiskey can grab the attention of so many of our senses. The golden spirit can be a visual treat and the first step to drink whiskey is to choose the right whiskey glassware. You&rsquo;ll want to swirl and sniff your liquor at a later stage, so make sure you pick the standard industry glassware designed for tasting &ndash; an elongated sherry glass with a short stem.</p>\r\n\r\n<p><img alt=\"\" src=\"https://thewhiskypedia.com/sites/default/files/glass.jpg\" /></p>\r\n\r\n<h2>NASAL SENSE&nbsp;&nbsp;<strong>&nbsp;</strong></h2>\r\n\r\n<p>Just any won&#39;t do justice to the next step, which involves our nasal sense that assesses the whisky. Tasting is simply to confirm what our nose has told us already. This is of utmost significance in the whole whiskey drinking experience. Your olfactory senses heighten the experience tenfold as it really brings out the aromas.</p>\r\n\r\n<p><img alt=\"\" src=\"https://thewhiskypedia.com/sites/default/files/nasal-new.jpg\" /></p>\r\n\r\n<h2>SWIRL&nbsp;&nbsp;<strong>&nbsp;&nbsp;</strong></h2>\r\n\r\n<p>To know what it communicates, it needs to be given a swirl to release the aromas while maintaining a certain distance between the nose and the glass to avoid the sting of the blend. The nasal sense and the swirling actually go hand in hand. Swirl your drink and sniff. Swirl some more and sniff again. After the essential swirling and sniffing, we finally get down to the palate where you savour the taste. Here&rsquo;s how to taste whiskey: take a sip and roll the delicious warm whiskey in your mouth. Observe the tastes and the flavors you might begin to notice. And once you feel you&rsquo;ve done the whiskey tasting properly you may now swallow the drink, relish it, and actually touch the years of blending only to swallow it to make yourself feel immortal.</p>\r\n\r\n<p><img alt=\"\" src=\"https://thewhiskypedia.com/sites/default/files/swirl.jpg\" /></p>\r\n', 'http://admin.spiritpedia.xceedtech.in/images/whiskypedia/How-to-Taste-Whisky-_B.jpg', '', 0, '2019-10-17 06:30:16', 0, NULL, 0, NULL, 0, 1, '', '', ''),
(4, 'How To Drink Whisky', 'How-To-Drink-Whisky', '<h1>HOW TO DRINK WHISKY</h1>\r\n\r\n<p>Whisky, the golden hued liquid sunlight. You call it whisky or spell it whiskey, the term just sounds melodious to the listening ear. Grabbing the attention of all our senses, it has a special place in the liquorish pleasures. A bottle of your choicest whisky lies stashed away in your closet, good enough only to be brought out on a worthy occasion. The question is, how to drink whiskey? How can you best enjoy a dram of this decadence? There are a number of ways to choose from. Since whisky is so versatile in nature, it can be drunk in so many different ways. Ways to drink whiskey is decidedly an art in itself. Whether it is downed neat, or on the rocks, there are just so many forms and variations of this rightly called &lsquo;elixir of life&rsquo;. Let&rsquo;s look at all the different ways one can learn how to drink whiskey, and then you can choose your own best way to drink whiskey.</p>\r\n\r\n<h2>Neat Whisky</h2>\r\n\r\n<p><img alt=\"Neat Whisky\" src=\"https://thewhiskypedia.com/sites/default/files/neat-whisky.jpg\" style=\"height:780px; width:1170px\" /></p>\r\n\r\n<p>Drinking whisky &lsquo;neat&rsquo; is to drink it without adding anything to it &mdash; at room temperature, poured out of the bottle into a glass. Arguably the best way to drink whiskey is to have it in the classic, neat form. Many advocates of this method consider it to be the &lsquo;purest&rsquo; way to drink whisky &mdash; undiluted and unsullied. However, this is not necessarily true for everybody. But if you love the bold taste of alcohol hitting the right spots on your taste buds, we&rsquo;d recommend go for it. For whisky amateurs, you have to try whisky neat at least once, even if it turns out to be not your thing.</p>\r\n\r\n<h2>Whisky on the Rocks</h2>\r\n\r\n<p><img alt=\"Whisky on the Rocks\" src=\"https://thewhiskypedia.com/sites/default/files/whiskyon-the-rocks_1.jpg\" style=\"height:780px; width:1170px\" /></p>\r\n\r\n<p>Rock your whisky with a few big cubes or balls of ice. It&rsquo;s not for everyone since it numbs the palate, but let&rsquo;s face it, sometimes you&rsquo;re not looking for any of the stylish ways to drink whiskey &ndash; sometimes a cold whiskey drink is all you need after a tough day. So why not have it on the rocks. So how to drink whiskey on the rocks? Well, if you do prefer your drink ice-cold, just add a few cubes of ice to the whisky. Make sure it is of high-quality water, and keep a spoon around to fish out those that start melting later.</p>\r\n\r\n<h2>Whisky with Water</h2>\r\n\r\n<p>When two elixirs are merged together, it makes this immortal drink which feels like a soft, warm blanket enveloping you on a chilly morning. When you&rsquo;re looking for ways to drink whiskey, do consider water. Adding a splash of water to your drink can really open up the flavours and aromas of the whisky and allow you to savour it fully. This is because cask strength whisky has an anesthetizing effect on your taste buds, numbing your tongue and keeping you from being able to taste your drink after a while. This is why it is often a better way than consuming it neat. About a capful of distilled water would be enough. Should you still feel a buzz in your tongue, add another capful.</p>\r\n\r\n<h2>Whisky in a Cocktail</h2>\r\n\r\n<p><img alt=\"How to Drink Whisky\" src=\"https://thewhiskypedia.com/sites/default/files/whisky-with-water.jpg\" style=\"height:780px; width:1170px\" /></p>\r\n\r\n<p>Although this is not recommended with an extremely high-end brand of whisky, you can always enjoy your whisky in a cocktail or mixed drink. They can be as innovative as they are delicious. Save your finest to be imbibed alone, and use your other whiskies to whip up some tasty tipples. You can learn some cool cocktail recipes. Even in a cocktail, it holds its own place and yet blends with exciting flavors.</p>\r\n\r\n<p>Try these ways to drink whisky, and you&rsquo;re good to go!</p>\r\n', 'http://admin.spiritpedia.xceedtech.in/images/whiskypedia/How-To-Drink-Whisky_B.jpg', '', 0, '2019-10-18 05:42:00', 0, NULL, 0, NULL, 0, 1, '', '', ''),
(5, 'GLOSSARY OF WHISKY TERMS', 'GLOSSARY-OF-WHISKY-TERMS', '<ul>\r\n	<li>Angel&rsquo;s Share&nbsp;- Whisky lost to evaporation, which is about 2% per annum.</li>\r\n	<li>Amylase&nbsp;- Enzyme used to convert starch into maltose.</li>\r\n	<li>Ageing&nbsp;- The maturation of whisky in an oak barrel or cask.</li>\r\n	<li>Aroma&nbsp;&ndash; The rich fragrance of the whisky after years of blending.</li>\r\n</ul>\r\n', 'http://admin.spiritpedia.xceedtech.in/images/whiskypedia/Whisky-Glossary_B.jpg', '', 0, '2019-10-18 05:28:04', 0, NULL, 0, NULL, 0, 1, '', '', ''),
(6, 'TYPES OF WHISKY GLASSES', 'TYPES-OF-WHISKY-GLASSES', '<h2>Whisky Glasses - Choosing the Right Sampling Tool</h2>\r\n\r\n<p>Whiskys from around the world always managed to capture the imagination and the senses of connoisseurs, aficionados, and average drinkers alike. The sheer diversity in terms of geography, blend character, and distillation technique provides the unique opportunity to explore an entire spectrum of flavours, and in some special cases, time &ndash; a good whisky having the ability to speak volumes about its origin and history through just a sip.</p>\r\n\r\n<p>Anybody who claims to enjoy their glass of whisky would wholeheartedly agree that there is an art underlying the entire process of pouring, mixing, and savoring the drink. Water, edible accompaniments, and breathing time aside, whisky whisky glasses are a critical part of the entire experience.&nbsp;</p>\r\n\r\n<h2>What&rsquo;s in a Shapes of Whisky Glasses?</h2>\r\n\r\n<p>Why? Because a good whisky deserves good glassware. It&rsquo;s not just about blunt aesthetics either. Whisky glasses in all their variations are designed to enhance and enable the tasting experience. The basic idea is to have a glass which has enough room to let the spirit breathe whilst focusing the entire bouquet of aromas around your nose when you pick up the glass for a sip. However, your choice of glassware for sampling whisky of any sorts is equally dependent on the kind of tasting experience you are looking for.</p>\r\n\r\n<p>For the sake of brevity, we&rsquo;ll go by the numbers, and start at the most common choice for a drinking vessel, slowly graduating to more complex, purpose designed glassware meant for tasting.</p>\r\n', 'http://admin.spiritpedia.xceedtech.in/images/whiskypedia/Whisky-Glasses_B.jpg', '', 0, '2019-10-18 05:39:09', 0, NULL, 0, NULL, 0, 1, '', '', ''),
(7, 'Health Benefits of Whisky', 'Health-Benefits-of-Whisky', '<h1>HEALTH BENEFITS OF WHISKY</h1>\r\n\r\n<p>An Irish man once said, &ldquo;What whisky will not cure, there is no cure for.&rdquo; Of course, the world has since found out that he was quite right. Over the years, many researchers have found out that this liquid sunshine contains a plethora of health benefits. But, what has today become a drink for the affluent class was once upon a time enjoyed by the cowboys and construction workers, and was even used as an antiseptic on battlefields. What&rsquo;s even more surprising is that in the 1920s, whiskey was considered as medicine and sold in pharmacies as a tonic. Intriguing, right?</p>\r\n\r\n<p>So, whether you like to sip on a tiny glass of<a href=\"https://thewhiskypedia.com/jim-beam-whiskey/jim-beam-8-star-bourbon\">&nbsp;Jim Beam Whiskey</a>, or prefer to enjoy your&nbsp;<a href=\"https://thewhiskypedia.com/jack-daniels-whiskey/jack-daniels-1907-whiskey\">Jack Daniel&rsquo;s Whiskey</a>&nbsp;on the rocks, know that every drop of this fine drink is helping your body in some way or another.</p>\r\n\r\n<p>Now, without further ado, let&rsquo;s learn all about the benefits of whisky for health:</p>\r\n\r\n<h2>Weight Loss</h2>\r\n\r\n<p>One of the most popular benefits of whisky is weight loss. A study conducted by American Journal of Critical Nutrition has found that alcohol can help in preventing obesity, assuming that you are drinking mindfully. Now, this is possible because whiskey contains carbohydrates, calories and a small amount of sugar that eventually breaks down into energy. So, the next time you head out, order a glass and enjoy the taste as well as the health benefits of whisky.</p>\r\n\r\n<p><img alt=\"Benefits of Whisky\" src=\"https://thewhiskypedia.com/sites/default/files/weight-loss.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Reduces Stress</h2>\r\n\r\n<p>One of the more important benefits of whisky is that it helps in reducing your stress. Drinking a small glass of whisky can help in calming your nerves and reducing anxiety. So, if you&rsquo;ve had a long and tiring day, relax and pour yourself a double to let your body soak in all the health benefits of drinking whisky.</p>\r\n\r\n<h2>Decreases the Risk of Ischemic Stroke</h2>\r\n\r\n<p>The third and the biggest advantage of whisky is that it prevents ischemic attack, which is caused when a blood clot occurs. Whisky allows free circulation of blood in the body, thereby helping the heart pump blood without any problem. Hence, the next time you need to give someone a reason for your choice of drink, do not forget to mention this solid advantage of whisky.</p>\r\n\r\n<h2>Controls Diabetes</h2>\r\n\r\n<p>Diabetes has become a growing concern in the world today. It has affected both young and old alike and is only growing with time. So, guess what can help prevent this pandemic from spreading? Whisky, of course! Whisky contains ellagic acid that can help in controlling the amount of glucose released from the liver. This keeps your sugar level in check and gives you another reason to keep a bottle handy. There really is no end to the health benefits of whiskey!</p>\r\n\r\n<h2>Healthy Cholesterol</h2>\r\n\r\n<p>When we are talking about the health benefits of whisky, we simply cannot forget that this delightful elixir helps increase the healthy cholesterol in our bodies. Definitely one of the most important advantages of whisky in our books!</p>\r\n\r\n<h2>Heart Health</h2>\r\n\r\n<p>Now, with your body pumping all that good cholesterol around, guess who&rsquo;s the healthiest of them all? Your heart! Besides, the antioxidants present in whiskey prevents coronary heart diseases. This makes it another one of the most important health benefits of whisky.</p>\r\n\r\n<h2>Reduces the Risk of Cancer</h2>\r\n\r\n<p>One of the prime benefits of whisky is that it inhibits the growth of cancerous cells in our body. The ellagic acid present in whisky is known to combat free radical cells and even prevents them from re-growing, making it one the most powerful whisky advantages you&rsquo;ll ever come across.</p>\r\n\r\n<h2>Prevents Dementia</h2>\r\n\r\n<p>While you may come across many benefits of whisky, there are a few that stand out as much as this one. Unlike other liquors, whisky is known to reduce your chances of developing Alzheimer&rsquo;s and any other memory related problems. You sure didn&rsquo;t know this one, did you?</p>\r\n\r\n<h2>Increases Life Span</h2>\r\n\r\n<p>Want to live longer? Take a shot. We are sure you would have never heard that sentence before, but longevity is another one of the advantages of drinking whisky. Wondering how? Well, the answer is antioxidants.</p>\r\n\r\n<h2>Boosts Immunity</h2>\r\n\r\n<p>And last but not the least, another popular advantage of whisky is that it boosts your immune system. Now, wait, that doesn&rsquo;t mean you go ahead and replace your morning coffee for a shot of whiskey, but maybe, you can include it in a weekend brunch. So go ahead and proudly add this to your list of advantages of drinking whisky.</p>\r\n\r\n<p>We have all heard that alcohol is a bad influence and a terrible companion, but these advantages of whisky clearly prove that it&rsquo;s not alcohol, but your decisions that make all the difference. If you drink responsibly, you can enjoy these amazing whisky advantages, and give your health a boost.</p>\r\n\r\n<p>Also, remember that while these benefits of drinking whisky sound brilliant, it is very important to not overdose or binge drink any type of alcohol. This is why consume a small or moderate amount and make sure to never give in to addiction to lead a happy and healthy life.</p>\r\n', 'http://admin.spiritpedia.xceedtech.in/images/whiskypedia/Whisky-Or-Whiskey-_B.jpg', '', 0, '2019-10-18 05:40:44', 0, NULL, 0, NULL, 0, 1, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `whisky_tube`
--

CREATE TABLE `whisky_tube` (
  `id` int(30) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `video_path` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail_path` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `added_date` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '1-active, 0-inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `address_book`
--
ALTER TABLE `address_book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_master`
--
ALTER TABLE `blog_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookmark_master`
--
ALTER TABLE `bookmark_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart_master`
--
ALTER TABLE `cart_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD KEY `city` (`city`),
  ADD KEY `stateID` (`stateID`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `distillery_master`
--
ALTER TABLE `distillery_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `distillery_type`
--
ALTER TABLE `distillery_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_images`
--
ALTER TABLE `event_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_master`
--
ALTER TABLE `event_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq_master`
--
ALTER TABLE `faq_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favourites`
--
ALTER TABLE `favourites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback_master`
--
ALTER TABLE `feedback_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feed_comments`
--
ALTER TABLE `feed_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feed_likes`
--
ALTER TABLE `feed_likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feed_master`
--
ALTER TABLE `feed_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `follower_master`
--
ALTER TABLE `follower_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institute_master`
--
ALTER TABLE `institute_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `level_master`
--
ALTER TABLE `level_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_master`
--
ALTER TABLE `menu_master`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `mobile_app_dashboard`
--
ALTER TABLE `mobile_app_dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules_list`
--
ALTER TABLE `modules_list`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `news_master`
--
ALTER TABLE `news_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `option_master`
--
ALTER TABLE `option_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `option_values`
--
ALTER TABLE `option_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_master`
--
ALTER TABLE `order_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_master`
--
ALTER TABLE `product_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_pedia`
--
ALTER TABLE `product_pedia`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_type`
--
ALTER TABLE `product_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipes`
--
ALTER TABLE `recipes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referal_master`
--
ALTER TABLE `referal_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_module_permission`
--
ALTER TABLE `role_module_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spam_master`
--
ALTER TABLE `spam_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`),
  ADD KEY `state` (`state`),
  ADD KEY `countryID` (`countryID`);

--
-- Indexes for table `thread_master`
--
ALTER TABLE `thread_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types_of_whiskies`
--
ALTER TABLE `types_of_whiskies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_module_permission`
--
ALTER TABLE `user_module_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_wallet`
--
ALTER TABLE `user_wallet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wallet_conversion`
--
ALTER TABLE `wallet_conversion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `whiskes_of_the_world`
--
ALTER TABLE `whiskes_of_the_world`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `whisky_pedia`
--
ALTER TABLE `whisky_pedia`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `whisky_tube`
--
ALTER TABLE `whisky_tube`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `address_book`
--
ALTER TABLE `address_book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `blog_master`
--
ALTER TABLE `blog_master`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bookmark_master`
--
ALTER TABLE `bookmark_master`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cart_master`
--
ALTER TABLE `cart_master`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=605;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `distillery_master`
--
ALTER TABLE `distillery_master`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `distillery_type`
--
ALTER TABLE `distillery_type`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event_images`
--
ALTER TABLE `event_images`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event_master`
--
ALTER TABLE `event_master`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faq_master`
--
ALTER TABLE `faq_master`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `favourites`
--
ALTER TABLE `favourites`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `feedback_master`
--
ALTER TABLE `feedback_master`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feed_comments`
--
ALTER TABLE `feed_comments`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `feed_likes`
--
ALTER TABLE `feed_likes`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feed_master`
--
ALTER TABLE `feed_master`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `follower_master`
--
ALTER TABLE `follower_master`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `institute_master`
--
ALTER TABLE `institute_master`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `level_master`
--
ALTER TABLE `level_master`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu_master`
--
ALTER TABLE `menu_master`
  MODIFY `menu_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `mobile_app_dashboard`
--
ALTER TABLE `mobile_app_dashboard`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `modules_list`
--
ALTER TABLE `modules_list`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `news_master`
--
ALTER TABLE `news_master`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `option_master`
--
ALTER TABLE `option_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `option_values`
--
ALTER TABLE `option_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order_master`
--
ALTER TABLE `order_master`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_master`
--
ALTER TABLE `product_master`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `product_pedia`
--
ALTER TABLE `product_pedia`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_type`
--
ALTER TABLE `product_type`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `recipes`
--
ALTER TABLE `recipes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `referal_master`
--
ALTER TABLE `referal_master`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `role_module_permission`
--
ALTER TABLE `role_module_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=365;

--
-- AUTO_INCREMENT for table `spam_master`
--
ALTER TABLE `spam_master`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `thread_master`
--
ALTER TABLE `thread_master`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `types_of_whiskies`
--
ALTER TABLE `types_of_whiskies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_module_permission`
--
ALTER TABLE `user_module_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=700;

--
-- AUTO_INCREMENT for table `user_wallet`
--
ALTER TABLE `user_wallet`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wallet_conversion`
--
ALTER TABLE `wallet_conversion`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `whiskes_of_the_world`
--
ALTER TABLE `whiskes_of_the_world`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `whisky_pedia`
--
ALTER TABLE `whisky_pedia`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `whisky_tube`
--
ALTER TABLE `whisky_tube`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `state`
--
ALTER TABLE `state`
  ADD CONSTRAINT `country` FOREIGN KEY (`countryID`) REFERENCES `country` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
