/*
Bootstable
 @description  Javascript library to make HMTL tables editable, using Bootstrap
 @version 1.1
 @autor Tito Hinostroza
*/
  "use strict";
  //Global variables
  var params = null;  		//Parameters
  var colsEdi =null;
  var newColHtml = '<div class="btn-group pull-right">'+
'<button id="bEdit" type="button" class="btn btn-sm btn-black" data-toggle="tooltip" data-original-title="Edit" onclick="rowEdit(this);">' +
'<i class="fa fa-pencil"></i>'+
'</button>'+
'<button id="bElim" type="button" class="btn btn-sm btn-black"  data-toggle="tooltip" data-original-title="Remove" onclick="rowElim(this);">' +
'<i class="fa fa-trash" aria-hidden="true"></i>'+
'</button>'+
'<button id="bAcep" type="button" class="btn btn-sm btn-black" data-toggle="tooltip" data-original-title="Save" style="display:none;" onclick="rowAcep(this);">' + 
'<i class="fa fa-check"></i>'+
'</button>'+
'<button id="bCanc" type="button" class="btn btn-sm btn-black" data-toggle="tooltip" data-original-title="Cancel" style="display:none;"  onclick="rowCancel(this);">' + 
'<i class="fa fa-times" aria-hidden="true"></i>'+
'</button>'+
    '</div>';
     var saveColHtml = '<div class="btn-group pull-right">'+
'<button id="bEdit" type="button" class="btn btn-sm btn-black" data-toggle="tooltip" data-original-title="Edit" style="display:none;" onclick="rowEdit(this);">' +
'<i class="fa fa-pencil"></i>'+
'</button>'+
'<button id="bElim" type="button" class="btn btn-sm btn-black" data-toggle="tooltip" data-original-title="Remove" style="display:none;" onclick="rowElim(this);">' +
'<i class="fa fa-trash" aria-hidden="true"></i>'+
'</button>'+
'<button id="bAcep" type="button" class="btn btn-sm btn-black" data-toggle="tooltip" data-original-title="Save"  onclick="rowAcep(this);">' + 
'<i class="fa fa-check"></i>'+
'</button>'+
'<button id="bCanc" type="button" class="btn btn-sm btn-black" data-toggle="tooltip" data-original-title="Cancel" onclick="rowCancel(this);">' + 
'<i class="fa fa-times" aria-hidden="true"></i>'+
'</button>'+
    '</div>';
    //alert(newColHtml)
var colEdicHtml = '<td name="buttons" class="btns">'+newColHtml+'</td>'; 
var colSaveHtml = '<td name="buttons" class="btns">'+saveColHtml+'</td>';
    
  $.fn.SetEditable = function (options) {
    var defaults = {
        columnsEd: null,         //Index to editable columns. If null all td editables. Ex.: "1,2,3,4,5"
        $addButton: null,        //Jquery object of "Add" button
        onEdit: function() {
            // var $td = $("tr[id='editing'] td");
            // var $row =  $("tr[id='editing']");  
            // var $cols = $row.find('td'); 
            var myObj =  {};
              var sid="";
            $('tr[id="edited"] td').each(function() {
                var ids=this.id;
                var fields=ids.split('-');
                var field_name = fields[0];
                sid = fields[1];// $("#" + ids)
                var values = $(this).html();
                var obj = {};

               if($(this).attr('id')!="" && typeof($(this).attr('id'))!="undefined")
               {
                 myObj[field_name] =values;
                   // myObj.push({[field_name]: values});
                    localStorage.setItem("fid", sid);

               }
                

                //prepare array and push values into new array  
            });
            //myObj.push({"id": localStorage.getItem("fid")});
            console.log(JSON.stringify(myObj));

            $.ajax({
                type:'POST',
                data:{'form_data': myObj, 'id':localStorage.getItem("fid") },
                url:"https://vishwaswealthadvisory.com/admin/stock/update-data",
                success:function(data)
                {
                    if(data=="success")
                    {
                        location.reload();
                    }
                    else if(data=='error'){
                        alert("Oops! Something went wrong. Please try again");
                    }
                    else
                    {
                        alert("No data found");
                    }
                }
            });
            
           
        // code to save data
        },   //Called after edition
		onBeforeDelete: function() {}, //Called before deletion
        onDelete: function(but) {
              var $row = $(but).parents('tr');  //accede a la fila
              var $cols = $row.find('td:eq(0)'); 
              var ids=$cols.attr('id');
              var fields=ids.split('-');
              var sid = fields[1];
              $.ajax({
                type:'POST',
                data:{ 'id':sid },
                url:"https://vishwaswealthadvisory.com/admin/stock/remove",
                success:function(data)
                {
                    location.reload(); 
                }
            });
        }, //Called after deletion
        onAdd: function() {}     //Called when added a new row
    };
    params = $.extend(defaults, options);
    
    this.find('thead tr').append('<th name="buttons"></th>');  //encabezado vacío    
    this.find('tbody tr').append(colEdicHtml);
	var $tabedi = this;   //Read reference to the current table, to resolve "this" here.
    //Process "addButton" parameter
    if (params.$addButton != null) {

        //Se proporcionó parámetro
        params.$addButton.click(function() {
            //alert($tabedi.attr("id"));
            rowAddNew($tabedi.attr("id"));
        });
    }
    //Process "columnsEd" parameter
    if (params.columnsEd != null) {
        //Extract felds
        colsEdi = params.columnsEd.split(',');
    }
  };
function IterarCamposEdit($cols, tarea) {
//Itera por los campos editables de una fila
    var n = 0;
    $cols.each(function() {
        n++;
        if ($(this).attr('name')=='buttons') return;  //excluye columna de botones
        if (!EsEditable(n-1)) return;   //noe s campo editable
        tarea($(this));
    });
    
    function EsEditable(idx) {
    //Indica si la columna pasada está configurada para ser editable
        if (colsEdi==null) {  //no se definió
            return true;  //todas son editable
        } else {  //hay filtro de campos
//alert('verificando: ' + idx);
            for (var i = 0; i < colsEdi.length; i++) {
              if (idx == colsEdi[i]) return true;
            }
            return false;  //no se encontró
        }
    }
}
function FijModoNormal(but) {
    $(but).parent().find('#bAcep').hide();
    $(but).parent().find('#bCanc').hide();
    $(but).parent().find('#bEdit').show();
    $(but).parent().find('#bElim').show();
    var $row = $(but).parents('tr');  //accede a la fila
    $row.attr('id', 'edited');  //quita marca
}
function FijModoEdit(but) {

    $(but).parent().find('#bAcep').show();
    $(but).parent().find('#bCanc').show();
    $(but).parent().find('#bEdit').hide();
    $(but).parent().find('#bElim').hide();
    var $row = $(but).parents('tr');  //accede a la fila
    $row.attr('id', 'editing');  //indica que está en edición
}
function ModoEdicion($row) {
    if ($row.attr('id')=='editing') {
        return true;
    } else {
        return false;
    }
}
function rowAcep(but) {
//Acepta los cambios de la edición
    
    
    var $row = $(but).parents('tr');  //accede a la fila
    var $cols = $row.find('td');  //lee campos

    if (!ModoEdicion($row)) return;  //Ya está en edición
    //Está en edición. Hay que finalizar la edición
    IterarCamposEdit($cols, function($td) {  //itera por la columnas
    var cont = $td.find('input:text').val(); //lee contenido del input
   // alert(cont);
      $td.html(cont);  //fija contenido y elimina controles
//params.onEdit($td.html(cont));
    });
    FijModoNormal(but);
    params.onEdit($row);
}
function rowCancel(but) {
//Rechaza los cambios de la edición
    var $row = $(but).parents('tr');  //accede a la fila
    var $cols = $row.find('td');  //lee campos
    if (!ModoEdicion($row)) return;  //Ya está en edición
    //Está en edición. Hay que finalizar la edición
    IterarCamposEdit($cols, function($td) {  //itera por la columnas
        var cont = $td.find('div').html(); //lee contenido del div
        $td.html(cont);  //fija contenido y elimina controles
    });
    FijModoNormal(but);
}
function rowEdit(but) {  
    var $td = $("tr[id='editing'] td");
    rowAcep($td)
    var $row = $(but).parents('tr');  
    var $cols = $row.find('td'); 
    if (ModoEdicion($row)) return;  //Ya está en edición
    //Pone en modo de edición
    IterarCamposEdit($cols, function($td) {  //itera por la columnas
       // alert($cols.length);
        var cont = $td.html(); //lee contenido
        var ids=$td.attr("id");
        //alert($cols.attr("id"))
        var div = '<div style="display: none;">' + cont + '</div>';  //guarda contenido
        var input = '<input class="form-control input-sm '+$cols.length+'" id="'+ids+'" value="' + cont + '">';
        $td.html(div + input);  //fija contenido
    });
    FijModoEdit(but);
}
function rowElim(but) {  //Elimina la fila actual
    var $row = $(but).parents('tr');  //accede a la fila
    params.onBeforeDelete($row);
    $row.remove();
    params.onDelete(but);
}
function rowAddNew(tabId) {  //Agrega fila a la tabla indicada.
var $tab_en_edic = $("#" + tabId);  //Table to edit
    var $filas = $tab_en_edic.find('tbody tr');
    if ($filas.length==0) {
        //No hay filas de datos. Hay que crearlas completas
        var $row = $tab_en_edic.find('thead tr');  //encabezado
        var $cols = $row.find('th');  //lee campos
        //construye html
        var htmlDat = '';
        $cols.each(function() {
            if ($(this).attr('name')=='buttons') {
                //Es columna de botones
                htmlDat = htmlDat + colEdicHtml;  //agrega botones
            } else {
                htmlDat = htmlDat + '<td></td>';
            }
        });
        $tab_en_edic.find('tbody').append('<tr>'+htmlDat+'</tr>');
    } else {
        //Hay otras filas, podemos clonar la última fila, para copiar los botones
        var $ultFila = $tab_en_edic.find('tr:last');
        $ultFila.clone().appendTo($ultFila.parent()); 
        $tab_en_edic.find('tr:last').attr('id','editing'); 
        $ultFila = $tab_en_edic.find('tr:last');
        var $cols = $ultFila.find('td');  //lee campos
        
        $cols.each(function() {
            if ($(this).attr('name')=='buttons') {
                //Es columna de botones
            } else {
                var div = '<div style="display: none;"></div>';  //guarda contenido
                var input = '<input class="form-control input-sm"  value="">';

                $(this).html(div + input);  //limpia contenido
            }
        });
         $ultFila.find('td:last').html(saveColHtml);

    }
	params.onAdd();
}
function TableToCSV(tabId, separator) {  //Convierte tabla a CSV
    var datFil = '';
    var tmp = '';
	var $tab_en_edic = $("#" + tabId);  //Table source
    $tab_en_edic.find('tbody tr').each(function() {
        //Termina la edición si es que existe
        if (ModoEdicion($(this))) {
            $(this).find('#bAcep').click();  //acepta edición
        }
        var $cols = $(this).find('td');  //lee campos
        datFil = '';
        $cols.each(function() {
            if ($(this).attr('name')=='buttons') {
                //Es columna de botones
            } else {
                datFil = datFil + $(this).html() + separator;
            }
        });
        if (datFil!='') {
            datFil = datFil.substr(0, datFil.length-separator.length); 
        }
        tmp = tmp + datFil + '\n';
    });
    return tmp;
}
