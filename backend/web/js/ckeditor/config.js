/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
//config.extraPlugins = 'lineheight';
config.line_height="1em;1.1em;1.2em;1.3em;1.5em;1.8em;2em;2.5em;3em" ;
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.extraAllowedContent = '*(*);*{*}';

};
