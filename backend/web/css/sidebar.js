"use strict";
! function (t) {
	var n = t(window),

		o = t("body");

	t.createOverlay = function () {
	(o.addClass("no-scroll").append('<div class="overlay"></div>'), t(".overlay").addClass("show"))
	}, t.removeOverlay = function () {
		o.removeClass("no-scroll"), t(".overlay").remove()
	}, t("[data-backround-image]").each(function (a) {
		t(this).css("background", "url(" + t(this).data("backround-image") + ")")
	}),   t(document).on("click", "[data-sidebar-open]", function () {
		var a = t(this).data("sidebar-open");
		return t(".sidebar").removeClass("show"), t(a).addClass("show"), t.createOverlay(), !1
	}), t(document).on("click", ".overlay", function () {
		t(".sidebar").removeClass("show"),t(".sidebar").removeClass("control-sidebar-open"), t.removeOverlay()
	})

}(jQuery);