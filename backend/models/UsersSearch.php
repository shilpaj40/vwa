<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Users;

/**
 * UsersSearch represents the model behind the search form of `app\models\User`.
 */
class UsersSearch extends Users
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','user_type'], 'integer'],
            [['first_name', 'mobile', 'email','birth_date', 'last_name', 'username', 'fullname', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $status, $user_type)
    {
        $query = Users::find()->alias('u');

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'u.id' => $this->id,
            'u.status' => $this->status,
            'u.fullname' => $this->fullname,
        ]);
        $query->andFilterWhere(['like', 'concat(u.first_name, " " ,u.last_name) ', $this->fullname]);
        if($user_type!=0){
        $query->andFilterWhere(['like', 'u.email', $this->email])
            ->andFilterWhere(['=','u.user_type', 2])
            ->andFilterWhere(['like', 'u.birth_date', $this->birth_date])
            ->andFilterWhere(['like', 'u.username', $this->username]);
        }
        else{
            $query->andFilterWhere(['like', 'u.email', $this->email])
            ->andFilterWhere(['!=','u.user_type', 2])
            ->andFilterWhere(['like', 'u.birth_date', $this->birth_date])
            ->andFilterWhere(['like', 'u.username', $this->username]);
        }
        if($status!="-1")
        {
           // echo "DCW".$status;
            $query->andFilterWhere(['=', 'u.status', $status]);
        }
      
        
        return $dataProvider;
    }
    public function searchUser($params, $status)
    {
        $query = Users::find()->alias('u');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
       
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
           // 'status' => $this->status,
        ]);
        $query->andFilterWhere(['like', 'concat(u.first_name, " " , u.last_name) ', $this->fullname])
        ->andFilterWhere(['=','u.user_type', 2])
            ->andFilterWhere(['like', 'u.mobile', $this->mobile])
            ->andFilterWhere(['like', 'u.email', $this->email])
            ->andFilterWhere(['like', 'u.username', $this->username])
            ->andFilterWhere(['=', 'u.status', $status]);
        return $dataProvider;
    }

    public function searchCompany($params, $company_id)
    {
        $query = Users::find();
        $query->joinWith(['company']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes['company'] = [
        'asc' => ['company.company_name' => SORT_ASC],
        'desc' => ['company.company_name' => SORT_DESC],
        ];
        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
           // 'status' => $this->status,
        ]);
        $query->andFilterWhere(['like', 'concat(first_name, " " , last_name) ', $this->fullname])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['=', 'company_id', $company_id]);
        return $dataProvider;
    }



 public function search_subscription_end($params)
    {
        $query = Users::find()->where('subscription_end_date<CURRENT_TIMESTAMP and status=1');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'first_name' => $this->first_name,
             'last_name' => $this->last_name,
            'mobile' => $this->mobile,
            'username' => $this->username,
            // 'order_status' => $this->order_status,
            
            'status' => $this->status,
            'subscription_start_date'=>$this->subscription_start_date,
            'subscription_end_date'=>$this->subscription_end_date
        ]);

        $query->andFilterWhere(['like', 'concat(first_name, " " , last_name) ', $this->fullname])
       // ->andFilterWhere(['=','u.user_type', 2])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'username', $this->username])
           // ->andFilterWhere(['>=', 'subscription_start_date', $this->subscription_end_date])
            ->andFilterWhere(['=', 'status', 1]);
        return $dataProvider;
    }


}
