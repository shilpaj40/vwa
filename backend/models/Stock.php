<?php

namespace backend\models;
use  v0lume\yii2\metaTags\MetaTagBehavior;

use Yii;

/**
 * This is the model class for table "stock".
 *
 * @property int $id
 * @property int $week_no
 * @property string $date
 * @property string $stock_name
 * @property string $share_price
 * @property int $no_of_shares
 * @property string $actual_event
 * @property float $open
 * @property float $high
 * @property float $low
 * @property float $close
 * @property string $suggestion
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class Stock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stock';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'stock_name', 'actual_event', 'suggestion','action','symbol','created_by'], 'required'],
            [['week_no', 'no_of_shares', 'status', 'is_closed','updatesapproval'], 'integer'],
            [['date', 'created_at', 'updated_at', 'deleted_at','action','symbol','created_by','updatesapproval', 'share_price', 'no_of_shares'], 'safe'],
            [['open', 'high', 'low', 'close'], 'number'],
            [['stock_name'], 'string', 'max' => 500],
            [['share_price'], 'string', 'max' => 75],
            [['actual_event'], 'string', 'max' => 300],
            [['suggestion'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'week_no' => 'Week No',
            'date' => 'Date',
            'stock_name' => 'Company Name',
            'share_price' => 'Share Price',
            'no_of_shares' => 'No Of Shares',
            'actual_event' => 'Actual Event',
            'open' => 'Open',
            'high' => 'High',
            'low' => 'Low',
            'close' => 'Close',
            'suggestion' => 'Suggestion',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
            'action'=>'Action',
            'symbol'=>'Symbol'
        ];
    }

     public function behaviors()
    {
        return [
            'MetaTag' => [
                'class' => MetaTagBehavior::className(),
            ],
        ];
    }
    public function sendStockmail($created_by,$stock_name,$flag)
    {                 
        require '../../vendor/autoload.php'; // If you're using Composer (recommended)
         $adminEmail = \app\models\User::find()->select('email')->where(['status' => 1, 'user_type'=>1])->one();
         $approvedEmail = \app\models\User::find()->select('email')->where(['id'=>Yii::$app->user->identity->id])->one();
          $createdUser = \app\models\User::find()->select('email')->where(['id' => $created_by])->one();
        $content='<div style="font:18px/23px Arial,Helvetica,sans-serif;background:#f3f3f3">
                 <div style="max-width:800px;margin-left:auto;margin-right:auto">
                    <div style="width:640px;margin-left:auto;margin-right:auto;padding:20px 0px 20px 0px; style="text-align: center;">
                        <table class="x_-665217761MsoNormalTable" border="1" cellspacing="3" cellpadding="0" style="border: solid rgb(204,204,204) 1.0pt;background: #fff;color: #000;">
                      <tbody>
                        <tr>';
                        if($flag==1) {
                          echo '<td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                          <h2 align="center" style="text-align: center;font-family: Helvetica;">Stock Approval</h2>
                          <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">'.$stock_name.' stock created by '.$createdUser->email.', Please check stock approval.</span></p>                      
                           </td>';
                        }
                        else if($flag==0) {
                          $content.= '<td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                          <h2 align="center" style="text-align: center;font-family: Helvetica;">Stock Approval</h2>
                         <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">'.$stock_name.' stock updated by '.$createdUser->email.', Please check stock approval.</span></p> </td>';
                        }
                        else if($flag==2) {
                          $content.= '<td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                          <h2 align="center" style="text-align: center;font-family: Helvetica;">Stock Approved</h2>
                          <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">'.$stock_name.' stock approved by '.$approvedEmail->email.'.</span></p>                     
                           </td>';
                        }
                         else if($flag==3) {
                          $content.= '<td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                          <h2 align="center" style="text-align: center;font-family: Helvetica;">Stock Approved</h2>
                          <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">'.$stock_name.' stock rejected by '.$approvedEmail->email.'.</span></p>                     
                           </td>';
                        }

                        $content.= '</tr>                       
                        <tr>
                          <td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                          <p class="MsoNormal" align="center" style="font-family: Helvetica;text-align: center;"><span style="font-size: 10.0pt;font-family: Helvetica;">Thank You</span></p>
                          </td>
                        </tr>
                      </tbody>
                    </table> 
                    </div>
                 </div>
              </div>';
                $email = new \SendGrid\Mail\Mail(); 
                $email->setFrom("info@vishwaswealthadvisory.com","Vishwas Wealth Advisory");
                if($flag==1)
                {
                $email->setSubject('Stock Approval' );
                $email->addTo($adminEmail->email);
                }
                else if($flag==0)
                {
                $email->setSubject('Stock Updated for Approval' );
                //$email->addTo($this->email);
                $email->addTo($adminEmail->email);
                }
                else if(($flag==2) || ($flag==3))
                {
                $email->setSubject('Stock Approved' );
                $email->addTo($createdUser->email);
                if($createdUser->email!=$adminEmail->email)
                $email->addCc($adminEmail->email);
                }
               
                $email->addContent(
                    "text/html", $content
                );
                $sendgrid = new \SendGrid(Yii::$app->params['SENDGRID_API_KEY']);
            try {
            $response = $sendgrid->send($email);
            // $response= $response->statusCode() . "\n";
            // $response=($response->headers());
            // $response= $response->body() . "\n";
        } catch (Exception $e) {
            $response= 'Caught exception: '. $e->getMessage() ."\n";
        }
        //print_r($response);die;
        return $response;
       
    }
}
