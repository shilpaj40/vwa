<?php

namespace backend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property int $status
 */
class Users extends \yii\db\ActiveRecord 
{
    /**
     * {@inheritdoc}
     */
    public $oldpass;
    public $newpass;
    public $repeatnewpass;
    public $fullname;
    const SCENARIO_REPORT = 'report';
    const SCENARIO_CREATE = 'create';
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'user_type','password','mobile','first_name','last_name'], 'required',  'on' => self::SCENARIO_CREATE],
            [['starsign','birth_date'], 'safe', 'on' => self::SCENARIO_REPORT],
            [['oldpass', 'newpass', 'repeatnewpass'], 'required', 'on' => 'resetpassword'],
            ['repeatnewpass','compare','compareAttribute'=>'newpass'],
            [['status'], 'integer'],
            [['username'], 'UniqueEmployeeRules'],
            // [['first_name'], 'string', 'max' => 20,'tooShort'=>'Please Enter Only First Name' ],
            //  [['last_name'], 'string', 'max' => 20,'tooShort'=>'Please Enter Only Last Name' ],

            [['email'], 'UniqueRules', 'on' => 'resetpassword'],
            [['username', 'password'], 'string', 'max' => 100],
            ['email', 'email'],
            [['mobile'], 'string','min'=>10, 'max' => 10,'tooShort'=>'Should be 10 digit long.(Example: 9999999999)' , 'tooLong' => 'Should be 10 digit long.(Example: 9999999999)' ],
            [['first_name','last_name','id','email', 'password', 'profile_photo', 'created_at', 'fullname','gender','birth_date','city'],'safe'],
            [['user_type'], 'exist', 'skipOnError' => true, 'targetClass' => RoleMaster::className(), 'targetAttribute' => ['user_type' => 'role_id']],

           
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'status' => 'Status',
            'email' => 'Email ID',
            'star_sign' => 'Star Sign',
            'birth_date' => 'Birth Date',
            'user_type'=>'Role'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
public static function getUsers()
    {
        return parent::findAll(['status' =>1]);
   }
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Security::generateRandomKey();
    }

  
    public function getName()
    {
        return Yii::$app->user->identity->name;
    }

    public function UniqueRules()
    {
        if(!empty($this->email)){
            $unique=User::find()->where(['email'=>$this->email])->count();

            if($unique>0)
            {
                $this->addError('email','Email already exists');
            }
            
        }
    }

    public function UniqueUsernameRules()
    {      
            $query=new Query;
            $query->from('user')
                    ->where(['username' => $this->username, 'status'=>1]);
            $command = $query->createCommand();
            $User = $command->queryOne(); 
            
            if($User!="" && $this->id!=$User['id'])
            {
                $this->addError('username','Username must be unique');
            }
    }
     public function getUsertype()
    {
        return $this->hasOne(RoleMaster::className(), ['role_id' => 'user_type']);
    }

    public function UniqueEmployeeRules()
    {
       
        if($this->id!="")
        {
            $query=new Query;
            $query->from('user')
                    ->where(['id' => $this->id, 'status'=>1]);
            $command = $query->createCommand();
            $User = $command->queryOne(); 
            
            if($User!="" && $this->id!=$User['id'])
            {
                $this->addError('id','Already created user for the same employee');
            }
        }
            
    }
   
}
