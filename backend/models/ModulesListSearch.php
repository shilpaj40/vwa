<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ModulesList;

/**
 * ModulesListSearch represents the model behind the search form of `backend\models\ModulesList`.
 */
class ModulesListSearch extends ModulesList
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['module_id', 'parent_id', 'is_active', 'sequence', 'show_in_sidebar'], 'integer'],
            [['module_name', 'controller', 'action', 'url', 'icon'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ModulesList::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'module_id' => $this->module_id,
            'parent_id' => $this->parent_id,
            'is_active' => $this->is_active,
            'sequence' => $this->sequence,
            'show_in_sidebar' => $this->show_in_sidebar,
        ]);

        $query->andFilterWhere(['like', 'module_name', $this->module_name])
            ->andFilterWhere(['like', 'controller', $this->controller])
            ->andFilterWhere(['like', 'action', $this->action])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'icon', $this->icon]);

        return $dataProvider;
    }
}
