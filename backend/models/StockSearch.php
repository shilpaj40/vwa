<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Stock;

/**
 * StockSearch represents the model behind the search form of `backend\models\Stock`.
 */
class StockSearch extends Stock
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'week_no', 'no_of_shares', 'status'], 'integer'],
            [['date', 'stock_name', 'share_price', 'actual_event', 'suggestion', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['open', 'high', 'low', 'close'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Stock::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'week_no' => $this->week_no,
            'date' => $this->date,
            'no_of_shares' => $this->no_of_shares,
            'open' => $this->open,
            'high' => $this->high,
            'low' => $this->low,
            'close' => $this->close,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'stock_name', $this->stock_name])
            ->andFilterWhere(['like', 'share_price', $this->share_price])
            ->andFilterWhere(['like', 'actual_event', $this->actual_event])
            ->andFilterWhere(['like', 'suggestion', $this->suggestion]);
        $query->groupBy(['stock_name']);

        return $dataProvider;
    }
}
