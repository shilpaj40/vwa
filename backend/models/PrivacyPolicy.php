<?php

namespace backend\models;
use  v0lume\yii2\metaTags\MetaTagBehavior;

use Yii;

/**
 * This is the model class for table "privacy_policy".
 *
 * @property int $id
 * @property string $title
 * @property string $alias
 * @property string $body
 * @property int $banner_id
 * @property int $created_by
 * @property string $created_at
 * @property int $updated_by
 * @property string|null $updated_at
 * @property int $deleted_by
 * @property string|null $deleted_at
 */
class PrivacyPolicy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'privacy_policy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'alias', 'body', 'banner_id', 'created_by'], 'required'],
            [['body'], 'string'],
            [['banner_id', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['title'], 'string', 'max' => 200],
            [['alias'], 'string', 'max' => 100],
            [['banner_image'], 'string', 'max' => 250],
             [['banner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Banner::className(), 'targetAttribute' => ['banner_id' => 'id']],


        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'alias' => 'Alias',
            'body' => 'Body',
            'banner_id' => 'Banner ID',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
        ];
    }
    public function behaviors()
    {
        return [
            'MetaTag' => [
                'class' => MetaTagBehavior::className(),
            ],
        ];
    }

    public function getBanner()
    {
        return $this->hasOne(Banner::className(), ['id' => 'banner_id']);
    }
}
