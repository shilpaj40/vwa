<?php

namespace backend\models;
use  v0lume\yii2\metaTags\MetaTagBehavior;
use Yii;

/**
 * This is the model class for table "resources".
 *
 * @property int $id
 * @property string $resource_type
 * @property string $title
 * @property string $description
 * @property string $file
 * @property int $content_for
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Resources extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'resources';
    }

    /**
     * {@inheritdoc}
     */
    const SCENARIO_CREATE = 'create';
    const SCENARIO_FILE = 'document';
    const SCENARIO_UPDATE="update";

 const SCENARIO_UPDATE_DOC="update_doc";

    public function rules()
    {
        return [
            [['resource_type', 'title', 'youtube_iframe', 'content_for'], 'required', 'on' => self::SCENARIO_CREATE], // Add more required fields on 'create' scenario.
            
            [['resource_type', 'title', 'file_path', 'content_for'], 'required', 'on' => self::SCENARIO_FILE], // Add more required fields on 'create' scenario.
            [['resource_type', 'title', 'youtube_iframe', 'content_for'], 'required', 'on' => self::SCENARIO_UPDATE],
            [['resource_type', 'title', 'file_path', 'content_for'], 'required', 'on' => self::SCENARIO_UPDATE_DOC],
             [['file_path'], 'file', 'extensions'=>'pdf'],
           // [['resource_type', 'title', 'description', 'file', 'content_for'], 'required'],
            [['content_for'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['resource_type'], 'string', 'max' => 100],
            [['title'], 'string', 'max' => 200],
            [['description'], 'string', 'max' => 500],
           // [['file_path'], 'file', 'skipOnEmpty' => false, 'extensions' => 'pdf'],

        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'resource_type' => 'Resource Type',
            'title' => 'Title',
            'description' => 'Description',
            'file_path' => 'File',
            'content_for' => 'Content For',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    public function behaviors()
    {
        return [
            'MetaTag' => [
                'class' => MetaTagBehavior::className(),
            ],
        ];
    }

    
}
