<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int $user_id
 * @property int $payment_id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $city
 * @property string $payment_status
 * @property string $created_date
 * @property int $order_status
 * @property int $amount
 * @property int $subcription_id
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'payment_id', 'first_name', 'last_name', 'email', 'city', 'payment_status', 'amount', 'subcription_id'], 'required'],
            [['user_id', 'payment_id', 'amount', 'subcription_id'], 'integer'],
            [['created_date'], 'safe'],
            [['first_name', 'last_name', 'email', 'city', 'payment_status'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'payment_id' => 'Payment ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'city' => 'City',
            'payment_status' => 'Payment Status',
            'created_date' => 'Created Date',
             'payment_start_date' => 'Start date',
             'payment_end_date'=>'End Date',
            'amount' => 'Amount',
            'subcription_id' => 'Script Per Week',
        ];
    }
}
