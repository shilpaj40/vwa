<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "stockmails".
 *
 * @property int $id
 * @property int $stock_id
 * @property int $user_id
 * @property int $is_used
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property string $subject
 * @property string $email_body
 */
class Stockmails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stockmails';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['stock_id', 'user_id', 'created_by','subject', 'email_body'], 'required'],
            [['stock_id', 'user_id', 'is_used', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['subject', 'email_body'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'stock_id' => 'Stock Name',
            'user_id' => 'User Name',
            'is_used' => 'Is Used',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'subject' => 'Subject',
            'email_body' => 'Email Body',
        ];
    }
}
