<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "role_master".
 *
 * @property int $role_id
 * @property string $role_name
 * @property int $status
 */
class RoleMaster extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'role_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['role_name'], 'required'],
            [['status'], 'integer'],
            [['role_name'], 'string', 'max' => 300],
        ];
    }
     

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'role_id' => 'Role ID',
            'role_name' => 'Role Name',
            'status' => 'Status',
        ];
    }
}
