<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "role_module_permission".
 *
 * @property int $id
 * @property int|null $role_id
 * @property int|null $module_id
 * @property int $access
 * @property string|null $added_at
 */
class RoleModulePermission extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'role_module_permission';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['role_id', 'module_id', 'access'], 'integer'],
            [['access'], 'required'],
            [['added_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role_id' => 'Role ID',
            'module_id' => 'Module ID',
            'access' => 'Access',
            'added_at' => 'Added At',
        ];
    }
}
