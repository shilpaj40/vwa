<?php

namespace backend\models;
use  v0lume\yii2\metaTags\MetaTagBehavior;

use Yii;

/**
 * This is the model class for table "home_blocks".
 *
 * @property int $id
 * @property string $best_advisors
 * @property string $productive_weekends
 * @property string $image
 * @property string $zero_surprises
 * @property int $status
 * @property string|null $created_at
 * @property int $created_by
 * @property string|null $updated_at
 * @property int $updated_by
 * @property string|null $deleted_at
 * @property int $deleted_by
 * @property string $about_details
 * @property string $footer_image
 * @property string $logo
 * @property string $copyright
 */
class HomeBlocks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $photo;
    public static function tableName()
    {
        return 'home_blocks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['best_advisors', 'productive_weekends', 'zero_surprises', 'created_by', 'copyright'], 'required'],
            [['status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at','photo'], 'safe'],
            [['about_details'], 'string'],
            [['best_advisors', 'productive_weekends', 'zero_surprises'], 'string', 'max' => 500],
            [['image', 'footer_image', 'logo'], 'string', 'max' => 250],
            [['copyright'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'best_advisors' => 'Best Advisors',
            'productive_weekends' => 'Productive Weekends',
            'image' => 'Image',
            'zero_surprises' => 'Zero Surprises',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
            'about_details' => 'About Details',
            'footer_image' => 'Footer Image',
            'logo' => 'Logo',
            'copyright' => 'Copyright',
        ];
    }
    
    public function behaviors()
    {
        return [
            'MetaTag' => [
                'class' => MetaTagBehavior::className(),
            ],
        ];
    }
}
