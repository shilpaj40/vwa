<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "stock_updates".
 *
 * @property int $id
 * @property int $stock_id
 * @property string $stock_name
 * @property string $date
 * @property string $symbol
 * @property int $no_of_shares
 * @property string $share_price
 * @property string $suggestion
 * @property string $action
 * @property string $actual_event
 * @property int $status
 * @property int $created_by
 * @property string $created_at
 */
class Stockupdates extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stock_updates';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['stock_id', 'stock_name', 'date', 'symbol', 'suggestion', 'action', 'actual_event', 'status', 'created_by'], 'required'],
            [['stock_id', 'no_of_shares', 'status', 'created_by','first_record'], 'integer'],
            [['date', 'created_at','first_record', 'no_of_shares', 'share_price'], 'safe'],
            [['stock_name'], 'string', 'max' => 500],
            [['symbol', 'suggestion'], 'string', 'max' => 200],
            [['share_price'], 'string', 'max' => 75],
            [['action'], 'string', 'max' => 400],
            [['actual_event'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'stock_id' => 'Stock ID',
            'stock_name' => 'Stock Name',
            'date' => 'Date',
            'symbol' => 'Symbol',
            'no_of_shares' => 'No Of Shares',
            'share_price' => 'Share Price',
            'suggestion' => 'Suggestion',
            'action' => 'Action',
            'actual_event' => 'Actual Event',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
        ];
    }
}
