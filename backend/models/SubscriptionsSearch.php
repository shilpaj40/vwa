<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Subscriptions;

/**
 * SubscriptionsSearch represents the model behind the search form of `backend\models\Subscriptions`.
 */
class SubscriptionsSearch extends Subscriptions
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'script_per_week', 'status', 'created_by', 'updated_by'], 'integer'],
            [['one_month', 'twelve_month', 'three_month'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Subscriptions::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'script_per_week' => $this->script_per_week,
            'one_month' => $this->one_month,
            'twelve_month' => $this->twelve_month,
            'three_month' => $this->three_month,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);
         $query->andFilterWhere(['like', 'script_per_week', $this->script_per_week])
            ->andFilterWhere(['like', 'one_month', $this->one_month])
            ->andFilterWhere(['like', 'twelve_month', $this->twelve_month])
            ->andFilterWhere(['like', 'three_month', $this->three_month])
            ->andFilterWhere(['like', 'status',1]);
            



        return $dataProvider;
    }
}
