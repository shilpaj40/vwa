<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Stockupdates;

/**
 * StockUpdatesSearch represents the model behind the search form of `backend\models\StockUpdates`.
 */
class StockupdatesSearch extends Stockupdates
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'stock_id', 'no_of_shares', 'status', 'created_by'], 'integer'],
            [['stock_name', 'date', 'symbol', 'share_price', 'suggestion', 'action', 'actual_event', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Stockupdates::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'stock_id' => $this->stock_id,
            'date' => $this->date,
            'no_of_shares' => $this->no_of_shares,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'first_record'=>$this->first_record,
        ]);

        $query->andFilterWhere(['like', 'stock_name', $this->stock_name])
            ->andFilterWhere(['like', 'symbol', $this->symbol])
            ->andFilterWhere(['like', 'share_price', $this->share_price])
            ->andFilterWhere(['like', 'suggestion', $this->suggestion])
            ->andFilterWhere(['like', 'action', $this->action])
            ->andFilterWhere(['like', 'actual_event', $this->actual_event]);

        return $dataProvider;
    }
}
