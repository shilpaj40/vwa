<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "bulk_emails".
 *
 * @property int $id
 * @property string $email_id
 * @property string $subject
 * @property string $body
 * @property string $created_at
 */
class BulkEmails extends \yii\db\ActiveRecord
{
    public $csv_file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bulk_emails';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email_id', 'subject', 'body', 'csv_file'], 'required'],
            [['body'], 'string'],
            [['created_at'], 'safe'],
            [['email_id'], 'string', 'max' => 1500],
            [['subject'], 'string', 'max' => 2000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email_id' => 'Email ID',
            'subject' => 'Subject',
            'body' => 'Body',
            'created_at' => 'Created At',
            'csv_file'=> 'CSV File'
        ];
    }
}
