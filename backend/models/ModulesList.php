<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "modules_list".
 *
 * @property int $module_id
 * @property int $parent_id
 * @property string|null $module_name
 * @property string $controller
 * @property string $action
 * @property string $url
 * @property string $icon
 * @property int|null $is_active
 * @property int $sequence
 * @property int $show_in_sidebar
 *
 * @property ModulesActionList[] $modulesActionLists
 */
class ModulesList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'modules_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id','module_name', 'controller', 'action', 'url', 'sequence', 'show_in_sidebar'], 'required'],
            [['parent_id', 'is_active', 'sequence', 'show_in_sidebar'], 'integer'],
            [['module_name', 'group_name'], 'string', 'max' => 100],
            [['controller'], 'string', 'max' => 50],
            [['action'], 'string', 'max' => 200],
            [['url'], 'string', 'max' => 300],
            [['icon'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'module_id' => 'Module ID',
            'parent_id' => 'Parent ID',
            'module_name' => 'Module Name',
            'controller' => 'Controller',
            'action' => 'Action',
            'url' => 'Url',
            'icon' => 'Icon',
            'is_active' => 'Is Active',
            'sequence' => 'Sequence',
            'show_in_sidebar' => 'Show In Sidebar',
        ];
    }

    /**
     * Gets query for [[ModulesActionLists]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModulesActionLists()
    {
        return $this->hasMany(ModulesActionList::className(), ['module_id' => 'module_id']);
    }
}
