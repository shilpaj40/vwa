<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "subscriptions".
 *
 * @property int $id
 * @property int $script_per_week
 * @property double $one_month
 * @property double $twelve_month
 * @property double $three_month
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class Subscriptions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}

     */
    public static function tableName()
    {
        return 'subscriptions';
    }

    /**
     * {@inheritdoc}

          */

    
    public function rules()
    {
        return [
            [['script_per_week', 'one_month', 'twelve_month', 'three_month', 'created_by', 'updated_by'], 'required'],
            [['script_per_week', 'status', 'created_by', 'updated_by'], 'integer'],
            [['one_month', 'twelve_month', 'three_month'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'script_per_week' => 'Script Per Week',
            'one_month' => '1 Month',
            'twelve_month' => '12 Month',
            'three_month' => '3 Month',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
