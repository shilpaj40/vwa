<?php

namespace backend\models;
use  v0lume\yii2\metaTags\MetaTagBehavior;
use Yii;

/**
 * This is the model class for table "contact_us".
 *
 * @property int $id
 * @property string $title
 * @property string $alias
 * @property string $office_address
 * @property string $email
 * @property string $mobile
 * @property int $banner_id
 * @property string $google_map
 * @property int $created_by
 * @property string $created_at
 * @property int $updated_by
 * @property string|null $updated_at
 * @property string|null $deleted_at
 * @property int $deleted_by
 */
class ContactUs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact_us';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'alias', 'office_address', 'email', 'banner_id', 'created_by'], 'required'],
            [['banner_id', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['title', 'email'], 'string', 'max' => 200],
            [['alias'], 'string', 'max' => 100],
             ['email', 'email'],
            [['mobile'], 'string','min'=>10, 'max' => 10,'tooShort'=>'Should be 10 digit long.(Example: 9999999999)' , 'tooLong' => 'Should be 10 digit long.(Example: 9999999999)' ],
            [['office_address', 'google_map'], 'string', 'max' => 500],
            [['mobile'], 'string', 'max' => 15],
            [['banner_image'], 'string', 'max' => 250],
            [['banner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Banner::className(), 'targetAttribute' => ['banner_id' => 'id']],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'alias' => 'Alias',
            'office_address' => 'Office Address',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'banner_id' => 'Banner ID',
            'google_map' => 'Google Map',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
        ];
    }
    
    public function behaviors()
    {
        return [
            'MetaTag' => [
                'class' => MetaTagBehavior::className(),
            ],
        ];
    }
     public function getBanner()
    {
        return $this->hasOne(Banner::className(), ['id' => 'banner_id']);
    }
}
