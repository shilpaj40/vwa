<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "carousel_item".
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $body
 * @property int $status
 * @property string|null $created_at
 * @property int $created_by
 * @property string|null $updated_at
 * @property int $updated_by
 * @property string|null $deleted_at
 * @property int $deleted_by
 */
class CarouselItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'carousel_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'image'], 'required',  'on' => 'create'],
            [['name'], 'required', 'on' => 'update'],

            [['status', 'created_by', 'updated_by', 'deleted_by', 'sequence_no', 'overlay'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name'], 'string', 'max' => 150],
            [['image'], 'string', 'max' => 250],
            [['body'], 'string', 'max' => 2500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'image' => 'Image',
            'body' => 'Body',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
        ];
    }
}
