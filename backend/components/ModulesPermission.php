<?php

/**
 * @author Prakash S
 * @copyright 2017
 */
 
namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\db\Query;
use backend\models\FeatureAccess;


class ModulesPermission extends Component
{
    public function getMenus()
    {
        $role_id = Yii::$app->user->identity->user_type;

        $user_id = Yii::$app->user->identity->id;
        $modules = \backend\models\ModulesList::find()
            ->select('module_id, module_name, controller, icon, url,parent_id')
            ->where('is_active = :is_active', [':is_active' => 1])
            ->andWhere('show_in_sidebar = :show_in_sidebar', [':show_in_sidebar' => 1])
            ->andWhere('parent_id = :parent_id', [':parent_id' => 0])
            ->asArray()->all();

        $items =""; 
        $items.='<ul class="nav nav-pills" >';
        // $items.= '<li class="header">Menu</li>';
        $items.= '<li id="dashboard"><a class="text-logo-color" href="'. \yii\helpers\Url::to(['site/']).'">Home</a></li>';
        for($i=0; $i<count($modules); $i++)
        {
            //echo "module:".$modules[$i]['module_name']."</br >";
            $submodules = \backend\models\RoleModulePermission::find()
                     ->select('ML.module_id, ML.module_name, ML.controller, ML.icon, ML.url, ML.parent_id, role_module_permission.access')
                     ->join('LEFT JOIN', 'modules_list AS ML', 'ML.module_id = role_module_permission.module_id')
                     ->where('role_id = :role_id', [':role_id' => $role_id])
                     ->andWhere('role_module_permission.access = :access', [':access' => 1])
                     ->andWhere('is_active = :is_active', [':is_active' => 1])
                    ->andWhere('ML.show_in_sidebar = :show_in_sidebar', [':show_in_sidebar' => 1])
                     ->andWhere('ML.parent_id = :parent_id', [':parent_id' =>$modules[$i]['module_id']])
                     ->asArray()->all();

            if($role_id==1)
            {
                $submodules = \backend\models\ModulesList::find()
                    ->select('module_id, module_name, controller, icon, url, parent_id')
                    ->where('is_active = :is_active', [':is_active' => 1])
                    ->andWhere('show_in_sidebar = :show_in_sidebar', [':show_in_sidebar' => 1])
                    ->andWhere('parent_id = :parent_id', [':parent_id' =>$modules[$i]['module_id']])
                    ->asArray()->all();
            }

            if(!empty($submodules))
            {                
                $items.='<li class="dropdown" id="'.$modules[$i]['controller'].'" ><a class="text-logo-color no-arrow dropdown-toggle" href="#" data-toggle="dropdown" href="#">'.$modules[$i]['module_name'].'<span class="caret"></span></a>
                <ul class="dropdown-menu">';
                        
                for($j=0; $j<count($submodules); $j++)
                {
                    
                    $items.= '<li id="'.$submodules[$j]['controller'].'" ><a class="text-logo-color" href="'. \yii\helpers\Url::to([$submodules[$j]['url'].'/']).'">'.$submodules[$j]['module_name'].'</a></li>';
                }
                $items.= '</ul></li>';
            }
            else
            {
                $CheckAccess = \backend\models\RoleModulePermission::find()
                     ->select('access')
                     ->where('role_id = :role_id', [':role_id' => $role_id])
                     ->andWhere('module_id = :module_id', [':module_id' =>$modules[$i]['module_id']])
                     ->one();
                if($CheckAccess['access']==1)
                {
                    $items.= '<li id="'.$modules[$i]['controller'].'" ><a class="text-logo-color" href="'. \yii\helpers\Url::to([$modules[$i]['url'].'/']).'">'.$modules[$i]['module_name'].'</a></li>';
                }  
            }
        }
        $items.="</ul>";
        return $items;
    }
    
    public function getPermission()
    {
        $actions = array();
        $role_id = Yii::$app->user->identity->user_type;
        $user_id = Yii::$app->user->identity->id;      
        $action = Yii::$app->controller->action->id;
        $controller = Yii::$app->controller->id;
        if($role_id==1)
        {
            return true;

        }
        else
        {
            $permission = \backend\models\RoleModulePermission::find()
                             ->select('ML.module_id, ML.module_name, ML.controller, ML.icon, role_module_permission.access')
                             ->join('LEFT JOIN', 'modules_list AS ML', 'ML.module_id = role_module_permission.module_id')
                             ->where('ML.action = :action', [':action' => $action])
                            ->andWhere('role_id = :role_id', [':role_id' => $role_id])
                             ->andWhere('role_module_permission.access = :access', [':access' => 1])
                             ->andWhere('controller = :controller', [':controller' => $controller])
                             ->one();
       // echo $permission["access"];die;
        return ($permission["access"]==1)? true : false;
        }
        
    }
    public function getAccessPermission($controller,$action)
    {
        $actions = array();
        $user_id = Yii::$app->user->identity->id;    
        $role_id = Yii::$app->user->identity->user_type;       
        if($role_id==1)
        {
            return true;
        }
        else
        {  
                            $permission = \backend\models\RoleModulePermission::find()
                             ->select('ML.module_id, ML.module_name, ML.controller, ML.icon, role_module_permission.access')
                             ->join('LEFT JOIN', 'modules_list AS ML', 'ML.module_id = role_module_permission.module_id')
                             ->where('ML.action = :action', [':action' => $action])
                            ->andWhere('role_id = :role_id', [':role_id' => $role_id])
                             ->andWhere('role_module_permission.access = :access', [':access' => 1])
                             ->andWhere('controller = :controller', [':controller' => $controller])
                             ->one();
                             // echo $controller."->".$action;
            //echo $permission["access"];die;
            return ($permission["access"]==1)? true : false;
        }
       
        
    }
     public function getFeatureAccessPermission($featureid)
     {
        $user_id = Yii::$app->user->identity->id;   
          $role_id = Yii::$app->user->identity->user_type;       
        if($role_id==1)
        {
            return true;
        }
        else
        {
        if($featureid==3)
        {
          $meetingUsers=FeatureAccess::find()->where(['user_id'=>$user_id,'feature_id'=>5])->one();
          if($meetingUsers)
          {

              if($meetingUsers->access==1)
              {
                $meeting=FeatureAccess::find()->where(['user_id'=>$user_id,'feature_id'=>$featureid])->one();
                        if($meeting->remain_val>10)
                            return 1;
                        else
                            return $meeting->remain_val;                        
              }
              else
                return 0;
            }
            else
                return 0;
        }
        else if($featureid==1)
        {
            $meetingUsers=FeatureAccess::find()->where(['user_id'=>$user_id,'feature_id'=>$featureid])->one();
            return $meetingUsers->remain_val;
        }
        else
        {
            $meeting=FeatureAccess::find()->where(['user_id'=>$user_id,'feature_id'=>$featureid])->one();
            if($meeting){
            return ($meeting["access"]==1)? 1 : 0;
            }
            return 0;
        }
      }


     }
}

?>