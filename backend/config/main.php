<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
          'rbac' => [
            'class' => 'yii2mod\rbac\Module',
            ],
            'auditlog' => [
                'class' => 'ruturajmaniyar\mod\audit\AuditEntryModule'
            ],
            'menu' => [
              'class' => '\pceuropa\menu\Menu',
            ],
            'cms' => [
                'class' => 'yii2mod\cms\Module',
            ],
            
    ],
   'timeZone' => 'Asia/Calcutta',
    //'timeZone' => 'Europe/London',
    'components' => [
        'Permission' => [
            'class' =>  'app\components\ModulesPermission',
      ],
        'metaTags' => [
            'class' => 'v0lume\yii2\metaTags\MetaTagsComponent',
            'generateCsrf' => false,
            'generateOg' => true,
        ],
      'dateTimeConversion' => [
                'class' => 'ruturajmaniyar\mod\audit\components\DateTimeHelper'
         ],
      'dateformatter'=>[
        'class'=>'app\components\DateFormat',
        ],
     
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest', 'user'],
        ],  
        'db'=>[
            'class'=>'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=vwa',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8'            
        ],
        'user' => [
            'class' => 'app\components\User', // extend User component
        ],
        // 'view' => [
        //      'theme' => [
        //          'pathMap' => [
        //             '@app/views' => '@vendor/ricar2ce/yii2-material-theme/view'
        //          ],
        //      ],
        // ],
        'view' => [
             'theme' => [
                 'pathMap' => [
                    '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
                 ],
             ],
        ],
        'request' => [
            'csrfParam' => '_csrf-backend',
            'class' => 'common\components\Request',
            'web'=> '/backend/web',
            'adminUrl' => '/admin'
        ],
        'user' => [
            'identityClass' => 'app\models\User',
             'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_backendUser', // unique for backend
            ]
        ],
        // 'user' => [
        //     'identityClass' => 'app\models\User',
        //     'enableAutoLogin' => true,
        //     'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        // ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
                'enablePrettyUrl' => true,
                'showScriptName' => false,
        ],
        
    ],
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'homeUrl' => 'https://vishwaswealthadvisory.com/admin/site/index',
    'params' => $params,
];
