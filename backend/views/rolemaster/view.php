<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\RoleModulePermission;
use backend\models\ModulesList;

/* @var $this yii\web\View */
/* @var $model backend\models\Role */
$um_id=$model->role_id;
$this->title = $model->role_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$firstChar = mb_substr($model->role_name, 0, 1, "UTF-8");

?>
<div class="role-view">
 <p class="create">
         <?= \Yii::$app->Permission->getAccessPermission(Yii::$app->controller->id,'create')? Html::a('+', ['create'], ['class' => 'btn btn-success',]):"" ?>
    </p>
<div class="row">
    <div class="col-md-3">
      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">

            <i class="profile-user-img role-name img-responsive img-circle"><?=$firstChar?></i>
         

          <h3 class="profile-username text-center"><?= $model->role_name?></h3>

             <?= \Yii::$app->Permission->getAccessPermission(Yii::$app->controller->id,'update')? Html::a('<i class="fa fa-pencil-square-o"></i> Edit Role', ['update', 'id' => $model->role_id], ['class' => 'btn btn-success btn-block']):"" ?>
             <?= \Yii::$app->Permission->getAccessPermission(Yii::$app->controller->id,'delete')? Html::a('<i class="fa fa-trash"></i> Delete Role', ['delete', 'id' => $model->role_id], [
                'class' => 'btn btn-danger btn-block',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
              ]):"" ?>
         
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </div>
    <div class="col-md-9">
        
        <?php
       
            $menuMaster=GenrateTopMenu($um_id);
            echo $menuMaster; 
           
        ?>
            
    </div>
</div>

</div>
<?php
function GenrateTopMenu($um_id)
{
    $topMenu="";
    $ParentMenuMaster=ModulesList::find()->where(['is_active' => 1, 'parent_id'=>0, 'show_in_sidebar'=>1])->orderBy(['sequence'=>SORT_ASC])->all();
    
    foreach($ParentMenuMaster as $parentModel)
    {
        //print_r($parentModel->menu_id);
        $topMenu.="<table style='width:100%;border-collapse: collapse;margin:0 auto;table-layout: fixed;
' class='table table-bordered table-striped'>";
        
        $topMenu.=CreateTopMenuChild($parentModel->module_id, $parentModel->module_name, $parentModel->url,$parentModel, $um_id);
        $topMenu.="</table>";
    }
    return $topMenu;

}
function CreateTopMenuChild($menuId, $menuName, $menuUrl,$parentModel, $um_id)
{
    $topMenu="";
    $ChildModelMaster=ModulesList::find()->where(['is_active' => 1, 'parent_id'=>$menuId])->groupBy(['group_name'])->orderBy(['sequence'=>SORT_ASC])->all();
    if (count($ChildModelMaster) == 0 )
    {
        
        $topMenu.= "<tr class='top-row' ><td style='width:35%; align:right;align-right:0px;'> <b>". ucfirst($parentModel->group_name).":</b></td><td style='width:65%; align:right;align-right:0px;'><div class='row' style=''>";
         $ChildMenulMaster=ModulesList::find()->where(['is_active' => 1, 'group_name'=>$parentModel->group_name])->orderBy(['sequence'=>SORT_ASC])->all();
         if(!empty($ChildMenulMaster))
         {

            foreach ($ChildMenulMaster as $ChildGroup) {
                $topMenu.="<div class='col-md-4'><span style='align:center'>".getAccessNew($parentModel->parent_id,$ChildGroup->module_id, $parentModel->module_id, $um_id)." ".ucfirst($ChildGroup->module_name)."</span></div>";
            }
         }
        $topMenu.= "</div></td></tr>";
    }
    else
    {
        
        $topMenu.= "<tr class='sub-row' ><td colspan='2' style='background:#FAB529' ><b>". $menuName."</b></td>";
      //  $topMenu.= "<input type='checkbox' name='".$menuId."-ck"."' id='".$menuId."-ck"."' value='yes' onchange=checkmenu('".$menuId."-ck'); />";
        $topMenu.="</tr>";
        /* Top Menu Childs */

        foreach ($ChildModelMaster as $ChildModel)
        {
            $topMenu.=CreateTopMenuChild($ChildModel->module_id, $ChildModel->module_name, $ChildModel->url,$ChildModel, $um_id);
        }

    }
    return $topMenu;
}

     
function getAccessNew($menuId, $id, $parentId, $um_id)
{
     $UserMenuAccess=RoleModulePermission::find()->where(['role_id'=>$um_id,'module_id'=>$id])->one();
    
    if(!empty($UserMenuAccess) && $UserMenuAccess->access!=0)
    {
        //return 1;
        return ("<i class='fa fa-check text-green'></i>");
    }
    else
    {
        //return 0;
        return ("<i class='fa fa-close text-red'></i>");
    }

}

?>
