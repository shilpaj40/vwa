<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\ModulesList;
use backend\models\RoleModulePermission;

/* @var $this yii\web\View */
/* @var $model backend\models\RoleMaster */
/* @var $form yii\widgets\ActiveForm */
$um_id=$model->role_id;

?>

<div class="resources-form">
    <div class="box box-primary">
        <div class="box-body">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model); ?>


         <div class="row">
              <div class="col-md-4">
            <?= $form->field($model, 'role_name')->textInput(['maxlength' => true]) ?>
                </div>      
            <div class="col-md-4 pt-4">  
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
      <div class="row">
        <div class="col-md-12">
            <?php
            echo "<div id='indirect-issue-grid' class='grid-view'>
                <table style='width:100%;border-collapse: collapse;margin:0 auto;background:#5FB355' class='table table-bordered table-striped'>
                        <thead>
                        <tr>
                            <th style='width:35%'>Select all menus</th>
<th><input type='checkbox' id='allcb' name='allcb'/> </th>
                        </tr></thead>
                        <tbody>";
                $menuMaster=GenrateTopMenu($um_id);
                echo $menuMaster; 
                echo "</tbody></table></div>";
            ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
<?php
function GenrateTopMenu($um_id)
{
    $topMenu="";
    $ParentMenuMaster=ModulesList::find()->where(['is_active' => 1, 'parent_id'=>0, 'show_in_sidebar'=>1])->orderBy(['sequence'=>SORT_ASC])->all();
    
    foreach($ParentMenuMaster as $parentModel)
    {
        //print_r($parentModel->menu_id);
        $topMenu.="<table style='width:100%;border-collapse: collapse;margin:0 auto;table-layout: fixed;
' class='table table-bordered table-striped'>";
        
        $topMenu.=CreateTopMenuChild($parentModel->module_id, $parentModel->module_name, $parentModel->url,$parentModel, $um_id);
        $topMenu.="</table>";
    }
    return $topMenu;

}
function CreateTopMenuChild($menuId, $menuName, $menuUrl,$parentModel, $um_id)
{
    $topMenu="";
    $ChildModelMaster=ModulesList::find()->where(['is_active' => 1, 'parent_id'=>$menuId])->groupBy(['group_name'])->orderBy(['sequence'=>SORT_ASC])->all();
    if (count($ChildModelMaster) == 0 )
    {
        
        $topMenu.= "<tr class='top-row' ><td style='width:35%; align:right;align-right:0px;'><input type='checkbox' name='".$menuId."-ck"."' id='".$menuId."-ck"."' value='yes' onchange=checkmenu('".$parentModel->module_id."-ck'); /> <b>". ucfirst($parentModel->group_name).":</b></td><td style='width:65%; align:right;align-right:0px;'><div class='row' style=''>";
         $ChildMenulMaster=ModulesList::find()->where(['is_active' => 1, 'group_name'=>$parentModel->group_name])->orderBy(['sequence'=>SORT_ASC])->all();
         if(!empty($ChildMenulMaster))
         {

            foreach ($ChildMenulMaster as $ChildGroup) {
                $topMenu.="<div class='col-md-4'><span style='align:center'>".getAccessNew($parentModel->parent_id,$ChildGroup->module_id, $parentModel->module_id, $um_id)." ".ucfirst($ChildGroup->module_name)."</span></div>";
            }
         }
        $topMenu.= "</div></td></tr>";
    }
    else
    {
        
        $topMenu.= "<tr class='sub-row'><td style='width:35%;'><input type='checkbox' name='".$menuId."-ck"."' id='".$menuId."-ck"."' value='yes' onchange=checkmenu('".$menuId."-ck'); /> <b>". $menuName."</b></td><td style='width:65%;'>";
      //  $topMenu.= "<input type='checkbox' name='".$menuId."-ck"."' id='".$menuId."-ck"."' value='yes' onchange=checkmenu('".$menuId."-ck'); />";
        $topMenu.="</td></tr>";
        /* Top Menu Childs */

        foreach ($ChildModelMaster as $ChildModel)
        {
            $topMenu.=CreateTopMenuChild($ChildModel->module_id, $ChildModel->module_name, $ChildModel->url,$ChildModel, $um_id);
        }

    }
    return $topMenu;
}

     
function getAccessNew($menuId, $id, $parentId, $um_id)
{
    $UserMenuAccess=RoleModulePermission::find()->where(['role_id'=>$um_id,'module_id'=>$id])->one();
    if(!empty($UserMenuAccess) && $UserMenuAccess->access!=0)
    {
        return ("<input type='checkbox' name=$id  class='".$menuId."-ck ".$parentId."-ck' checked=true value='OK'></input>");
    }
    else
    {
        return ("<input type='checkbox' name=$id  class='".$menuId."-ck ".$parentId."-ck'></input>");
    } 
}

?>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

<script>
$('#allcb').change(function(){
    if($(this).prop('checked')){
        $('tbody tr td input[type="checkbox"]').each(function(){
            $(this).prop('checked', true);
        });
    }else{
        $('tbody tr td input[type="checkbox"]').each(function(){
            $(this).prop('checked', false);
        });
    }
});
function checkmenu(ele)
{
    if($('#' + ele).is(":checked"))
        $('.'+ele).each(function() { $(this).prop('checked', true) });
    else
        $('.'+ele).each(function() { $(this).prop('checked', false) });
}
</script>