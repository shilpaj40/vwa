<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\RoleMasterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Roles';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs('
$(document).ready(function(){

$(\'#MyButton\').click(function(){
if (confirm("Are you sure you want to delete this items?")) {
    var HotId = $(\'#w0\').yiiGridView(\'getSelectedRows\');
    //alert(HotId);
    $.ajax({
    type: \'POST\',
    url : \'Multipledeleterole\',
    data : {row_id: HotId},
    success : function(response) {
        alert(response);
        $(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
    }
    });
}
else
{
}
});
});', \yii\web\View::POS_READY);
?>
<div class="resources-index">
     <div class="nav-tabs-custom">
    <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title"> <i class="fa fa-list mr-2"></i>Roles</h3>
            </div>
            <div class="col-md-4">
                <div class="pull-right">
                    <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success', 'data-toggle'=>"tooltip", 'data-original-title'=>"Add New"]) ?>
                    <!--  <?= Html::a('<i class="fa fa-trash"></i>', '', ['class' => 'btn btn-danger', 'data-toggle'=>"tooltip", 'data-original-title'=>"Delete", "id"=>'MyButton']) ?> -->
                </div>
            </div>
        </div>
        <div id="smain">
          <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'class' => 'yii\grid\CheckboxColumn', 'checkboxOptions' => function($data) {
                                    return ['value' => $data->role_id];
                            },
                        ],
                        'role_name',  
                        [
                                    'label'=> '#',
                                    'format'=> 'raw',
                                    'value' => function($model) { 
                                         return  ($model->status == 1)?Html::a('<i class="fa fa-times-circle"></i>', ['rolemaster/changestatus?id='.$model->role_id.'&flag=0'],array('style'=>'padding:2px 10px','class'=>'btn btn-danger','data-confirm'=>'Are you sure you want to deactivate this role?')):Html::a('<i class="fa fa-check"></i>', ['rolemaster/changestatus?id='.$model->role_id.'&flag=1'],array('style'=>'padding:2px 10px','class'=>'btn btn-success','data-confirm'=>'Are you sure you want to activate this role?'));
                                    }
                                ],                      
                       [
                            'header'=>'Actions',
                            'format' => 'raw',
                            'value' => function ($model) {                              
                               return Html::a('<i class="fa fa-pencil"></i>', ['rolemaster/update?id='.$model['role_id']], ['data-toggle'=>"tooltip", 'data-original-title'=>"Update"]);
                             
                            },
                        ],
                    ],
                ]); ?>
             
          </div>
        </div>
       </div>
    </div>
</div>        
<!-- <div class="card">
    <div class="card-body">
        <div class="user-index">
            <div class="row rowBlock text-center">
                 <div class="pull-right mr-4">
                    <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success', 'data-toggle'=>"tooltip", 'data-original-title'=>"Add New"]) ?>
                    
                </div>
            </div>
                       <div class="row mt-2-2">
                <div class="col-md-12">
                    <div id="result">
                        <?= DataTables::widget([
                          'dataProvider' => $dataProvider,
                          'clientOptions' => [
                              'lengthMenu' => [200],
                              'responsive' => true, 
                              'dom' => 'BfTrtip',
                              'buttons' => [
                                'csv','excel', 'pdf', 'print'
                                ]
                            ],
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'], 
                               
                                'role_name',                                
                               [
                                    'label'=> '#',
                                    'format'=> 'raw',
                                    'value' => function($model) { 
                                         return  ($model->status == 1)?Html::a('<i class="fa fa-times-circle"></i>', ['rolemaster/changestatus?id='.$model->role_id.'&flag=2'],array('style'=>'padding:2px 10px','class'=>'btn btn-danger','data-confirm'=>'Are you sure you want to deactivate this role?')):Html::a('<i class="fa fa-check"></i>', ['rolemaster/changestatus?id='.$model->role_id.'&flag=1'],array('style'=>'padding:2px 10px','class'=>'btn btn-success','data-confirm'=>'Are you sure you want to activate this role?'));
                                    }
                                ],
                            ],
                      ]);?>
                    
                    </div>
                    <div id="resultdata" style="display:none">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div> -->

