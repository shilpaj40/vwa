<?php
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\StockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Stocks';
$this->params['breadcrumbs'][] = $this->title;
$js = <<<SCRIPT
/* To initialize BS3 tooltips set this below */
$(function () { 
    $("[data-toggle='tooltip']").tooltip(); 
});;
/* To initialize BS3 popovers set this below */
$(function () { 
    $("[data-toggle='popover']").popover(); 
});
SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js);
?>
<div class="nav-tabs-custom">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#tab_1" data-toggle="tab">Active Stocks</a></li>
    <li><a href="<?= \yii\helpers\Url::to(['stock/closed-stocks']);?>" >Closed Stocks</a></li>
   
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tab_1">
      <div class="box box-primary" style="border-top:0">
        <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title"> <i class="fa fa-list"></i> Active Stock List</h3>
            </div>
            <div class="col-md-4">
                <div class="pull-right">
                    <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success', 'data-toggle'=>"tooltip", 'data-original-title'=>"Add New"]) ?>
                </div>
            </div>
        </div>
        <div id="smain">
          <div class="box-body">
              <?php $stockNAme = \backend\models\Stock::find()->where(['is_closed'=>0,'status'=>1])->orWhere(['is_closed'=>0,'status'=>2])->orWhere(['is_closed'=>1,'status'=>1])->groupby(['stock_name'])->orderBy(['id'=>SORT_DESC])->all(); 
              $i=0;?>
              <input type="hidden" id="scount" value="<?= count($stockNAme);?>">
              <?php if(count($stockNAme)>0){
              foreach ($stockNAme as $stocks){ $i++;
                ?>
              <div class="card mt-2-2">
                  <div class="card-header">
                    <div class="row">
                      <div class="col-md-7">
                        <h5 style="font-size:16px;"><strong><?= $stocks->stock_name;?></strong></h5>
                      </div>
                      <div class="col-md-5 text-right">
                        <?php if(($stocks->created_by==Yii::$app->user->identity->id) || (Yii::$app->user->identity->user_type==1)) {
                        echo Html::a('<i class="fa fa-plus"></i> Update Stock', ['add', 'id'=>$stocks->stock_name], ['class' => 'btn btn-black', 'data-toggle'=>"tooltip", 'data-original-title'=>"Update Stock"]); 
                         echo Html::a('<i class="fa fa-times"></i> Close Stock', ['closed', 'id'=>$stocks->stock_name], ['class' => 'btn btn-danger', 'data-toggle'=>"tooltip", 'data-original-title'=>"Close Stock"]);
                         } ?>
                      </div>
                    </div>
                  </div>
                  <div class="card-body">
                     <!-- <button class="btn btn-primary" id="submit_data">Submit</button> -->
                     <table class="table table-responsive-md table-sm table-bordered" id="makeEditable_<?= $i?>">
                        <thead>
                           <tr>
                              <th>Date</th>
                              <th>Symbol</th>
                              <!-- <th>No. of Share</th>
                              <th>Share Price</th> -->
                              <th>Suggestion </th>
                              <th>Actual Event </th>
                              <th>Action</th>
                              <th>Status</th>
                           </tr>
                        </thead>
                        <tbody>
                          <?php
                          $stock = \backend\models\Stock::find()->where(['stock_name'=>$stocks->stock_name])->andWhere(['is_closed'=>0,'status'=>1])->orWhere(['is_closed'=>0,'status'=>2])->orWhere(['is_closed'=>1,'status'=>1])->orderBy(['id'=>SORT_DESC])->all();
                           foreach($stock as $list){
                              echo "<tr id='".$list->created_by."->".$list->status."->".Yii::$app->user->identity->id."->".Yii::$app->user->identity->user_type."' >";
                               echo "<td id='date-".$list->id."'>".$list->date."</td>";
                                echo "<td id='symbol-".$list->id."'>".$list->symbol."</td>";
                                //echo "<td id='no_of_shares-".$list->id."'>".$list->no_of_shares."</td>";
                                //echo "<td id='share_price-".$list->id."'>".$list->share_price."</td>";                               
                                  echo "<td id='suggestion-".$list->id."'>".$list->suggestion."</td>";
                                  echo "<td id='actual_event-".$list->id."'>".$list->actual_event."</td>";
                                  echo "<td id='action-".$list->id."'>".$list->action."</td>";
                                    echo "<td class='statusbtn' id='status-".$list->id."'>";
                                    if($list->updatesapproval==1)
                                      {
                                        echo "Updated data pending for Approval";
                                      }
                                      else
                                      {
                                         if($list->status==2)
                                        echo "Approved";
                                      else if($list->status==1)
                                        echo "Pending for Approval";
                                      else
                                        echo "Rejected";
                                    }
                                  echo "</td>";
                              echo "</tr>";
                          }?>
                          
                        </tbody>
                     </table>
                     <!-- <span style="float:right"><button id="but_add_<?= $i?>" class="btn btn-danger">Add New Row</button></span> -->
                  </div>
              </div>
              <?php }
              }?>
             
          </div>
        </div>
       
    </div>
      
    </div>
    <!-- /.tab-pane -->
    <div class="tab-pane" id="tab_2">
      The European languages are members of the same family. Their separate existence is a myth.
      For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ
      in their grammar, their pronunciation and their most common words. Everyone realizes why a
      new common language would be desirable: one could refuse to pay expensive translators. To
      achieve this, it would be necessary to have uniform grammar, pronunciation and more common
      words. If several languages coalesce, the grammar of the resulting language is more simple
      and regular than that of the individual languages.
    </div>
    <!-- /.tab-pane -->
    
  </div>
  <!-- /.tab-content -->
</div>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script>
   
    $(function() {

      //$('#makeEditable').SetEditable({ $addButton: $('#but_add')});
      var count=$("#scount").val();
      var j=0;
      for(var i=0;i<count;i++)
      {
        j++;
        $('#makeEditable_'+j).SetEditable({ $addButton: $('#but_add_'+j)});

      }
      var table = document.getElementById("makeEditable_1");
        for (var i = 1, row; row = table.rows[i]; i++) {
          var res = row.id.split("->");
          var rowid=res[0];
          var rowstatus=res[1];
          var loginid=res[2];
          var roleid=res[3];
          if((rowid!=loginid) && (roleid!=1))
          {

            row.deleteCell(-1);
          }
          else
          {
            if((rowstatus!=1) && (roleid!=1))
            {
              row.deleteCell(-1);
            }            
          }          
        }
      $('#submit_data').on('click',function() {
      var td = TableToCSV('makeEditable', ',');
      var ar_lines = td.split("\n");
      var each_data_value = [];
      for(i=0;i<ar_lines.length;i++)
      {
          each_data_value[i] = ar_lines[i].split(",");
      }
  
      for(i=0;i<each_data_value.length;i++)
      {
          if(each_data_value[i]>1)
          {
              console.log(each_data_value[i][2]);
              console.log(each_data_value[i].length);
          }
  
      }
  
    });
  });
  
</script>
