<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use fedemotta\datatables\DataTables;


/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-body">
        <div class="user-index">
          
            <div class="row mt-2-2">
                <div class="col-md-12">
                    <div id="result">
                        <?= DataTables::widget([
                          'dataProvider' => $dataProvider,
                          'clientOptions' => [
                              'lengthMenu' => [200],
                              'responsive' => true, 
                              'dom' => 'BfTrtip',
                              'buttons' => [
                                'csv','excel', 'pdf', 'print'
                                ]
                            ],
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'], 
                               
                                /*[
                                    'attribute' => 'fullname',
                                    'value' => function($data) { return $data->first_name  ." ". $data->last_name; }
                                ],*/
                                'stock_name',
                                'symbol', 
                                /*'share_price',
                                'no_of_shares',*/
                                [
                                    'attribute' => 'date',
                                    'value' => function ($model, $key, $index, $widget) { 
                                    return ($model->date!="0000-00-00")?(date("d-m-Y", strtotime($model->date))):"Not Set";
                                    },
                                ], 
                                [
                                    'attribute' => 'status',
                                    'value' => function ($data) { 
                                    return ($data->status=="2")?"Approved":(($data->status=="1")?"Pending for Approval":"Rejected");
                                    },
                                ], 
                                [
                                    'label'=> '#',
                                    'format'=> 'raw',
                                    'value' => function($model) { 
                                         
                                         if($model->status==2)
                                         {
                                            return  Html::a('<i class="fa fa-times-circle"></i>', ['stock/changestockstatus?id='.$model->id.'&flag=0&stock_id='.$model->stock_id],array('style'=>'padding:2px 10px','class'=>'btn btn-danger','data-confirm'=>'Are you sure you want to reject this stock?'))." ".Html::a('<i class="fa fa-envelope"></i>', ['stock/send-email?id='.$model['stock_id']],array('style'=>'padding:2px 10px','class'=>'btn btn-info'), ['data-toggle'=>"tooltip", 'data-original-title'=>"Send email"]);
                                         }
                                         else if($model->status==1)
                                         {
                                           return  Html::a('<i class="fa fa-check"></i>', ['stock/changestockstatus?id='.$model->id.'&flag=2&stock_id='.$model->stock_id],array('style'=>'padding:2px 10px','class'=>'btn btn-success','data-confirm'=>'Are you sure you want to approve this stock?'))." ".Html::a('<i class="fa fa-times-circle"></i>', ['stock/changestockstatus?id='.$model->id.'&flag=0&stock_id='.$model->stock_id],array('style'=>'padding:2px 10px','class'=>'btn btn-danger','data-confirm'=>'Are you sure you want to reject this stock?'));
                                         }
                                         else
                                         {
                                            return  Html::a('<i class="fa fa-check"></i>', ['stock/changestockstatus?id='.$model->id.'&flag=2&stock_id='.$model->stock_id],array('style'=>'padding:2px 10px','class'=>'btn btn-success','data-confirm'=>'Are you sure you want to approve this stock?'));
                                         }
                                    }
                                ],
                               
                         /*['class' => 'yii\grid\ActionColumn', 'template'=> '{update} {view}',
                                'visibleButtons' => [
                                   'update' => \Yii::$app->Permission->getAccessPermission(Yii::$app->controller->id,'update'),
                                  'view' => \Yii::$app->Permission->getAccessPermission(Yii::$app->controller->id,'view'),                                  
                            'delete' => \Yii::$app->Permission->getAccessPermission(Yii::$app->controller->id,'delete'),                                  
                             ], ],*/
                            ],
                      ]);?>
                    
                    </div>
                    <div id="resultdata" style="display:none">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<input type="hidden" id="status" value="-1"/>
