<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Stock */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Stocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<!-- <div class="box box-primary">
 -->    <!--<div class="box-header with-border">
         <div class="col-md-8">
            <h3 class="box-title"> <i class="fa fa-list"></i> Stock Summary</h3>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <?= Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-success', 'data-toggle'=>"tooltip", 'data-original-title'=>"Add New"]) ?>
            </div>
        </div>
    </div> -->
   
    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
            <div class="box-body box-profile">
             

              <h3 class="profile-username text-center"><?= $model->stock_name; ?></h3>

              <p class="text-muted text-center">Software Engineer</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Created At</b> <a class="pull-right"><?= date_format(date_create($model->created_at), 'd-m-Y')?></a>
                </li>
                <li class="list-group-item">
                  <b>Updated At</b> <a class="pull-right"><?= ($model->updated_at!="0000-00-00 00:00:00" && $model->updated_at!=NULL)?date_format(date_create($model->updated_at), 'd-m-Y'):"Not set"?></a>
                </li>
              </ul>
              <a href="#" class="btn btn-primary btn-block"><b>Close Stock</b></a>
            </div>
          </div>
        </div>
        <div class="col-md-9">
            
            <div class="panel-group wrap" id="bs-collapse">
                <?php $stock = \backend\models\Stock::find()->where(['stock_name' => $model->stock_name])->all();
                //print_r($stock);die;
                $i=0;
                foreach($stock as $list)
                {
                    $i++;
                    echo '<div class="panel">
                            <div class="panel-heading">
                              <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="" href="#'.$i.'">
                                  <strong>WEEK NO: </strong>'.$list->week_no.'<br /><strong>Date:</strong>'.date_format(date_create($list->date),'d-m-Y').'
                                </a>
                              </h4>
                            </div>
                            <div id="'.$i.'" class="panel-collapse collapse">
                              <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label class="control-label">No. of Shares:</label><br/>
                                            '.$list->no_of_shares.'
                                        
                                        </div>
                                        <div class="col-md-3">
                                            <label class="control-label">Share Price:</label><br/>
                                            '.$list->share_price.'
                                        
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label">Suggestion:</label><br/>
                                            '.$list->suggestion.'
                                        </div>
                                        
                                    </div>
                                    <br />
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            <label class="control-label">Actual Event:</label><br/>
                                            '.$list->actual_event.'
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="u-heading-v3-1 g-mb-40">
                                                <h2 class="text-uppercase h3 u-heading-v3__title g-brd-primary" style="margin-bottom:0">Weekly Data</h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label class="control-label">Open:</label><br/>
                                            '.$list->open.'
                                        </div>
                                        <div class="col-md-3">
                                            <label class="control-label">High:</label><br/>
                                            '.$list->high.'
                                        </div>
                                        <div class="col-md-3">
                                            <label class="control-label">Low:</label><br/>
                                            '.$list->low.'
                                        </div>
                                        <div class="col-md-3">
                                            <label class="control-label">Close:</label><br/>
                                            '.$list->close.'
                                        </div>
                                    </div>
                              </div>
                            </div>
                          </div>';
                    // echo '<div class="panel panel-default">
                    //           <div class="panel-heading" role="tab" id="headingOne">
                    //             <h4 class="panel-title">
                    //             <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    //               <strong>WEEK NO: </strong>'.$list->week_no.'<br /><strong>Date:</strong>'.date_format(date_create($list->date),'d-m-Y').'
                    //             </a>
                    //           </h4>
                    //           </div>
                    //           <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    //             <div class="panel-body">
                    //               Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird
                    //               on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table,
                                  
                    //             </div>
                    //           </div>
                    //         </div>';
                }
                ?>
            </div>
        </div>
    </div>

    
<!-- </div> -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.collapse.in').prev('.panel-heading').addClass('active');
        $("#bs-collapse")
            .on('show.bs.collapse', function(a) {
              $(a.target).prev('.panel-heading').addClass('active');
        })
        .on('hide.bs.collapse', function(a) {
          $(a.target).prev('.panel-heading').removeClass('active');
        });
    });
</script>