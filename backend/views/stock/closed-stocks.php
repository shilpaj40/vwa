<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\StockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Stocks';
$this->params['breadcrumbs'][] = $this->title;
$js = <<<SCRIPT
/* To initialize BS3 tooltips set this below */
$(function () { 
    $("[data-toggle='tooltip']").tooltip(); 
});;
/* To initialize BS3 popovers set this below */
$(function () { 
    $("[data-toggle='popover']").popover(); 
});
SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js);
?>
<div class="nav-tabs-custom">
  <ul class="nav nav-tabs">
    <li><a href="<?= \yii\helpers\Url::to(['stock/index'])?>" >Active Stocks</a></li>
    <li class="active"><a href="<?= \yii\helpers\Url::to(['stock/closed-stocks'])?>" >Closed Stocks</a></li>
   
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tab_1">
      <div class="box box-primary" style="border-top:0">
        <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title"> <i class="fa fa-list"></i> Closed Stock List</h3>
            </div>
            <div class="col-md-4">
                <div class="pull-right">
                    <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success', 'data-toggle'=>"tooltip", 'data-original-title'=>"Add New"]) ?>
                </div>
            </div>
        </div>
        <div id="smain">
          <div class="box-body">
              <?php $stockNAme = \backend\models\Stock::find()->where(['status' => 2, 'is_closed'=>1])->groupby(['stock_name'])->orderBy(['id'=>SORT_DESC])->all(); 
              $i=0;?>
              <input type="hidden" id="scount" value="<?= count($stockNAme);?>">
              <?php if(count($stockNAme)>0){
              foreach ($stockNAme as $stocks){ $i++;?>
              <div class="card mt-2-2">
                  <div class="card-header">
                    <div class="row">
                      <div class="col-md-7">
                        <h5 style="font-size:16px;"><strong><?= $stocks->stock_name;?></strong></h5>
                      </div>
                     
                    </div>
                  </div>
                  <div class="card-body">
                     <!-- <button class="btn btn-primary" id="submit_data">Submit</button> -->
                     <table class="table table-responsive-md table-sm table-bordered" id="makeEditable_<?= $i?>">
                        <thead>
                           <tr>
                              <!--<th>No. of Share</th>
                              <th>Share Price</th>-->
                              <!--<th>Week no. </th>-->
                              <!--<th>Weekend Date </th>-->
                              <th>Date </th>
                              <th>Suggestion </th>
                              <th>Actual Event </th>
                              <th>Action</th>
                              <th>Status</th>
                              <!--<th>Open </th>
                              <th>High </th>
                              <th>Low </th>
                              <th>Close </th>-->
                           </tr>
                        </thead>
                        <tbody>
                          <?php
                          $stock = \backend\models\Stock::find()->where(['status' => 2, 'is_closed'=>1, 'stock_name'=>$stocks->stock_name])->orderBy(['id'=>SORT_DESC])->all();
                           foreach($stock as $list){
                              echo "<tr >";
                                  //echo "<td id='no_of_shares-".$list->id."'>".$list->no_of_shares."</td>";
                                  //echo "<td id='share_price-".$list->id."'>".$list->share_price."</td>";
                                  //echo "<td id='week_no-".$list->id."'>".$list->week_no."</td>";
                                  //echo "<td id='date-".$list->id."'>".$list->date."</td>";
                                  echo "<td id='date-".$list->id."'>".date("d-m-Y",strtotime($list->date))."</td>";
                                  echo "<td id='suggestion-".$list->id."'>".$list->suggestion."</td>";
                                  echo "<td id='actual_event-".$list->id."'>".$list->actual_event."</td>";
                                  /*echo "<td id='open-".$list->id."'>".$list->open."</td>";
                                  echo "<td id='high-".$list->id."'>".$list->high."</td>";
                                  echo "<td id='low-".$list->id."'>".$list->low."</td>";
                                  echo "<td id='close-".$list->id."'>".$list->close."</td>";*/
                                  echo "<td id='action-".$list->id."'>".$list->action."</td>";
                                    echo "<td class='statusbtn' id='status-".$list->id."'>";
                                    if($list->updatesapproval==1)
                                      {
                                        echo "Updated data pending for Approval";
                                      }
                                      else
                                      {
                                         if($list->status==2)
                                        echo "Approved";
                                      else if($list->status==1)
                                        echo "Pending for Approval";
                                      else
                                        echo "Rejected";
                                    }
                                  echo "</td>";
                              echo "</tr>";
                          }?>
                          
                        </tbody>
                     </table>
                     <!-- <span style="float:right"><button id="but_add_<?= $i?>" class="btn btn-danger">Add New Row</button></span> -->
                  </div>
              </div>
              <?php }
              }?>
             
          </div>
        </div>
       
      </div>
      
    </div>
    <!-- /.tab-pane -->
   
  </div>
  <!-- /.tab-content -->
</div>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script>
   
    $(function() {
      var count=$("#scount").val();
      var j=0;
      // for(var i=0;i<count;i++)
      // {
      //   j++;
      //   $('#makeEditable_'+j).SetEditable({ $addButton: $('#but_add_'+j)});
      // }
    
  });
  
</script>
