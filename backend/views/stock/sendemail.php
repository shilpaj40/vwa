<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CouponSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Send Mail';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = ['label' => $model->stock_name, 'url' => ['view', 'id' => $model->id]];

$js = <<<SCRIPT
/* To initialize BS3 tooltips set this below */
$(function () { 
    $("[data-toggle='tooltip']").tooltip(); 
});;
/* To initialize BS3 popovers set this below */
$(function () { 
    $("[data-toggle='popover']").popover(); 
});
SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js);
?>
<?php
$this->registerJs('
$(document).ready(function(){

$(\'#MyButton\').click(function(){
if (confirm("Are you sure you want to delete this item/s?")) {
    var HotId = $(\'#w0\').yiiGridView(\'getSelectedRows\');
    //alert(HotId);
    $.ajax({
    type: \'POST\',
    url : \'multipledelete\',
    data : {row_id: HotId},
    success : function(response) {
        alert(response);
        $(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
    }
    });
}
else
{

}
});
});', \yii\web\View::POS_READY);
?>
<script src="<?php echo Yii::$app->params['image']?>js/ckeditor/ckeditor.js"></script>

<div class="coupon-index">
	<div class="box box-primary">

      <div class="box-header with-border">
        <div class="col-md-8">
            <h3 class="box-title"> <i class="fa fa-list"></i> User List</h3>
        </div>
       
      </div>
      <!-- /.box-header -->

      <div class="box-body">
      	<div class="row">
      		<div class="col-md-12">
      			<?= GridView::widget([
		            'dataProvider' => $dataProvider,
		            'filterModel' => $searchModel,
		            'columns' => [
		                ['class' => 'yii\grid\SerialColumn'],

		               [
		                  'attribute'=>'user_id',
		                   'format' => 'raw',
		                   'filter' => false,
		                  'value'=>function($model) {
		                  	return Html::a(getUsername($model->user_id),['/coupon/view', 'id' => $model->id]);
		                  },
		                ],
		              
		               
		                [
		                  'attribute'=>'created_at',
		                  'label'=>'Email sent on',
		                  'filter' => false,
		                  'value'=>function($model) {
		                    return ($model->created_at!=NULL)? date_format(date_create($model->created_at),'d-m-Y H:i:s'):"";
		                  },
		                ],
		                
		               
		                [
		                    'format' => 'raw',
		                    'value' => function ($model) {
		                    	return getButton($model->stock_id, $model->user_id);
		                      
		                    },
		                ],
		            ],
		        ]); ?>

      		</div>
      	</div>
        
      </div>
      

    </div>
   <div class="box box-primary">

      <div class="box-header with-border">
        <div class="col-md-8">
            <h3 class="box-title"> <i class="fa fa-envelope"></i> Send email to users for <?= $model->stock_name; ?></h3>
        </div>
       
      </div>
      <!-- /.box-header -->
      <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

      <div class="box-body">
      	<div class="row">
      		<div class="col-md-12">
      			
      			<div class="form-group">
      				<div class="row">
      					<div class="col-md-4">
		      				<?php  
		      				  $Userlist = \app\models\User::find()
		                     ->select(['user.id', 'user.first_name as name',])
		                     ->join('LEFT OUTER JOIN', 'stockmails AS ML', 'user.id = ML.user_id')
		                     ->Where('user.user_type = :user_type', [':user_type' => 2])
		                     ->andWhere('user.status = :status', [':status' => 1])->asArray()->all();
		                    $listData=ArrayHelper::map($Userlist,'id','name');
		      				echo $form->field($stockmails, 'user_id')->widget(Select2::classname(), [
		                        'data' => $listData,
		                        'options' => ['placeholder' => '-- Select --','multiple'=>"multiple"],
		                        'pluginOptions' => [
		                                        'allowClear' => true
		                                    ],
		                    ]);?>
		                </div>
		                <div class="col-md-8">
		                	<?php echo $form->field($stockmails, 'subject')->textInput(['class'=>'form-control', 'value'=>'Company Name : '.$model->stock_name]) ?>
		                </div>
      				</div>
      				<div class="row">
      					<div class="col-md-12">
      						<?php echo $form->field($stockmails, 'email_body')->textArea(['class'=>'form-control']) ?>
      					</div>
      				</div>
      				
      			</div>


      		</div>
      	</div>
        
      </div>
      <div class="box-footer">
          <?= Html::submitButton('Send', ['id'=>'submit_lower','class' => 'btn btn-success']) ?>
          <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
      </div>
       <?php ActiveForm::end(); ?>

    </div>
</div>
<?php
function getUsername($id)
{
	$User= \app\models\User::find()->where(['status'=>1, 'id'=>$id])->one();
	return $User->first_name;
}
function getButton($stock_id, $user_id)
{

	$User= \backend\models\Stock::find()->where(['status'=>2, 'id'=>$stock_id])->one();
	if(!empty($User))
	{
		return Html::a('<i class="fa fa-envelope"></i> Resend', ['stock/resend-email', 'id'=>$stock_id, 'user_id'=>$user_id], ['data-toggle'=>"tooltip", 'data-original-title'=>"Resend email", "class"=>"btn btn-primary"]); 
	}
	else
	{
		return "<span class='label bg-danger'>Not Approved</span>";		
	}

}
?>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
CKEDITOR.replace('couponhistory-email_body', {
          height: 100,

        // set toolbar that you want to show
     removeButtons: 'PasteText,PasteFromWord,SpecialChar,Styles,About,SpellChecker',
    toolbar: [
        { name: 'basicstyles', items: [ 'Source','Bold', 'Underline',  'Table', 'Cut','Copy','Paste','Link','Unlink'] },
        { name: 'styles', items: [ 'Format'] },

    ],
    toolbarGroups: [
        { name: 'group1', groups: [ 'basicstyles' ] },
    ],
    removePlugins: 'elementspath',
    removeDialogTabs: 'link:advanced;image:Link;image:advanced',
            } );
$("#submit_lower").mousedown(function(){
  for (var i in CKEDITOR.instances){
    CKEDITOR.instances[i].updateElement();
  }
});
$("#submit_upper").mousedown(function(){
  for (var i in CKEDITOR.instances){
    CKEDITOR.instances[i].updateElement();
  }
});
</script>

