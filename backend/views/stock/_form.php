<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model backend\models\Stock */
/* @var $form yii\widgets\ActiveForm */
$sunday=date("d-m-Y", strtotime('sunday this week'));
//echo date("Y-m-d", strtotime('sunday this week')), "\n";
?>

<div class="box box-primary">
    <div class="box-body">
        <div class="stock-form">
            <?php $form = ActiveForm::begin(); ?>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'stock_name')->textInput(['maxlength' => true]) ?>
                </div>
                <!-- <div class="col-md-4">
                    <?= $form->field($model, 'week_no')->textInput(['value'=>date('W'), 'readonly'=>true]) ?>
                </div> -->
                <div class="col-md-4">
                    <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
                          'name' => 'date', 
                          'options' => [//'placeholder' => 'Select issue date ...'
                          //'onchange'=>'return getEnddate(this)',
                          'value'=>date('d-m-Y'),
                          'readonly'=>true
                           ],
                          'pluginOptions' => [
                            'format' => 'dd-mm-yyyy',
                            'todayHighlight' => true
                          ]
                    ]);?>
                </div>
           <!--  </div>
            <div class="row"> -->
                 <div class="col-md-4">
                     <?= $form->field($model, 'symbol')->textInput(['maxlength' => true]) ?>
                </div>
                <?php /*
                <div class="col-md-4">
                    <?= $form->field($model, 'no_of_shares')->textInput() ?>
                </div>

                <div class="col-md-4">
                    <?= $form->field($model, 'share_price')->textInput(['maxlength' => true]) ?>
                </div>

                */?>

                 <div class="col-md-4">
                     <?= $form->field($model, 'suggestion')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4">
                     <?= $form->field($model, 'actual_event')->textInput(['maxlength' => true]) ?>
                </div>
                 <div class="col-md-4">
                     <?= $form->field($model, 'action')->textInput(['maxlength' => true]) ?>
                </div>
                
            </div>            
            <div class="row">
               
            </div>
           <!--  <div class="row">
                <div class="col-md-12">
                    <div class="u-heading-v3-1 g-mb-40">
                        <h2 class="text-uppercase h3 u-heading-v3__title g-brd-primary">Weekly Data</h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'open')->textInput() ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'high')->textInput() ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'low')->textInput() ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'close')->textInput() ?>
                </div>
            </div> -->
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-black']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

