<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\models\RoleMaster;

$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
function getRole($role_id)
{

    $role="";
    $Role=RoleMaster::findOne($role_id);
    if($Role!="")
        $role=$Role->role_name;
    return $role;
}
?>
<div class="row gutters-sm">
  <div class="col-md-4 mb-3">
    <div class="card">
      <div class="card-body">
        <div class="d-flex flex-column align-items-center text-center">
          <?php if($model->profile_pic=="" && $model->profile_pic==null) {
            echo '<img src="'.Yii::$app->params['image'].'images/user.jpg" alt="Admin" class="rounded-circle" width="150">';
            }
            else
              {
                echo '<img src="'.$model->profile_pic.'" alt="Admin" class="rounded-circle" width="150">';
              }?>
          <div class="mt-3">
            <h4><?= $model->first_name?></h4>
            <p class="text-secondary mb-1"><?= getRole($model->user_type)?></p><br />
             <a class="btn btn-primary" href="<?= \yii\helpers\Url::to(['site/edit-profile','id'=>Yii::$app->user->identity->id])?>"><i class="fa fa-pencil"></i> Edit Profile</a>
          </div>
        </div>
      </div>
    </div>
   
  </div>
  <div class="col-md-8">
    <div class="card mb-3">
      <div class="card-body">
        <div class="row">
          <div class="col-sm-3">
            <h6 class="mb-0">Full Name</h6>
          </div>
          <div class="col-sm-9 text-secondary">
            <?= $model->first_name?> <?= $model->last_name?>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-sm-3">
            <h6 class="mb-0">Email</h6>
          </div>
          <div class="col-sm-9 text-secondary">
            <?= $model->email?>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-sm-3">
            <h6 class="mb-0">Mobile</h6>
          </div>
          <div class="col-sm-9 text-secondary">
            <?= $model->mobile;?>
          </div>
        </div>
        <hr>
         <div class="row">
          <div class="col-sm-3">
            <h6 class="mb-0">City</h6>
          </div>
          <div class="col-sm-9 text-secondary">
            <?= $model->city;?>
          </div>
        </div>
        <hr>
         <div class="row">
          <div class="col-sm-3">
            <h6 class="mb-0">Gender</h6>
          </div>
          <div class="col-sm-9 text-secondary">
           <?= ($model->gender==1)?"Male":"Female"; ?>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-sm-3">
            <h6 class="mb-0">Birth Date</h6>
          </div>
          <div class="col-sm-9 text-secondary">
            <?= (!empty($model->birth_date))?date_format(date_create($model->birth_date),'d-m-Y'): "Not set";?>
          </div>
        </div>
        <hr>
        
        
      </div>
    </div>
  </div>
</div>
       

<style type="text/css">
  .nav{display:block;}

</style>