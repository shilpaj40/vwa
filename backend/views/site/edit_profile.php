<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\models\RoleMaster;
use yii\helpers\Url;
use kartik\date\DatePicker;
$model->birth_date=date("d-M-Y",strtotime($model->birth_date));
/* @var $this yii\web\View */
/* @var $model backend\models\Role */
/* @var $form yii\widgets\ActiveForm */
if( \Yii::$app->session->hasFlash('msg'))
        echo \Yii::$app->session->getFlash('msg');
?>
<div class="role-form">
    <div class="box box-primary">
        <div class="box-body">
    <?php $form = ActiveForm::begin(['enableClientValidation' => false]); ?>
    <div class="row">
     <div class="col-md-4">       

                 <?php
                 $roles=RoleMaster::find()->where(['status' => 1])->all();
                $listData=ArrayHelper::map($roles,'role_id','role_name');
             echo $form->field($model, 'user_type')->widget(Select2::classname(), [
                    'data' => $listData,
                    'options' => ['placeholder' => '-- Select --'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ;?>
        </div>
  
      <div class="col-md-4">
                <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
      </div>
      <div class="col-md-4">
                <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
      </div>
      <div class="col-md-4">
        <?= $form->field($model, 'email')->textInput(['maxlength' => true,'onfocusout'=>"checkUser()();",'readonly'=>true]) ?>
      </div>
      <div class="col-md-4">
                <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
      </div> 
       <div class="col-md-4">
                <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
      </div>      
      
      <div class="col-md-4 radiobtn">
                    <?php $list = ["1"=>"Male", "2"=>"Female"];
                    echo $form->field($model, 'gender')->radioList($list); ?>
                </div> 
       
      <div class="col-md-4">
        <label>Date Of Birth</label>
                    <?= DatePicker::widget([
                                'model'=>$model,
                                'attribute' => 'birth_date',
                                'value' => date('d-M-Y', strtotime('+2 days')),
                                'options' => ['placeholder' => 'Select date ...'],
                                'pluginOptions' => [
                                    'format' => 'dd-M-yyyy',
                                    'todayHighlight' => true
                                ]
                            ]); ?>
                </div>
           <div class="col-md-4">
                <?= $form->field($model, 'profile_pic')->fileInput() ?>
            </div>
            
                <?php
                if($model['profile_pic']!=""){
                    echo "<div class='col-md-4'>";
                    echo "<label>Uploaded Profile Pic</label>";
                 echo "<img src='".$model->profile_pic."' class='profile-user-img role-name img-responsive img-circle' onerror='this.src=\"".Yii::$app->params['image']."images/user.jpg\"'  alt='image' style='height:75px'>";
                 echo "</div>";
                }
           ?>
      
</div>
       <div class="form-group text-center">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success mt-2-2']) ?>
       </div>
 
<?php ActiveForm::end(); ?>

</div>
</div>
</div>
