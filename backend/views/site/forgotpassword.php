<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Forgot Password';

?>

<div class="login-box">
    <div class="login-logo">
        <a href="#"><img src="<?= Yii::$app->params['logo']?>" /></a>
    </div>
    <!-- /.login-logo -->
    <div id="om-lightbox-case-study-optin-bar" class="boston-clearfix boston-optin-bar om-clearfix"><span class="boston-bar boston-yellow-bar om-bar om-red-bar"></span><span class="boston-bar boston-black-bar om-bar om-blue-bar"></span><span class="boston-bar boston-yellow-bar om-bar om-red-bar"></span><span class="boston-bar boston-black-bar om-bar om-blue-bar"></span></div>
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?= $form->field($model,'username',['inputOptions'=>[
            'placeholder'=>'Username'
        ]])->textInput() ?>
       
        <?= $form->field($model,'newpass',['inputOptions'=>[
            'placeholder'=>'New Password'
        ]])->passwordInput() ?>
       
        <?= $form->field($model,'repeatnewpass',['inputOptions'=>[
            'placeholder'=>'Repeat New Password'
        ]])->passwordInput() ?>

        <div class="row">
           
            <div class="col-xs-12">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
                <?= Html::a(
                                    'Back to login',
                                    ['site/login'],
                                    ['class' => 'btn btn-primary btn-block btn-flat']
                                ) ?>
            </div>
            <!-- /.col -->
        </div>

        <?php ActiveForm::end(); ?>
         
    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
<style type="text/css">
    .login-page, .register-page {
    height: auto;
    background: #d2d6de;
    display: block;
    position: fixed;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    background: url(<?= Yii::$app->params['image']?>images/bg.png) transparent;
    background-size: auto 100%;
    z-index: -1;
}
.btn-primary {
    background-color: #e7a54f!important;
    border-color: #e7a54f!important;
}
.btn-primary:hover {
    background-color: #fff!important;
    border-color: #e7a54f!important;
    color: #e7a54f;
}
.boston-optin-bar {
    margin-bottom: 20px;
}
.boston-yellow-bar {
    background-color: #e7a54f;
}
.boston-bar {
    float: left;
    width: 25%;
    height: 11px;
    display: block;
}
.boston-black-bar {
    background-color: #191919;
}
</style>