<?php 
	$this->title = 'Access Denied';

?>
<div class="site-error">

    <div class="alert alert-danger">
        <span class="fa fa-warning"></span> Access is denied!
    </div>

    <p>
        You don't have permission to access this module.
    </p>
    <p>
        Please refer to your system administrator. Thank you.
    </p>

</div>