<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset your password';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset">
    <main id="page-main" role="main">
        <div class="container">
           <a id="main-content" tabindex="-1"></a>
           <div class="row">
              <div class="col-lg-12">
                 <div id="page-content">
                    <div class="content">
                       <div>
                          <div id="block-shelley-local-tasks" class="block">
                             <ul class="nav nav-tabs my-3">
                                <li class="nav-item"><a href="<?= yii\helpers\Url::to(["site/login"])?>" class="nav-link">Log in</a></li>                                
                                <li class="nav-item"><a href="<?= yii\helpers\Url::to(["site/request-password-reset"])?>" class="nav-link active is-active">Reset Your Password<span class="visually-hidden"></span></a></li>
                             </ul>
                          </div>
                          <?php if (Yii::$app->session->hasFlash('success')): ?>
                              <div class="alert alert-success alert-dismissable">
                                   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                   <?= Yii::$app->session->getFlash('success') ?>
                              </div>
                          <?php endif; ?>

                          <?php if (Yii::$app->session->hasFlash('error')): ?>
                              <div class="alert alert-danger alert-dismissable">
                                   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                   <h4><i class="icon fa fa-close"></i>Error!</h4>
                                   <?= Yii::$app->session->getFlash('error') ?>
                              </div>
                          <?php endif; ?>
                          <div class="mt-3 mb-3">
                          <div id="block-pagetitle" class="block  ">
                             <h1><?= $this->title; ?></h1>
                          </div>
                          <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                             <div class="form-group js-form-item form-item js-form-type-textfield form-item-name js-form-item-name">
                                
                                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
                             </div>
                             <p>Password reset instructions will be sent to your registered email address.</p>
                             
                             <div data-drupal-selector="edit-actions" class="form-actions js-form-wrapper form-wrapper" id="edit-actions"><?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?></div>
                          <?php ActiveForm::end(); ?>
                       </div>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
    </main>
</div>
