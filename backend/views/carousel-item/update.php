<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CarouselItem */

$this->title = 'Update Carousel Item';
$this->params['breadcrumbs'][] = ['label' => 'Carousel Item', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="carousel-item-update">

    <?= $this->render('_form_update', [
        'model' => $model,
    ]) ?>

</div>
