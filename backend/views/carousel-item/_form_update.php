<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CarouselItem */
/* @var $form yii\widgets\ActiveForm */
$User=\app\models\User::find()->where(['id' =>  $model->created_by])->one();
$authorname=$User->first_name." ".$User->last_name;
?>

<div class="carousel-item-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="box box-primary">
      <div class="box-header with-border">
        <div class="col-md-8">
            <h3 class="box-title"> <i class="fa fa-pencil"></i> Add Carousel Item</h3>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>
        </div>
       
      </div>
      <!-- /.box-header -->
      <!-- form start -->

        <div class="box-body">
            <div class="row">
                <div class="col-md-7">
                    <div class="form-group">
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'class'=>'form-control']) ?>
                    </div>
                    
                   
                    <div class="form-group">
                        <details class="claro-details" open="">    <summary role="button" aria-expanded="true" aria-pressed="true" class="claro-details__summary js-form-required form-required">Image<span class="required-mark"></span></summary><div class="claro-details__wrapper details-wrapper">
    
                              <div class="js-form-item form-item js-form-type-managed-file form-type--managed-file js-form-item-field-image-0 form-item--field-image-0">
                              <label for="edit-field-image-0-upload" id="edit-field-image-0--label" class="form-item__label js-form-required form-required">Add a new file</label>
                                <div class="image-widget js-form-managed-file form-managed-file form-managed-file--image is-single has-upload no-value no-meta">
                                  <div class="form-managed-file__main">
                                    <?= $form->field($model, 'image')->fileInput(['class'=>'form-element', 'onchange'=>'validateImage(this.id)'])->label(false) ?>
                                   
                                  </div>

                                </div>

                                    <div id="edit-field-image-0--description" class="form-item__description">
                              One file only.<br>2 MB limit.<br>Allowed types: png gif jpg jpeg.
                              <br>Recommended Image Size: 962x482

                            </div>
                          </div>
                          </div>
                        </details>
                       
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'body')->textArea([ 'class'=>'form-control', 'rows'=>9]) ?>
                        <div id="edit-body-0--description" class="form-item__description">Text shown over the image.</div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="entity-meta accordion js-form-wrapper form-wrapper" id="edit-advanced">
                       <div class="entity-meta__header accordion__item js-form-wrapper form-wrapper" id="edit-meta">
                          <div class="entity-meta__last-saved js-form-item form-item js-form-type-item form-type--item js-form-item-meta-changed form-item--meta-changed" id="edit-meta-changed">
                            <label for="edit-meta-changed" class="form-item__label">Carousel Preview</label>
                            <div class="layout__region layout__region--first">
                              <div class="block-with-background block">

                                <figure>
                                    
                                  <div class="field field--name-field-image field--type-entity-reference field--label-hidden field__item">  
                                    <img src="<?= $model->image?>" id="preview_profile_photo" name="preview_profile_photo">
                                  </div>
                                  <figcaption>
                                    <div class="container">
                                      <div class="field field--name-body field--type-text-with-summary field--label-hidden field__item"><div id="banner_heading"></p></div>
                                    </div>
                                  </figcaption>
                                </figure>
                              </div>
                            </div>
                            
                          </div>
                          <div class="entity-meta__last-saved js-form-item form-item js-form-type-item form-type--item js-form-item-meta-changed form-item--meta-changed" id="edit-meta-changed">
                             <label for="edit-meta-changed" class="form-item__label">Last saved</label>
                             <?= ($model->updated_at==NULL && empty($model->updated_at))?date_format(date_create($model->created_at),'d-m-Y H:i:s') : date_format(date_create($model->updated_at),'d-m-Y H:i:s');?>
                          </div>
                          <div class="entity-meta__author js-form-item form-item js-form-type-item form-type--item js-form-item-meta-author form-item--meta-author" id="edit-meta-author">
                            <label for="edit-meta-author" class="form-item__label">Author</label>

                             <?= $authorname;?>
                          </div>
                          <div class="entity-meta__author js-form-item form-item js-form-type-item form-type--item js-form-item-meta-author form-item--meta-author" id="edit-meta-author">
                            <label for="edit-meta-author" class="form-item__label">Creation Date</label>
                             <?= $model->created_at;?>
                          </div>
                          
                       </div>
                       
                      
                    
                    </div>
                </div>
            </div>
            
          
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
          <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
      
    </div>
   
       <?php ActiveForm::end(); ?>

</div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

<script type="text/javascript">

 function validateImage(id) {

    //Get reference of FileUpload.
    var fileUpload = document.getElementById(id);

    //Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif|.jpeg)$");
    if (regex.test(fileUpload.value.toLowerCase())) {
 
        //Check whether HTML5 is supported.
        if (typeof (fileUpload.files) != "undefined") {
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(fileUpload.files[0]);
            var file = fileUpload.files[0];
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();
 
                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;
                       
                //Validate the File Height and Width.
                image.onload = function () {
                    if (Math.round(file.size / (1024 * 1024)) > 2) { // make it in MB so divide by 1024*1024
                    alert('Please select image size less than 2 MB');
                    return false;

                    }
                    else
                    {
                       var height = this.height;
                        var width = this.width;
                    //     if (height !=345 && width != 1170) {
                    //         alert("Image size should be 1170X345");
                    //         fileUpload.value="";
                    //         return false;
                              
                    // } 
                    $('#preview_profile_photo').attr('src', e.target.result);

                    return true;
                    }
                    
                   
                };
            }
        } else {
            alert("This browser does not support HTML5.");
            fileUpload.value="";
            return false;
        }
    } else {
        alert("Please select a valid Image file.");
            fileUpload.value="";
        return false;
    }
}
// $(function(){
//   $("#carouselitem-body").change(function()
//   {
//     var key=$("#carouselitem-body").val();
//     $("#banner_heading").html(key);
//   });
// });

</script>