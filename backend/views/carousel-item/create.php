<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CarouselItem */

$this->title = 'Add Carousel Item';
$this->params['breadcrumbs'][] = ['label' => 'Carousel Item', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carousel-item-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
