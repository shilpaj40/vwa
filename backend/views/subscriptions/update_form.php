<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Resources */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="resources-form">

    <div class="box box-primary">
        <div class="box-header with-border">
        <div class="col-md-6">
            <h3 class="box-title"> <i class="fa fa-list"></i> Update Subcriptions Plan </h3>
        </div>

        <div class="col-md-4">
            <div class="pull-right">
                
            </div>
        </div>
        
      </div>
        <div class="box-body">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'script_per_week')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'one_month')->textInput(['maxlength' => true]) ?>
                </div>
                
                <div class="col-md-3">
                    <?= $form->field($model, 'three_month')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'twelve_month')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            
            <div class="form-group">
                <center>
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
                </center>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
