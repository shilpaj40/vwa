<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CarouselItem */
/* @var $form yii\widgets\ActiveForm */
$model->created_by = Yii::$app->user->id;
$User=\app\models\User::find()->where(['id' =>  Yii::$app->user->id])->one();
$authorname=$User->first_name." ".$User->last_name;
$model->created_at = date('Y-m-d H:i:s');
?>


<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<head>
<style type="text/css">
 .table>tfoot>tr>td {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 0px solid #ddd !important;
}
</style>
</head>

<div class="carousel-item-index">
     <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="box box-primary">

        <div class="box-header with-border">
        <div class="col-md-6">
            <h3 class="box-title"> <i class="fa fa-list"></i> Add Subcriptions Plan </h3>
        </div>

        <div class="col-md-4">
            <div class="pull-right">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>
        </div>
        
      </div>
<div class="box-body">
<div class="container">
    <table id="myTable" class="table order-list" style="border: 1;">
    <thead>
        <tr>
           
            <td>No. of Scripts Per Week</td>
            <td>1 Month</td>
            <td>2 Month</td>
            <td>3 Month</td>

        </tr>
    </thead>
    <tbody>
        <div class="row">
        <tr>
            
            
            <td class="col-md-2">
                
                        <?= $form->field($model, 'script_per_week')->textInput(['maxlength' => true, 'class'=>'form-control','id'=>'script_per_week'])->label(false) ?>
                         <!-- <input type="mail" name="script_per_week"  class="form-control"/> -->
                    
            </td>
            <td class="col-md-2">

                       <?= $form->field($model, 'one_month')->textInput(['maxlength' => true, 'class'=>'form-control','id'=>'one_month'])->label(false) ?> 
               <!-- <input type="mail" name="one_month"  class="form-control"/> -->
            </td>
            <td class="col-md-2">
                 <?= $form->field($model, 'two_month')->textInput(['maxlength' => true, 'class'=>'form-control','id'=>'two_month'])->label(false) ?>
                <!-- <input type="text" name="two_month"  class="form-control"/> -->
            </td>
            <td class="col-md-2">
                 <?= $form->field($model, 'three_month')->textInput(['maxlength' => true, 'class'=>'form-control','id'=>'three_month'])->label(false) ?>
                <!-- <input type="text" name="three_month"  class="form-control"/> -->
            </td>
            <input type="hidden" name="rowcount" id="rowcount" value="1" />
            <td class="col-md-2"><a class="deleteRow"></a>

            </td>
        
        </tr>
        </div>
    </tbody>
    <tfoot>
        <tr><center>
            <td  style="text-align: left;">
               <!--  //<input type="button" class="btn btn-primary btn-block " id="addrow" value="Add Row" />
                 -->
                  <?= Html::Button('Add Row', ['class' => 'btn btn-success','id'=>"addrow"]) ?>
            </td>

        </center>
        </tr>
        <tr>
        </tr>
    </tfoot>
</table><center>
<div class="box-footer">
          <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
          <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div></center>
         <?php ActiveForm::end(); ?>
</div>




<script src='jquery-1.9.1.min.js'></script>
<script>
    $(document).ready(function () {
    var counter = 0;
     var rowcount=$('#rowcount').val();
    $("#addrow").on("click", function () {
       // var newRow = $("<tr id='+'>");
       
       alert(rowcount);
        var cols = "";
        // var script_per_week= $("#script_per_week").val();
        //  var one_month= $("#one_month").val();
        //  var two_month=$("#two_month").val();
        //  var three_month=$("#three_month").val();
        //  alert(script_per_week);
      
        cols += '<tr id="'+rowcount+'"><td><input type="text" class="form-control" name="script_per_week" id="script_per_week' + counter + '"/></td>';
        cols += '<td><input type="text" class="form-control" name="one_month" id="one_month' + counter + '"/></td>';
        cols += '<td><input type="text" class="form-control" name="two_month" id="two_month' + counter + '"/></td>';
        cols += '<td><input type="text" class="form-control" name="three_month" id="three_month' + counter + '"/></td>';

        cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td></tr>';
        rowcount=parseInt(rowcount)+1;
        $("#rowcount").val(rowcount);
       // newRow.append(cols);
        $("tbody").append(cols);
        // $('#mytable tbody>tr:last').clone(true).insertAfter('#mytable tbody>tr:last');
        counter++;
    });



    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();       
        counter -= 1
    });


});




function calculateRow(row) {
    var price = +row.find('input[name^="price"]').val();

}

function calculateGrandTotal() {
    var grandTotal = 0;
    $("table.order-list").find('input[name^="price"]').each(function () {
        grandTotal += +$(this).val();
    });
    $("#grandtotal").text(grandTotal.toFixed(2));
}
    </script>

    </div></div></div>