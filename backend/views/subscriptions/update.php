<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Subscriptions */

$this->title = 'Update Subscriptions';
$this->params['breadcrumbs'][] = ['label' => 'Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="subscriptions-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1>
 -->
    <?= $this->render('form_update', [
        'model' => $model,
    ]) ?>

</div>
