<!DOCTYPE html>
    <html lang="en">
    <head>
    <title>jQuery Dynamic Rows</title>
    <meta charset="utf-8">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script>
    $(document).ready(function(){

        $("#add").on("click",function(){

          var rowcount = $("#rowcount").val();
          var row = '<tr class="dynamic" id="'+rowcount+'"><td>'+$("#script_per_week").val()+'</td><td>'+$("#one_month").val()+'</td><td>'+$("#two_month").val()+'</td><td>'+$("#three_month").val()+'</td></tr>'; 

           rowcount = parseInt(rowcount)+1;

           $("#rowcount").val(rowcount);
           $("#dataTab").append(row);
           $("#dataTab").show();
           $("#submit").show();            

        });

        $("#submit").on("click",function(){
                alert("submit");
                var jsonObj = [];
                i=0;
            $("#dataTab tr.dynamic").each(function(){

               var td = $(this).find('td');

                script_per_week = td.eq(0).text();
                one_month = td.eq(1).text();
                two_month = td.eq(2).text();
                three_month = td.eq(3).text();

                jsonObj.push({
                    script_per_week: script_per_week,
                    one_month: one_month,
                    two_month: two_month,
                    three_month: three_month,
                });
            });

            var dataString = JSON.stringify(jsonObj);


                $.ajax({
                    url: "submit.php",
                    type: "POST",
                    data: {json:dataString},
                    success: function(response){
                               alert(response);
                             }
                });

        });

    });

</script>
    </head>

    <body>


     <form name="jqtest" action="#">

      script_per_week: <input type="text" name="script_per_week"  id='script_per_week'  class="form-control"/><br/><br/>
       one_month:<input type="text" name="one_month[]" id='one_month' class="form-control"/><br/><br/>
       two_month:<input type="text" name="two_month[]" id='two_month' class="form-control"/><br/><br/>
       three_month :<input type="text" name="three_month[]" id='three_month' class="form-control"/><br/><br/>

       <p> <input type="reset" name="reset" id="reset" value="RESET"/>&nbsp;&nbsp;<input type="button" name="add" id="add" value="ADD"/> </p>
        <input type="hidden" name="rowcount" id="rowcount" value="1"/>
     </form>

     <table id="dataTab" style="display:none;" border="1">

      <tr>
        <th>script_per_week</th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
      </tr>

     </table>

    <p> <input style="display:none;" type="button" name="submit" id="submit" value="submit"/> </p>

    </body>

    </html>