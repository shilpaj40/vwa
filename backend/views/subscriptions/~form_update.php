<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\CarouselItem */
/* @var $form yii\widgets\ActiveForm */
$model->created_by = Yii::$app->user->id;
$User=\app\models\User::find()->where(['id' =>  Yii::$app->user->id])->one();
$authorname=$User->first_name." ".$User->last_name;
$model->created_at = date('Y-m-d H:i:s');

$query=(new \yii\db\Query())->select('*')->from('subscriptions');
$command = $query->createCommand();
 $subscriptions = $command->queryAll();
 $cnt=count($subscriptions);
 //print_r($subscriptions);die;
//echo count($subscriptions);die //9
?>


<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<head>
<style type="text/css">
 .table>tfoot>tr>td {
    padding: 20px !important;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 0px solid #ddd !important;
    text-align: center;
}
th {
    text-align: center !important;
}

</style>
</head>

<div class="carousel-item-index">
    
    <div class="box box-primary">

        <div class="box-header with-border">
        <div class="col-md-6">
            <h3 class="box-title"> <i class="fa fa-list"></i> Add Subcriptions Plan </h3>
        </div>

        <div class="col-md-4">
            <div class="pull-right">
                
            </div>
        </div>
        
      </div>
<div class="box-body">
<!-- <div class="container"> -->
  <?php $form = ActiveForm::begin(); ?>
    <table id="myTable" class="table order-list" style=" border-style:solid;
  border-collapse: collapse;">
    <thead>
        
        <tr>
            <th colspan="6">Subcription Plan</th>
        </tr>

        <tr>
          

          <th>No. of Scripts Per Week</th>
            <th>1 Month</th>
            <th>3 Month</th>
            <th>12 Month</th>

        </tr>
    </thead>
    
    <tbody>
        
        <div class="row">
            <input type="hidden" name="rowcount" id="rowcount" value="<?php echo count($subscriptions);?>" />
            <?php for($i=0;$i<$cnt;$i++){?>
        <tr>
            
            
            <td class="col-md-2">
                
                        <input type="hidden" class="form-control" name="id<?php echo $i;?>" id="id<?php echo $i;?>" value="<?php echo $subscriptions[$i]['id'];?>" />

            <input type="text" name='script_per_week<?php echo $i;?>'  id='script_per_week<?php echo $i;?>'  class="form-control" value="<?php echo $subscriptions[$i]['script_per_week'];?>" required/>
                    
            </td>
            <td class="col-md-2">

                      
               <input type="text" name="one_month<?php echo $i;?>" id='one_month<?php echo $i;?>' class="form-control" value="<?php echo $subscriptions[$i]['one_month'];?>" required/>
            </td>
            <td class="col-md-2">
                
                <input type="text" name="three_month<?php echo $i;?>" id='three_month<?php echo $i;?>' class="form-control" value="<?php echo $subscriptions[$i]['three_month'];?>" required/>
            </td>
            <td class="col-md-2">
                
                <input type="text" name="twelve_month<?php echo $i;?>" id='twelve_month<?php echo $i;?>' class="form-control" value="<?php echo $subscriptions[$i]['twelve_month'];?>" required/>
            </td>
            
            
           
            <td class="col-md-2"><a class="deleteRow"></a>

            </td>
        
        </tr>
    <?php }?>
        </div>

    </tbody>

    <tfoot>
        <tr><center>
            <td  style="text-align: left;">
            <input type="button" class="btn btn-primary btn-block " id="addrow" value="Add Row" />
                
                 
            </td>
 
        </center>
        </tr>
        <tr>
        </tr>
    </tfoot>
</table>

<center>
<div class="box-footer">
    
          <input type="submit" class="btn btn-primary" id="submit" value="Save" />
          <input type="reset" class="btn btn-default" id="reset" value="Reset" />
         
        </div></center>
         
<!-- </div> -->
<?php ActiveForm::end(); ?>


<script src='jquery-1.9.1.min.js'></script>
<script>
    $(document).ready(function () {
    //var counter = 1;
     var rowcount=$('#rowcount').val();
    $("#addrow").on("click", function () {
        //debugger;
         rowcount=parseInt(rowcount)+1;
       // var newRow = $("<tr id='+'>");
       
    alert(rowcount);
        var cols = "";
        // var script_per_week= $("#script_per_week").val();
        //  var one_month= $("#one_month").val();
        //  var two_month=$("#two_month").val();
        //  var three_month=$("#three_month").val();
        //  alert(script_per_week);
      
        cols += '<tr id="'+ rowcount +'"><td><input type="text" class="form-control" name="script_per_week' + rowcount + '" id="script_per_week' + rowcount + '" required/></td>';
        cols += '<td><input type="text" class="form-control" name="one_month' + rowcount + '" id="one_month' + rowcount + '" required/></td>';
        
        cols += '<td><input type="text" class="form-control" name="three_month' + rowcount + '" id="three_month' + rowcount + '" required/></td>';

        cols += '<td><input type="text" class="form-control" name="twelve_month' + rowcount + '" id="twelve_month' + rowcount + '" required/></td>';
        
        cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td><tr>';
        

       

        $("#rowcount").val(rowcount);
       // newRow.append(cols);
      // counter++;
        $("tbody").append(cols);
        
    });



    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();       
        //counter -= 1
        rowcount=parseInt(rowcount)-1;

        $("#rowcount").val(rowcount);
    });


});




function calculateRow(row) {
    var price = +row.find('input[name^="price"]').val();
    alert(price)

}

function calculateGrandTotal() {
    var grandTotal = 0;
    $("table.order-list").find('input[name^="price"]').each(function () {
        grandTotal += +$(this).val();
    });
    $("#grandtotal").text(grandTotal.toFixed(2));
}



    </script>

    </div></div></div>