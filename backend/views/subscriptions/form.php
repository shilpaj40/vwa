<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CarouselItem */
/* @var $form yii\widgets\ActiveForm */
$model->created_by = Yii::$app->user->id;
$User=\app\models\User::find()->where(['id' =>  Yii::$app->user->id])->one();
$authorname=$User->first_name." ".$User->last_name;
$model->created_at = date('Y-m-d H:i:s');
?>


<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<head>
<style type="text/css">
 .table>tfoot>tr>td {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 0px solid #ddd !important;
}
</style>
</head>

<div class="carousel-item-index">
    <!--  <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?> -->
    <div class="box box-primary">

        <div class="box-header with-border">
        <div class="col-md-6">
            <h3 class="box-title"> <i class="fa fa-list"></i> Add Subcriptions Plan </h3>
        </div>

        <div class="col-md-4">
            <div class="pull-right">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>
        </div>
        
      </div>
<div class="box-body">
<div class="container">
  <form method="POST">
    <table id="myTable" class=" table order-list">
    <thead>
        <tr>
            <th>No. of Scripts Per Week</th>
            <th>1 Month</th>
            <th>2 Month</th>
            <th>3 Month</th>

        </tr>
    </thead>
    <tbody>
        <div class="row" id="data">
        <tr>
            
            <td class="col-md-2">
                
                       
                         <input type="mail" name="script_per_week"  class="form-control"/>
                    
            </td>
            <td class="col-md-2">

                       
               <input type="mail" name="one_month"  class="form-control"/>
            </td>
            <td class="col-md-2">
                 <!-- <?= $form->field($model, 'two_month')->textInput(['maxlength' => true, 'class'=>'form-control'])->label(false) ?> -->
                <input type="text" name="two_month"  class="form-control"/>
            </td>
            <td class="col-md-2">
                <!--  <?= $form->field($model, 'three_month')->textInput(['maxlength' => true, 'class'=>'form-control'])->label(false) ?> -->
                <input type="text" name="three_month"  class="form-control"/>
            </td>
            <td class="col-md-2"><a class="deleteRow"></a>

            </td>
        
        </tr>
        </div>
    </tbody>
    <tfoot>
        <tr><center>
            <td  style="text-align: left;">
                <input type="button" class="btn btn-primary btn-block " id="addrow" value="Add Row" />
            </td>
        </center>
        </tr>
        <tr>
        </tr>
    </tfoot>
</table><center>
<div class="box-footer">
          <input type="submit" value="save" id="submit" name="submit">
        </div></center>
        
</div>



<script>

$("#submit").on("click",function(){
                alert("submit");
                var jsonObj = [];
                i=0;
            $("#data tr").each(function(){

               var td = $(this).find('td').html();
               alert(td);
                script_per_week = td.eq(0).text();
                one_month = td.eq(1).text();
                two_month = td.eq(2).text();
                three_month = td.eq(3).text();
                 alert(script_per_week);
                jsonObj.push({
                    script_per_week: script_per_week,
                    one_month: one_month,
                    two_month: two_month,
                    three_month: three_month,
                });
            });

            var dataString = JSON.stringify(jsonObj);
                 alert(dataString);

                $.ajax({
                    url: "submit.php",
                    type: "POST",
                    data: {json:dataString},
                    success: function(response){
                               alert(response);
                             }
                });

        });

    

    $(document).ready(function () {
    var counter = 0;

    $("#addrow").on("click", function () {
        var newRow = $("<tr>");
        var cols = "";

        cols += '<td><input type="text" class="form-control" name="script_per_week' + counter + '"/></td>';
        cols += '<td><input type="text" class="form-control" name="one_month' + counter + '"/></td>';
        cols += '<td><input type="text" class="form-control" name="two_month' + counter + '"/></td>';
        cols += '<td><input type="text" class="form-control" name="three_month' + counter + '"/></td>';

        cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>';
        newRow.append(cols);
        $("table.order-list").append(newRow);
        counter++;
    });



    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();       
        counter -= 1
    });


});



function calculateRow(row) {
    var price = +row.find('input[name^="price"]').val();

}

function calculateGrandTotal() {
    var grandTotal = 0;
    $("table.order-list").find('input[name^="price"]').each(function () {
        grandTotal += +$(this).val();
    });
    $("#grandtotal").text(grandTotal.toFixed(2));
}
    </script>

    </div></div></div>