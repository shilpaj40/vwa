<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SubscriptionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Subscriptions';
$this->params['breadcrumbs'][] = $this->title;

$js = <<<SCRIPT
/* To initialize BS3 tooltips set this below */
$(function () { 
    $("[data-toggle='tooltip']").tooltip(); 
});;
/* To initialize BS3 popovers set this below */
$(function () { 
    $("[data-toggle='popover']").popover(); 
});
SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js);
?>
<?php
$this->registerJs('
$(document).ready(function(){

$(\'#MyButton\').click(function(){
if (confirm("Are you sure you want to delete this item/s?")) {
    var HotId = $(\'#w0\').yiiGridView(\'getSelectedRows\');
    //alert(HotId);
    $.ajax({
    type: \'POST\',
    url : \'multipledelete\',
    data : {row_id: HotId},
    success : function(response) {
        alert(response);
        $(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
    }
    });
    return false;
}
else
{

}
});
});', \yii\web\View::POS_READY);
?>
<div class="carousel-item-index">
    <div class="box box-primary">

      <div class="box-header with-border">
        <div class="col-md-8">
            <h3 class="box-title"> <i class="fa fa-list"></i> Subcriptions Plan List</h3>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
              <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success', 'data-toggle'=>"tooltip", 'data-original-title'=>"Add New"]) ?>
               <?= Html::a('<i class="fa fa-trash"></i>', '', ['class' => 'btn btn-danger', 'data-toggle'=>"tooltip", 'data-original-title'=>"Delete", "id"=>'MyButton']) ?>
            </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'class' => 'yii\grid\CheckboxColumn', 'checkboxOptions' => function($data) {
                            return ['value' => $data->id];
                    },
                ],         
                

                 [
                  'attribute'=>'script_per_week',
                  'format' => 'html',
                  'value'=>function($data) {
                            return '<p>'.$data->script_per_week.'</p>';
                    },
                ],
                [
                  'attribute'=>'one_month',
                  'format' => 'html',
                  'value'=>function($data) {
                            return '<p>'.$data->one_month.'</p>';
                    },
                ],
                
                
                
                [
                  'attribute'=>'three_month',
                  'format' => 'html',
                  'value'=>function($data) {
                            return '<p>'.$data->three_month.'</p>';
                    },
                ],

                [
                  'attribute'=>'twelve_month',
                  'format' => 'html',
                  'value'=>function($data) {
                            return '<p>'.$data->twelve_month.'</p>';
                    },
                ],
                [
                  'attribute'=>'created_at',
                  'format' => 'html',
                  'value'=>function($data) {
                            return date_format(date_create($data->created_at), 'd-m-Y H:i:s');
                    },
                ],

                [
                    'header'=>'Actions',
                    'format' => 'raw',
                    'value' => function ($model) {
                       return Html::a('<i class="fa fa-pencil"></i>', ['subscriptions/update?id='.$model['id']]);
                      
                    },
                ],
            ],
        ]); ?>

        <?php Pjax::end(); ?>
      </div>
      
    </div>

</div>