<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\models\HomeBlocks;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\HomeBlocksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Blocks');
$this->params['breadcrumbs'][] = $this->title;
$js = <<<SCRIPT
/* To initialize BS3 tooltips set this below */
$(function () { 
    $("[data-toggle='tooltip']").tooltip(); 
});;
/* To initialize BS3 popovers set this below */
$(function () { 
    $("[data-toggle='popover']").popover(); 
});
SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js);
?>
<?php
$this->registerJs('
$(document).ready(function(){

$(\'#MyButton\').click(function(){
if (confirm("Are you sure you want to delete this item/s?")) {
    var HotId = $(\'#w0\').yiiGridView(\'getSelectedRows\');
    //alert(HotId);
    $.ajax({
    type: \'POST\',
    url : \'multipledelete\',
    data : {row_id: HotId},
    success : function(response) {
        alert(response);
        $(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
    }
    });
}
else
{

}
});
});', \yii\web\View::POS_READY);
?>

<div class="home-blocks-index">
  <div class="nav-tabs-custom">
      <!-- <ul class="nav nav-tabs">
        <li class="active"><a href="<?= \yii\helpers\Url::to(['home-blocks/index']); ?>">Blocks</a></li>
        <li ><a href="<?= \yii\helpers\Url::to(['home-blocks/seo']); ?>">SEO</a></li>
        
      </ul> -->
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
          <div class="row setup-content" id="step-1">
            <div class="col-md-12">
               <div class="box box-primary" style="border:0;">
                  <div class="box-header with-border">
                    <div class="col-md-8">
                        <h3 class="box-title"> <i class="fa fa-list"></i> Block List</h3>
                    </div>
                    <div class="col-md-4">
                        <div class="pull-right">
                            <?php $block=HomeBlocks::find()->all(); 
                                if(count($block)==0)
                                {
                                echo Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success', 'data-toggle'=>"tooltip", 'data-original-title'=>"Add New"]); 
                                }
                            ?>
                          <!--  <?= Html::a('<i class="fa fa-trash"></i>', '', ['class' => 'btn btn-danger', 'data-toggle'=>"tooltip", 'data-original-title'=>"Delete", "id"=>'MyButton']) ?> -->
                        </div>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <?php Pjax::begin(); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                           /* [
                                'class' => 'yii\grid\CheckboxColumn', 'checkboxOptions' => function($data) {
                                        return ['value' => $data->id];
                                },
                            ],*/

                            [
                            'attribute'=>'best_advisors',
                            'value' => function ($data) {
                                  return substr(strip_tags($data['best_advisors'],"<br>"),0,100);
                                     },
                             ],
                             [
                                'attribute'=>'productive_weekends',
                                'value' => function ($data) {
                                      return substr(strip_tags($data['productive_weekends'],"<br>"),0,100);
                                     },
                             ],
                           // 'subtitle',
                            [
                              'attribute'=>'logo',
                              'format' => 'html',
                              'value'=>function($data) {
                                        return '<img src="'.$data->logo.'" class="img-responsive" style="width:70px;" />';
                                },
                            ],
                            [
                                'header'=>'Actions',
                                'format' => 'raw',
                                'value' => function ($model) {
                                   return Html::a('<i class="fa fa-pencil"></i>', ['home-blocks/update?id='.$model['id']]);
                                  
                                },
                            ],
                        ],
                    ]); ?>

                    <?php Pjax::end(); ?>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.tab-content -->
  </div>
</div>
