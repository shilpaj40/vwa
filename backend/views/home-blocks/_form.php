<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use  v0lume\yii2\metaTags\MetaTags;

/* @var $this yii\web\View */
/* @var $model backend\models\HomeBlocks */
/* @var $form yii\widgets\ActiveForm */
$model->created_by = Yii::$app->user->id;
$User=\app\models\User::find()->where(['id' =>  Yii::$app->user->id])->one();
$authorname=$User->first_name." ".$User->last_name;
$model->created_at = date('Y-m-d H:i:s');
?>
<script src="<?php echo Yii::$app->params['image']?>js/ckeditor/ckeditor.js"></script>


<div class="home-blocks-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="box box-primary" style="border:0">
      <div class="box-header with-border">
        <div class="col-md-8">
            <h3 class="box-title"> <i class="fa fa-pencil"></i> Add Block</h3>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>
        </div>
       
      </div>
      <!-- /.box-header -->
      <!-- form start -->

        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="form-group col-md-6">
                <?= $form->field($model, 'best_advisors')->textArea([ 'class'=>'form-control']) ?>
                    </div>
                    <div class="form-group col-md-6">
                <?= $form->field($model, 'productive_weekends')->textArea([ 'class'=>'form-control']) ?>
                    </div>
                    <div class="form-group col-md-6">
                <?= $form->field($model, 'zero_surprises')->textArea([ 'class'=>'form-control']) ?>
                    </div>
                    <div class="form-group col-md-6">
                <?= $form->field($model, 'about_details')->textArea([ 'class'=>'form-control']) ?>
                    </div>
                     <div class="form-group col-md-6">
                     <?= $form->field($model, 'copyright')->textArea([ 'class'=>'form-control']) ?>
                </div>
                                     <!--- image1--->
                    <div class="form-group col-md-12 nopadding claro-details">
                    <div class="col-md-6 nopadding">
                    <details class="" open="">    <summary role="button" aria-expanded="true" aria-pressed="true" class="claro-details__summary js-form-required form-required">Logo<span class="required-mark"></span></summary><div class="claro-details__wrapper details-wrapper">
                          <div class="js-form-item form-item js-form-type-managed-file form-type--managed-file js-form-item-field-image-0 form-item--field-image-0">
                          <label for="edit-field-image-0-upload" id="edit-field-image-0--label" class="form-item__label js-form-required form-required">Add a new file</label>
                            <div class="image-widget js-form-managed-file form-managed-file form-managed-file--image is-single has-upload no-value no-meta">
                                <div class="form-managed-file__main">
                                    <?= $form->field($model, 'logo',['inputOptions'=>[ 'class'=>'form-element','onchange'=>'validateImage(this.id,1)']])->fileInput()->label(false) ?>
                                </div>
                            </div>
                            <div id="edit-field-image-0--description" class="form-item__description">
                            One file only.<br>2 MB limit.<br>Allowed types: png gif jpg jpeg.<br>
                        </div>
                      </div>
                      </div>
                    </details>
                    </div>
                    <div class="col-md-6 bookslides homeimg">
                        <div class="entity-meta__last-saved js-form-item js-form-type-item form-type--item js-form-item-meta-changed form-item--meta-changed" id="edit-meta-changed">
                        <label for="edit-meta-changed" class="form-item__label">Image Preview</label>
                        <div class="layout__region layout__region--first">
                          <div class="block-with-background block">
                            <figure>   
                              <div class="field field--name-field-image field--type-entity-reference field--label-hidden field__item">  
                                <img src="<?= Yii::$app->params['server'];?>images/preview.png" id="preview_profile_photo1" name="preview_profile_photo">
                              </div>
                            </figure>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>
                    <!--- image1--->
                    <div class="form-group col-md-12 nopadding claro-details">
                    <div class="col-md-6 nopadding">
                    <details class="" open="">    <summary role="button" aria-expanded="true" aria-pressed="true" class="claro-details__summary js-form-required form-required">About Us Image<span class="required-mark"></span></summary><div class="claro-details__wrapper details-wrapper">
                          <div class="js-form-item form-item js-form-type-managed-file form-type--managed-file js-form-item-field-image-0 form-item--field-image-0">
                          <label for="edit-field-image-0-upload" id="edit-field-image-0--label" class="form-item__label js-form-required form-required">Add a new file</label>
                            <div class="image-widget js-form-managed-file form-managed-file form-managed-file--image is-single has-upload no-value no-meta">
                                <div class="form-managed-file__main">
                                    <?= $form->field($model, 'image',['inputOptions'=>[ 'class'=>'form-element','onchange'=>'validateImage(this.id,3)']])->fileInput()->label(false) ?>
                                </div>
                            </div>
                            <div id="edit-field-image-0--description" class="form-item__description">
                            One file only.<br>2 MB limit.<br>Allowed types: png gif jpg jpeg.<br>
                        </div>
                      </div>
                      </div>
                    </details>
                    </div>
                    <div class="col-md-6 bookslides homeimg">
                        <div class="entity-meta__last-saved js-form-item js-form-type-item form-type--item js-form-item-meta-changed form-item--meta-changed" id="edit-meta-changed">
                        <label for="edit-meta-changed" class="form-item__label">Image Preview</label>
                        <div class="layout__region layout__region--first">
                          <div class="block-with-background block">
                            <figure>   
                              <div class="field field--name-field-image field--type-entity-reference field--label-hidden field__item">  
                                <img src="<?= Yii::$app->params['server'];?>images/preview.png" id="preview_profile_photo3" name="preview_profile_photo">
                              </div>
                            </figure>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>

                <!--- image2--->
                 <div class="form-group col-md-12 nopadding claro-details">
                    <div class="col-md-6 nopadding">
                    <details class="" open="">    <summary role="button" aria-expanded="true" aria-pressed="true" class="claro-details__summary js-form-required form-required">Footer Image<span class="required-mark"></span></summary><div class="claro-details__wrapper details-wrapper">
                          <div class="js-form-item form-item js-form-type-managed-file form-type--managed-file js-form-item-field-image-0 form-item--field-image-0">
                          <label for="edit-field-image-0-upload" id="edit-field-image-0--label" class="form-item__label js-form-required form-required">Add a new file</label>
                            <div class="image-widget js-form-managed-file form-managed-file form-managed-file--image is-single has-upload no-value no-meta">
                                <div class="form-managed-file__main">
                                    <?= $form->field($model, 'footer_image',['inputOptions'=>[ 'class'=>'form-element','onchange'=>'validateImage(this.id,2)']])->fileInput()->label(false) ?>
                                </div>
                            </div>
                            <div id="edit-field-image-0--description" class="form-item__description">
                            One file only.<br>2 MB limit.<br>Allowed types: png gif jpg jpeg.<br>
                        </div>
                      </div>
                      </div>
                    </details>
                    </div>
                    <div class="col-md-6 bookslides homeimg">
                        <div class="entity-meta__last-saved js-form-item js-form-type-item form-type--item js-form-item-meta-changed form-item--meta-changed" id="edit-meta-changed">
                        <label for="edit-meta-changed" class="form-item__label">Image Preview</label>
                        <div class="layout__region layout__region--first">
                          <div class="block-with-background block">
                            <figure>   
                              <div class="field field--name-field-image field--type-entity-reference field--label-hidden field__item">  
                                <img src="<?= Yii::$app->params['server'];?>images/preview.png" id="preview_profile_photo2" name="preview_profile_photo">
                              </div>
                            </figure>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>
                 </div>
                 <div class="form-group col-md-12">  
                <details class="path-form accordion__item js-form-wrapper form-wrapper claro-details claro-details--accordion-item" id="edit-path-0" open="">   
                      <summary role="button" aria-controls="edit-path-0" aria-expanded="false" aria-pressed="false" class="claro-details__summary claro-details__summary--accordion-item">SEO</summary>
                      <div class="claro-details__wrapper details-wrapper claro-details__wrapper--accordion-item">
                        <div class="claro-details__content claro-details__content--accordion-item">
                          <div class="js-form-item form-item js-form-type-textfield form-type--textfield js-form-item-path-0-alias form-item--path-0-alias">
                          <?php  echo MetaTags::widget([
                                'model' => $model,
                                'form' => $form
                            ]);?>
                          </div>
                        </div>
                      </div>
                  </details>
              </div>

                   
               
               
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
          <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
      </form>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload & Crop Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 text-center">
                          <div id="image_demo" style=" margin-top:30px"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4" style="padding-top:30px;">
                       
                          <button class="btn btn-success crop_image">Crop & Upload Image</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('homeblocks-copyright', {
          height: 100,

        // set toolbar that you want to show
     removeButtons: 'PasteText,PasteFromWord,SpecialChar,Styles,About,SpellChecker',
    toolbar: [
        { name: 'basicstyles', items: ['Cut','Copy','Paste','Link','Unlink'] },

    ],
    toolbarGroups: [
        { name: 'group1', groups: [ 'basicstyles' ] },
    ],
    removePlugins: 'elementspath',
    removeDialogTabs: 'link:advanced;image:Link;image:advanced',
            } );

    CKEDITOR.replace('homeblocks-best_advisors', {
          height: 100,
     removeButtons: 'PasteText,PasteFromWord,SpecialChar,Styles,About,SpellChecker',
         toolbar: [
            { name: 'basicstyles', items: [ 'Source','Bold', 'Underline',  'Table', 'Cut','Copy','Paste','Link','Unlink','Strike'] },
            { name: 'styles', items: [ 'Format','NumberedList','BulletedList','Styles','Font','FontSize'] },
            ],
    toolbarGroups: [
        { name: 'group1', groups: [ 'basicstyles' ] },
    ],
    removePlugins: 'elementspath',
    removeDialogTabs: 'link:advanced;image:Link;image:advanced',
    } );

    CKEDITOR.replace('homeblocks-productive_weekends', {
          height: 100,
     removeButtons: 'PasteText,PasteFromWord,SpecialChar,Styles,About,SpellChecker',
         toolbar: [
            { name: 'basicstyles', items: [ 'Source','Bold', 'Underline',  'Table', 'Cut','Copy','Paste','Link','Unlink','Strike'] },
            { name: 'styles', items: [ 'Format','NumberedList','BulletedList','Styles','Font','FontSize'] },
            ],
    toolbarGroups: [
        { name: 'group1', groups: [ 'basicstyles' ] },
    ],
    removePlugins: 'elementspath',
    removeDialogTabs: 'link:advanced;image:Link;image:advanced',
    } );

    CKEDITOR.replace('homeblocks-zero_surprises', {
          height: 100,
     removeButtons: 'PasteText,PasteFromWord,SpecialChar,Styles,About,SpellChecker',
         toolbar: [
            { name: 'basicstyles', items: [ 'Source','Bold', 'Underline',  'Table', 'Cut','Copy','Paste','Link','Unlink','Strike'] },
            { name: 'styles', items: [ 'Format','NumberedList','BulletedList','Styles','Font','FontSize'] },
            ],
    toolbarGroups: [
        { name: 'group1', groups: [ 'basicstyles' ] },
    ],
    removePlugins: 'elementspath',
    removeDialogTabs: 'link:advanced;image:Link;image:advanced',
    } );

    CKEDITOR.replace('homeblocks-about_details', {
          height: 100,
     removeButtons: 'PasteText,PasteFromWord,SpecialChar,Styles,About,SpellChecker',
         toolbar: [
            { name: 'basicstyles', items: [ 'Source','Bold', 'Underline',  'Table', 'Cut','Copy','Paste','Link','Unlink','Strike'] },
            { name: 'styles', items: [ 'Format','NumberedList','BulletedList','Styles','Font','FontSize'] },
            ],
    toolbarGroups: [
        { name: 'group1', groups: [ 'basicstyles' ] },
    ],
    removePlugins: 'elementspath',
    removeDialogTabs: 'link:advanced;image:Link;image:advanced',
    } );
$(document).ready(function(){
/*
    $image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:300,
      height:330,
      type:'square' //circle
    },
    boundary:{
      width:270,
      height:300
    }
  });
  $('#homeblocks-image').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });



  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
        $('#uploadimageModal').modal('hide');
        $("#preview_profile_photo").attr("src",response);
        $("#homeblocks-photo").val(response);
     
    })
  });*/

});  

function validateImage(id,imgid) {
    //Get reference of FileUpload.
    var fileUpload = document.getElementById(id);

    //Check whether the file is valid Image.
    var regex = new RegExp("([a-zA-Z0-9()\s_\\.\-:])+(.jpg|.JPG|.JPEG|.PNG|.GIF|.png|.gif|.jpeg)$");
    if (regex.test(fileUpload.value.toLowerCase())) {
 
        //Check whether HTML5 is supported.
        if (typeof (fileUpload.files) != "undefined") {
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(fileUpload.files[0]);
            var file = fileUpload.files[0];
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();
 
                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;
                       
                //Validate the File Height and Width.
                image.onload = function () {
                    if (Math.round(file.size / (1024 * 1024)) > 2) { // make it in MB so divide by 1024*1024
                    alert('Please select image size less than 2 MB');
                    return false;

                    }
                    else
                    {
                       var height = this.height;
                        var width = this.width;                        
                       /* if (height !=766 && width != 1200) {
                            alert("Image size should be 1200X766");
                            fileUpload.value="";
                            return false;                              
                        } */
                    $('#preview_profile_photo'+imgid).attr('src', e.target.result);

                    return true;
                    }
                    
                   
                };
            }
        } else {
            alert("This browser does not support HTML5.");
            fileUpload.value="";
            return false;
        }
    } else {
        alert("Please select a valid Image file.");
            fileUpload.value="";
        return false;
    }
}
</script>
