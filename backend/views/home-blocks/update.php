<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\HomeBlocks */

$this->title = Yii::t('app', 'Update Home Blocks');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Home Blocks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="home-blocks-update">

   <div class="nav-tabs-custom">
    <!--   <ul class="nav nav-tabs">
        <li class="active"><a href="<?= \yii\helpers\Url::to(['home-blocks/index']); ?>" >Blocks</a></li>
        <li ><a href="<?= \yii\helpers\Url::to(['home-blocks/seo']); ?>" >SEO</a></li>
        
      </ul> -->
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
          <div class="row setup-content" id="step-1">
            <div class="col-md-12">
               <div class="box box-primary" style="border:0;">
                  
                  <div class="box-body">
                    	<?= $this->render('_form_update', [
					        'model' => $model,
					    ]) ?>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.tab-content -->
  	</div>

</div>
