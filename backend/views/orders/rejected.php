<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\Users;
use backend\models\Cart;
use fedemotta\datatables\DataTables;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Subscribed Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resources-index">
    <div class="nav-tabs-custom">
  <ul class="nav nav-tabs">
    <li><a href="<?= \yii\helpers\Url::to(['orders/approved']);?>" >Pending</a></li>
    <li><a href="<?= \yii\helpers\Url::to(['orders/approved']);?>" >Approved</a></li>
    <li  class="active"><a href="#tab_1" data-toggle="tab">Rejected</a></li>
   
  </ul>
<div class="Orders-index">
   <div class="box box-primary">
      <div class="box-header with-border">
        <div class="col-md-12">
            <h3 class="box-title"> <i class="fa fa-list"></i> Subscribed Users</h3>
        </div>
      </div>
       <div class="box-body">
          <?= DataTables::widget([
              'dataProvider' => $dataProvider,
                'clientOptions' => [
                    'lengthMenu' => false,
                    'responsive' => true, 
                    'dom' => 'BfTrtip',
                    'buttons' => [
                      'csv','excel', 'pdf', 'print'
                    ]
               ],
              'columns' => [
                 // ['class' => 'yii\grid\SerialColumn'],
                 ['class' => 'yii\grid\SerialColumn'],
                     [
                       'attribute' => 'id',
                       'format' => 'raw',
                      'value'=>function($model) {
                            return Html::a($model->id,['/orders/view', 'id' => $model->id]);
                        },
                      ], 
                          

                      'email',
                      [
                        'attribute'=>'amount',
                        'format' => 'raw',
                        'value'=>function($data) {
                           // print_r($data);die;
                              // return getItemAmount($data['id']);
                            return $data->amount;
                          },
                      ],
                      [
                          'attribute'=>'payment_status',
                           'value' => function ($model) {
                             return getpaymentstatus($model->payment_status);
                            },
                      ],
                      [
                          'attribute'=>'subcription_id',
                           'value' => function ($model) {
                            //print_r($model);die;
                             return getscript($model->subcription_id);
                            },
                      ],
                      [
                        'attribute' => 'subcription_plan',
                        'format' => 'raw',
                        'value' => function ($model) {
                         return $model->subcription_plan."<span>  Month</span>";
                            //return $model->order_status;
                        },
                      ],

                      [
                        'attribute' => 'payment_start_date',
                        'format' => 'raw',
                        'value' => function ($model) {
                          // return "<span class='hidden-lg'>'".date_format(date_create($model->created_date),'Ymd')."'</span>".date_format(date_create($model->created_date),'d-m-Y'); 
                          return " ";
                            //return $model->order_status;
                        },
                      ],


                      [
                        'attribute' => 'payment_end_date',
                        'format' => 'raw',
                        'value' => function ($model) {
                          // return "<span class='hidden-lg'>'".date_format(date_create($model->created_date),'Ymd')."'</span>".date_format(date_create($model->created_date),'d-m-Y'); 
                            // return "<span class='hidden-lg'>'".date_format(date_create($model->payment_end_date),'Ymd')."'</span>".date_format(date_create($model->payment_end_date),'d-m-Y'); 
                          return " ";
                            //return $model->payment_end_date;
                        },
                      ],
                      'city',
                      [
                          'attribute'=>'created_date',
                          'format'=>'html',
                          'value' => function ($model) {
                           return "<span class='hidden-lg'>'".date_format(date_create($model->created_date),'Ymd')."'</span>".date_format(date_create($model->created_date),'d-m-Y'); 
                          },
                          'headerOptions' => ['style' => 'width:10%'],
                      ] 
                ],
          ]);?>

        <?php
        function getItemType($cart_id)
        {      

            $model = \backend\models\Cart::find()->where(['id'=>$cart_id])->one();
            if($model->is_chart==0)
            {
                 $data = \backend\models\Subscription::find()->where(['id'=>$model->item_id])->one();
                 $title=$data->name;
            }
            else
            {
                $data = \backend\models\Charts::find()->where(['id'=>$model->item_id])->one();
                $title=$data->title;
            }
            return  $title;
        } 
        function getItemAmount($cart_id)
        {   
          $title=0;        
            $model = \backend\models\Cart::find()->where(['id'=>id])->one();
            if($model->is_chart==0)
            {
                 $data = \backend\models\Subscription::find()->where(['id'=>$model->item_id])->one();
                 $title=$data->amount;
            }
            else
            {
                $data = \backend\models\Charts::find()->where(['id'=>$model->item_id])->one();
                $title=$data->price;
            }
            return "£". $title;
        } 
        function getAmount($payment_id)
        {
            $model = \backend\models\TblPayment::find()->where(['id'=>$payment_id])->one();
                    return $model->amount." ".$model->currency_code;
        }

        function getscript($id)
        {
            //print_r($id);die;
            $model = \backend\models\Subscriptions::find()->where(['id'=>$id])->one();
            //print_r($model);die;
                    return $model->script_per_week;
        }

        function getplan($id,$amt)
        {

         $model = \backend\models\Subscriptions::find()->where(['id'=>$id])->one();
            //print_r($model);die;

            if($model->one_month==$amt)
            {
               return '1  Month';
            }else if($model->twelve_month==$amt)
            {
                return '12  Month';

            }else
            {
                return '3  Month';

            }
                  
        }

        function getpaymentstatus($status)
        {
          if($status==0)
          {
            return "Pending";
          }else if($status==1){

            return "Accepted";

          }else{

            return "Rejected";
          }
       
        }
        function getDispatch($status, $is_chart,$id)
        {
             if($is_chart==1)
             {
                if($status==1)
                {
                  return "Dispatch";
                }
                else
                {
                  return Html::a('<span class="btn btn-success">Dispatch</span>', ['orders/updatestatus?id='.$id],array('title'=>'Change Status', 'data-confirm'=>'Are you sure you want to change status?'));
                }
             }
             else
              return "";
        }
        ?>
      </div> 
</div>
</div></div></div>
