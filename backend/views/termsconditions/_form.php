<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\helpers\Url;
use  v0lume\yii2\metaTags\MetaTags;



/* @var $this yii\web\View */
/* @var $model backend\models\Faq */
/* @var $form yii\widgets\ActiveForm */
$model->created_by = Yii::$app->user->id;
$User=\app\models\User::find()->where(['id' =>  Yii::$app->user->id])->one();
$authorname=$User->first_name." ".$User->last_name;
$model->created_at = date('Y-m-d H:i:s');
?>
<script src="<?php echo Yii::$app->params['image']?>js/ckeditor/ckeditor.js"></script>
<div class="about-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="box box-primary">
      <div class="box-header with-border">
        <div class="col-md-8">
            <h3 class="box-title"> <i class="fa fa-pencil"></i> Add Details</h3>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <?= Html::submitButton('Save', ['id'=>'submit_upper','class' => 'btn btn-success']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                 <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'class'=>'form-control']) ?>
                </div>
                <div class="form-group"> 
                  <details class="path-form accordion__item js-form-wrapper form-wrapper claro-details claro-details--accordion-item" id="edit-path-0" open="">   
                      <summary role="button" aria-controls="edit-path-0" aria-expanded="false" aria-pressed="false" class="claro-details__summary claro-details__summary--accordion-item">URL alias</summary>
                      <div class="claro-details__wrapper details-wrapper claro-details__wrapper--accordion-item">
                        <div class="claro-details__content claro-details__content--accordion-item">
                          <div class="js-form-item form-item js-form-type-textfield form-type--textfield js-form-item-path-0-alias form-item--path-0-alias">
                            <?= $form->field($model, 'alias')->textInput(['maxlength' => true, 'class'=>'form-control']) ?>
                           
                            <div id="edit-path-0-alias--description" class="form-item__description">
                              Specify an alternative path by which this data can be accessed. For example, type "about" when writing an about page.
                            </div>
                          </div>
                        </div>
                      </div>
                  </details></div>
                 
                 <div class="form-group">
                    <?= $form->field($model, 'body')->textArea([ 'class'=>'form-control']) ?>
                 <?= $form->field($model, 'created_at')->hiddenInput(['value' => $model->created_at])->label(false); ?>
                </div>
               
               <div class="form-group">
                    <details class="claro-details" open="">    <summary role="button" aria-expanded="true" aria-pressed="true" class="claro-details__summary js-form-required form-required">Banner Image<span class="required-mark"></span></summary><div class="claro-details__wrapper details-wrapper">
                          <div class="js-form-item form-item js-form-type-managed-file form-type--managed-file js-form-item-field-image-0 form-item--field-image-0">
                          <label for="edit-field-image-0-upload" id="edit-field-image-0--label" class="form-item__label js-form-required form-required">Select a banner image</label>
                            <div class="image-widget js-form-managed-file form-managed-file form-managed-file--image is-single has-upload no-value no-meta">
                                <div class="form-managed-file__main">
                                  <div class="row">
                                  <div class="col-md-6">
                                  <?php  $query=new Query;
                                        $query->from('banner')
                                         ->where(['status' => 1])
                                            ->select('banner_name, id');
                                        $command = $query->createCommand();
                                        $employee = $command->queryAll();
                                        $countryData=ArrayHelper::map($employee,'id','banner_name');
                                        echo $form->field($model, 'banner_id')->widget(Select2::classname(), [
                                        'data' => $countryData,
                                        'options' => ['placeholder' => '-- Select --',
                                         'onchange'=>'
                                            $.get( "'.Url::toRoute('/starsign/get-image').'", { id: $(this).val() } )
                                                .done(function( data ) {
                                                    $( "#'.Html::getInputId($model, 'banner_image').'" ).val( data );
                                                     $( "#bimg" ).attr("src", data );
                                                }
                                            );
                                          '    
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ])->label(false);
                                  ?>
                                  <?= $form->field($model, 'banner_image')->hiddenInput([ 'class'=>'form-control'])->label(false); ?>
                                </div>
                                <div class="col-md-6">
                                  <img src="" id="bimg" class="img-responsive" /> 
                                </div>
                              </div>
                              </div>
                            </div>
                            
                      </div>
                      </div>
                    </details>
                </div> 
            </div>  

            <div class="form-group col-md-12">   
            <details class="path-form accordion__item js-form-wrapper form-wrapper claro-details claro-details--accordion-item" id="edit-path-0" open="">   
                      <summary role="button" aria-controls="edit-path-0" aria-expanded="false" aria-pressed="false" class="claro-details__summary claro-details__summary--accordion-item">SEO</summary>
                      <div class="claro-details__wrapper details-wrapper claro-details__wrapper--accordion-item">
                        <div class="claro-details__content claro-details__content--accordion-item">
                          <div class="js-form-item form-item js-form-type-textfield form-type--textfield js-form-item-path-0-alias form-item--path-0-alias">
                          <?php  echo MetaTags::widget([
                                'model' => $model,
                                'form' => $form
                            ]);?>
                          </div>
                        </div>
                      </div>
                  </details>
              </div>

        </div>
       </div>
       <!-- /.box-body -->
        <div class="box-footer">
          <?= Html::submitButton('Save', ['id'=>'submit_lower','class' => 'btn btn-success']) ?>
          <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('termsconditions-body', {
          height: 100,

        // set toolbar that you want to show
     removeButtons: 'PasteText,PasteFromWord,SpecialChar,Styles,About,SpellChecker',
    toolbar: [
        { name: 'basicstyles', items: [ 'Source','Bold', 'Underline',  'Table', 'Cut','Copy','Paste','Link','Unlink','Strike'] },
        { name: 'styles', items: [ 'Format','NumberedList','BulletedList','Styles','Font','FontSize'] },

    ],
    toolbarGroups: [
        { name: 'group1', groups: [ 'basicstyles' ] },
    ],
    removePlugins: 'elementspath',
    removeDialogTabs: 'link:advanced;image:Link;image:advanced',
            } );

$(function(){
  $("#termsconditions-title").keyup(function()
  {
    var key=$("#termsconditions-title").val();
     key=key.split(' ').join('-');
     key=key.split('.').join('-');
     key=key.split('&').join('');
     key=key.split('$').join('-');
     key=key.split('#').join('-');
     key=key.split('!').join('-');
     key=key.split('#').join('-');    
     key=key.split('%').join('-');
     key=key.split('^').join('-');
     key=key.split('*').join('-');
     key=key.split('(').join('-');
     key=key.split(')').join('-');
     key=key.split('+').join('-');
     key=key.split('_').join('-');
     key=key.split('"').join('-');
     key=key.split('/').join('-');
     key=key.split('<').join('-');
     key=key.split('>').join('-');
     key=key.split('|').join('-');
     key=key.split('?').join('-');
     key=key.split(',').join('-');
     key=key.split('`').join('-');
     key=key.split('{').join('-');
     key=key.split('}').join('-');
     key=key.split('[').join('-');
     key=key.split(']').join('-');
     key=key.split(':').join('-');
     key=key.split(';').join('-');
     key=key.split('~').join('-');
     key=key.split('!').join('-');
     key=key.split('@').join('-');
     key=key.split('=').join('-');
     key=key.split('/').join('-');

    $("#termsconditions-alias").val(key.toLowerCase());
  });
});
</script>

