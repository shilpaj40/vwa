<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\About;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AboutSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'About');
$this->params['breadcrumbs'][] = $this->title;
$js = <<<SCRIPT
/* To initialize BS3 tooltips set this below */
$(function () { 
    $("[data-toggle='tooltip']").tooltip(); 
});;
/* To initialize BS3 popovers set this below */
$(function () { 
    $("[data-toggle='popover']").popover(); 
});
SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js);
?>

<div class="about-index">
    <div class="box box-primary">
      <div class="box-header with-border">
        <div class="col-md-8">
            <h3 class="box-title"> <i class="fa fa-list"></i> List</h3>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <?php $about=About::find()->all(); 
                    if(count($about)==0)
                    {
                        echo Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success', 'data-toggle'=>"tooltip", 'data-original-title'=>"Add New"]);
                    }
                ?>
            </div>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                  'attribute'=>'title',
                  'format' => 'raw',
                  'value'=>function($model) {
                      return Html::a(Html::encode($model->title), "",["onclick"=>"return showdetails(".$model['id'].")"]);
                  },
                ],
                'alias',
                [
                  'attribute'=>'image',
                  'format' => 'html',
                  'value'=>function($data) {
                            return '<img src="'.$data->image.'" class="img-responsive" style="width:70px;" />';
                    },
                ],
                [
                    'attribute'=>'created_at',
                    'format' => 'html',
                    'value' => function ($model) {
                       return date_format(date_create($model->created_at),'d-m-Y H:i:s');
                    },
                ],
                [
                    'header'=>'Actions',
                    'format' => 'raw',
                    'value' => function ($model) {
                       return Html::a('<i class="fa fa-pencil"></i>', ['about/update?id='.$model['id']]);
                    },
                ],
            ],
        ]); ?>
      </div>
    </div>
    
</div>
<div class="modal fade" id="view-celebrity" tabindex="-1" role="dialog"  aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-search"></i> View About Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="ti-close"></i>
                </button>
            </div>
            <div class="modal-body" id="view-category">
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                </button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function showdetails(ele)
    {
        $.ajax({
        url: '<?php echo Yii::$app->request->baseUrl. '/about/view' ?>',
        type: "POST",
        data:{"id":ele},
        success:function(data)
        {
          //alert(data);
          $("#view-category").html(data);
        //  $("viewCategory").show();
         $('#view-celebrity').modal({
            show: 'true'
        }); 

        }
      });
        return false;
    }
</script>