<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\db\Query;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use  v0lume\yii2\metaTags\MetaTags;

$User=\app\models\User::find()->where(['id' =>  $model->created_by])->one();
$authorname=$User->first_name." ".$User->last_name;
/* @var $this yii\web\View *//* @var $model backend\models\About */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="<?php echo Yii::$app->params['image']?>js/ckeditor/ckeditor.js"></script>

<div class="about-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="box box-primary">
      <div class="box-header with-border">
        <div class="col-md-8">
            <h3 class="box-title"> <i class="fa fa-pencil"></i> Add Details</h3>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <?= Html::submitButton('Save', ['id'=>'submit_upper','class' => 'btn btn-success']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
            <div class="col-md-7">
                <div class="form-group">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'class'=>'form-control']) ?>
                </div>
                <div class="form-group">
                    <details class="claro-details" open="">    <summary role="button" aria-expanded="true" aria-pressed="true" class="claro-details__summary js-form-required form-required">Image<span class="required-mark"></span></summary><div class="claro-details__wrapper details-wrapper">
                          <div class="js-form-item form-item js-form-type-managed-file form-type--managed-file js-form-item-field-image-0 form-item--field-image-0">
                          <label for="edit-field-image-0-upload" id="edit-field-image-0--label" class="form-item__label js-form-required form-required">Add a new file</label>
                            <div class="image-widget js-form-managed-file form-managed-file form-managed-file--image is-single has-upload no-value no-meta">
                                <div class="form-managed-file__main">
                                    <?= $form->field($model, 'image')->fileInput(['class'=>'form-element'])->label(false) ?>
                                </div>
                            </div>
                            <div id="edit-field-image-0--description" class="form-item__description">
                            One file only.<br>2 MB limit.<br>Allowed types: png gif jpg jpeg.<br>Image Size: 195x250
                        </div>
                      </div>
                      </div>
                    </details>
                </div>
                <div class="form-group">
                    <?= $form->field($model, 'photo')->hiddenInput()->label(false); ?>

                    <?= $form->field($model, 'body')->textArea([ 'class'=>'form-control']) ?>
                    <?= $form->field($model, 'our_role')->textArea([ 'class'=>'form-control']) ?>
                  <?= $form->field($model, 'why_we_are')->textArea([ 'class'=>'form-control']) ?>
                    <?= $form->field($model, 'vision')->textArea([ 'class'=>'form-control']) ?>
                    <?= $form->field($model, 'objective')->textArea([ 'class'=>'form-control']) ?>
                    <?= $form->field($model, 'assurance')->textArea([ 'class'=>'form-control']) ?>

                </div>
                <div class="form-group">
                    <details class="claro-details" open="">    <summary role="button" aria-expanded="true" aria-pressed="true" class="claro-details__summary js-form-required form-required">Banner Image<span class="required-mark"></span></summary><div class="claro-details__wrapper details-wrapper">
                          <div class="js-form-item form-item js-form-type-managed-file form-type--managed-file js-form-item-field-image-0 form-item--field-image-0">
                          <label for="edit-field-image-0-upload" id="edit-field-image-0--label" class="form-item__label js-form-required form-required">Select a banner image</label>
                            <div class="image-widget js-form-managed-file form-managed-file form-managed-file--image is-single has-upload no-value no-meta">
                                <div class="form-managed-file__main">
                                  <div class="row">
                                  <div class="col-md-6">
                                  <?php  $query=new Query;
                                        $query->from('banner')
                                         ->where(['status' => 1])
                                            ->select('banner_name, id');
                                        $command = $query->createCommand();
                                        $employee = $command->queryAll();
                                        $countryData=ArrayHelper::map($employee,'id','banner_name');
                                        echo $form->field($model, 'banner_id')->widget(Select2::classname(), [
                                        'data' => $countryData,
                                        'options' => ['placeholder' => '-- Select --',
                                         'onchange'=>'
                                            $.get( "'.Url::toRoute('/starsign/get-image').'", { id: $(this).val() } )
                                                .done(function( data ) {
                                                    $( "#'.Html::getInputId($model, 'banner_image').'" ).val( data );
                                                     $( "#bimg" ).attr("src", data );
                                                }
                                            );
                                          '    
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ])->label(false);
                                  ?>
                                  <?= $form->field($model, 'banner_image')->hiddenInput([ 'class'=>'form-control'])->label(false); ?>
                                </div>
                                <div class="col-md-6">
                                  <img src="" id="bimg" class="img-responsive" /> 
                                </div>
                              </div>
                              </div>
                            </div>
                            
                      </div>
                      </div>
                    </details>
                </div>
            </div>
            <div class="col-md-5">
                <div class="entity-meta accordion js-form-wrapper form-wrapper" id="edit-advanced">
                  <div class="entity-meta__header accordion__item js-form-wrapper form-wrapper" id="edit-meta">
                      <div class="entity-meta__last-saved js-form-item form-item js-form-type-item form-type--item js-form-item-meta-changed form-item--meta-changed" id="edit-meta-changed">
                        <label for="edit-meta-changed" class="form-item__label">Image Preview</label>
                        <div class="layout__region layout__region--first">
                          <div class="block-with-background block">
                            <figure>   
                              <div class="field field--name-field-image field--type-entity-reference field--label-hidden field__item">  
                                <img src="<?= $model->image;?>" id="preview_profile_photo" name="preview_profile_photo">
                              </div>
                            </figure>
                          </div>
                        </div>
                      </div>
                      <div class="entity-meta__last-saved js-form-item form-item js-form-type-item form-type--item js-form-item-meta-changed form-item--meta-changed" id="edit-meta-changed">
                         <label for="edit-meta-changed" class="form-item__label">Last saved</label>
                          <?= ($model->updated_at==NULL && empty($model->updated_at))?date_format(date_create($model->created_at),'d-m-Y H:i:s') : date_format(date_create($model->updated_at),'d-m-Y H:i:s');?>
                      </div>
                      <div class="entity-meta__author js-form-item form-item js-form-type-item form-type--item js-form-item-meta-author form-item--meta-author" id="edit-meta-author">
                        <label for="edit-meta-author" class="form-item__label">Author</label>
                         <?= $authorname;?>
                      </div>
                      <div class="entity-meta__author js-form-item form-item js-form-type-item form-type--item js-form-item-meta-author form-item--meta-author" id="edit-meta-author">
                        <label for="edit-meta-author" class="form-item__label">Creation Date</label>
                      
                         <?= date_format(date_create($model->created_at),'d-m-Y H:i:s');?>
                      </div>
                  </div>
                  <details class="path-form accordion__item js-form-wrapper form-wrapper claro-details claro-details--accordion-item" id="edit-path-0" open="">   
                      <summary role="button" aria-controls="edit-path-0" aria-expanded="false" aria-pressed="false" class="claro-details__summary claro-details__summary--accordion-item">URL alias</summary>
                      <div class="claro-details__wrapper details-wrapper claro-details__wrapper--accordion-item">
                        <div class="claro-details__content claro-details__content--accordion-item">
                          <div class="js-form-item form-item js-form-type-textfield form-type--textfield js-form-item-path-0-alias form-item--path-0-alias">
                            <?= $form->field($model, 'alias')->textInput(['maxlength' => true, 'class'=>'form-control']) ?>
                           
                            <div id="edit-path-0-alias--description" class="form-item__description">
                              Specify an alternative path by which this data can be accessed. For example, type "about" when writing an about page.
                            </div>
                          </div>
                        </div>
                      </div>
                  </details>
                   <details class="path-form accordion__item js-form-wrapper form-wrapper claro-details claro-details--accordion-item" id="edit-path-0" open="">   
                      <summary role="button" aria-controls="edit-path-0" aria-expanded="false" aria-pressed="false" class="claro-details__summary claro-details__summary--accordion-item">SEO</summary>
                      <div class="claro-details__wrapper details-wrapper claro-details__wrapper--accordion-item">
                        <div class="claro-details__content claro-details__content--accordion-item">
                          <div class="js-form-item form-item js-form-type-textfield form-type--textfield js-form-item-path-0-alias form-item--path-0-alias">
                          <?php  echo MetaTags::widget([
                                'model' => $model,
                                'form' => $form
                            ]);?>
                          </div>
                        </div>
                      </div>
                  </details>
                </div>
            </div>
        </div>
       </div>
       <!-- /.box-body -->
        <div class="box-footer">
          <?= Html::submitButton('Save', ['id'=>'submit_lower','class' => 'btn btn-success']) ?>
          <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>
<div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload & Crop Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 text-center">
                        <div id="image_demo" style=" margin-top:30px"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4" style="padding-top:30px;">
                       
                          <button class="btn btn-success crop_image">Crop & Upload Image</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
CKEDITOR.replace('about-body', {
          height: 100,

        // set toolbar that you want to show
     removeButtons: 'PasteText,PasteFromWord,SpecialChar,Styles,About,SpellChecker',
    toolbar: [
        { name: 'basicstyles', items: [ 'Source','Bold', 'Underline',  'Table', 'Cut','Copy','Paste','Link','Unlink','Strike'] },
        { name: 'styles', items: [ 'Format','NumberedList','BulletedList','Styles','Font','FontSize'] },

    ],
    toolbarGroups: [
        { name: 'group1', groups: [ 'basicstyles' ] },
    ],
    removePlugins: 'elementspath',
    removeDialogTabs: 'link:advanced;image:Link;image:advanced',
            } );
CKEDITOR.replace('about-our_role', {
          height: 100,

        // set toolbar that you want to show
     removeButtons: 'PasteText,PasteFromWord,SpecialChar,Styles,About,SpellChecker',
    toolbar: [
        { name: 'basicstyles', items: [ 'Source','Bold', 'Underline',  'Table', 'Cut','Copy','Paste','Link','Unlink','Strike'] },
        { name: 'styles', items: [ 'Format','NumberedList','BulletedList','Styles','Font','FontSize'] },

    ],
    toolbarGroups: [
        { name: 'group1', groups: [ 'basicstyles' ] },
    ],
    removePlugins: 'elementspath',
    removeDialogTabs: 'link:advanced;image:Link;image:advanced',
            } );
CKEDITOR.replace('about-why_we_are', {
          height: 100,

        // set toolbar that you want to show
     removeButtons: 'PasteText,PasteFromWord,SpecialChar,Styles,About,SpellChecker',
    toolbar: [
        { name: 'basicstyles', items: [ 'Source','Bold', 'Underline',  'Table', 'Cut','Copy','Paste','Link','Unlink','Strike'] },
        { name: 'styles', items: [ 'Format','NumberedList','BulletedList','Styles','Font','FontSize'] },

    ],
    toolbarGroups: [
        { name: 'group1', groups: [ 'basicstyles' ] },
    ],
    removePlugins: 'elementspath',
    removeDialogTabs: 'link:advanced;image:Link;image:advanced',
            } );
$(document).ready(function(){

    $image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:182,
      type:'square' //circle
    },
    boundary:{
      width:170,
      height:152
    }
  });
  $('#about-image').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });



  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
        $('#uploadimageModal').modal('hide');
        $("#preview_profile_photo").attr("src",response);
        $("#about-photo").val(response);
     
    })
  });

});  
$("#submit_lower").mousedown(function(){
  for (var i in CKEDITOR.instances){
    CKEDITOR.instances[i].updateElement();
  }
});
$("#submit_upper").mousedown(function(){
  for (var i in CKEDITOR.instances){
    CKEDITOR.instances[i].updateElement();
  }
});
$(function(){
  $("#about-title").keyup(function()
  {
    var key=$("#about-title").val();
     key=key.split(' ').join('-');
     key=key.split('.').join('-');
     key=key.split('&').join('');
     key=key.split('$').join('-');
     key=key.split('#').join('-');
     key=key.split('!').join('-');
     key=key.split('#').join('-');    
     key=key.split('%').join('-');
     key=key.split('^').join('-');
     key=key.split('*').join('-');
     key=key.split('(').join('-');
     key=key.split(')').join('-');
     key=key.split('+').join('-');
     key=key.split('_').join('-');
     key=key.split('"').join('-');
     key=key.split('/').join('-');
     key=key.split('<').join('-');
     key=key.split('>').join('-');
     key=key.split('|').join('-');
     key=key.split('?').join('-');
     key=key.split(',').join('-');
     key=key.split('`').join('-');
     key=key.split('{').join('-');
     key=key.split('}').join('-');
     key=key.split('[').join('-');
     key=key.split(']').join('-');
     key=key.split(':').join('-');
     key=key.split(';').join('-');
     key=key.split('~').join('-');
     key=key.split('!').join('-');
     key=key.split('@').join('-');
     key=key.split('=').join('-');
     key=key.split('/').join('-');

    $("#about-alias").val(key.toLowerCase());
  });
});
</script>