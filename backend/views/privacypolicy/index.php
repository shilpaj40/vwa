
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;
use backend\models\PrivacyPolicy;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\RoleMasterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Privacy Policy";
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs('
$(document).ready(function(){

$(\'#MyButton\').click(function(){
if (confirm("Are you sure you want to delete this items?")) {
    var HotId = $(\'#w0\').yiiGridView(\'getSelectedRows\');
    //alert(HotId);
    $.ajax({
    type: \'POST\',
    url : \'privacypolicy/deletepolicy\',
    data : {row_id: HotId},
    success : function(response) {
        alert(response);
        $(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
    }
    });
}
else
{
}
});
});', \yii\web\View::POS_READY);
?>
<div class="resources-index">
     <div class="nav-tabs-custom">
    <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title"> <i class="fa fa-list mr-2"></i>Privacy Policy
                </h3>
            </div>
            <div class="col-md-4">
                <div class="pull-right">
                     <?php $policy=PrivacyPolicy::find()->all(); 
                    if(count($policy)==0)
                    {
                    echo Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success', 'data-toggle'=>"tooltip", 'data-original-title'=>"Add New"]);
                    }
                     ?>
                    <!--  <?= Html::a('<i class="fa fa-trash"></i>', '', ['class' => 'btn btn-danger', 'data-toggle'=>"tooltip", 'data-original-title'=>"Delete", "id"=>'MyButton']) ?> -->
                </div>
            </div>
        </div>
        <div id="smain">
          <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        /*[
                            'class' => 'yii\grid\CheckboxColumn', 'checkboxOptions' => function($data) {
                                    return ['value' => $data->id];
                            },
                        ],*/
                         [
                            'attribute'=>'body',
                            'value' => function ($data) {
                                  return substr(strip_tags($data['body'], "<br>"),0,100);
                                 },
                         ],                   
                       [
                            'header'=>'Actions',
                            'format' => 'raw',
                            'value' => function ($model) {                              
                               return Html::a('<i class="fa fa-pencil"></i>', ['privacypolicy/update?id='.$model['id']], ['data-toggle'=>"tooltip", 'data-original-title'=>"Update"]);
                             
                            },
                        ],
                    ],
                ]); ?>
             
          </div>
        </div>
       </div>
    </div>
</div> 