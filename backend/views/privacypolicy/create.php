<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PrivacyPolicy */

$this->title = 'Create Privacy Policy';
$this->params['breadcrumbs'][] = ['label' => 'Privacy Policies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="privacy-policy-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
