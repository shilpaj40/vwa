<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Resources */

$this->title = Yii::t('app', 'Add Video');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Resources'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resources-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
