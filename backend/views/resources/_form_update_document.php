<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Resources */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="resources-form">
    <div class="box box-primary">
        <div class="box-body">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'description')->textArea(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'file_path')->fileInput(); 
                    if($model->file_path!="")
                    {
                        echo  "<br /><a href='".$model->file_path."'>View Document</a>";
                    }
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 radiobtn">
                    <?php
                    $list = ["1"=>"Free Content", "2"=>"Login member", "3"=>"Paid member"];
                    echo $form->field($model, 'content_for')->radioList($list); 
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
