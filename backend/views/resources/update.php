<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Resources */

$this->title = Yii::t('app', 'Update Resources: {name}', [
    'name' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Resources'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="resources-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
