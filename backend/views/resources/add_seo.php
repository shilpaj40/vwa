<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\HomeBlocks */

$this->title = Yii::t('app', 'SEO');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'SEO'), 'url' => ['index']];
$this->params['breadcrumbs'][] = "Add SEO";
?>
<div class="home-blocks-create">
	<div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
       <li ><a href="<?= \yii\helpers\Url::to(['resources/index']);?>" data-toggle="tab">Video</a></li>
    <li><a href="<?= \yii\helpers\Url::to(['resources/admin']);?>" >Documents</a></li>
    <li class="active"><a href="<?= \yii\helpers\Url::to(['resources/seo']);?>" >SEO</a></li>
        
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
          <div class="row setup-content" id="step-1">
            <div class="col-md-12">
               <div class="box box-primary" style="border:0;">
                  
                  <div class="box-body">
                    	<?= $this->render('_form_seo', [
					        'model' => $model,
					    ]) ?>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.tab-content -->
  	</div>
    

</div>
