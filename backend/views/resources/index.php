<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ResourcesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Resources');
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs('
$(document).ready(function(){

$(\'#MyButton\').click(function(){
if (confirm("Are you sure you want to delete this item/s?")) {
    var HotId = $(\'#w0\').yiiGridView(\'getSelectedRows\');
    //alert(HotId);
    $.ajax({
    type: \'POST\',
    url : \'resources/multipledelete1\',
    data : {row_id: HotId},
    success : function(response) {
        alert(response);
        $(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
    }
    });
}
else
{
}
});
});', \yii\web\View::POS_READY);
?>
<style>
    iframe{
        width:100%;height:80%;
    }
</style>
<div class="resources-index">
    <div class="nav-tabs-custom">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#tab_1" data-toggle="tab">Video</a></li>
    <li><a href="<?= \yii\helpers\Url::to(['resources/admin']);?>" >Documents</a></li>
    <li><a href="<?= \yii\helpers\Url::to(['resources/seo']);?>" >SEO</a></li>
   
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tab_1">
      <div class="box box-primary" style="border-top:0">
        <div class="box-header with-border">
            <div class="col-md-8">
                <h3 class="box-title"> <i class="fa fa-list"></i>Resources Video List</h3>
            </div>
            <div class="col-md-4">
                <div class="pull-right">
                    <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success', 'data-toggle'=>"tooltip", 'data-original-title'=>"Add New"]) ?>
                     <?= Html::a('<i class="fa fa-trash"></i>', '', ['class' => 'btn btn-danger', 'data-toggle'=>"tooltip", 'data-original-title'=>"Delete", "id"=>'MyButton']) ?>
                </div>
            </div>
        </div>
        <div id="smain">
          <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'class' => 'yii\grid\CheckboxColumn', 'checkboxOptions' => function($data) {
                                    return ['value' => $data->id];
                            },
                        ],
                        'title',
                        // 'description',


                //         [
                //   'attribute'=>'description',
                //   'format' => 'html',
                //   'value'=>function($data) {
                //     $small = substr($data->description, 0, 10);
                //             return '<p>'.$small.'</p>';
                //     },
                // ],

                        [
                            'attribute' =>'youtube_iframe',
                            'format' => 'raw',
                            'contentOptions' => ['style' => 'width:100px'],
                            'value' => function($model){
                                return "<div style='width:250px'>".$model->youtube_iframe."</div>";
                            }
                        ],
                       [
                            'header'=>'Actions',
                            'format' => 'raw',
                            'value' => function ($model) {
                              
                               return Html::a('<i class="fa fa-pencil"></i>', ['resources/update?id='.$model['id']], ['data-toggle'=>"tooltip", 'data-original-title'=>"Update"]);
                             
                            },
                        ],
                    ],
                ]); ?>
             
          </div>
        </div>
       
    </div>
      
    </div>
    <!-- /.tab-pane -->
  </div>
  <!-- /.tab-content -->
</div>
   
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    

    <?php Pjax::end(); ?>

</div>
