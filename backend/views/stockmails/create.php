<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Stockmails */

$this->title = 'Create Stockmails';
$this->params['breadcrumbs'][] = ['label' => 'Stockmails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stockmails-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
