
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\RoleMasterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "FAQ's";
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs('
$(document).ready(function(){

$(\'#MyButton\').click(function(){
if (confirm("Are you sure you want to delete this items?")) {
    var HotId = $(\'#w0\').yiiGridView(\'getSelectedRows\');
    //alert(HotId);
    $.ajax({
    type: \'POST\',
    url : \'faq/deletefaq\',
    data : {row_id: HotId},
    success : function(response) {
        alert(response);
        $(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
    }
    });
}
else
{
}
});
});', \yii\web\View::POS_READY);
?>
<div class="resources-index">
     <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
        <li class="active"><a href="<?= \yii\helpers\Url::to(['faq/index']); ?>">Faq</a></li>
        <li ><a href="<?= \yii\helpers\Url::to(['faq/seo']); ?>">SEO</a></li>        
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
          <div class="row setup-content" id="step-1">
            <div class="col-md-12">
               <div class="box box-primary" style="border:0;">
                    <div class="box-header with-border">
                            <div class="col-md-8">
                                <h3 class="box-title"> <i class="fa fa-list mr-2"></i>FAQ's</h3>
                            </div>
                            <div class="col-md-4">
                                <div class="pull-right">
                                    <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success', 'data-toggle'=>"tooltip", 'data-original-title'=>"Add New"]) ?>
                                     <?= Html::a('<i class="fa fa-trash"></i>', '', ['class' => 'btn btn-danger', 'data-toggle'=>"tooltip", 'data-original-title'=>"Delete", "id"=>'MyButton']) ?>
                                </div>
                            </div>
                        </div>
        <div id="smain">
          <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'class' => 'yii\grid\CheckboxColumn', 'checkboxOptions' => function($data) {
                                    return ['value' => $data->id];
                            },
                        ],
                        'question',  
                        'answer',
                        [
                                    'label'=> '#',
                                    'format'=> 'raw',
                                    'value' => function($model) { 
                                         return  ($model->status == 1)?Html::a('<i class="fa fa-times-circle"></i>', ['faq/changestatus?id='.$model->id.'&flag=0'],array('style'=>'padding:2px 10px','class'=>'btn btn-danger','data-confirm'=>'Are you sure you want to deactivate this role?')):Html::a('<i class="fa fa-check"></i>', ['faq/changestatus?id='.$model->id.'&flag=1'],array('style'=>'padding:2px 10px','class'=>'btn btn-success','data-confirm'=>'Are you sure you want to activate this role?'));
                                    }
                                ],                      
                       [
                            'header'=>'Actions',
                            'format' => 'raw',
                            'value' => function ($model) {                              
                               return Html::a('<i class="fa fa-pencil"></i>', ['faq/update?id='.$model['id']], ['data-toggle'=>"tooltip", 'data-original-title'=>"Update"]);
                             
                            },
                        ],
                    ],
                ]); ?>
             
          </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
       
</div> 