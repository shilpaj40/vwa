<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use  v0lume\yii2\metaTags\MetaTags;

//use budyaga\cropper\Widget;

/* @var $this yii\web\View *//* @var $model backend\models\About */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="about-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="box box-primary" style="border:0">
      <div class="box-header with-border">
        <div class="col-md-8">
            <h3 class="box-title"> <i class="fa fa-pencil"></i> Add SEO </h3>
        </div>
        
      </div>
      <div class="box-body">
        <div class="row">
            <div class="col-md-7">
              <?= $form->field($model, 'title')->textInput(['maxlength' => true]); ?>
                <details class="path-form accordion__item js-form-wrapper form-wrapper claro-details claro-details--accordion-item" id="edit-path-0" open="">   
                      <summary role="button" aria-controls="edit-path-0" aria-expanded="false" aria-pressed="false" class="claro-details__summary claro-details__summary--accordion-item">SEO</summary>
                      <div class="claro-details__wrapper details-wrapper claro-details__wrapper--accordion-item">
                        <div class="claro-details__content claro-details__content--accordion-item">
                          <div class="js-form-item form-item js-form-type-textfield form-type--textfield js-form-item-path-0-alias form-item--path-0-alias">
                          <?php  echo MetaTags::widget([
                                'model' => $model,
                                'form' => $form
                            ]);?>
                          </div>
                        </div>
                      </div>
                </details>
            </div>
            
        </div>
      </div>
       <!-- /.box-body -->
      <div class="box-footer">
        <?= Html::submitButton('Save', ['id'=>'submit_lower','class' => 'btn btn-success']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
      </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>
