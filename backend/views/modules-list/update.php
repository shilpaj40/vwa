<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ModulesList */

$this->title = Yii::t('app', 'Update Modules List: {name}', [
    'name' => $model->module_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Modules Lists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->module_id, 'url' => ['view', 'id' => $model->module_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="modules-list-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
