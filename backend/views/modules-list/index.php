<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ModulesListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Modules Lists');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modules-list-index">

    <p class="create">
        <?= Html::a(Yii::t('app', 'Create Modules List'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
            'module_name',
            'controller',
            'action',
            //'url:url',
            //'icon',
            //'is_active',
            //'sequence',
            //'show_in_sidebar',

            ['class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                  'update' => \Yii::$app->Permission->getAccessPermission(Yii::$app->controller->id,'update'),
                  'view' => \Yii::$app->Permission->getAccessPermission(Yii::$app->controller->id,'view'),
                  'delete' => \Yii::$app->Permission->getAccessPermission(Yii::$app->controller->id,'delete'),
                  
             ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
