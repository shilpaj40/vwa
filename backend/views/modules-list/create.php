<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ModulesList */

$this->title = Yii::t('app', 'Create Modules List');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Modules Lists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modules-list-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
