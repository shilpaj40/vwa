<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\ModulesList;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\models\ModulesList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modules-list-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-7">
        <div class="row">
            <div class="col-md-4">
                <?php $listData=ArrayHelper::map(ModulesList::find()->where(['is_active' => 1])->all(),'module_id','module_name');
                     echo $form->field($model, 'parent_id')->widget(Select2::classname(), [
                                    'data' => array("0"=>"No Parent")+$listData,
                                    'options' => ['placeholder' => '-- Select --',],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'module_name')->textInput(['maxlength' => true]) ?>
            </div>
            

        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'controller')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'action')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
            </div>
            
        </div>
        <div class="row">
            
            <div class="col-md-4">
                <?php echo $form->field($model, 'icon')->widget(Select2::classname(), [
                                'data' => array('fa-user'=>'fa-user','fa-book'=>'fa-book','fa-shopping-cart'=>'fa-shopping-cart','fa-user-plus'=>'fa-user-plus','fa-bank'=>'fa-bank','fa-users'=>'fa-users','fa-file-text'=>'fa-file-text','fa-bars'=>'fa-bars','fa-newspaper-o'=>'fa-newspaper-o','fa-picture-o'=>'fa-picture-o','fa-building'=>'fa-building','fa-phone-square'=>'fa-phone-square','fa-archive'=>'fa-archive','fa-cloud'=>'fa-cloud','fa-paper-plane'=>'fa-paper-plane','fa-cloud-download'=>'fa-cloud-download','fa-gear'=>'fa-gear','fa-pencil-square'=>'fa-pencil-square','fa-comments'=>'fa-comments','fa-question-circle'=>'fa-question-circle','fa-envelope'=>'fa-envelope','fa-map-marker'=>'fa-map-marker','fa-folder-open'=>'fa-folder-open','fa-server'=>'fa-server','fa-life-ring'=>'fa-life-ring','fa-briefcase'=>'fa-briefcase','fa-graduation-cap'=>'fa-graduation-cap','fa-download'=>'fa-download','fa-globe'=>'fa-globe','fa-pencil'=>'fa-pencil','fa-laptop'=>'fa-laptop','fa-calendar'=>'fa-calendar','fa-cutlery'=>'fa-cutlery','fa-files-o'=>'fa-files-o','fa-paste'=>'fa-paste','fa-table'=>'fa-table', 'fa-truck'=>'fa-truck', 'fa-map-marker'=>'fa-map-marker','None'=>'None'),
                                'options' => ['placeholder' => '-- Select --',],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'sequence')->textInput() ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, "show_in_sidebar")->checkbox(); ?>
            </div>
        </div>
       
        <div class="row">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>



    
    <div class="col-md-5">
        <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Menu Icons </h5>
             

        </div>
        <div class="ibox-content">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Icon Name</th>
                    <th>Icon</th>
                    <th>Icon Name</th>
                    <th>Icon</th>
               </tr>
                </thead>
                <tbody>
                <tr>
                    <td>fa-user</td>
                    <td class="text-navy"> <i class="fa fa-user"></i> </td>
                    <td>fa-user-plus</td>
                    <td class="text-navy"> <i class="fa fa-user-plus"></i></td>
                </tr>
    <tr>
                    <td>fa-book</td>
                    <td class="text-navy"> <i class="fa fa-book"></i> </td>
                    <td>fa-shopping-cart</td>
                    <td class="text-navy"> <i class="fa fa-shopping-cart"></i></td>
                </tr>   
                <tr>
                    <td>fa-bank</td>
                    <td class="text-warning"> <i class="fa fa-bank"></i>  </td>
                    <td>fa-users</td>
                    <td class="text-warning"> <i class="fa fa-users"></i>  </td>
                </tr>
                <tr>
                    <td>fa-file-text</td>
                    <td class="text-navy"> <i class="fa fa-file-text"></i> </td>
                    <td>fa-bars</td>
                    <td class="text-navy"> <i class="fa fa-bars"></i></td>
                </tr>
    <tr>
                    <td>fa-newspaper-o</td>
                    <td class="text-warning"> <i class="fa fa-newspaper-o"></i>  </td>
                    <td>fa-picture-o</td>
                    <td class="text-warning"> <i class="fa fa-picture-o"></i>  </td>
                </tr>
                <tr>
                    <td>fa-building</td>
                    <td class="text-navy"> <i class="fa fa-building"></i> </td>
                    <td>fa-phone-square</td>
                    <td class="text-navy"> <i class="fa fa-phone-square"></i></td>
                </tr>
     <tr>
                    <td>fa-archive</td>
                    <td class="text-warning"> <i class="fa fa-archive"></i>  </td>
                    <td>fa-cloud</td>
                    <td class="text-warning"> <i class="fa fa-cloud"></i>  </td>
                </tr>
                <tr>
                    <td>fa-paper-plane</td>
                    <td class="text-navy"> <i class="fa fa-paper-plane"></i> </td>
                    <td>fa-cloud-download</td>
                    <td class="text-navy"> <i class="fa fa-cloud-download"></i></td>
                </tr>
    <tr>
                    <td>fa-gear</td>
                    <td class="text-warning"> <i class="fa fa-gear"></i>  </td>
                    <td>fa-pencil-square</td>
                    <td class="text-warning"> <i class="fa fa-pencil-square"></i>  </td>
                </tr>
                <tr>
                    <td>fa-comments</td>
                    <td class="text-navy"> <i class="fa fa-comments"></i> </td>
                    <td>fa-question-circle</td>
                    <td class="text-navy"> <i class="fa fa-question-circle"></i></td>
                </tr>
    <tr>
                    <td>fa-envelope</td>
                    <td class="text-warning"> <i class="fa fa-envelope"></i>  </td>
                    <td>fa-map-marker</td>
                    <td class="text-warning"> <i class="fa fa-map-marker"></i>  </td>
                </tr>
                <tr>
                    <td>fa-folder-open</td>
                    <td class="text-navy"> <i class="fa fa-folder-open"></i> </td>
                    <td>fa-server</td>
                    <td class="text-navy"> <i class="fa fa-server"></i></td>
                </tr>
    <tr>
                   
                    <td>fa-life-ring</td>
                    <td class="text-warning"> <i class="fa fa-life-ring"></i>  </td>
    <td>fa-briefcase</td>
                    <td class="text-warning"> <i class="fa fa-briefcase"></i> </td>
                </tr>
                <tr>
                    
                    <td>fa-graduation-cap</td>
                    <td class="text-navy"> <i class="fa fa-graduation-cap"></i></td>
    <td>fa-download</td>
                    <td class="text-navy"> <i class="fa fa-download"></i></td>
                </tr>
                 <tr>
                    <td>fa-globe</td>
                    <td class="text-warning"> <i class="fa fa-globe"></i></td>
                    <td>fa-pencil</td>
                    <td class="text-warning"> <i class="fa fa-pencil"></i></td>
                </tr>
                 <tr>
                    <td>fa-laptop</td>
                    <td class="text-navy"> <i class="fa fa-laptop"></i></td>
                    <td>fa-calendar</td>
                    <td class="text-navy"> <i class="fa fa-calendar"></i></td>
                </tr>
                <tr>
                    <td>fa-cutlery</td>
                    <td class="text-warning"> <i class="fa fa-cutlery"></i></td>
                    <td>fa fa-files-o</td>
                    <td class="text-warning"> <i class="fa fa-files-o"></i></td>
                </tr>
                <tr>
                    <td>fa-paste</td>
                    <td class="text-navy"> <i class="fa fa-paste"></i></td>
                    <td>fa-table</td>
                    <td class="text-navy"> <i class="fa fa-table"></i></td>
                </tr>
                <tr>
                    <td>fa-truck</td>
                    <td class="text-navy"> <i class="fa fa-truck"></i></td>
                    <td>fa-map-marker</td>
                    <td class="text-navy"> <i class="fa fa-map-marker"></i></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
