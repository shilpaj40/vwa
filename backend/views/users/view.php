 <?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\ModulesList;
use backend\models\UserModulePermission;
use backend\models\RoleMaster;


/* @var $this yii\web\View */
/* @var $model app\models\User */
$this->title='Summary of '. $model->first_name." ".$model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->first_name." ".$model->last_name;
\yii\web\YiiAsset::register($this);
function getRole($role_id)
{

    $role="";
    $Role=RoleMaster::findOne($role_id);
    if($Role!="")
        $role=$Role->role_name;
    return $role;
}
?>
<div class="card">
        <div class="card-body p-50">
<div class="user-view">
   
 
<div class="row">
    <div class="col-md-12">
      <!-- Profile Image -->
      <div class="box box-primary" style="border-top-color: #d2d6de;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
        <div class="box-body box-profile">
 <div class="logoDiv">
                <?php  
                   if($model['profile_pic']=="")
                    {
                       echo "<i class='profile-user-img role-name img-responsive img-circle'>".mb_substr($model->first_name, 0, 1, "UTF-8")."</i>";
                    }
                        else
                        echo "<img src='".$model->profile_pic."' class='profile-user-img role-name img-responsive img-circle' onerror='this.src=\"".Yii::$app->params['image']."images/user.jpg\"'  alt='image' style='height:75px'>";

                     /* echo "<img src='".$model->profile_pic."' class='profile-user-img role-name img-responsive img-circle' onerror='this.src=\""."/vwa/backend/web/images/starsign/16063929793.jpg\"'  alt='image' style='height:75px'>";*/
                      
                       ?>
                   </div>
           
         
          <h3 class="profile-username text-center"><?= $model->first_name." ".$model->last_name?></h3>
          <p class="text-muted text-center"><small>Role: <?= getRole($model->user_type)?></small></p>

          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <b>Email ID</b> <a class="pull-right"><?= $model->email?></a>
            </li>
            <li class="list-group-item">
              <b>Mobile</b> <a class="pull-right"><?= $model->mobile?></a>
            </li>
            <li class="list-group-item">
              <b>Username</b> <a class="pull-right"><?= $model->username?></a>
            </li>
            <li class="list-group-item">
              <b>Gender</b> <a class="pull-right"><?= ($model->gender==1)?"Male":"Female"; ?></a>
            </li>
           <!-- <li class="list-group-item">
              <b>Date Of Birth</b> <a class="pull-right"><?= ($model->birth_date!="0000-00-00" || $model->birth_date!=null) ? date('d-m-Y', strtotime($model->birth_date)): "" ?></a>
            </li>-->
            <li class="list-group-item">
              <b>City</b> <a class="pull-right"><?= $model->city?></a>
            </li>
          </ul>
          <?= Yii::$app->Permission->getAccessPermission(Yii::$app->controller->id,'update')?Html::a('<i class="fa fa-pencil-square-o"></i> Edit User', ['update', 'id' => $model->id], ['class' => 'btn btn-success btn-block']):"" ?>
          <?= Yii::$app->Permission->getAccessPermission(Yii::$app->controller->id,'delete')? Html::a('<i class="fa fa-trash"></i> Delete User', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-block',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]):"" ?>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
   
</div>
