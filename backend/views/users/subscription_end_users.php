<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use fedemotta\datatables\DataTables;
use backend\models\Users;


/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users: Subscription End';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-body">
        <div class="user-index">
            <ul class="nav nav-tabs">
       <?php echo"
   <li ><a href=".\yii\helpers\Url::to(['users/index']).">Customers</a></li>
        <li ><a href=".\yii\helpers\Url::to(['users/userslist']).">Internal Users</a></li>
        <li class='active'><a href=".\yii\helpers\Url::to(['users/subscription-end']).">Subscription End</a></li>";

            ?>


                
      </ul>
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="btn-group">

                        <button type="button" id="changebtntxt" class="btn btn-success" >All Users</button>
                        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a onclick='return (getstatuswise("-1"))' style='cursor:pointer'>All Users</a></li>
                            <li><a onclick='return (getstatuswise(1))' style='cursor:pointer'>Active</a></li>
                            <li><a onclick='return (getstatuswise(2))' style='cursor:pointer'>Inactive</a></li>       
                        </ul>
                        
                    </div>
                    <div class="pull-right">
                          <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success', 'data-toggle'=>"tooltip", 'data-original-title'=>"Add New"]) ?>
                      </div>
                </div>
            </div>
            <div class="row mt-2-2">
                <div class="col-md-12">
                    <div id="result">
                        <?= DataTables::widget([
                          'dataProvider' => $dataProvider,
                          'clientOptions' => [
                              'lengthMenu' => [200],
                              'responsive' => true, 
                              'dom' => 'BfTrtip',
                              'buttons' => [
                                'csv','excel', 'pdf', 'print'
                                ]
                            ],
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'], 
                                'customer_id',
                                
                                [
                                    'attribute' => 'fullname',
                                    'value' => function($data) { return $data->first_name  ." ". $data->last_name; }
                                ],
                                'username',
                                //'email',
                                'mobile',
                                'city', 
                                    /*[
                                     'attribute' => 'user_type',
                                   // 'filter'=>ArrayHelper::map(Users::getUsertype(), 'role_id', 'role_name'), 
                                    'value' => 'usertype.role_name'
                                    ], */                               /*[
                                    'header' => 'Last Visit Date',
                                    'format'=> 'raw',
                                    'value' => function ($model) { 
                                        return getLastVisit($model->id);
                                    },
                                ],*/
                                /*[
                                    'header' => 'Last Visit Date',
                                    'format'=> 'raw',
                                    'value' => function ($model) { 
                                        return getLastVisit($model->id);
                                    },
                                ],*/
                                [
                                    'attribute' => 'created_at',
                                    'format'=> 'raw',
                                    'header' => 'Registration Date',
                                    'value' => function ($model) { 
                                       
                                        return ($model->created_at!=NULL && $model->created_at!="0000-00-00")?/*"<span style='display:none'>".date_format(date_create($model->created_at),'dmYHis')."</span>".*/date_format(date_create($model ->created_at),'d-m-Y'):"-";
                                    },
                                ],
                                [
                                    'label'=> '#',
                                    'format'=> 'raw',
                                    'value' => function($model) { 
                                         return  ($model->status == 1)?Html::a('<i class="fa fa-times-circle"></i>', ['users/changestatus?id='.$model->id.'&flag=2'],array('style'=>'padding:2px 10px','class'=>'btn btn-danger','data-confirm'=>'Are you sure you want to deactivate this user?')):Html::a('<i class="fa fa-check"></i>', ['users/changestatus?id='.$model->id.'&flag=1'],array('style'=>'padding:2px 10px','class'=>'btn btn-success','data-confirm'=>'Are you sure you want to activate this user?'));
                                    }
                                ],

                               
                         ['class' => 'yii\grid\ActionColumn', 'template'=> '{update} {view}',
                                'visibleButtons' => [
                                   'update' => \Yii::$app->Permission->getAccessPermission(Yii::$app->controller->id,'update'),

                                  'view' => \Yii::$app->Permission->getAccessPermission(Yii::$app->controller->id,'view'),
                                  
                                //  'delete' => \Yii::$app->Permission->getAccessPermission(Yii::$app->controller->id,'delete'),
                                  
                             ],
                            
                            ],

                            ],
                      ]);?>
                    
                    </div>
                    <div id="resultdata" style="display:none">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<input type="hidden" id="status" value="-1"/>
<?php
function getLastVisit($id)
{
    $login = \app\models\LoginDetails::find()->where(['login_user_id' => $id])->orderBy(['login_detail_id'=>SORT_DESC])->one();
    return (!empty($login))?"<span style='display:none'>".date_format(date_create($login->login_at),'dmYHis')."</span>".date_format(date_create($login->login_at),'d-m-Y  H:i:s'):"-";
}
?>

<script type="text/javascript">
    
function getstatuswise(flag)
{
    var status="";
    if(flag==1)
        status="Active";
    else if(flag==2)
        status="Inactive";
    else
        status="All Users";
    $("#status").val(flag);
   
    $("#changebtntxt").text(status);
        $.ajax({
            type:'POST',
            url:"<?php echo Url::to(['users/getstatuswiselisting']) ?>",
            data:{'status':flag},
            success:function(items)
            {
                //alert(items);
                $("#result").hide();
                $("#resultdata").show();
                $("#resultdata").html(items);
            }
        });
}
</script>

