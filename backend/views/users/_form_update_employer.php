<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use backend\models\User;
use kartik\date\DatePicker;
?>
 <?php $form = ActiveForm::begin(); ?>

  <div class="card">
        <div class="card-body">
            <h6 class="card-title"> Basic Form Wizard with Validation</h6>
            <div id="wizard2">
                <h3>Personal Information</h3>
                <section>
                    <h4>Personal Information</h4>
                    <p>Try the keyboard navigation by clicking arrow left or right!</p>
                    <form id="form1">
                        <div class="form-group wd-xs-300">
                            <label>First name</label>
                            <input type="text" class="form-control" placeholder="First name" required>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                        <div class="form-group wd-xs-300">
                            <label>Last name</label>
                            <input type="text" class="form-control" name="lastname" placeholder="Enter lastname"
                                   required>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                    </form>
                </section>
                <h3>Billing Information</h3>
                <section>
                    <h4>Billing Information</h4>
                    <p>Wonderful transition effects.</p>
                    <form id="form2">
                        <div class="form-group wd-xs-300">
                            <label class="form-control-label">Email: <span class="tx-danger">*</span></label>
                            <input id="email" class="form-control" name="email" placeholder="Enter email address"
                                   type="email" required>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                        </div>
                    </form>
                </section>
                <h3>Payment Details</h3>
                <section>
                    <h4>Payment Details</h4>
                    <p>The next and previous buttons help you to navigate through your content.</p>
                </section>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
            <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
 <div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload & Crop Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 text-center">
                          <div id="image_demo" style="width:350px; margin-top:30px"></div>
                    </div>
                    <div class="col-md-4" style="padding-top:30px;">
                        <br />
                        <br />
                        <br/>
                          <button class="btn btn-success crop_image" data-dismiss="modal">Crop Image</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
 <script>  
  function triggerClick() {
     $('#user-profile_photo_url').trigger('click');
}
$(document).ready(function(){

    $image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:200,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });

   $('#user-profile_photo_url').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });
  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
document.getElementById("user-profile_photo").value = response;
     
    })
  });
  });  
</script>


