<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use backend\models\User;
use backend\models\Language;
use backend\models\Trades;
use backend\models\TradeRoles;
use backend\models\UserTradeRoles;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\jui\Accordion;
use sjaakp\bandoneon\Bandoneon; 
$form = ActiveForm::begin(); ?>

  <div class="card">
        <div class="card-body">
            <h6 class="card-title"> Basic Form Wizard with Validation</h6>
            <div id="wizard1">
                <h3>Personal Information</h3>
                <section>
                    <h4>Personal Information</h4>
                        <div class="form-group wd-xs-300 col-md-6">
                             <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="form-group wd-xs-300 col-md-6">
                             <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
                        </div>
                         <div class="form-group wd-xs-300 col-md-6">
                             <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
                        </div>
                         <div class="form-group wd-xs-300 col-md-6">
                             <?= $form->field($model, 'email')->input('email') ?>   
                        </div>
                         <div class="form-group wd-xs-300 col-md-6">
                         <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
                        </div>
                         <div class="form-group wd-xs-300 col-md-6">
                         <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                        </div>
                         <div class="form-group wd-xs-300 col-md-6">
                         <?= $form->field($model, 'gender')->radioList(array('Male'=>'Male','Female'=>'Female')); ?>
                        </div>                        
                         <div class="form-group wd-xs-300 col-md-6">
                         <label>DOB</label><?=  
                DatePicker::widget([
                    'model'=>$model,
                    'attribute' => 'dob',
                    'value' => date('d-M-Y', strtotime('+2 days')),
                    'options' => ['placeholder' => 'Select date ...'],
                    'pluginOptions' => [
                        'format' => 'dd-M-yyyy',
                        'todayHighlight' => true
                    ]
                ]); ?>
                        </div>
                </section>
                <h3>Profile Picture</h3>
                <section>
                      <?= $form->field($model, 'profile_photo')->hiddenInput()->label(false); ?>
                       <div class="form-group wd-xs-300 col-md-6">
                       <?= $form->field($model, 'profile_photo_url')->fileInput() ?></div>
                        <div class="form-group wd-xs-300 col-md-6">
                    <?php 
                    if($model->profile_photo_url!=null)
                    { ?>
                <h3>Uploaded Photo </h3>   
                  <img src="<?php echo $model->profile_photo_url ?>" onClick="triggerClick()">
               <?php } ?></div>
                    
                </section>
                <h3>Language</h3>
                <section>
                    <div class="table-email-list">
                        <div class="table-responsive" tabindex="1" style="overflow: hidden; outline: none;">
                           
                                <div class=" cursor-pointer" data-toggle="modal" data-target="#read">
                             <input type="hidden" name="language_select" id="language_select" value=<?php echo $model->language ;?> />                           
                            <?php
                             $languages=Language::getlanguage();
                                $languages=ArrayHelper::map($languages,'id','name');?>
    <?php echo $form->field($model, 'language')->checkboxList($languages)->label(false); ?>
                                    
                                </div>                               
                                
                        </div>
                    </div>
                </section>
                <h3>Trades & Avalability</h3>
                <section>                   
                    <?php
                     $trades=Trades::gettradesupdate(); 
                      
                    Bandoneon::begin();
                    for($i=0;$i<count($trades);$i++)
                      {
                        echo "<h4 id='".$trades[$i]['trade_id']."'>".$trades[$i]['trade_name']."</h4><div>";
                        $traderoles=TradeRoles::gettraderoles($trades[$i]['trade_id']);

                        for($j=0;$j<count($traderoles);$j++)
                         {
                          $isPresent=UserTradeRoles::find()->where(['trade_role_id'=>$traderoles[$j]['id'],'user_id'=>$model->id])->one(); 
                          if($isPresent)
                          echo "<p><input type='checkbox' class='form-check-input' id='trade_role_".$traderoles[$j]['id']."' checked>".$traderoles[$j]['name']."</p>";
                        else
                          echo "<p><input type='checkbox' class='form-check-input' id='trade_role_".$traderoles[$j]['id']."'>".$traderoles[$j]['name']."</p>";
                         }
                         echo "</div>";
                      } ?>
                        
                    <?php Bandoneon::end() ?>
                </section>
                <h3>Work Experience</h3>
                <section>
                    <h4>Payment Details</h4>
                    <p>The next and previous buttons help you to navigate through your content.</p>
                </section>
            </div>
        </div>
    </div>
     <?php ActiveForm::end(); ?>
      <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
 <div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload & Crop Image</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8 text-center">
                          <div id="image_demo" style="width:350px; margin-top:30px"></div>
                    </div>
                    <div class="col-md-4" style="padding-top:30px;">
                        <br />
                        <br />
                        <br/>
                          <button class="btn btn-success crop_image" data-dismiss="modal">Crop Image</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

 <script>  
  function triggerClick() {
     $('#user-profile_photo_url').trigger('click');
}
$(document).ready(function(){
   // $( "#accordion" ).accordion();

    $image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:200,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });
var language_select=document.getElementById("language_select").value;
var language = language_select.split(',');

 $('#user-language input[type="checkbox"]').each(function() {
      for(var l=0;l<language.length;l++)
      {
        if($(this).val()==language[l])
        {
          $(this).attr('checked',true);
        }
      }
    });

   $('#user-profile_photo_url').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });
  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
document.getElementById("user-profile_photo").value = response;
     
    })
  });
  });  
</script>