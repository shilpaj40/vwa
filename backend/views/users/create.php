<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User 
<h1><?= Html::encode($this->title) ?></h1>*/

$this->title = 'Create User';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

   

    <?= $this->render('_form', [
        'model' => $model,
        //'user_type'=>$user_type
    ]) ?>

</div>
