<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/croppie.css',
        'css/sidebar.css',
        'css/font/flaticon.css',
        'css/seo/font/flaticon.css',
        
        // 'themes/protable/assets/css/app.min.css',
        // 'themes/protable/vendors/bundle.css',
        // 'themes/protable/vendors/datepicker/daterangepicker.css',
        // 'themes/protable/vendors/slick/slick.css',
        // 'themes/protable/vendors/slick/slick-theme.css',
        // 'themes/protable/vendors/vmap/jqvmap.min.css',
    ];
    public $js = [
       // 'https://code.jquery.com/jquery-3.1.1.min.js',
        'css/sidebar.js',
        'css/croppie.js',
        
        //'https://code.jquery.com/jquery-3.3.1.slim.min.js',
        'js/bootstable.js',
        'https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js',
        'https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js',
        'https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js', 
        'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js', 
        'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js', 
        'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js', 
        'https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js', 
        'https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js',
        // 'themes/protable/vendors/bundle.js',
        // 'themes/protable/vendors/charts/chartjs/chart.min.js',
        // 'themes/protable/vendors/charts/apex/apexcharts.min.js',
        // 'themes/protable/vendors/circle-progress/circle-progress.min.js',
        // 'themes/protable/vendors/charts/peity/jquery.peity.min.js',
        // 'themes/protable/assets/js/examples/charts/peity.js',
        // 'themes/protable/vendors/datepicker/daterangepicker.js',
        // 'themes/protable/vendors/slick/slick.min.js',
        // 'themes/protable/vendors/vmap/jquery.vmap.min.js',
        // 'themes/protable/vendors/vmap/maps/jquery.vmap.usa.js',
        // 'themes/protable/assets/js/examples/vmap.js',
        // 'themes/protable/assets/js/app.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',

    ];
}
