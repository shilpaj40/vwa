<?php

namespace backend\controllers;

use Yii;
use backend\models\Banner;
use backend\models\BannerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BannerController implements the CRUD actions for Banner model.
 */
class BannerController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Banner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Banner model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Banner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Banner();

        if ($model->load(Yii::$app->request->post()) ) {
           // print_r(Yii::$app->request->post());die;
            $model->created_by=Yii::$app->user->id;
            $model->banner_image = UploadedFile::getInstance($model, 'banner_image');
            if (isset($model->banner_image)) {   
                 $model->banner_image->saveAs('images/' . $model->banner_image->baseName . '.' . $model->banner_image->extension, false);
                 $model->banner_image=Yii::$app->params['server'].'images/'.$model->banner_image;
            }
            if($model->save())
            {
                Yii::$app->session->setFlash('success', $model->banner_name." banner added successfully."); 
                return $this->redirect(['/banner']);
            } 
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Banner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $banner_image=$model->banner_image;

        if ($model->load(Yii::$app->request->post())) {
            $model->updated_by = Yii::$app->user->id;
            $model->updated_at = new \yii\db\Expression('NOW()');
            $model->banner_image = UploadedFile::getInstance($model, 'banner_image');
            if($model->banner_image=="" && $model->banner_image!=null)
            {
                $model->banner_image=$banner_image;
            }
            else {   
                $model->banner_image->saveAs('images/' . $model->banner_image->baseName . '.' . $model->banner_image->extension, false);
                $model->banner_image=Yii::$app->params['server'].'images/'.$model->banner_image;
            }
            if($model->save())
            {
                Yii::$app->session->setFlash('success', $model->banner_name." banner updated successfully."); 
                return $this->redirect(['/banner']);

            }
            
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Banner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['/banner']);
    }

    /**
     * Finds the Banner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Banner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Banner::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionMultipledelete()
    {
        $pk = Yii::$app->request->post('row_id');
        foreach ($pk as $key => $value)
        {
            $model = $this->findModel($value);
            $model->status=0;
            $model->deleted_at=date('Y-m-d H:i:s');
            $model->deleted_by=Yii::$app->user->id;
            $model->save(); 
        }
        Yii::$app->session->setFlash('success', "Records deleted successfully."); 
        return $this->redirect(['/banner']);
    }

}
