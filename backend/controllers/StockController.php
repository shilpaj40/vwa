<?php

namespace backend\controllers;

use Yii;
use backend\models\Stock;
use backend\models\StockSearch;
use backend\models\Stockupdates;
use backend\models\StockupdatesSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;


/**
 * StockController implements the CRUD actions for Stock model.
 */
class StockController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Stock models.
     * @return mixed
     */
    public function beforeAction($action) 
    { 
        $this->enableCsrfValidation = false; 
        return parent::beforeAction($action); 
    }
    public function actionIndex()
    {
        $searchModel = new StockSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionClosedStocks()
    {
        $searchModel = new StockSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('closed-stocks', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Stock model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $stock = \backend\models\Stock::find()->where(['stock_name' => $id])->groupBy('stock_name')->one();
       
        return $this->render('view', [
            'model' => $stock,
        ]);
    }

    public function actionClosed($id)
    {
        //$stock = \backend\models\Stock::find()->where(['stock_name' => $id])->groupBy('stock_name')->one();

        $all = \backend\models\Stock::find()
            ->where(['stock_name'=>$id, 'status'=>2, 'is_closed'=>0])
            ->all();
        foreach ($all as $key => $model) {
            // code...
            $stockupdate = new Stockupdates();
            $stockupdate->stock_id=$model->id;
            $stockupdate->stock_name=$model->stock_name;
            $stockupdate->symbol=$model->symbol;
            $stockupdate->no_of_shares=$model->no_of_shares;
            $stockupdate->share_price=$model->share_price;
            $stockupdate->suggestion=$model->suggestion;
            $stockupdate->action=$model->action;
            $stockupdate->actual_event=$model->actual_event;
            $stockupdate->created_at=date('Y-m-d H:i:s');
            $stockupdate->status=1;
            $stockupdate->first_record=1;
            $stockupdate->created_by=Yii::$app->user->identity->id;
            $stockupdate->date=date_format(date_create($model->date),'Y-m-d');
            $stockupdate->save();
        }

        $updateall = \backend\models\Stock::updateAll([
             'is_closed'=>1,'status'=>1
        ], ['stock_name'=>$id, 'status'=>2, 'is_closed'=>0]);

        return $this->redirect(['index']);
    }

    /**
     * Creates a new Stock model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Stock();
        $stockupdate = new Stockupdates();
        if ($model->load(Yii::$app->request->post())) {
            $model->created_at=date('Y-m-d H:i:s');
            $model->status=1;
            $model->created_by=Yii::$app->user->identity->id;
            $model->date=date_format(date_create($_POST['Stock']['date']),'Y-m-d');

            if($model->save())
            {
                $stockupdate->stock_id=$model->id;
                $stockupdate->stock_name=$model->stock_name;
                $stockupdate->symbol=$model->symbol;
                $stockupdate->no_of_shares=$model->no_of_shares;
                $stockupdate->share_price=$model->share_price;
                $stockupdate->suggestion=$model->suggestion;
                $stockupdate->action=$model->action;
                $stockupdate->actual_event=$model->actual_event;
                $stockupdate->created_at=date('Y-m-d H:i:s');
                $stockupdate->status=1;
                $stockupdate->first_record=1;
                $stockupdate->created_by=Yii::$app->user->identity->id;
                $stockupdate->date=date_format(date_create($_POST['Stock']['date']),'Y-m-d');
                $stockupdate->save();
                //1:Create, 0: Update, 2:approval
                $model->sendStockmail($stockupdate->created_by,$stockupdate->stock_name,1);
                if ($model->save()) {
                return $this->redirect(['index']);
            } 
        }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAdd($id)
    {
        $model = new Stock();
        $stockupdate = new Stockupdates();
        //$model = \backend\models\Stock::find()->where(['stock_name' => $id])->groupBy('stock_name')->one();
        if ($model->load(Yii::$app->request->post())) {
            $model->created_at=date('Y-m-d H:i:s');
            $model->status=1;
            $model->created_by=Yii::$app->user->identity->id;
            $model->date=date_format(date_create($_POST['Stock']['date']),'Y-m-d');
            if($model->save())
            {
                $stockupdate->stock_id=$model->id;
                $stockupdate->stock_name=$model->stock_name;
                $stockupdate->symbol=$model->symbol;
                $stockupdate->no_of_shares=$model->no_of_shares;
                $stockupdate->share_price=$model->share_price;
                $stockupdate->suggestion=$model->suggestion;
                $stockupdate->action=$model->action;
                $stockupdate->actual_event=$model->actual_event;
                $stockupdate->created_at=date('Y-m-d H:i:s');
                $stockupdate->status=1;
                $stockupdate->first_record=1;
                $stockupdate->created_by=Yii::$app->user->identity->id;
                $stockupdate->date=date_format(date_create($_POST['Stock']['date']),'Y-m-d');
                $stockupdate->save();
                //1:Create, 0: Update, 2:approval
                $model->sendStockmail($stockupdate->created_by,$stockupdate->stock_name,1);
                return $this->redirect(['index']);
            } 
        }
        return $this->render('add', [
            'model' => $model,
            'id' =>$id
        ]);
    }

    public function actionRemove()
    {
        $id=$_POST['id'];
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Updates an existing Stock model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdateData()
    {
        $formdata=$_POST['form_data'];
        $id=$_POST['id'];
        $html="";
        $searchModel = new StockSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = \backend\models\Stock::find()->where(['id' => $id])->one();
        $stockupdate = new Stockupdates();
        if($model!="")
        {
           /* $model->date= $formdata['date'];
            $model->symbol= $formdata['symbol'];
            $model->no_of_shares=$formdata['no_of_shares'];
            $model->share_price=$formdata['share_price'];
            //$model->week_no= $formdata['week_no'];            
            $model->suggestion=$formdata['suggestion'];
            $model->actual_event=$formdata['actual_event'];
            $model->action=$formdata['action'];*/
            $stockupdate->stock_id=$model->id;
                $stockupdate->stock_name=$model->stock_name;
                $stockupdate->symbol=$formdata['symbol'];
                $stockupdate->no_of_shares=$formdata['no_of_shares'];
                $stockupdate->share_price=$formdata['share_price'];
                $stockupdate->suggestion=$formdata['suggestion'];
                $stockupdate->action=$formdata['action'];
                $stockupdate->actual_event=$formdata['actual_event'];
                $stockupdate->created_at=date('Y-m-d H:i:s');
                $stockupdate->status=1;
                $stockupdate->first_record=0;
                $stockupdate->created_by=Yii::$app->user->identity->id;
                $stockupdate->date=$formdata['date'];
                $stockupdate->save();
                $model->updatesapproval=1;
           /* $model->open = $formdata['open'];
            $model->low=$formdata['low'];
            $model->high=$formdata['high'];
            $model->close=$formdata['close'];*/
            if($model->save())
            {
                //1:Create, 0: Update, 2:approval
                $model->sendStockmail($stockupdate->created_by,$stockupdate->stock_name,0);
                return "success";
            }
            else
            {
                return "error";
            }
           
        }
        else
            return "No data";
    }

    /**
     * Deletes an existing Stock model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Stock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Stock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Stock::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

     public function actionStockapproval()
     {       
        /*$searchModel = new StockSearch();  
        $status="";    
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$status);*/
        $dataProvider = new ActiveDataProvider([
            'query' => StockupdatesSearch::find()->orderBy('status','1','2','0'),
        ]);
        return $this->render('stock_approvals',['dataProvider' => $dataProvider]);
     }

    public function actionChangestockstatus($id,$flag,$stock_id)
    {
        $model = \backend\models\Stockupdates::find()->where(['stock_id' => $stock_id,'id'=>$id])->one();
        $stockupdate = \backend\models\Stock::find()->where(['id' => $model->stock_id])->one();
        if($model->first_record==1)
        {
            $model->status=$flag;
             $success=Yii::$app->db->createCommand()
                    ->update('stock', [
                    'status'=>$flag
                    ],'id = '. $stock_id)->execute();
        }
        else
        {
             $model->status=$flag;
             if($model->status==2)
             {
                /*$success=Yii::$app->db->createCommand()
                    ->update('stock', [
                    'status'=>$flag
                    ],'id = '. $stock_id)->execute();*/
                $stockupdate->stock_name=$model->stock_name;
                $stockupdate->symbol=$model->symbol;
                $stockupdate->no_of_shares=$model->no_of_shares;
                $stockupdate->share_price=$model->share_price;
                $stockupdate->suggestion=$model->suggestion;
                $stockupdate->action=$model->action;
                $stockupdate->actual_event=$model->actual_event;
                $stockupdate->status=2;
                $stockupdate->date=$model->date;
                $stockupdate->save();
             }
        }
        if($model->status==2)
            { $stockupdate->sendStockmail($stockupdate->created_by,$stockupdate->stock_name,2); }
        else if($model->status==0)
        { $stockupdate->sendStockmail($stockupdate->created_by,$stockupdate->stock_name,3); }

        $model->save();       
        //1:Create, 0: Update, 2:approval
        
        return $this->redirect(['stockapproval']);

    }
    public function actionSendEmail($id)
    {
        $stockmails = new \backend\models\Stockmails();
        $searchModel = new \backend\models\StockmailsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);
        $model = $this->findModel($id);

        if ($stockmails->load(Yii::$app->request->post())) {
            $user_id = implode(",",$_POST['Stockmails']['user_id']);
            $Userid  = explode (",", $user_id);  
            for($i=0;$i<count($Userid);$i++)
            {
               // echo $Userid[$i];die;
                $stockemail= new \backend\models\Stockmails();
                $stockemail->stock_id = $model->id;
                $stockemail->user_id = $Userid[$i];
                $User= \app\models\User::find()->where(['status'=>1, 'id'=>$Userid[$i]])->one();
                $sent_to=$User['email'];
                $sender_name=$User['first_name'];
                $stockemail->subject= $_POST['Stockmails']['subject'];
                $stockemail->email_body= $_POST['Stockmails']['email_body'];
                $stockemail->created_by=Yii::$app->user->id;
                $stockemail->created_at=date('Y-m-d H:i:s');
                $sent_from='info@vishwaswealthadvisory.com';
                if($stockemail->save())
                {
                                        //echo $sent_to."->".$Coupon->subject.'->'.$Coupon->email_body;die;
                    $this->actionSendstockemail($sent_from, $sent_to, $sender_name, $stockemail->subject, $stockemail->email_body);
                }
            }
           
            return $this->redirect(['index']);
        }

        return $this->render('sendemail', [
            'model' => $model,
            'stockmails' => $stockmails,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);


    }
    public function actionResendEmail($id, $user_id)
    {
        $stockmails = new \backend\models\Stockmails();
        $searchModel = new \backend\models\StockmailsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);
        $model = $this->findModel($id);
        $MailHistory= \backend\models\Stockmails::find()->where(['coupon_id'=>$model->id, 'user_id'=>$user_id])->one();
        $User= \app\models\User::find()->where(['status'=>1, 'id'=>$user_id])->one();
        $sent_to=$User['email'];
        $sender_name=$User['first_name'];
        $sent_from='info@vishwaswealthadvisory.com';
        if($this->actionSendstockemail($sent_from, $sent_to, $sender_name, $MailHistory->subject, $MailHistory->email_body))
        {
            Yii::$app->session->setFlash('success', $model->code. " mail resent to ". $sender_name." successfully."); 
            return $this->redirect(['send-email', 'id'=>$model->id]);
          
        }

    }
    public function actionSendstockemail($sent_from, $sent_to, $sender_name, $subject, $content)
    {
        require '../../vendor/autoload.php'; // If you're using Composer (recommended)        
        $email = new \SendGrid\Mail\Mail(); 
        $email->setFrom($sent_from, "Vishwas Wealth Advisory");
        $email->setSubject($subject);
        $email->addTo($sent_to, $sender_name);
        $email->addContent(
            "text/html", $content
        );
       // $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'))
        $sendgrid = new \SendGrid(Yii::$app->params['SENDGRID_API_KEY']);
        try {
            $response = $sendgrid->send($email);
            return true;
            // print $response->statusCode() . "\n";
            // print_r($response->headers());
            // print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";die;
        }
    }
   
    public function actionSendbulkemail()
    {
        $stockmails = new \backend\models\BulkEmails();
        $model = $stockmails;

        if(isset($_POST["submit_lower"])){

            $model->attributes = \Yii::$app->request->post('BulkEmails');

            $file = UploadedFile::getInstance($model, 'csv_file');
            
            $fp = fopen($file->tempName, 'r');
            if($fp)
            {
                //  $line = fgetcsv($fp, 1000, ",");
                //  print_r($line); exit;
                $first_time = true;
                do {
                    if ($first_time == true) {
                        $first_time = false;
                        continue;
                    }

                    $email = filter_var($line[0], FILTER_SANITIZE_EMAIL);
                    //echo $email;die;
                    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

                        $BulkEmails = new \backend\models\BulkEmails;
                        $BulkEmails->email_id = $line[0];
                        $BulkEmails->subject = $model->subject;
                        $BulkEmails->body = $model->body;
                        $BulkEmails->save(false);

                        $sent_to = $BulkEmails->email_id;
                        $sender_name = "Vishwas Wealth Advisory";
                        $sent_from = 'info@vishwaswealthadvisory.com';
                        $this->actionSendstockemail($sent_from, $sent_to, $sender_name, $model->subject, $model->body);
                    }
                }while( ($line = fgetcsv($fp, 1000, ";")) != FALSE);

                Yii::$app->session->setFlash('success', " Emails are sent successfully."); 
                $this->redirect('sendbulkemail');

            }else{
                echo "file not";die;
            }
        }

        return $this->render('sendbulkemail', [
            'model' => $model,
            'stockmails' => $stockmails,
        ]);
    }
}
