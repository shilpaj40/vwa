<?php

namespace backend\controllers;

use Yii;
use backend\models\Users;
use app\models\User;
use yii\grid\GridView;
use yii\helpers\Html;

use backend\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use fedemotta\datatables\DataTables;
use yii\web\UploadedFile;

/**
 * UsersController implements the CRUD actions for User model.
 */
class UsersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function beforeAction($action) 
    { 
        $this->enableCsrfValidation = false; 
        return parent::beforeAction($action); 
    }


 public function actionSubscriptionEnd()
    {

         $searchModel = new UsersSearch();
        // $status="";
        $dataProvider = $searchModel->search_subscription_end(Yii::$app->request->queryParams);
        // $model=new Users();
        // $model->scenario = Users::SCENARIO_REPORT;

        return $this->render('subscription_end_users', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            
        ]);
        

    }



    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $status="";
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$status,2);
        $model=new Users();
        $model->scenario = Users::SCENARIO_REPORT;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
            'flag' => 2
        ]);
        
    }
    public function actionUserslist()
    {
        $searchModel = new UsersSearch();
        $status="";
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$status,0);
        $model=new Users();
        $model->scenario = Users::SCENARIO_REPORT;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
            'flag' => 1
        ]);
        
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

   
    public function actionChangestatus($id,$flag)
    {
        $success=Yii::$app->db->createCommand()
                    ->update('user', [
                    'status'=>$flag
                    ],'id = '. $id)->execute();
 
        return $this->redirect(['index']);

    }
    public function actionUnblock($id,$flag)
    {
        $success=Yii::$app->db->createCommand()
                    ->update('user', [
                    'status'=>1
                    ],'id = '. $id)->execute();
        return $this->redirect(['index']);

    }

    public function actionGetcompanywiselisting( $status)
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $status);
        $data="";
        echo  DataTables::widget([
                  'dataProvider' => $dataProvider,
                  'id'=>'result',
                  'clientOptions' => [
                      'lengthMenu' => [200],
                      'responsive' => true, 
                      'dom' => 'BfTrtip',
                      'buttons' => [
                        'csv','excel', 'pdf', 'print'
                        ]
                    ],
                  'columns' => [
                     // ['class' => 'yii\grid\SerialColumn'],
                        'customer_id',
                        [
                            'attribute' => 'fullname',
                            'value' => function($data) { return $data->first_name  ." ". $data->last_name; }
                        ],
                        'username',
                        'email',
                        'mobile',
                        'city',
                        // [
                        //     'attribute' => 'birth_date',
                        //     'value' => function ($model) { 
                        //           return ($model->birth_date!="0000-00-00")?date_format(date_create($model->birth_date),'d-m-Y'):"";
                        //     },
                        // ],
                        [
                            'header' => 'Last Visit Date',
                            'format'=> 'raw',
                            'value' => function ($model) { 
                                   return $this->getLastVisit($model->id);
                            },
                        ], 
                        [
                            'attribute' => 'created_at',
                            'format'=> 'raw',
                            'header' => 'Registration Date',
                            'value' => function ($model) { 
                                return ($model->created_at!=NULL && $model->created_at!="0000-00-00")?"<span style='display:none'>".date_format(date_create($model->created_at),'dmYHis')."</span>".date_format(date_create($model ->created_at),'d-m-Y  H:i:s'):"-";
                            },
                        ], 
                        
                        [
                            'label'=> '#',
                            'format'=> 'raw',
                            'value' => function($model) { 
                                 return  ($model->status == 1)?Html::a('<i class="fa fa-times-circle"></i>', ['users/changestatus?id='.$model->id.'&flag=2'],array('style'=>'padding:2px 10px','class'=>'btn btn-danger','data-confirm'=>'Are you sure you want to deactivate this user?')):Html::a('<i class="fa fa-check"></i>', ['users/changestatus?id='.$model->id.'&flag=1'],array('style'=>'padding:2px 10px','class'=>'btn btn-success','data-confirm'=>'Are you sure you want to activate this user?'));
                            }
                        ],
                    ],
              ]).'<script>jQuery(function ($) {
$("#datatables_result").DataTable();
});</script> ';

    }

    public function actionGetstatuswiselisting()
    {
        if($_POST)
        {
            $status=$_POST['status'];

            $data=$this->actionGetcompanywiselisting($status);
           // echo $data;
        }
    }

    
    public function showbtn($status, $id)
    {
        if($status==1)
            return Html::a('Inactive', ['users/changestatus?id='.$id.'&flag=2'],array('class'=>'btn btn-danger','data-confirm'=>'Are you sure you want to block this user?'));
        else if($status==2)
            return Html::a('Active', ['users/changestatus?id='.$id.'&flag=1'],array('class'=>'btn btn-green','data-confirm'=>'Are you sure you want to activate this user?'));
        else
            return "";
    }

    public function getLastVisit($id)
    {
        $login = \app\models\LoginDetails::find()->where(['login_user_id' => $id])->orderBy(['login_detail_id'=>SORT_DESC])->one();
        return (!empty($login))?"<span style='display:none'>".date_format(date_create($login->login_at),'dmYHis')."</span>".date_format(date_create($login->login_at),'d-m-Y  H:i:s'):"-";
    }

    public function actionCreate()
    {
        $model = new Users();
        $model->scenario = Users::SCENARIO_CREATE;

        if ($model->load(Yii::$app->request->post()))
             {
                if($model->user_type==2){
                    $UserCount = Yii::$app->db->createCommand('SELECT count(*) as count FROM user where MONTH(created_at)=MONTH(CURDATE()) and YEAR(created_at) = YEAR(CURDATE()) and status="1" and user_type="2"')
                   ->queryOne();
                   $UserCount = sprintf('%03d',($UserCount['count']+1)); 
                        $alphachar = array_merge(range('A', 'Z'), range('a', 'z'));
                        $upperArr = range('A', 'Z') ;                
                        $fCharYear= $upperArr[(substr( date("y"), -2,1))-1];
                        $sCharYear= $upperArr[(substr( date("y"), -1,1))-1];
                        $yearQuarter = ceil(date("n") / 3);
                        $month = ltrim(date("m"), '0'); 
                        $monthCount = $upperArr[$yearQuarter-1];
                    $customer_id = $fCharYear.$sCharYear.$monthCount.$month.$UserCount;
                    echo $customer_id;
                     $model->customer_id=$customer_id;
                } 
                 $oldpassword=$model->password;
                 $model->birth_date=date("Y-m-d",strtotime($model->birth_date));
                 $model->username=$model->email;
                 $model->password=sha1($model->password);
                 $model->profile_pic = UploadedFile::getInstance($model, 'profile_pic');
                if($model->save()) {
                    if(isset($model->profile_pic)){
                    $model->profile_pic->saveAs('profile/' . $model->profile_pic->baseName . '.' . $model->profile_pic->extension, false);
                $filepath=Yii::$app->params['server']."profile/". $model->profile_pic;
                $model->profile_pic= $filepath;
                    }
                    if($model->save())
                    {
                        require '../../vendor/autoload.php'; 
                       $email = new \SendGrid\Mail\Mail(); 
                       $content='<div style="font:18px/23px Arial,Helvetica,sans-serif;background:#f3f3f3">
                     <div style="max-width:800px;margin-left:auto;margin-right:auto">
                        <div style="width:640px;margin-left:auto;margin-right:auto;padding:20px 0px 20px 0px; style="text-align: center;">
                            <table class="x_-665217761MsoNormalTable" border="1" cellspacing="3" cellpadding="0" style="border: solid rgb(204,204,204) 1.0pt;background: #fff;color: #000;width:98%">
                          <tbody>
                          <tr>
                              <td style="border: none;padding: 2pt 22.5pt 2pt 22.5pt;">
                              <h2 align="center" style="text-align: center;font-family: Helvetica;">Welcome to vishwaswealthadvisory.com</h2>                             
                              </td>
                            </tr>
                          <tr>
                              <td style="border: none;padding: 2pt 22.5pt 2pt 22.5pt;">
                               <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">
                              Thank you for registering with us</p> </td></tr>';
                           if($model->user_type==2){                           
                              $content.='<tr>
                              <td style="border: none;padding: 2pt 22.5pt 2pt 22.5pt;">
                               <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">
                               Your customer id is '.$model->customer_id.'
                               Please use the above customer id for further communication </p> </td></tr>';
                              }
                              else
                              {
                                 $content.='<tr>
                              <td style="border: none;padding: 2pt 22.5pt 2pt 22.5pt;">
                               <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">Log in your new account.Your new account details</p>
                              </td>
                            </tr>
                             <tr>
                              <td style="border: none;padding: 2pt 22.5pt 2pt 22.5pt;">
                               <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;"><b>Username - </b>'.$model->username.'<br><b>Password - </b>'.$oldpassword.'</p>
                              </td>
                            </tr>';
                              }
                              $content.='<tr>
                              <td style="border: none;padding: 5pt 22.5pt 5pt 22.5pt;">
                               <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">Vishwas Soman <br>Thank You<br><a href="https://www.vishwaswealthadvisory.com/" target="_blank"> www.vishwaswealthadvisory.com</a></span></p>
                              </td>
                            </tr>                           
                          </tbody>
                        </table> 
                        </div>
                     </div>
                  </div>';
                  $email->setFrom("info@vishwaswealthadvisory.com","Vishwas Wealth Advisory");
                $email->setSubject('Sign-in to Vishwas Wealth Advisory' );
                $email->addTo($model->email);
                $adminEmail = \app\models\User::find()->select('email')->where(['status' => 1, 'user_type'=>1])->one();
                if($adminEmail->email!="")
                 $email->addCc($adminEmail->email);
                $email->addContent(
                    "text/html", $content
                );
                $sendgrid = new \SendGrid(Yii::$app->params['SENDGRID_API_KEY']);
                try {
                    $response = $sendgrid->send($email);
                    } catch (Exception $e) {
            $response= 'Caught exception: '. $e->getMessage() ."\n";
                    }
                    }
                    return $this->redirect(['view', 'id' => $model->id]);
                
                }
           }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

     public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $image=$model->profile_pic;
        if ($model->load(Yii::$app->request->post()))
             {
                 $model->birth_date=date("Y-m-d",strtotime($model->birth_date));
                 $model->username=$model->email;
                 $model->profile_pic = UploadedFile::getInstance($model, 'profile_pic');
            if($model->profile_pic=="")
            {
                $model->profile_pic=$image;
            }
            else
            {
                if ($model->profile_pic && $model->validate()) {                
                    $model->profile_pic->saveAs('profile/' . $model->profile_pic->baseName . '.' . $model->profile_pic->extension, false);
                    $filepath=Yii::$app->params['server']."profile/". $model->profile_pic;
                    $model->profile_pic= $filepath;
                }
            }
                if($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
           }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
}
