<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\LoginForm;
use yii\web\UploadedFile;
use backend\models\PasswordForm;
use backend\models\PasswordResetRequestForm;
use backend\models\ResetPasswordForm;

/**
* Site controller
*/
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'permission','changepassword', 'profile', 'edit-profile','request-password-reset','reset-password'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                   
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
   

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $login = new \app\models\LoginDetails();

        if ($model->load(Yii::$app->request->post()) ) {
            $log = \app\models\User::find()->where(['username' => $_POST['LoginForm']['username'], 'status' => 1])->andWhere(['<>','user_type', 2])->one();

           // print_r($_POST['LoginForm']['username']);die;
            if(empty($log)) {
                \Yii::$app->session->setFlash('loginError', '<i class="fa fa-warning"></i><b> Incorrect username or password. !</b>');
                return $this->render('login', ['model' => $model]);
            }
            $login->login_user_id = $log['id'];
            $login->login_status = 1;
            $login->login_at = new \yii\db\Expression('NOW()');
            $login->user_ip_address=$_SERVER['REMOTE_ADDR'];
            $login->save(false);
            if($model->login()) {
                return $this->goBack();
            }
            else
                return $this->render('login', ['model' => $model,]);
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {

        if(isset(Yii::$app->user->id))
        \app\models\LoginDetails::updateAll(['login_status' => 0, 'logout_at'=> new \yii\db\Expression('NOW()')],'login_user_id='.Yii::$app->user->id.' AND login_status = 1');
        Yii::$app->user->logout();
        return $this->redirect(['login']);

        //return $this->goHome();
    }
    public function actionPermission()
    {
          return $this->render('denied');
    }
     public function actionProfile($id)
    {
        $model = \app\models\User::findOne($id);

        return $this->render('profile',[
                    'model'=>$model
                ]);
    }
    public function actionEditProfile($id)
    {
        $model = \backend\models\Users::findOne($id);
        $image=$model->profile_pic;
        if ($model->load(Yii::$app->request->post()))
             {
                 $model->birth_date=date("Y-m-d",strtotime($model->birth_date));
                 $model->username=$model->email;
                 $model->profile_pic = UploadedFile::getInstance($model, 'profile_pic');
            if($model->profile_pic=="")
            {
                $model->profile_pic=$image;
            }
            else
            {
                if ($model->profile_pic && $model->validate()) {                
                    $model->profile_pic->saveAs('profile/' . $model->profile_pic->baseName . '.' . $model->profile_pic->extension, false);
                    $filepath=Yii::$app->params['server']."profile/". $model->profile_pic;
                    $model->profile_pic= $filepath;
                }
            }
            if($model->save())
            {
                Yii::$app->session->setFlash('success', "Profile updated successfully."); 
                return $this->redirect(['profile', 'id'=>$id]);
            }
        }
        return $this->render('edit_profile',[
                    'model'=>$model
                ]);
    }
public function actionRequestPasswordReset()
    {
        $this->layout = '/../layouts/main-login';
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
               // return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    
    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = '/../layouts/main-login';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');
            return $this->goHome();
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionChangepassword(){

        $model = new PasswordForm;
        $modeluser = \backend\models\Users::find()->where([
            'username'=>Yii::$app->user->identity->username
        ])->one();
        
        if($model->load(Yii::$app->request->post())){
            if($model->validate()){
                try{
                    $modeluser->password = sha1($_POST['PasswordForm']['newpass']);
                    if($modeluser->save()){
                    require '../../vendor/autoload.php'; 
                    $email = new \SendGrid\Mail\Mail(); 
                        $content='<div style="font:18px/23px Arial,Helvetica,sans-serif;background:#f3f3f3">
                 <div style="max-width:800px;margin-left:auto;margin-right:auto">
                    <div style="width:640px;margin-left:auto;margin-right:auto;padding:20px 0px 20px 0px; style="text-align: center;">
                        <table class="x_-665217761MsoNormalTable" border="1" cellspacing="3" cellpadding="0" style="border: solid rgb(204,204,204) 1.0pt;background: #fff;color: #000;width:98%">
                      <tbody>
                        <tr>
                          <td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                          <h2 align="center" style="text-align: center;font-family: Helvetica;">Change Password</h2>
                          <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">Your password for VWA changed successfully. New login details: </span></p>                        
                          
                          </td>
                        </tr>
                       
                        <tr>
                          <td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                           <p class="MsoNormal" align="center" style="text-align: left;font-family: Helvetica;"><span style="font-size: 10.0pt;"><b>Username: </b>'.$modeluser->username.'</span></p>
                          </td>
                        </tr>
                         <tr>
                          <td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                           <p class="MsoNormal" align="center" style="text-align: left;font-family: Helvetica;"><span style="font-size: 10.0pt;"><b>Password: </b>'.$_POST['PasswordForm']['newpass'].'</span></p>
                          </td>
                        </tr>
                        <tr>
                          <td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                          <p class="MsoNormal" align="center" style="font-family: Helvetica;text-align: center;"><span style="font-size: 10.0pt;font-family: Helvetica;">Thank You</span></p></td>
                        </tr>
                      </tbody>
                    </table> 
                    </div>
                 </div>
              </div>';
                $email->setFrom("info@vishwaswealthadvisory.com","Vishwas Wealth Advisory");
                $email->setSubject('Change Password - Vishwas Wealth Advisory' );
                $email->addTo($modeluser->email);
                $email->addContent(
                    "text/html", $content
                );
                $sendgrid = new \SendGrid(Yii::$app->params['SENDGRID_API_KEY']);
                        try {
                            $response = $sendgrid->send($email);
                            // $response= $response->statusCode() . "\n";
                            // $response=($response->headers());
                            // $response= $response->body() . "\n";
                        } catch (Exception $e) {
                            $response= 'Caught exception: '. $e->getMessage() ."\n";
                        }


                        Yii::$app->getSession()->setFlash(
                            'success','Password changed. Password sent on your mail'
                        );
                        return $this->redirect(['profile','id'=>$modeluser->id]);
                    }else{
                        Yii::$app->getSession()->setFlash(
                            'error','Password not changed'
                        );
                       // return $this->redirect(['index']);
                    }
                }catch(Exception $e){
                    Yii::$app->getSession()->setFlash(
                        'error',"{$e->getMessage()}"
                    );
                    return $this->render('changepassword',[
                        'model'=>$model
                    ]);
                }
            }else{
                return $this->render('changepassword',[
                    'model'=>$model
                ]);
            }
        }else{
            return $this->render('changepassword',[
                'model'=>$model
            ]);
        }
  }
   
}