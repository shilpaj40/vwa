<?php

namespace backend\controllers;

use Yii;
use backend\models\Orders;
use backend\models\OrdersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Orders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Orders();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }





   public function actionApproved()
   {
      $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search_approve(Yii::$app->request->queryParams);

        return $this->render('approved', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

  // return $this->render('approved',['model'=>$model]);
   }

 public function actionRejected()
   {

 $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search_rejected(Yii::$app->request->queryParams);

        return $this->render('rejected', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
 //return $this->render('rejected');
   }



public function actionRejectdstatus($id,$flag)
{

     $success=Yii::$app->db->createCommand()->update('orders', [
                    'payment_status'=>$flag
                    ],'id = '. $id)->execute();

    Yii::$app->session->setFlash('success', "Subscribed User are Rejected successfully."); 
     return $this->redirect(['index']);

}

public function actionApprovestatus($id,$flag)
{

     $success=Yii::$app->db->createCommand()->update('orders', [
                    'payment_status'=>$flag
                    ],'id = '. $id)->execute();

  $orderdata = \backend\models\Orders::find()->where(['id' => $id])->one();
   $user_id=$orderdata['email'];

 // print_r($user_id);die;
   $user = \app\models\User::find()->where(['username' => $user_id])->one();

   $payment_start=date('Y-m-d H:i:s');

   $effectiveDate = date('Y-m-d H:i:s', strtotime("+".$orderdata['subcription_plan']." months", strtotime($payment_start)));
           // echo "$effectiveDate";die;
      //payment_start_date=date('Y-m-d H:i:s');;
      // $model->payment_end_date=$effectiveDate;

  //print_r($user['id']);die;
   if(!empty($user))
   {

    $success=Yii::$app->db->createCommand()->update('user', [
                    'status'=>1,'subscription_start_date'=>$payment_start,'subscription_end_date'=>$effectiveDate
                    ],'id = '. $user['id'])->execute();

    $successfully=Yii::$app->db->createCommand()->update('orders', [
                    'user_id'=>$user['id'],'payment_end_date'=>$effectiveDate,'payment_start_date'=>$payment_start
                    ],'id = '. $id)->execute();


   }else
   {

            $successdata=Yii::$app->db->createCommand()
        ->insert('user', [
        'first_name' => $orderdata['first_name'],
        'last_name' => $orderdata['last_name'],
        'mobile' => $orderdata['phone'],
        'email'=>$orderdata['email'],
        'username'=>$orderdata['email'],
        'city'=>$orderdata['city'],
        'subscription_start_date'=>$payment_start,
        'subscription_end_date'=>$effectiveDate,
        'status'=>1,
        ])->execute();

        if($successdata==1)
$user_id = Yii::$app->db->getLastInsertID();


 $successfully=Yii::$app->db->createCommand()->update('orders', [
                    'user_id'=>$user_id,'payment_end_date'=>$effectiveDate,'payment_start_date'=>$payment_start
                    ],'id = '. $id)->execute();


   }

      Yii::$app->session->setFlash('success', "Subscribed User are Approved successfully."); 
     return $this->redirect(['index']);

}







   

    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
