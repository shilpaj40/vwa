<?php

namespace backend\controllers;

use Yii;
use backend\models\About;
use backend\models\AboutSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\helpers\Json;
/**
 * AboutController implements the CRUD actions for About model.
 */
class AboutController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    // public function actions()
    // {
    //     return [
    //         'uploadPhoto' => [
    //             'class' => 'budyaga\cropper\actions\UploadAction',
    //             'url' => 'http://localhost:81/shelly/backend/web/images',
    //             'path' => '@backend/web/images',
    //         ]
    //     ];
    // }
    /**
     * Lists all About models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AboutSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single About model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView()
    {
        $id=$_REQUEST['id'];
        $model = $this->findModel($id);
        $html="";
        $html.='<div class="pull-left">
                    <img src="'.$model->image.'" class="col-lg-6 mb-20" class="img-responsive" alt="Responsive image">
                    <p class="col-md-6">'.$model->body.' </p> 
                </div>';
      
        return $html;
    }

    /**
     * Creates a new About model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new About();

        if ($model->load(Yii::$app->request->post()) ) {
            $model->created_by=Yii::$app->user->id;
            if(isset($model->photo))
            {
                $image=$model->photo;
                $uploaddir="images";
                $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $image));
                $filename=$model->id.".jpg";
                $image_name=time().$filename;
                $newname=$uploaddir."/".$image_name; 
                file_put_contents($newname, $data); 
                $filepath=Yii::$app->params['server'].''.$newname;
                $model->photo="";
                $model->image = $filepath; 
            }
            if($model->save())
            {
                Yii::$app->session->setFlash('success', $model->title. " added successfully."); 
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing About model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $image=$model->image;

        if ($model->load(Yii::$app->request->post())) {
            $model->updated_at = new \yii\db\Expression('NOW()');
            $model->updated_by = Yii::$app->user->id;
            if($model->photo=="")
            {
                $model->image=$image;
            }
            else
            {
                $image=$model->photo;
                $uploaddir="images";
                $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $image));
                $filename=$model->id.".jpg";
                $image_name=time().$filename;
                $newname=$uploaddir."/".$image_name; 
                file_put_contents($newname, $data); 
                $filepath=Yii::$app->params['server']."".$newname;
                $model->photo="";
                $model->image = $filepath;     
            }
            if($model->save())
            {
                Yii::$app->session->setFlash('success', $model->title. " updated successfully."); 
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing About model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the About model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return About the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = About::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
     
}
