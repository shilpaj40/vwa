<?php

namespace backend\controllers;

use Yii;
use backend\models\Faq;
use backend\models\FaqSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FaqController implements the CRUD actions for Faq model.
 */
class FaqController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Faq models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FaqSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Faq model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Faq model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Faq();
        if ($model->load(Yii::$app->request->post()) ) {
            $model->created_by=Yii::$app->user->id;
            if($model->save())
            {
                Yii::$app->session->setFlash('success', "FAQ added successfully."); 
                return $this->redirect(['/faq']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Faq model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if($model->load(Yii::$app->request->post())) {
            $model->updated_at = new \yii\db\Expression('NOW()');
            $model->updated_by = Yii::$app->user->id;
            if($model->save())
            {
                Yii::$app->session->setFlash('success', "FAQ updated successfully."); 
                return $this->redirect(['/faq']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Faq model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['/faq']);
    }

    /**
     * Finds the Faq model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Faq the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Faq::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
     public function actionChangestatus($id,$flag)
    {
        $success=Yii::$app->db->createCommand()
                    ->update('faq', [
                    'status'=>$flag
                    ],'id = '. $id)->execute();
 
        return $this->redirect(['/faq']);

    }
    public function actionDeletefaq()
    {
        $pk = Yii::$app->request->post('row_id');
        foreach ($pk as $key => $value)
        {
           $this->findModel($value)->delete();
        }
        Yii::$app->session->setFlash('success', "Records deleted successfully."); 
        return $this->redirect(['/faq']);
    }
    public function actionSeo()
    {
        $id="-1";
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save() ) {
            Yii::$app->session->setFlash('success', "SEO added successfully."); 
            return $this->redirect(['index']);
            
        }
        return $this->render('add_seo', [
            'model' => $model,
        ]);
    }
}
