<?php

namespace backend\controllers;

use Yii;
use backend\models\HomeBlocks;
use backend\models\HomeBlocksSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * HomeBlocksController implements the CRUD actions for HomeBlocks model.
 */
class HomeBlocksController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all HomeBlocks models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HomeBlocksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HomeBlocks model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new HomeBlocks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new HomeBlocks();

        if ($model->load(Yii::$app->request->post()) ) {
            $model->created_by=Yii::$app->user->id;
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->logo = UploadedFile::getInstance($model, 'logo');
            $model->footer_image = UploadedFile::getInstance($model, 'footer_image');
             if (isset($model->image)) {   
                 $model->image->saveAs('images/homeblock/' . $model->image->baseName . '.' . $model->image->extension, false);
                 $model->image=Yii::$app->params['server'].'images/homeblock/'.$model->image;
            }
            if (isset($model->logo)) {   
                 $model->logo->saveAs('images/homeblock/' . $model->logo->baseName . '.' . $model->logo->extension, false);
                 $model->logo=Yii::$app->params['server'].'images/homeblock/'.$model->logo;
            }
            if (isset($model->footer_image)) {   
                 $model->footer_image->saveAs('images/homeblock/' . $model->footer_image->baseName . '.' . $model->footer_image->extension, false);
                 $model->footer_image=Yii::$app->params['server'].'images/homeblock/'.$model->footer_image;
            }
           /* if(isset($model->photo))
            {
                $image=$model->photo;
                $uploaddir="images/homeblock";
                $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $image));
                $filename=$model->id.".jpg";
                $image_name=time().$filename;
                $newname=$uploaddir."/".$image_name; 
                file_put_contents($newname, $data); 
                $filepath=Yii::$app->params['server'].''.$newname;
                $model->photo="";
                $model->image = $filepath; 
            }*/
            if($model->save())
            {
                Yii::$app->session->setFlash('success', " Details added successfully."); 
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionSeo()
    {
        $id="-1";
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save() ) {
            Yii::$app->session->setFlash('success', "SEO added successfully."); 
            return $this->redirect(['index']);
            
        }
        return $this->render('add_seo', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing HomeBlocks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $image=$model->image;
        $logo=$model->logo;
        $footer_image=$model->footer_image;
        if ($model->load(Yii::$app->request->post()) ) {
            $model->updated_by = Yii::$app->user->id;
            $model->updated_at = new \yii\db\Expression('NOW()');
            $model->logo = UploadedFile::getInstance($model, 'logo');
             $model->image = UploadedFile::getInstance($model, 'image');
             $model->footer_image = UploadedFile::getInstance($model, 'footer_image');
             if(($model->image=="" )||($model->image==null))
            {
                $model->image=$image;
            }
            else {   
                $model->image->saveAs('images/homeblock/' . $model->image->baseName . '.' . $model->image->extension, false);
                $model->image=Yii::$app->params['server'].'images/homeblock/'.$model->image;
            }
            if(($model->logo=="" )||($model->logo==null))
            {
                $model->logo=$logo;
            }
            else {   
                $model->logo->saveAs('images/homeblock/' . $model->logo->baseName . '.' . $model->logo->extension, false);
                $model->logo=Yii::$app->params['server'].'images/homeblock/'.$model->logo;
            }
            if(($model->footer_image=="" )||($model->footer_image==null))
            {
                $model->footer_image=$footer_image;
            }
            else {   
                $model->footer_image->saveAs('images/homeblock/' . $model->footer_image->baseName . '.' . $model->footer_image->extension, false);
                $model->footer_image=Yii::$app->params['server'].'images/homeblock/'.$model->footer_image;
            }
            /*if($model->photo=="")
            {

                $model->image=$image;
            }
            else
            {
                $image=$model->photo;
                $uploaddir="images/homeblock";
                $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $image));
                $filename=$model->id.".jpg";
                $image_name=time().$filename;
                $newname=$uploaddir."/".$image_name; 
                file_put_contents($newname, $data); 
                $filepath=Yii::$app->params['server']."".$newname;
                $model->photo="";
                $model->image = $filepath;     
            }*/
            if($model->save())
            {
                Yii::$app->session->setFlash('success', " Details updated successfully."); 
                return $this->redirect(['index']);

            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing HomeBlocks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the HomeBlocks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return HomeBlocks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HomeBlocks::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
