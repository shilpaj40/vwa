<?php

namespace backend\controllers;

use Yii;
use backend\models\Resources;
use backend\models\ResourcesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ResourcesController implements the CRUD actions for Resources model.
 */
class ResourcesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Resources models.
     * @return mixed
     */
    public function beforeAction($action) 
    { 
        $this->enableCsrfValidation = false; 
        return parent::beforeAction($action); 
    }
    public function actionIndex()
    {
        $searchModel = new ResourcesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, "Video");

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAdmin()
    {
        $searchModel = new ResourcesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, "Documents");

        return $this->render('admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Resources model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Resources model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Resources();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post()) ) {
            $model->resource_type="Video";
            $model->created_at=date('Y-m-d H:i:s');
            
            if($model->save(false))
            {
                return $this->redirect(['/resources']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAdd()
    {
        $model = new Resources();
        $model->scenario = 'document';
        if ($model->load(Yii::$app->request->post()) ) {
            $model->resource_type="Documents";
            $model->created_at=date('Y-m-d H:i:s');
            $model->file_path = UploadedFile::getInstance($model, 'file_path');
            $file_name= $model->file_path->baseName . '.' . $model->file_path->extension;
            if ($model->file_path) {   
                $model->file_path->saveAs('resources/' . $model->file_path->baseName . '.' . $model->file_path->extension, false);// echo "dw";die;
                 $model->file_path=Yii::$app->params['server'].'resources/'.$model->file_path;
            }
            if($model->save(false))
            {
             // Yii::$app->db->createCommand()
             //        ->update('resources', [
             //        'file_path'=>Yii::$app->params['server'].'resources/'.$model->file_path
             //        ],'id = '. $model->id)->execute();
                return $this->redirect(['admin']);
            }
        }

        return $this->render('add', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Resources model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
       // $model->scenario="create";
        $model->scenario = "update";
        if ($model->load(Yii::$app->request->post())) {
            //echo $model->scenario;die;
            $model->scenario = "update";
            if($model->save())
                return $this->redirect(['/resources']);
            else
            {
                print_r($model->errors);die;
            }
        }
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdateDocument($id)
    {
        $model = $this->findModel($id);
         $model->scenario = "update_doc";
        $file=$model->file_path;

        if ($model->load(Yii::$app->request->post())) {
             $model->scenario = "update_doc";
            $model->file_path = UploadedFile::getInstance($model, 'file_path');
            if($model->file_path=="")
            {
                //echo "D";die;
                $model->file_path=$file;  
            }
            if($model->save())
            {
                if($model->file_path!=$file)
                {
                   $model->file_path->saveAs('resources/' . $model->file_path->baseName . '.' . $model->file_path->extension, false);// echo "dw";die;
                  
                    Yii::$app->db->createCommand()
                    ->update('resources', [
                    'file_path'=>Yii::$app->params['server'].'resources/'.$model->file_path
                    ],'id = '. $model->id)->execute();
                }
            return $this->redirect(['admin']);

            }
        }
        return $this->render('update-document', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Resources model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['/resources']);
    }

    /**
     * Finds the Resources model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Resources the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Resources::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    public function actionMultipledelete()
    {
        $pk = Yii::$app->request->post('row_id');
        foreach ($pk as $key => $value)
        {
           $this->findModel($value)->delete();
        }
        Yii::$app->session->setFlash('success', "Records deleted successfully."); 
        return $this->redirect(['admin']);
    }
    public function actionMultipledelete1()
    {
        $pk = Yii::$app->request->post('row_id');
        foreach ($pk as $key => $value)
        {
           $this->findModel($value)->delete();
        }
        Yii::$app->session->setFlash('success', "Records deleted successfully."); 
        return $this->redirect(['/resources']);
    }
    public function actionSeo()
    {
        $id="-2";
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save() ) {
            Yii::$app->session->setFlash('success', "SEO added successfully."); 
            return $this->redirect(['index']);
            
        }
        return $this->render('add_seo', [
            'model' => $model,
        ]);
    }
}
