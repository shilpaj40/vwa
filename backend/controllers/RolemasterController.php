<?php

namespace backend\controllers;

use Yii;
use backend\models\RoleMaster;
use backend\models\RoleMasterSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\ModulesList;
use backend\models\RoleModulePermission;

/**
 * RoleMasterController implements the CRUD actions for RoleMaster model.
 */
class RolemasterController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RoleMaster models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RoleMasterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RoleMaster model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RoleMaster model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RoleMaster();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           $role_id=$model->role_id;
            $AllMenu=ModulesList::find()->where(['is_active' => 1])->orderBy(['sequence'=>SORT_ASC])->all();
            foreach ($AllMenu as $Menu)
            {
                $menu_id=$Menu->module_id;
                if(isset($_POST[$menu_id]))
                {
                    $ispresent=RoleModulePermission::find()->where(['role_id' => $role_id, 'module_id'=>$menu_id])->one();
                    if($ispresent==null)
                    {
                        $model =new RoleModulePermission;
                        $model->role_id=$role_id;
                        $model->module_id=$menu_id;
                        $model->access=1;
                        $model->save(); 
                    }
                    else
                    { 
                        $ispresent->access=1;
                        $ispresent->save();  
                    }
                    $MenuModel=ModulesList::findOne($menu_id);
                    $parent_id=$MenuModel->parent_id;
                    while($parent_id!=0)
                    {

                        $ispresent=RoleModulePermission::find()->where(['role_id' => $role_id, 'module_id'=>$menu_id])->one();
                        if($ispresent==null)
                        {
                            $model =new RoleModulePermission;
                            $model->role_id=$role_id;
                            $model->module_id=$parent_id;
                            $model->access=1;
                            $model->save(); 
                        }
                        else
                        { 
                            $ispresent->access=1;
                            $ispresent->save();  
                        }
                        $MenuModel=ModulesList::findOne($parent_id);
                        $parent_id=$MenuModel->parent_id;               
                    }

                }
            }


            return $this->redirect(['view', 'id' => $role_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing RoleMaster model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           
            $role_id=$model->role_id;
            $result=Yii::$app->db->createCommand()
                            ->update('role_module_permission', [
                            'access'=>0
                            ],'role_id = '. $role_id)->execute();
            $AllMenu=ModulesList::find()->where(['is_active' => 1])->orderBy(['sequence'=>SORT_ASC])->all();
            foreach ($AllMenu as $Menu)
            {
                $menu_id=$Menu->module_id;
               // echo $_POST[$menu_id];
                if(isset($_POST[$menu_id]))
                {
                    $ispresent=RoleModulePermission::find()->where(['role_id' => $role_id, 'module_id'=>$menu_id])->one();
                   // print_r($ispresent);
                    if($ispresent==null)
                    {
                        $model =new RoleModulePermission;
                        $model->role_id=$role_id;
                        $model->module_id=$menu_id;
                        $model->access=1;
                        $model->save(); 
                    }
                    else
                    { 
                        $ispresent->access=1;
                        $ispresent->save();  
                    }
                    $MenuModel=ModulesList::findOne($menu_id);
                    $parent_id=$MenuModel->parent_id;
                    while($parent_id!=0)
                    {
                        $ispresent=RoleModulePermission::find()->where(['role_id' => $role_id, 'module_id'=>$menu_id])->one();
                        if($ispresent==null)
                        {
                            $model =new RoleModulePermission;
                            $model->role_id=$role_id;
                            $model->module_id=$parent_id;
                            $model->access=1;
                            $model->save(); 
                        }
                        else
                        { 
                            $ispresent->access=1;
                            $ispresent->save();  
                        }
                        $MenuModel=ModulesList::findOne($parent_id);
                        $parent_id=$MenuModel->parent_id;               
                    }
                }
               
            }
            return $this->redirect(['view', 'id' => $role_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RoleMaster model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RoleMaster model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RoleMaster the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RoleMaster::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionChangestatus($id,$flag)
    {
        $success=Yii::$app->db->createCommand()
                    ->update('role_master', [
                    'status'=>$flag
                    ],'role_id = '. $id)->execute();
 
        return $this->redirect(['index']);

    }
    public function actionMultipledeleterole()
    {
        $pk = Yii::$app->request->post('row_id');
        foreach ($pk as $key => $value)
        {
           $this->findModel($value)->delete();
        }
        Yii::$app->session->setFlash('success', "Records deleted successfully."); 
        return $this->redirect(['admin']);
    }
}
