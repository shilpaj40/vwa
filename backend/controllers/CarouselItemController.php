<?php

namespace backend\controllers;

use Yii;
use backend\models\CarouselItem;
use backend\models\CarouselItemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;


/**
 * CarouselItemController implements the CRUD actions for CarouselItem model.
 */
class CarouselItemController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CarouselItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CarouselItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CarouselItem model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CarouselItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CarouselItem();
        $model->scenario = 'create';

         if ($model->load(Yii::$app->request->post()) ) {
            $model->created_by=Yii::$app->user->id;
            $model->image = UploadedFile::getInstance($model, 'image');
            if (isset($model->image)) {   
                 $model->image->saveAs('images/banner/' . $model->image->baseName . '.' . $model->image->extension, false);
                 $model->image=Yii::$app->params['server'].'images/banner/'.$model->image;
            }
            if($model->save())
            {
                Yii::$app->session->setFlash('success', $model->name." carousel item added successfully."); 
                return $this->redirect(['/carouselitem']);
            } 
            else
            {
                print_r($model->errors);die;
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CarouselItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';

        $image=$model->image;

        if ($model->load(Yii::$app->request->post())) {
           // print_r(Yii::$app->request->post());die;
            $model->updated_by = Yii::$app->user->id;
            $model->updated_at = new \yii\db\Expression('NOW()');
            $model->image = UploadedFile::getInstance($model, 'image');

            if(isset($model->image))
            {
                 $model->image->saveAs('images/banner/' . $model->image->baseName . '.' . $model->image->extension, false);
                $model->image=Yii::$app->params['server'].'images/banner/'.$model->image;
                //$model->image=$image;
            }
            else {   
                $model->image=$image;
            }
            if($model->save())
            {
                
                Yii::$app->session->setFlash('success', $model->name." carousel item updated successfully."); 
                return $this->redirect(['/carouselitem']);

            }
            
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CarouselItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['/carouselitem']);
    }

    /**
     * Finds the CarouselItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CarouselItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CarouselItem::findOne($id)) !== null) {
            return $model;
        }
        
        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionDeleteslide()
    {
        $pk = Yii::$app->request->post('row_id');
        foreach ($pk as $key => $value)
        {
            $model = $this->findModel($value);
            $model->status=0;
            $model->deleted_at=date('Y-m-d H:i:s');
            $model->deleted_by=Yii::$app->user->id;
            $model->save(); 
        }
        Yii::$app->session->setFlash('success', "Records deleted successfully."); 
        return $this->redirect(['/carouselitem']);
    }
}
