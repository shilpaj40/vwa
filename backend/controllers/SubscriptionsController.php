<?php

namespace backend\controllers;

use Yii;
use backend\models\Subscriptions;
use backend\models\SubscriptionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SubscriptionsController implements the CRUD actions for Subscriptions model.
 */
class SubscriptionsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Subscriptions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SubscriptionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Subscriptions model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Subscriptions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Subscriptions();
        //print_r("expression");die;

       // if(isset($_POST['submit'])){
       //  echo "hii";die;
       // }
        if (Yii::$app->request->post() ) {
            //print_r($_POST);die;
            $num=$_POST['rowcount'];
            //echo $num;die;

            for($i=1;$i<=$num;$i++)
            {
                 $model = new Subscriptions();
                //($_POST['script_per_week'.$i]);die;
            $model->created_by=Yii::$app->user->id;
            $model->created_at=new \yii\db\Expression('NOW()');

              $model->script_per_week=$_POST['script_per_week'.$i];
              $model->one_month=$_POST['one_month'.$i];
              $model->twelve_month=$_POST['twelve_month'.$i];
              $model->three_month=$_POST['three_month'.$i];
              
             // echo $model->script_per_week;
        $model->save(false);   
         }
         Yii::$app->session->setFlash('success', "Records Added successfully."); 
         return $this->redirect(['index']);
            //print_r($model);

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Subscriptions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
       
       if($model->load(Yii::$app->request->post())) {
            $model->updated_at = new \yii\db\Expression('NOW()');
            $model->updated_by = Yii::$app->user->id;
            $model->script_per_week=$_POST['Subscriptions']['script_per_week'];
              $model->one_month=$_POST['Subscriptions']['one_month'];
              $model->twelve_month=$_POST['Subscriptions']['twelve_month'];
              $model->three_month=$_POST['Subscriptions']['three_month'];
            if($model->save())
            {
             
         Yii::$app->session->setFlash('success', "Records Updates successfully."); 
         return $this->redirect(['index']);

          }
            
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Subscriptions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

     public function actionMultipledelete()
    {
        $pk = Yii::$app->request->post('row_id');
        //print_r($pk);die;
        foreach ($pk as $key => $value)
        {
            $model = $this->findModel($value);
            $model->status=0;
            // $model->deleted_at=date('Y-m-d H:i:s');
            // $model->deleted_by=Yii::$app->user->id;
            $model->save(); 
        }
        Yii::$app->session->setFlash('success', "Records deleted successfully."); 
        return $this->redirect(['index']);
    }

    /**
     * Finds the Subscriptions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Subscriptions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Subscriptions::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
