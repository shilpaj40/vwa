<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'timeZone' => 'Europe/London',
    'components' => [
         'metaTags' => [
            'class' => 'v0lume\yii2\metaTags\MetaTagsComponent',
            'generateCsrf' => false,
            'generateOg' => true,
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'class' => 'common\components\Request',
            'web'=> '/frontend/web'
        ],
        
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
       
        'view' => [
            'theme' => [
                'pathMap' => ['@frontend/views' => '@frontend/web/themes/atlantis'],
                'baseUrl' => '@frontend/web/themes/atlantis',
            ],
        ],
        'urlManager' => [
                'enablePrettyUrl' => true,
                'showScriptName' => false,
        ],
        // 'urlManager' => [
        //         'enablePrettyUrl' => true,
        //         'showScriptName' => false,
        //        'enableStrictParsing' => true,
        //         'rules' => [
        //             'class' => 'yii\rest\UrlRule',
        //             '<alias:index|about|contact>' => 'site/<alias>',   
        //             '<alias:login|logout|register>' => 'account/<alias>', 
        //             // '/' => 'site/index',
        //             // 'login' => 'site/login',
        //             // 'about-us' => 'site/aboutus',
        //             // 'logout' => 'site/logout',
        //             // 'signup' => 'site/signup',
        //             // 'request-password-reset' => 'site/request-password-reset',
        //             // 'reset-password' => 'site/reset-password',
        //     ],
        // ],

        
        // 'urlManager' => [
        //     'enablePrettyUrl' => true,
        //     'showScriptName' => false,
        //     'rules' => [
        //     ],
        // ],

        
    ],
    // 'as beforeRequest' => [  //if guest user access site so, redirect to login page.
    //     'class' => 'yii\filters\AccessControl',
    //     'rules' => [
    //         [
    //             'actions' => ['login', 'error'],
    //             'allow' => true,
    //         ],
    //         [
    //             'allow' => true,
    //             'roles' => ['@'],
    //         ],
    //     ],
    // ],
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'homeUrl' => '/vwa/site/index',

    //'homeUrl' => '/advance/admin/site/index',

    'params' => $params,

];
