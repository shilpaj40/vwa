<?php 
Yii::$app->metaTags->register($model);
$cond="";
if(!Yii::$app->user->isGuest){
  $cond=2;
  //echo "not guest";
  $User = \app\models\User::find()->where(['id' => Yii::$app->user->id, 'status' => 1, 'user_type'=>2])->one();
  if($User->is_paid_user!=0)
  {
    $cond=3;
  }
}

$documents = \backend\models\Resources::find()->where(['content_for'=>1]);
if($cond==2)
  $documents->orWhere(['content_for'=>$cond]);
if($cond==3)
{
  $documents->orWhere(['content_for'=>2]);
  $documents->orWhere(['content_for'=>3]);
}
$documents= $documents->andWhere(['resource_type'=>'Documents'])->orderBy(['id'=>SORT_DESC])->all();
//echo count($model);die;
//echo $model->createCommand()->getRawSql();die;
$video = \backend\models\Resources::find()->where(['content_for'=>1]);
if($cond==2)
  $video->orWhere(['content_for'=>$cond]);
if($cond==3)
{
  $video->orWhere(['content_for'=>2]);
  $video->orWhere(['content_for'=>3]);
}
$video=$video->andWhere(['resource_type'=>'Video'])->orderBy(['id'=>SORT_DESC])->all();
//echo $video->createCommand()->getRawSql();die;
//print_r($video);die;
?>
<!--Page Title-->
<section class="page-title centred" style="background-image: url(<?= Yii::$app->params['basepath'];?>/images/bg/page-title-2.jpg);">
    
    <div class="auto-container">
        <div class="content-box clearfix">
            <h1>Resources</h1>
        </div>
    </div>
</section>
<!--End Page Title-->
<!-- project-style-three -->
<section class="about-style-two about-page-1 bg-color-1">
    <div class="auto-container">
        
        <div id="content_block_three">
            <div class="content-box">
                <div class="tabs-box">
                    <div class="tab-btn-box">
                        <ul class="tab-btns tab-buttons clearfix">
                            <li class="tab-btn active-btn" data-tab="#tab-1">Videos</li>
                            <li class="tab-btn" data-tab="#tab-2">Document</li>
                        </ul>
                    </div>
                    <div class="tabs-content">
                        <div class="tab active-tab" id="tab-1">

                            <div class="content-inner box">
                                <div class="box-body">
                                  <?php $j=0; if(count($video)>0){
                                      foreach($video as $videos){ $j++;
                                        if($j==1)
                                        { echo "<div class='row'>";}?>

                                        <div class="col-md-4" >

                                           <?= $videos->youtube_iframe; ?>
                                           <?= $videos->title; ?><br>
                                                                                   
                                        </div>
                                        
                                       <?php if($j==3 || count($video)==$j){echo "</div> ";$j=0;}

                                     }?> 
                                 


                                  <?php    }
                                  else
                                  { echo "<h4>No Data found</h4>";}?>
                                 
                                 
                              </div>
                            </div>
                        </div>
                        <div class="tab" id="tab-2">
                            <div class="content-inner box">
                                <div class="box-body">
                                  <?php $i=0; if(count($documents)>0 ){
                                      foreach($documents as $resources){ $i++;
                                        if($i==1)
                                            { echo "<div class='row'>";}?>
                                        <div class="col-lg-4 col-md-6 col-sm-12 m mb-2" >
                                           <article class="h-100 g-flex-middle g-brd-left g-brd-3 g-brd-primary g-brd-white--hover g-bg-black-opacity-0_8 g-transition-0_3 g-pa-20">
                                                <div class="g-flex-middle-item">
                                                  <h4 class="h6 g-color-white g-font-weight-600 text-uppercase g-mb-10 text-left"><?= $resources->title; ?></h4>
                                                  <p class="g-color-white-opacity-0_7 mb-0 text-left"><a href="">View Document </a></p>
                                                </div>
                                            </article>
                                        </div>
                                        
                                       <?php if($i==3 || count($documents)==$i){echo "</div> ";$i=0;}

                                     }
                                  }
                              else
                                    { echo "<h4>No Data found</h4>";}?>
                                 
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
            
     </div>
       
</section>
<!-- project-style-three end -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
   $(document).ready(function(){
  
    $("li#resources").addClass("current");

});
</script>
