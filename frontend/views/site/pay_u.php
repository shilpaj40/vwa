<?php

//print_r($user);die;
 //Yii::$app->params['MERCHANT_KEY'];

$MERCHANT_KEY = Yii::$app->params['MERCHANT_KEY'];;

$SALT = Yii::$app->params['SALT'];;
// Merchant Key and Salt as provided by Payu.


$PAYU_BASE_URL = "https://sandboxsecure.payu.in";	// For Sandbox Mode
//$PAYU_BASE_URL = "https://test.payu.in/_payment";
//$PAYU_BASE_URL = "https://secure.payu.in";			// For Production Mode

$action = '';

$posted = array();
if(!empty($_POST)) {
    //print_r($_POST);


  foreach($_POST as $key => $value) {    
    $posted[$key] = $value; 
	
  }
}

$formError = 0;

if(empty($posted['txnid'])) {
  // Generate random transaction id
  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
  $txnid = $posted['txnid'];
}
$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
  //print_r($posted);die;
  if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
		  || empty($posted['service_provider'])
  ) {
    $formError = 1;
  } else {
    //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
	$hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';	
	foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
      //echo $hash_string;die;
    }

    $hash_string .= $SALT;


    $hash = strtolower(hash('sha512', $hash_string));
    //echo $hash;die;
    $action = $PAYU_BASE_URL . '/_payment';
  }
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}
?>
<html>
  <head>
   
  <script>
    <?php //echo $hash;die;?>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
       // alert("hii");
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
  <script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-
color="<color-code>"
 bolt-logo="<image path>"></script>
  <style type="text/css">
  #content_block_nine .content-box .form-group input[type='text'],#content_block_nine .content-box .form-group textarea {
    position: relative;
    width: 100%;
    background: #fff;
    height: 45px;
    padding: 10px 30px;
    font-size: 16px;
    border: 2px solid #fff;
    border-radius: 30px;
    transition: all 500ms ease;
}



  </style>
  </head>
  <body onload="submitPayuForm()">

    <div class="site-login">
  <main id="page-main" role="main">
    <section class="contact-section" style="background-image: url(<?= Yii::$app->params['basepath'];?>/images/contact-1.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-xl-6 col-lg-7 col-md-7 col-md-offset-3 content-column">
                    <div id="content_block_nine">
                        <div class="content-box bg-color-white">
                            <div class="sec-title style-three centred mb-0 pb-0">
                               
                                <h2>Pay U Form</h2>
                            </div>
                            
   
    <br/>
    <?php if($formError) { ?>
	
      <span style="color:red">Please fill all mandatory fields.</span>
      <br/>
      <br/>
    <?php } ?>
    <div class="form-group" style="padding: 10px;">
    <form action="<?php echo $action; ?>" method="post" name="payuForm">
      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
      <table class="table" style="padding: 30px;">
      <!--  <tr> 
        <td colspan="6"><b>Mandatory Parameters</b></td></tr>
        <tr> -->
          <td class="control-label">Amount </td>
          <td><input type="text" name="amount" class="form-control" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>" /></td>

          <td class="control-label">Name </td>
          <!-- <td><input type="text"  name="firstname"  class="form-control"id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" /></td> -->
          <td><input type="text"  name="firstname"  class="form-control"id="firstname" value="<?php echo (empty($posted['firstname'])) ? $name  : $posted['firstname']; ?>" /></td>

        </tr>
        <tr>
          <td class="control-label">Email </td>
          <td><input type="text"  name="email" class="form-control" id="email" value="<?php echo (empty($posted['email'])) ? $email : $posted['email']; ?>" /></td>
          <td class="control-label">Phone </td>
          <td><input  type="text" name="phone" class="form-control" value="<?php echo (empty($posted['phone'])) ? $mobile : $posted['phone']; ?>" /></td>
        </tr>
        <tr>
          <td class="control-label">Product Info </td>
          <td colspan="3"><textarea type="textarea" name="productinfo" class="form-control"><?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?></textarea></td>
        </tr>
        <tr>
          <td class="control-label">Success URI </td>
          <td colspan="3"><input type="text" name="surl"  class="form-control" value="<?php echo (empty($posted['surl'])) ? 'http://localhost/vwa/site/success' : $posted['surl'] ?>" size="64" /></td>
        </tr>
        <tr>
          <td class="control-label">Failure URI </td>
          <td colspan="3"><input type="text" name="furl" class="form-control" value="<?php echo (empty($posted['furl'])) ? 'http://localhost/vwa/site/failure' : $posted['furl'] ?>" size="64" /></td>
        </tr>

        <!-- <tr>
          <td colspan="3"> -->
            <input type="hidden"  class="form-control" name="service_provider" value="payu_paisa" size="64" />
          <!-- </td>
        </tr> -->
             <!--  <tr>
          <td colspan="6"><b>Optional Parameters</b></td>
        </tr>
        <tr>
          <td>Last Name: </td>
          <td><input name="lastname" id="lastname"  class="form-control" value="<?php echo (empty($posted['lastname'])) ? '' : $posted['lastname']; ?>" /></td>
          <td>Cancel URI: </td>
          <td><input class="form-control" name="curl" value="" /></td>
        </tr>
        <tr>
          <td>Address1: </td>
          <td><input name="address1"  class="form-control" value="<?php echo (empty($posted['address1'])) ? '' : $posted['address1']; ?>" /></td>
          <td>Address2: </td>
          <td><input name="address2"  class="form-control" value="<?php echo (empty($posted['address2'])) ? '' : $posted['address2']; ?>" /></td>
        </tr>
        <tr>
          <td>City: </td>
          <td><input name="city" class="form-control"  value="<?php echo (empty($posted['city'])) ? '' : $posted['city']; ?>" /></td>
          <td>State: </td>
          <td><input name="state" class="form-control"  value="<?php echo (empty($posted['state'])) ? '' : $posted['state']; ?>" /></td>
        </tr>
        <tr>
          <td>Country: </td>
          <td><input name="country" class="form-control" value="<?php echo (empty($posted['country'])) ? '' : $posted['country']; ?>" /></td>
          <td>Zipcode: </td>
          <td><input name="zipcode"  class="form-control" value="<?php echo (empty($posted['zipcode'])) ? '' : $posted['zipcode']; ?>" /></td>
        </tr>
        <tr>
          <td>UDF1: </td>
          <td><input name="udf1"  class="form-control" value="<?php echo (empty($posted['udf1'])) ? '' : $posted['udf1']; ?>" /></td>
          <td>UDF2: </td>
          <td><input name="udf2"  class="form-control" value="<?php echo (empty($posted['udf2'])) ? '' : $posted['udf2']; ?>" /></td>
        </tr>
        <tr>
          <td>UDF3: </td>
          <td><input name="udf3" class="form-control" value="<?php echo (empty($posted['udf3'])) ? '' : $posted['udf3']; ?>" /></td>
          <td>UDF4: </td>
          <td><input name="udf4"  class="form-control" value="<?php echo (empty($posted['udf4'])) ? '' : $posted['udf4']; ?>" /></td>
        </tr>
        <tr>
          <td>UDF5: </td>
          <td><input name="udf5" class="form-control"  value="<?php echo (empty($posted['udf5'])) ? '' : $posted['udf5']; ?>" /></td>
          <td>PG: </td>
          <td><input name="pg" class="form-control" value="<?php echo (empty($posted['pg'])) ? '' : $posted['pg']; ?>" /></td>
        </tr> -->
        <tr>
          <?php if(!$hash) { ?>
            <td colspan="4"><center><input type="submit"  class="theme-btn style-three" value="Submit" /></center></td>
          <?php } ?>
        </tr>
      </table>
    </form>
    </div> </div>
 
                    </div>
                </div>
            </div>
        </div>
    </section>
    
  </main>

</div>
  </body>
</html>
