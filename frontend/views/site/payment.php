<?php

//print_r($user);die;

// $MERCHANT_KEY = "Jjrw8q";
// $SALT = "1HkwrgxkmryDFIFog9fgWQPGZUJ0e1MI";

$MERCHANT_KEY = Yii::$app->params['MERCHANT_KEY'];;

$SALT = Yii::$app->params['SALT'];;
// Merchant Key and Salt as provided by Payu.

//echo $SALT;die;

$PAYU_BASE_URL = "https://sandboxsecure.payu.in";	// For Sandbox Mode
//$PAYU_BASE_URL = "https://test.payu.in/_payment";
//$PAYU_BASE_URL = "https://secure.payu.in";			// For Production Mode

$action = '';

$posted = array();
if(!empty($_POST)) {
    //print_r($_POST);die;


  foreach($_POST as $key => $value) {    
    $posted[$key] = $value; 
	
  }
}

$formError = 0;

if(empty($posted['txnid'])) {
  // Generate random transaction id
  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
  $txnid = $posted['txnid'];
}
$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|lasttname|email|city|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
  //print_r($posted);die;
  if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['lastname'])
          || empty($posted['email'])
          || empty($posted['city'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
		  || empty($posted['service_provider'])
  ) {
    $formError = 1;
  } else {
    //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
	$hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';	
	foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
      //echo $hash_string;die;
    }

    $hash_string .= $SALT;


    $hash = strtolower(hash('sha512', $hash_string));
    //echo $hash;die;
    $action = $PAYU_BASE_URL . '/_payment';
  }
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}
?>
<html>
  <head>
   
  <script>
    <?php //echo $hash;die;?>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
       // alert("hii");
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
  <script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-
color="<color-code>"
 bolt-logo="<image path>"></script>
  <style type="text/css">
  #content_block_nine .content-box .form-group input[type='text'],#content_block_nine .content-box .form-group textarea ,#content_block_nine .content-box .form-group input[type='email']{
    position: relative;
    width: 100%;
    background: #fff;
    height: 45px;
    padding: 10px 30px;
    font-size: 16px;
    border: 2px solid #fff;
    border-radius: 30px;
    transition: all 500ms ease;
}
.disabled { cursor: not-allowed; }



  </style>
  </head>
  <body onload="submitPayuForm()">

    <div class="site-login">
  <main id="page-main" role="main">
    <section class="contact-section" style="background-image: url(<?= Yii::$app->params['basepath'];?>/images/contact-1.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-xl-6 col-lg-7 col-md-7 col-md-offset-3 content-column">
                    <div id="content_block_nine">
                        <div class="content-box bg-color-white">
                            <div class="sec-title style-three centred mb-0 pb-0">
                                <h2>Payment</h2>
                            </div>
                            
   
    <br/>

<!-- <span style="color:red">Paying amount for <span id='script'></span> script per week for <span id='month'></span> <span id='month1'></span></span> -->
    <?php if($formError) { ?>
	
      <span style="color:red">Please fill all mandatory fields.</span>
      <br/>
      <br/>
    <?php } ?>
    <div class="form-group" style="padding: 10px;">
    <form action="<?php echo $action; ?>" method="post" name="payuForm">
      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
       <input type="hidden"  name="productinfo" class="form-control" id="productinfo" value="<?php echo (empty($posted['productinfo'])) ? "productinfo" : "productinfo" ?>" />         
        <input type="hidden" name="surl"  class="form-control" value="<?php echo (empty($posted['surl'])) ? 'site/success' : 'site/success' ?>" size="64" />
       <input type="hidden" name="furl" class="form-control" value="<?php echo (empty($posted['furl'])) ? 'site/failure' : 'site/failure' ?>" size="64" />
        <input type="hidden"  class="form-control" name="service_provider" value="payu_paisa" size="64" />

         <input type="hidden"  class="form-control" name="subcription_id" id="subscription_id"/>
      
      <div class="error-messages" style="display:none;color:red;"></div>
        <div class="form-inner">
 
         <div class="row">
          <div class="form-group">

            <div class="col-md-6">
               
          <label class="control-label" for="payment-firstname">First Name </label> <span style="color:red"> *</span>
          
          <input type="text"  pattern="[A-Za-z]{1,32}" name="firstname"  class="form-control" id="firstname" value="<?php echo (empty($posted['firstname'])) ? ""  : $posted['firstname']; ?>" required />
        </div>
         
         <div class="col-md-6">
           <label class="control-label" for="payment-lastname">Last Name </label> <span style="color:red"> *</span> 
          
          <input type="text"  name="lastname"   pattern="[A-Za-z]{1,32}" class="form-control" id="lastname" value="<?php echo (empty($posted['lastname'])) ? ""  : $posted['lastname']; ?>" required/>
        </div>
      </div></div>

        
        <div class="row">
         
           <div class="form-group">
            <div class="col-md-6 ">
               
          <label class="control-label" for="payment-email">Email </label><span style="color:red"> *</span>
          <input type="email"  name="email" class="form-control" id="email" value="<?php echo (empty($posted['email'])) ? "" : $posted['email']; ?>" required/>
        </div>

        <div class="col-md-6">
          <label class="control-label" for="payment-mobile">Mobile Number </label><span style="color:red"> *</span> 
          <input  type="text"  id="mobile-num" minlength="10" maxlength="10"  name="phone" class="form-control" value="<?php echo (empty($posted['phone'])) ? "" : $posted['phone']; ?>" required/>
         

         </div>

       </div>
         </div>




         <div class="row">
          <div class="form-group">
            <div class="col-md-6">
               
          <label class="control-label" for="payment-city">City</label> <span style="color:red"> *</span>
          
          <input type="text" id="city"   pattern="[A-Za-z]{1,32}" name="city" class="form-control" value="<?php echo (empty($posted['city'])) ? '' : $posted['city'] ?>" />
        </div>

            <div class="col-md-6">
               
          <label class="control-label" for="payment-amount">Amount (Rs)</label>
          
          <input type="text" id="amount"  name="amount" class="form-control disabled" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>"  readonly/>
        </div></div></div>
<br><br>


            <div class="row">
             <div class="form-group">
          <?php if(!$hash) { ?>
            <center><!-- <button style="width:50%;height:100%;" type="submit" id="submit_form" class="theme-btn style-three" value="Submit" /></button> -->

              <button type="submit" style="width:25%;" class="theme-btn style-three" type="submit" id="submit_form" name="login-button">Submit</button></center>
          <?php } ?>
           </div></div></div>
        
    </form>
    </div> </div>
 
                    </div>
                </div>
            </div>
        </div>
    </section>
    
  </main>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">

$("#amount").val(localStorage.getItem ("amount_val"));
$("#subscription_id").val(localStorage.getItem ("subscription_id"));



$(document).ready(function(){
  
  $("#mobile-num").on("blur", function(){
        var mobNum = $(this).val();
        var filter = /^\d*(?:\.\d{1,2})?$/;

          if (filter.test(mobNum)) {
            if(mobNum.length==10){
                //  alert("valid");
                  $(".error-messages").empty().fadeOut();
              $("#mobile-valid").removeClass("hidden");
              $("#folio-invalid").addClass("hidden");
             } else {
                //alert('Please put 10  digit mobile number');
                $(".error-messages").text("Please Enter Valid Mobile Number").fadeIn();
               $("#folio-invalid").removeClass("hidden");
               $("#mobile-valid").addClass("hidden");
               $('#mobile-num').val("");
                return false;
              }
            }
            else {
              //alert('Not a valid mobile number');
              $(".error-messages").text("Please Enter Valid Mobile Number").fadeIn();
              $("#folio-invalid").removeClass("hidden");
              $("#mobile-valid").addClass("hidden");
                $('#mobile-num').val("");
              return false;
           }
    
  });


  $("#firstname").on("blur", function() {
    if ( $(this).val().match('^[a-zA-Z]{1,32}$') ) {
        $(".error-messages").empty().fadeOut();
              $("#mobile-valid").removeClass("hidden");
              $("#folio-invalid").addClass("hidden");
    } else {
       $(".error-messages").text("Please Enter Valid First Name").fadeIn();
               $("#folio-invalid").removeClass("hidden");
               $("#mobile-valid").addClass("hidden");
               $('#firstname').val("");
                return false;
    }
});



  $("#lastname").on("blur", function() {
    if ( $(this).val().match('^[a-zA-Z]{1,32}$') ) {
        $(".error-messages").empty().fadeOut();
              $("#mobile-valid").removeClass("hidden");
              $("#folio-invalid").addClass("hidden");
    } else {
       $(".error-messages").text("Please Enter Valid Last Name").fadeIn();
               $("#folio-invalid").removeClass("hidden");
               $("#mobile-valid").addClass("hidden");
               $('#lastname').val("");
                return false;
    }
});



  $("#city").on("blur", function() {
    if ( $(this).val().match('^[a-zA-Z]{1,32}$') ) {
        $(".error-messages").empty().fadeOut();
              $("#mobile-valid").removeClass("hidden");
              $("#folio-invalid").addClass("hidden");
    } else {
       $(".error-messages").text("Please Enter Valid City Name").fadeIn();
               $("#folio-invalid").removeClass("hidden");
               $("#mobile-valid").addClass("hidden");
               $('#city').val("");
                return false;
    }
});

  
//   var m=$("#subscription_id").val();
//   //alert(m);
//  var sub= m.split("_");
//    //alert(sub);
//    for(var i = 0; i < sub.length; i++){
//             //document.write("<p>" + strArray[i] + "</p>");
//             if(i==0)
//             {
//             $('#script').val(+ sub[i] +);

//           }else if(i==1)
//           {
//             $('#month').val(sub[i]);

//           }
//           else{
//             $('#month1').val(sub[i]);
//           }
//         }

// });


// document.addEventListener("DOMContentLoaded", function() {
//     var elements = document.getElementsByTagName("INPUT");
//     for (var i = 0; i < elements.length; i++) {
//         elements[i].oninvalid = function(e) {
//             e.target.setCustomValidity("");
//             if (!e.target.validity.valid) {
//                 e.target.setCustomValidity("This field cannot be left blank");
//             }
//         };
//         elements[i].oninput = function(e) {
//             e.target.setCustomValidity("");
//         };
//     }
// })
// $('#submit_form').submit(function(e) {
//             e.preventDefault();
//             if(!$('#mobile-num').val().match('[0-9]{10}'))  {
//                 alert("Please put 10 digit mobile number");
//                 return;
//             }  

        });

</script>
  </body>
</html>
