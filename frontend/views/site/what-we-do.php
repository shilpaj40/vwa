<!--Page Title-->
<section class="page-title centred" style="background-image: url(<?= Yii::$app->params['basepath'];?>/images/bg/page-title-2.jpg);">
    <div class="auto-container">
        <div class="content-box clearfix">
            <h1>What We Do</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="">Home</a></li>
                <li>What We Do</li>
            </ul>
        </div>
    </div>
</section>
<!--End Page Title-->
<!-- service-section -->
    #parallelogram {
      width: 150px;
      height: 100px;
      transform: skew(20deg);
      background: red;
    }

<section class="service-section">
    <div class="auto-container">
        <div class="title-box">
            <div class="row clearfix">
                <div class="col-lg-6 col-md-6 col-sm-12 title-column">
                    <div class="sec-title right">
                        <h5>What we provides</h5>
                        <h2>Get Exceptional <br />Service For Growth</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 text-column">
                    <div class="text">
                        <p>A financial doctor a day keeps market risks at bay!</p><p>How often have you got lured by advertisements that read – Want to double your money, invest with us and reap high returns.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="inner-content">
            <div class="row clearfix">
                <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                    <div id="content_block_13">
                        <div class="content-box">
                            <div class="sec-title style-four right">
                                <h5>About fionca</h5>
                                <h2>Digital Future Planning Based On True Facts</h2>
                            </div>
                            <div class="text">
                                <p>Exercitation llamco laboris nis aliquip sed conseqrure dolorn repreh deris luptate velit excepteur duis aute irure dolor voluptate. Lorem ipsum dolor sit amet, consectetur adipisicing elit sed eiusmod.</p>
                                <p>Tempor incididunt ut labore et dolore magna aliqua. Ut enim minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
                            </div>
                           
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                    <div id="image_block_four">
                        <div class="image-box">
                            <div class="pattern-layer" style="background-image: url(<?= Yii::$app->params['basepath'];?>/images/pattern/shape-23.png);"></div>
                            <figure class="image wow slideInRight animated animated animated" data-wow-delay="00ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: slideInRight;"><img src="assets/images/resource/about-3.jpg" alt=""></figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- service-section end -->