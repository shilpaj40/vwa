<!--Page Title-->
<?php
Yii::$app->metaTags->register($model);
 if($model->banner->banner_image!="")
            {
                 echo "<section class='page-title centred' style='background-image: url(".$model->banner->banner_image.");'>";
            }
            else
            {
                echo "<section class='page-title centred' style='background-image: url(<?= Yii::$app->params['basepath'];?>/images/bg/page-title-5.jpg);'>";
            }
         ?>

    <div class="auto-container">
        <div class="content-box clearfix">
            <h1><?php echo $model->title; ?></h1>
            <!--<ul class="bread-crumb clearfix">-->
            <!--    <li><a href="">Home</a></li>-->
            <!--    <li>Contact</li>-->
            <!--</ul>-->
        </div>
    </div>
</section>
    <section class="policy-section bg-color-2">

        <div class="pattern-layer" style="background-image: url(https://vishwaswealthadvisory.com/frontend/web/images/pattern/shape-6.png);"></div>
        <div class="auto-container">
            <div class="row clearfix contact-content">
                <div class="col-lg-6 col-md-6 col-sm-12 content-column">
                    <div id="content_block_five">
                        <div class="content-box">
                            <div class="sec-title left">
                                <h2><?php echo $model->title; ?></h2>
                                <p class="text-black">If you have any questions regarding stock market trading or if you would like to know more about our services. Feel free to drop in your contact details in the form below. </p>
                            </div>
                            <ul class="info-list clearfix">
                           <?php if($model->office_address!=""){ 
                        echo "<li><i class='fas fa-map-marker-alt' style='border-color:#337ab7'> </i><h4 class='text-black'>Office Address</h4>
                            <p class='text-black'><a href='mailto:".$model->office_address."' class='text-black'>".$model->office_address."</a></p></li>"; } ?>
                               <?php if($model->email!=""){ 
                        echo "<li><i class='fas fa-envelope-open' style='border-color:#337ab7'> </i><h4 class='text-black'>Email Support</h4>
                            <p class='text-black'><a href='mailto:".$model->email."' class='text-black'>".$model->email."</a></p></li>"; } ?>
                             <?php if($model->mobile!=""){ 
                        echo "<li><i class='fas fa-mobile' style='border-color:#337ab7'> </i><h4 class='text-black'>Mobile No</h4>
                            <p class='text-black'>".$model->mobile."</p></li>"; } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 inner-column">
                    <div id="content_block_six">
                        <div class="content-box">
                            <div class="tabs-box">
                                <div class="tab-btn-box">
                                    <ul class="tab-btns tab-buttons clearfix" style="display:none">
                                        <li class="tab-btn active-btn text-black" data-tab="#tab-4">Get in touch with us</li>
                                       
                                    </ul>
                                </div>
                                <div class="tabs-content">
                                    <div class="tab active-tab" id="tab-4">
                                        <div class="content-inner">
                                            
                                            <form action="#" method="post">
                                                <div class="form-group">
                                                    <i class="fas fa-user"></i>
                                                    <input type="text" name="name" placeholder="Name" required="">
                                                </div>
                                                <div class="form-group">
                                                    <i class="fas fa-envelope-open"></i>
                                                    <input type="email" name="email" placeholder="Email" required="">
                                                </div>
                                                <div class="form-group">
                                                    <i class="fa fa-phone" style="transform: rotate(90deg);"></i>
                                                    <input type="text" name="phone" placeholder="Phone" required="">
                                                </div>
                                                <div class="form-group">
                                                    <i class="fa fa-file-text"></i>
                                                    <input type="text" name="city" placeholder="City" required="">
                                                </div>
                                                <div class="form-group">
                                                    <i class="fa fa-file-text"></i>
                                                   <textarea name="msg" placeholder="Your comment" ></textarea>
                                                </div>
                                                
                                                <div class="form-group message-btn">
                                                    <button type="submit" class="theme-btn style-three">Contact Me!</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- policy-section end -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
   $(document).ready(function(){
  
    $("li#Contact").addClass("current");

});
</script>

