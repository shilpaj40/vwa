<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
//https://affixtheme.com/html/xmee/demo/register-4.html
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

?>

<div class="site-login">
  <main id="page-main" role="main">
    <section class="contact-section" style="background-image: url(<?= Yii::$app->params['basepath'];?>/images/contact-1.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-xl-5 col-lg-5 col-md-5 col-md-offset-4 content-column">
                    <div id="content_block_nine">
                        <div class="content-box bg-color-white">
                            <div class="sec-title style-three left mb-0">
                                <h5>Log In</h5>
                            </div>
                           
                            <div class="form-inner" style="padding-top:0;">
                              <?php if (Yii::$app->session->hasFlash('success')): ?>
                                <div class="alert alert-success alert-dismissable">
                                     <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                     <?= Yii::$app->session->getFlash('success') ?>
                                </div>
                            <?php endif; ?>

                            <?php if (Yii::$app->session->hasFlash('error')): ?>
                                <div class="alert alert-danger alert-dismissable">
                                     <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                     <h4>Error!</h4>
                                     <?= Yii::$app->session->getFlash('error') ?>
                                </div>
                            <?php endif; ?>
                            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                             <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= $form->field($model, 'username', ['template' => '
                                           <div class="row"><div class="col-md-12">{label}</div></div>
                                           <div class="row">
                                               <div class="col-sm-12">
                                                   <div class="input-group">
                                                      
                                                      {input}
                                                   </div>
                                                   {error}{hint}
                                               </div>
                                            </div>'])->textInput(['class'=>'form-control ']) ?>
                                    </div>
                                </div>
                             </div>
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group">
                                   <?= $form->field($model, 'password')->passwordInput(['autofocus' => true, 'class'=>'form-control']) ?>
                                   
                                            <i toggle="#password" class="fa fa-fw toggle-pwdpassword field-icon fa-eye"></i>
                                  
                                  </div>
                                </div>
                                <div class="col-md-12 text-right">
                                  <div class="form-group mb-2">
                                <a href="<?= yii\helpers\Url::to(["site/request-password-reset"])?>" class="nav-link" >Reset Your Password</a>
                                    </div>
                                </div>
                                
                              </div>
                              <div class="form-group">
                                <!-- <button type="submit" class="fxt-btn-fill">Log in</button> -->
                                <?= Html::submitButton('Login', ['class' => 'theme-btn style-three', 'name' => 'login-button']) ?>
                              </div>
                            <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
  </main>

</div>

