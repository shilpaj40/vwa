<!--Page Title-->
<section class="page-title centred" style="background-image: url(<?= Yii::$app->params['basepath'];?>/images/bg/page-title-2.jpg);">
    <div class="auto-container">
        <div class="content-box clearfix">
            <h1>About Us</h1>
            <!--<ul class="bread-crumb clearfix">-->
            <!--    <li><a href="">Home</a></li>-->
            <!--    <li>About Us</li>-->
            <!--</ul>-->
        </div>
    </div>
</section>
<!--End Page Title-->
<!-- service-section -->

<section class="service-style-one about-section pb-0">
    <div class="auto-container">
        <div class="sec-title centred">
            <h5>About Us</h5>
            <!--<h2>A financial doctor a day keeps market risks at bay!</h2>-->
        </div>
        <!--<div class="title-box">-->
        <!--    <div class="row clearfix">-->
        <!--        <div class="col-lg-6 col-md-6 col-sm-12 title-column">-->
        <!--            <div class="sec-title right">-->
        <!--                <h5>About Us</h5>-->
        <!--                <h2>A financial doctor a day keeps market risks at bay</h2>-->
        <!--            </div>-->
        <!--        </div>-->
                
        <!--    </div>-->
        <!--</div>-->
        <div class="row clearfix">
            <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                <div id="content_block_13">
                    <div class="content-box">
                        
                        <div class="text">
                            <p>How often have you got lured by advertisements that read – Want to double your money, invest with us and reap high returns.</p>
                            <p>As novice investors we are bound to be flummoxed by the idea of high returns and fatter wallets. But managing money isn’t that easy. If you do not manage your money correctly during your working years, you will not be able to enjoy a blissful retirement on reaching your golden years. Without falling prey to any marketing gimmicks, one must judiciously weigh the pros and cons and make an informed decision as to whether it satisfies one’s investment objectives.</p>

                            <p>Vishwas Wealth Advisory (VWA) is a team of finance experts, statisticians, mathematicians and data scientists. These extremely talented and experienced individuals with their diverse backgrounds and extensive knowledge of markets and opportunities will give an insight to the millennials to make right choices for creating more wealth.</p>
                            <p>This insight of the dynamic market and a fast-changing world will definitely help deliver ‘Value for Our Clients’. We educate the millennials on trading stocks, mutual funds and financial assets as well as help them sail through the uncertainties caused by macroeconomic factors like the demonetization of 2018 or the recent Covid-19 pandemic that crippled the world economy.</p>
                            <p>We believe that growing rich or achieving financial targets is not restricted for a selective few but by making the right choices from the plethora of options available, every single individual can secure their long-term financial well-being.</p>
                            <p>Our aim is to assist every individual based on our technical analysis of the market and the robust algorithm developed by us. We are aware that clients are dealing with multiple issues with regards to creation and preservation of wealth and hence our team will solely function for the benefit of the families we represent.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                <div id="image_block_four">
                    <div class="image-box">
                        <div class="pattern-layer" style="background-image: url(<?= Yii::$app->params['basepath'];?>/images/pattern/shape-23.png);"></div>
                        <figure class="image wow slideInRight animated animated animated" data-wow-delay="00ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: slideInRight;"><img src="<?= Yii::$app->params['basepath'];?>images/about-3.jpg" alt=""></figure>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="row clearfix pb-50">-->
        <!--    <div class="col-md-12">-->
        <!--        <div id="content_block_one">-->
        <!--            <div class="content-box">-->
        <!--                <div class="inner-box">-->
        <!--                    <div class="single-item">-->
        <!--                        <div class="icon-box">-->
        <!--                            <span class="bg-box"></span>-->
        <!--                            <i class="flaticon-computer-1"></i>-->
        <!--                        </div>-->
        <!--                        <h4>Our Believe</h4>-->
        <!--                        <p>We believe that growing rich or achieving financial targets is not restricted for a selective few but by making the right choices from the plethora of options available, every single individual can secure their long-term financial well-being.</p>-->
        <!--                    </div>-->
        <!--                    <div class="single-item">-->
        <!--                        <div class="icon-box">-->
        <!--                            <span class="bg-box"></span>-->
        <!--                            <i class="flaticon-browser-1"></i>-->
        <!--                        </div>-->
        <!--                        <h4>Our Aim</h4>-->
        <!--                        <p>Our aim is to assist every individual based on our technical analysis of the market and the robust algorithm developed by us. We are aware that clients are dealing with multiple issues with regards to creation and preservation of wealth and hence our team will solely function for the benefit of the families we represent.</p>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--    </div>-->
        <!--</div>-->
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="service-details-content">
                    <div class="content-style-one mr-0">
                        <div class="sec-title left pb-0 mr-0">
                            <h5 class="pb-0 mr-0">Our Role</h5>
                        </div>
                        <p>We hope to work in capacities of –</p>
                        <ul class="list-item clearfix">
                            <li style="font-weight:400"><strong>An Expert – </strong>where we will analyze the market, nearly 500 companies, every week to develop risk-aware strategies to help you achieve your goals.</li>
                            <li style="font-weight:400"><strong>An Advisor – </strong>to prescribe you four trades every month which includes buying and selling stocks depending on the fundamentals of the various companies. Our complex interactive algorithm will guide investors based on their financial behavior.</li>
                            <li style="font-weight:400"><strong>An Architect – </strong>we build wealth management strategies to match client’s risk appetites and lifetime goals</li>
                            <li style="font-weight:400"><strong>A Coach – </strong>even when strategies are in place, doubts and fears are inevitable and here we act as a coach to reinforce the initial principles and keep the client on track</li>
                            <li style="font-weight:400"><strong>A Listener – </strong>emotions triggered by financial uncertainties are real and hence we listen to our client’s fears and issues driving those feelings and provide them with practical solutions.</li>
                            <li style="font-weight:400"><strong>A Teacher – </strong>shuffling between the fear-and-flight phases is a matter of teaching investors about the risks and returns which is by virtue of discipline.</li>
                            <li style="font-weight:400"><strong>A Guardian – </strong>we act as a lighthouse keeper, constantly scanning the horizons for issues that may affect the clients</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>
</section>
<section class="feature-style-three pt-50 pb-0">
    <div class="fluid-container">
        <div class="inner-content clearfix">
            <div class="feature-block-three">
                <div class="inner-box">
                    <div class="hidden-icon"><i class="flaticon-search"></i></div>
                    <div class="inner">
                        <div class="icon-box"><i class="flaticon-search"></i></div>
                        <h3>Our Objective</h3>
                        <p>Our objective is to help investors navigate through the bizarre world of the share market and ensure that an adequate return of over 24% is achieved over a period of time. If you buy at an appropriate time at an appropriate price and sell it at an opportune time, you can easily meet your target of returns. It is very important to trade making use of its right algorithm in a scientific manner and not based on sentiments.</p>
                    </div>
                </div>
            </div>
            <div class="feature-block-three">
                <div class="inner-box">
                    <div class="hidden-icon"><i class="flaticon-search"></i></div>
                    <div class="inner">
                        <div class="icon-box"><i class="flaticon-search"></i></div>
                        <h3>Our Vision</h3>
                        <p>Wealth is not created overnight. It is a process that combines disciplined financial behavior, better understanding of the volatile market and a right mix of confidence and caution. A qualified financial doctor can be relied upon for his prescription of the right combination of investment opportunities. Our team of finance experts, statisticians, mathematicians and data scientist’s advice based on the technical analysis of the market and the robust algorithm developed by us. We believe in building moneyed investors.</p>
                    </div>
                </div>
            </div>
            <div class="feature-block-three">
                <div class="inner-box">
                    <div class="hidden-icon"><i class="flaticon-claim"></i></div>
                    <div class="inner">
                        <div class="icon-box"><i class="flaticon-claim"></i></div>
                        <h3>Our Assurance</h3>
                        <p>We assure our investors that while they are busy with their lives, we are paving a way for them to set aside their money that can reap rewards for their future. Smart investing is a means to a happier ending. Our experience and expertise and your money – together we will achieve your blissful investment goals.
</p>
                    </div>
                </div>
            </div>
           
        </div>
    </div>
</section>

<section class="our-mission bg-color-1">
    <div class="pattern-layer" style="background-image: url(<?= Yii::$app->params['basepath'];?>/images/pattern/shape-6.png);"></div>
    <div class="auto-container">
        <div class="row clearfix">
            <div class="col-lg-7 col-md-12 col-sm-12 content-column">
                <div id="content_block_four">
                    <div class="content-box">
                        <div class="tabs-box">
                            <div class="tab-btn-box">
                                <ul class="tab-btns tab-buttons clearfix">
                                    <li class="tab-btn active-btn" data-tab="#tab-1">A Mountaineer Perspective</li>
                                    <li class="tab-btn" data-tab="#tab-2">Our Mentor</li>
                                    <!--<li class="tab-btn" data-tab="#tab-3">What We Offer</li>-->
                                </ul>
                            </div>
                            <div class="tabs-content">
                                <div class="tab active-tab" id="tab-1">
                                    <div class="content-inner">
                                        <p>We may or may not have mountain climbing as one of our hobbies, but when we read or hear stories about someone’s mountain exhilarating journey, we feel like going up that path. The feeling is pretty much the same when we read about a successful investor who managed to create wealth. We hope to imbibe this mountaineer’s perspective in our investors where:</p>
                                        <ul class="list-item clearfix">
                                            <li>We guide them on setting their targets right</li>
                                            <li>We help them start climbing, but cautiously with smaller steps</li>
                                            <li>We assist them on changing paths, if required, en route reaching the financial targets</li>
                                           
                                        </ul>
                                        <p>We insist on doing it often as we believe, that as small steps reach you to the peak, similarly small investments every week will eventually reach them to their set goals.</p>
                                       
                                    </div>
                                </div>
                                <div class="tab" id="tab-2">
                                    <div class="content-inner">
                                        <p>Vishwas Soman, financial planner and Gold medallist in Mathematics from Mumbai University (1970) and IIM Ahmedabad (Finance) (1978) and a hard-core mountaineer is the founder and CEO of VWA with an experience of more than four decades in the financial services industry. An avid Math lover, Soman was always fascinated by the share market and its functioning, that much later in life he started reading up on how the equations at the share markets change thereby earning huge profits for some and losses to the others.</p>
                                        <p>In the age of extreme volatility, global impacts and unpredictable inflation, financial planning and its implementation is the only key to become wealthy in an informed manner. Our investment strategies will not be on an ad-hoc basis but will augment wealth creation.</p>
                                    </div>
                                </div>
                                <!--<div class="tab" id="tab-3">-->
                                <!--    <div class="content-inner">-->
                                       
                                <!--        <ul class="list-item clearfix">-->
                                <!--            <li>Act as trustworthy money mentors for our investors</li>-->
                                <!--            <li>Nurture and safeguard our investors’ wealth</li>-->
                                <!--            <li>Assist our investors in securing their financial future</li>-->
                                <!--            <li>Convert potential opportunities to help create wealth</li>-->
                                <!--            <li>Enable our investors to reach their financial goals</li>-->
                                <!--        </ul>-->
                                <!--    </div>-->
                                <!--</div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-12 col-sm-12 image-column">
                <div id="image_block_two">
                        <div class="image-box">
                            <figure class="image"><img src="<?= Yii::$app->params['basepath'];?>/images/about-2.jpg" alt=""></figure>
                            <div class="content-box">
                                <i class="fas fa-headphones-alt"></i>
                                <h4>Make wise investments!</h4>
                                <h5>Any questions? Email us <a href="mailto:info@vishwaswealthadvisory.com">info@vishwaswealthadvisory.com
</a></h5>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</section>
