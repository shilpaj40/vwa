<!--Page Title-->
<?php
Yii::$app->metaTags->register($model);
     if($model->banner->banner_image!="")
            {
                 echo "<section class='page-title centred' style='background-image: url(".$model->banner->banner_image.");'>";
            }
            else
            {
                echo "<section class='page-title centred' style='background-image: url(<?= Yii::$app->params['basepath'];?>/images/bg/page-title-2.jpg);'>";
            }
        
         ?>
    <div class="auto-container">
        <div class="content-box clearfix">
            <h1><?php echo $model->title; ?></h1>            
        </div>
    </div>
</section>
<!--End Page Title-->

<section class="service-style-one about-section">
    <div class="auto-container">
        <div class="service-details-content privacy">           
           <?php echo $model->body; ?>            
        </div>
        
    </div>
</section>



