<style>
    .upper-inner .text {
    position: relative;
    display: block;
    font-size: 30px;
    line-height: 36px;
    font-weight: 700;
    color: #fff;
    margin-bottom: 2px;
}
.upper-inner .btn-box {
    position: relative;
    margin-top: 11px;
}
</style>
<!--Page Title-->
<section class="page-title centred" style="background-image: url(<?= Yii::$app->params['basepath'];?>/images/bg/page-title-2.jpg);">
    <div class="auto-container">
        <div class="content-box clearfix">
            <h1>Services</h1>
            <!--<ul class="bread-crumb clearfix">-->
            <!--    <li><a href="">Home</a></li>-->
            <!--    <li>Services</li>-->
            <!--</ul>-->
        </div>
    </div>
</section>
<!--End Page Title-->
<!-- service-section -->

<section class="service-section">
    <div class="auto-container">
        <div class="sec-title centred">
            <h5>WHAT WE PROVIDE</h5>
            <h2>Paint your own masterpiece!</h2>
        </div>
    </div>
    <div class="auto-container">
        <div class="sec-title centred">
            <p>You may not always have the time to curate and plan your wealth. VWA will help make your family wealth planning easier by chalking out solutions that will help you achieve your financial goals. VWA does not manage your money, your money will always be in your control. We invest experience, you invest money. An hour-and-a-half every weekend is what we want our investors to invest with us to reap maximum benefits. And once our informed investors get confident, they will better evolve to a balanced portfolio and truly paint their own masterpiece. VWA sincerely hopes to become contributors to its investor's wealth creation process.</p>
        </div>
    </div>
</section>
<section class="service-style-five bg-color-1 pb-50 pt-50" id="content_block_four">
    <div class="auto-container content-box " >
        <div class="sec-title style-four right">
            <h5>What we offer</h5>
            <h2>Quality Services With Difference</h2>
        </div>
        <div class="tabs-content">
            <div class="row">
                <div class="col-md-4">
                    <ul class="list-item clearfix">
                     <li>Act as trustworthy money mentors for our investors</li>
                     <li>Nurture and safeguard our investors’ wealth</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-item clearfix">
                     <li>Assist our investors in securing their financial future</li>
                     <li>Convert potential opportunities to help create wealth</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-item clearfix">
                    <li>Enable our investors to reach their financial goals</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="pricing-section " style="background-image: url(<?= Yii::$app->params['basepath'];?>/images/pricing-bg.jpg);">
    <div class="auto-container">
        <div class="tabs-box centred">
            <!--<div class="sec-title style-four right">-->
            <!--    <h5>What we offer</h5>-->
            <!--    <h2>Quality Services With Difference</h2>-->
            <!--</div>-->
            <div class="tabs-content">
                <div class="tab active-tab" id="tab-1">
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-6 col-sm-12 pricing-block col-md-offset-2">
                            <div class="pricing-block-one">
                                <div class="pricing-table">
                                    <div class="table-header">
                                        <h3>Trial</h3>
                                        <!--<div class="price-box">-->
                                            <!-- <span>recommended</span> -->
                                        <!--    <h2>Free for lifetime</h2><br/>-->
                                        <!--</div>-->
                                        <!-- <span class="text">This Plan Includes Global Relations</span> -->
                                    </div>
                                    <div class="table-content">
                                        <ul class="clearfix"> 
                                            <li>A Personal Financial Planning Tool</li>
                                            <li>Weekly Buy/Sell Dynamic Advice</li>
                                            <li>Educational Material </li>
                                            <li><del>VWA Tool to Plan & Track Scripts</del></li>
                                            <li><del>Advance Analytical Tools</del></li>
                                            <li><del>Performance Reports</del></li>
                                            <li><del>Additional Script Advices </del></li>
                                            <li><del>Free Access to Expert Webinars</del></li>
                                            <li><del>Additional Script Advices </del></li>
                                            <li>Free for 90 days</li>
                                        </ul>
                                    </div>
                                    <div class="table-footer">
                                        <a href="#">Signup</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 pricing-block">
                            <div class="pricing-block-one active-block">
                                <div class="pricing-table">
                                    <div class="table-header">
                                        <h3>Premium</h3>
                                        <!--<div class="price-box">-->
                                        <!--    <span>recommended</span>-->
                                        <!--    <h2>₹ 400</h2>-->
                                        <!--    <p>/ Month</p>-->
                                        <!--</div>-->
                                    </div>
                                    <div class="table-content">
                                        <ul class="clearfix"> 
                                            <li>A Personal Financial Planning Tool</li>
                                            <li>Weekly Buy/Sell Dynamic Advice</li>
                                            <li>Educational Material </li>
                                            <li>VWA Tool to Plan & Track Scripts</li>
                                            <li>Advance Analytical Tools</li>
                                            <li>Performance Reports</li>
                                            <li>Additional Script Advices</li>
                                            <li>Free Access to Expert Webinars</li>
                                            <li>Additional Script Advices </li>
                                            <li>&nbsp;</li>
                                        </ul>
                                    </div>
                                    <div class="table-footer">
                                        <a href="#">Pre-book</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cta-section">
        <div class="pattern-layer" style="background-image: url(<?= Yii::$app->params['basepath'];?>//images/pattern/shape-3.png);"></div>
        <div class="auto-container">
            <div class="inner-container clearfix">
                <div class="title pull-left col-md-12">
                    <h4 style="color:#fff">Presently, the ‘Premium Membership’ is open only to a select few. Fill up the pre-book form today to guarantee your subscription in the next available slot. No credit card or payment confirmation required.</h4>
                </div>
                <!--<div class="btn-box pull-right col-md-4">-->
                <!--    <a href="">Signup</a>-->
                <!--    <a>Pre-book</a>-->
                <!--</div>-->
            </div>
        </div>
    </section>
