<div class="fxt-bg-img" data-bg-image="<?= Yii::$app->params['basepath'];?>images/login-bg.jpg" style="background-image: url(<?= Yii::$app->params['basepath'];?>images/login-bg.jpg);">
    <div class="fxt-header">
      <div class="fxt-transformY-50 fxt-transition-delay-1">
        <a href="#" class="fxt-logo"><img src="<?= Yii::$app->params['logo'];?>" alt="Logo"></a>
      </div>
      <div class="fxt-transformY-50 fxt-transition-delay-2">
        <h1>Welcome to Kalcy</h1>
      </div>
      <div class="fxt-transformY-50 fxt-transition-delay-3">
        <p>Kalcy Business solutions Pvt. Ltd. originated with the idea of providing all types of accounting services under one roof that would make clients feel safe and find refuge. We strongly believe that every number counts…</p>
      </div>
    </div>
    <ul class="fxt-socials">
      <li class="fxt-facebook fxt-transformY-50 fxt-transition-delay-4"><a href="https://www.facebook.com/kalcy.Business" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
      <li class="fxt-twitter fxt-transformY-50 fxt-transition-delay-5"><a href="#" title="twitter"><i class="fa fa-twitter"></i></a></li>
      <li class="fxt-google fxt-transformY-50 fxt-transition-delay-6"><a href="https://www.instagram.com/kalcy_business_solution/" title="instagram" target="_blank"><i class="fa fa-instagram"></i></a></li>
      <li class="fxt-linkedin fxt-transformY-50 fxt-transition-delay-7"><a href="https://www.linkedin.com/in/kalcy-business-solutions-0580901b6/" title="linkedin" target="_blank"><i class="fa fa-linkedin"></i></a></li>
      <!-- <li class="fxt-youtube fxt-transformY-50 fxt-transition-delay-8"><a href="#" title="youtube"><i class="fa fa-youtube"></i></a></li> -->
    </ul>
</div>