<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Profile';
?>

<div class="site-login">
 
        
    <!-- Login -->
  <main id="page-main" role="main">
     <div class="container">
        <a id="main-content" tabindex="-1"></a>
        <div class="row">
           <div class="col-lg-12">
              <div id="page-content">
                 <div class="content">
                    <div>
                       <div id="block-shelley-local-tasks" class="block">
                          <ul class="nav nav-tabs my-3">
                             <li class="nav-item"><a href="<?= \yii\helpers\Url::to(['site/profile', "id"=>Yii::$app->user->id])?>" class="nav-link active">View Profile<span class="visually-hidden"></span></a></li>
                            <!--  <li class="nav-item"><a href="/user/69/payment-methods" class="nav-link">Payment methods</a></li> -->
                             <li class="nav-item"><a href="<?= \yii\helpers\Url::to(['site/edit-profile', "id"=>Yii::$app->user->id])?>" class="nav-link" >Edit Profile</a></li>
                             
                          </ul>
                       </div>
                       <div class="col-md-12 col-lg-12">
                        <?php if (Yii::$app->session->hasFlash('success')): ?>
                            <div class="alert alert-success alert-dismissable">
                                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                 <h4><i class="icon fa fa-check"></i>Saved!</h4>
                                 <?= Yii::$app->session->getFlash('success') ?>
                            </div>
                        <?php endif; ?>

                        <?php if (Yii::$app->session->hasFlash('error')): ?>
                            <div class="alert alert-danger alert-dismissable">
                                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                 <h4><i class="icon fa fa-close"></i>Error!</h4>
                                 <?= Yii::$app->session->getFlash('error') ?>
                            </div>
                        <?php endif; ?>
                         <div class="well profile ">
                              <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-xs-12 col-lg-7">
                                      <h2><?= $model->first_name." ".$model->last_name?></h2>
                                      <p><strong>Email: </strong> <?= $model->email?> </p>
                                      <p><strong>Username: </strong> <?= $model->username?> </p>
                                      <p><strong>Mobile: </strong> <?= $model->mobile?> </p>
                                     <p><strong>City: </strong> <?= $model->city?> </p>
                                  </div> 
                                  <div class="col-xs-12 col-lg-5 text-center">
                                      <figure>
                                          <img src="http://www.localcrimenews.com/wp-content/uploads/2013/07/default-user-icon-profile.png" alt="" class="img-circle img-responsive">
                                          <figcaption class="ratings">
                                              <p><strong>Last Login:</strong> <?= date_format(date_create($login->login_at),'d-m-Y  H:i:s')?></p>
                                          </figcaption>
                                      </figure>
                                  </div> 
                                </div>
                                             
                                  
                              </div>            
                              <div class="col-xs-12 divider text-center">
                                <div class="row">
                                   
                                  <div class="col-xs-12 col-lg-4 emphasis">
                                     
                                      <a class="btn btn-info btn-block" href="<?=\yii\helpers\Url::to(['site/changepassword', 'id'=>$model->id])?>"><span class="fa fa-key"></span> &nbsp;&nbsp;Change Password </a>
                                  </div>
                                 

                                </div>
                                  
                                  
                              </div>
                         </div>                 
                      </div>
                      
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
  </main>
   
    <!-- End Login -->
</div>