
<!--Page Title-->

<?php
Yii::$app->metaTags->register($model);
$title="Subscription";
 if($model->banner->banner_image!="")
            {
                 echo "<section class='page-title centred' style='background-image: url(".$model->banner->banner_image.");'>";
            }
            else
            {
                echo "<section class='page-title centred' style='background-image: url(<?= Yii::$app->params['basepath'];?>/images/bg/page-title-2.jpg);'>";
            }
         ?>
    <div class="auto-container">
        <div class="content-box clearfix">
            <h1><?php echo "Subscription"; ?></h1>           
        </div>
    </div>
</section>
<!--End Page Title-->
<!-- service-section -->

<section class="service-section">
    <div class="auto-container">
        <div class="sec-title centred">
            <center><h5>Subscription</h5><center>
        </div>
    </div>
    
</section>


<section class="pricing-section " style="padding-top: 10px;background-image: url(<?= Yii::$app->params['basepath'];?>/images/pricing-bg.jpg);">
    <div class="auto-container">
        <div class="tabs-box centred">           
            <div class="tabs-content">
                <div class="tab active-tab" id="tab-1">
                    <div class="row clearfix">
                        <?php 

                        for($i=0;$i<count($subscription);$i++) { ?>
                        <div class="col-lg-4 col-md-4 col-sm-6 pricing-block " id="subdiv">
                            <div class="pricing-block-one">
                                <div class="pricing-table">
                                    <div class="table-header">
                                        <h3><?php echo $subscription[$i]['script_per_week']. " Script Per Week";?></h3>
                                    </div>
                                    <div class="table-content subscribe-plan">
                                        <?php echo "<input type='radio' id='".$subscription[$i]['id']."_one_month' name='".$subscription[$i]['id']."_month' value='".$subscription[$i]['one_month']."' >
                                            <label for='".$subscription[$i]['id']."_one_month'>One Month: ".$subscription[$i]['one_month']." Rs.</label><br>";
                                            echo "<input type='radio' id='".$subscription[$i]['id']."_three_month' name='".$subscription[$i]['id']."_month' value='".$subscription[$i]['three_month']."'>
                                            <label for='".$subscription[$i]['id']."_three_month'>Three Month: ".$subscription[$i]['three_month']." Rs.</label><br>";
                                            echo "<input type='radio' id='".$subscription[$i]['id']."_twelve_month' name='".$subscription[$i]['id']."_month' value='".$subscription[$i]['twelve_month']."' >
                                            <label for='".$subscription[$i]['id']."_twelve_month'>Twelve Month: ".$subscription[$i]['twelve_month']." Rs.</label><br>";
                                            
                                        ?>
                                    </div>
                                    <div class="table-footer">
                                        <!--
                                            href=" \yii\helpers\Url::to(['site/signup'])" -->
                                        <!-- <a  class="theme-btn style-three" href="payment" onclick="return Validate(<?= $subscription[$i]['id'] ?>)"   >Subscribe</a> -->
                                        <button  class="theme-btn style-three" onclick="return Validate(<?= $subscription[$i]['id'] ?>)"   >Subscribe</button>
                                        
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>

                    <?php } ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    function Validate(id) {
        
        var val=document.querySelector('input[name="'+id+'_month"]:checked');  

 var i=$('input:radio:checked').length;
         if(i>1)
    {
        alert("Please Select Only One Subscription Plan");
        window.location.reload();
      //  window.location="http://localhost/vwa/site/subscription";
       // exit();
       return false;

    }     
        if(val!=null)
        {
            localStorage.setItem ("amount_val",val.value);
            localStorage.setItem ("amount_id",val.id);
            localStorage.setItem ("subscription_id",val.id);
            window.location.href="subscription-request";           
        }
        else
        {
              alert("Please select at least one subscription plan");
             // window.location.href = window.location.href;
               
               return false;

        }  

        


   }
</script>


