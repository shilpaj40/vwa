<?php 
Yii::$app->metaTags->register($model);
$faq = \backend\models\Faq::find()->where(['status'=>1])->all();?>
<!--Page Title-->
<section class="page-title centred" style="background-image: url(<?= Yii::$app->params['basepath'];?>/images/bg/page-title-2.jpg);">
    <div class="auto-container">
        <div class="content-box clearfix">
            <h1>FAQ's</h1>           
        </div>
    </div>
</section>
<!--End Page Title-->
<!-- service-section -->
<section class="service-section pb-50">  
    <div class="auto-container">
        <div class="blog-details-content">
            <div class="comments-area">
               <div class="comment-box">
                <?php
                for($i=0;$i < count($faq);$i++){
                   echo "<div class='comment' style='padding-left:0'><div class='comment-inner'>";
                echo "<div class='comment-info clearfix'><div class='name pull-left'><h5>";
                echo $faq[$i]['question']."</h5></div></div><div class='text'><p>";
                echo $faq[$i]['answer']."</p></div></div></div>";
                }
                ?>
                    <!-- <div class="comment" style="padding-left:0">
                        <div class="comment-inner">
                            <div class="comment-info clearfix">
                                <div class="name pull-left">
                                    <h5>Why isn’t the Purchase Price (PP) advised on weekdays?</h5>
                                </div>
                            </div>
                            <div class="text">
                                <p>Our proprietary algorithm is based on the weekly behaviour of stock prices, which is computed at market closing and emailed on Sunday mornings.</p>
                            </div>
                        </div>
                    </div>
                    <div class="comment" style="padding-left:0">
                        <div class="comment-inner">
                            <div class="comment-info clearfix">
                                <div class="name pull-left">
                                    <h5>How many units do I purchase on a subsequent purchase? i.e. 2nd, 3rd, etc.?</h5>
                                </div>
                            </div>
                            <div class="text">
                                <p>We recommend purchasing an equal or a greater number of units on a subsequent purchase. Purchasing lesser units on a subsequent purchase deviates you from the targeted goal. Paid subscribers who update their purchases on our portal, can track changes in their targeted sell price.</p>
                            </div>
                        </div>
                    </div>
                    <div class="comment" style="padding-left:0">
                        <div class="comment-inner">
                            <div class="comment-info clearfix">
                                <div class="name pull-left">
                                    <h5>What if I couldn’t make transactions as advice by VWA?</h5>
                                </div>
                            </div>
                            <div class="text">
                                <p>We understand that life isn’t always fair! Hence, our portal is built keeping such uncertainties in mind, even if you deviate from the advice, but have an updated transaction history on the portal; you would have a customised solution ready. In case you’re unsure of your transaction frequency, we recommend upgrading to a premium account.</p>
                            </div>
                        </div>
                    </div>
                    <div class="comment" style="padding-left:0">
                        <div class="comment-inner">
                            <div class="comment-info clearfix">
                                <div class="name pull-left">
                                    <h5>How are the returns calculated?</h5>
                                </div>
                            </div>
                            <div class="text">
                                <p>Our proprietary algorithm has similarities with a banking method, wherein rupee weeks of investment are computed to calculate annual returns using simple arithmetic of gains.</p>
                            </div>
                        </div>
                    </div>
                    <div class="comment" style="padding-left:0">
                        <div class="comment-inner">
                            <div class="comment-info clearfix">
                                <div class="name pull-left">
                                    <h5>What if I have an appetite for more transactions than what’s recommended in VWA’s weekly advisory?</h5>
                                </div>
                            </div>
                            <div class="text">
                                <p>Our premium plan is designed keeping in mind an urban, retail investor. If you intend to make more transactions each month, we have an add-on plan for you as well.</p>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
           
        </div>
    </div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
   $(document).ready(function(){
  
    $("li#FAQ").addClass("current");

});
</script>
