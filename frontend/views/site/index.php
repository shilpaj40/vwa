<?php

/* @var $this yii\web\View */
$this->title = 'Vishwas Soman';
Yii::$app->metaTags->register($model);
?>
<div class="site-index">
    <!-- banner-section -->
    <section class="banner-section style-three">
        <div class="banner-carousel owl-theme owl-carousel owl-dots-none">
        <?php $slider=\backend\models\CarouselItem::find()->where(['status'=>1])->orderBy(['sequence_no'=>SORT_ASC])->all();
                                $i=0;
                                foreach ($slider as $carousel)
                                {
        echo "<div class='slide-item'><div class='image-layer' style='background-image:url(".$carousel->image.")'></div><div class='auto-container'><div class='content-box centred'><h1>".$carousel->name."</h1><p>".$carousel->body."</p></div></div></div>";
             } ?>              
        </div>
    </section>
    <!-- banner-section end -->

    <section class="intro-section">
        <div class="auto-container">
            <div class="upper-content" style="width:100%">
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                        <div class="single-item">
                            <div class="inner-box">
                                <div class="icon-box"><i class="flaticon-search"></i></div>
                                <?php echo $model->best_advisors; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                        <div class="single-item">
                            <div class="inner-box">
                                <div class="icon-box"><i class="flaticon-mountain"></i></div>
                               <?php echo $model->productive_weekends; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                        <div class="single-item">
                            <div class="inner-box">
                                <div class="icon-box"><i class="flaticon-networking-1"></i></div>
                               <?php echo $model->zero_surprises; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="info-section pb-0">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-md-2">
                </div>
               <!--  <div class="col-lg-7 col-md-4 col-sm-12 title-column"> -->
                <div class="col-lg-4 col-md-4 col-sm-12 title-column">
                    <div class="title-inner">
                        <div class="year-box">
                            <?php echo "<figure class='image-box'>'<img src=".$model->image."></figure>"; ?>                            
                           
                        </div>
                      
                    </div>
                </div>
                
               <!--  <div class="col-lg-5 col-md- col-sm-12 text-column"> -->
                <div class="col-lg-4 col-md-4 col-sm-12 text-column">
                    <div class="text">
                     <?php echo $model->about_details; ?>
                        <a href="<?= \yii\helpers\Url::to(['site/who-we-are'])?>"><i class="fas fa-arrow-right"></i><span>Read More</span></a>
                    </div>
                </div>
                <div class="col-md-2">
                </div>
            </div>
        </div>
    </section>
   
    
    <section class="service-style-two bg-color-2 cta-style-two">
        <div class="pattern-layer"> <figure class="image-box"><img src="<?= Yii::$app->params['basepath'];?>/images/shape-21.png" alt=""></figure></div>
        <div class="auto-container">
            <div class="inner-box clearfix">
                <div class="sec-title light pull-left mb-0">
                    
                    <h2>90 days Free offer!</h2>
                    <!--<h4 class="text-white">Benefit from VWA's algorithm-based stock investment advisory.</h4>-->
                    <ul class="clearfix">
                    <li><p class="text-white"><i class="fa fa-check"></i> Weekly Buy/Sell Dynamic Advice</p></li>
                     <li><p class="text-white"><i class="fa fa-check"></i> Access to Educational Material</p></li>
                    </ul>
                </div>
                <div class="btn-box pull-right">
                    <a href="<?= \yii\helpers\Url::to(['site/signup'])?>" class="theme-btn style-two">Join Now</a>
                </div>
            </div>
        </div>
    </section>

    <section class="service-style-two pt-0 pb-0" id="service">
        <div class="" >
             <?php echo "<img src=".$model->footer_image." style='width:100%' />"; ?>   
           
        </div>
        <!--<div class="auto-container">-->
        <!--    <div class="sec-title light centred">-->
        <!--        <h5>WHAT WE PROVIDES</h5>-->
        <!--        <h2>Financial doctor a day<br> keeps market risks at bay! </h2>-->
        <!--        <p>How often have you got lured by advertisements that read – Want to double your money, invest with us and reap high returns.</p>-->
        <!--        <p>As novice investors we are bound to be flummoxed by the idea of high returns and fatter wallets. But managing money isn’t that easy. If you do not manage your money correctly during your working years, you will not be able to enjoy a blissful retirement on reaching your golden years. Without falling prey to any marketing gimmicks, one must judiciously weigh the pros and cons and make an informed decision as to whether it satisfies one’s investment objectives.</p>-->
                
        <!--    </div>-->
        <!--    <div class=" centred">-->
        <!--        <div class="btn-box ">-->
        <!--            <a href="#" class="btn-bg-white style-three">Read more <i class="fas fa-arrow-right"></i></a>-->
        <!--        </div>-->
        <!--    </div>-->
           
        <!--</div>-->
    </section>
  

    <!-- testimonial-section -->
    <!--<section class="testimonial-section" style="background-image: url(<?= Yii::$app->params['basepath'];?>/images/testimonial-bg.jpg);">-->
    <!--    <div class="auto-container">-->
    <!--        <div class="title-box">-->
    <!--            <div class="row clearfix">-->
    <!--                <div class="col-lg-6 col-md-12 col-sm-12 title-column">-->
    <!--                    <div class="sec-title right">-->
    <!--                        <h5>testimonials</h5>-->
    <!--                        <h2>What Our Loving <br />Clients Say</h2>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="col-lg-6 col-md-12 col-sm-12 text-column">-->
    <!--                    <div class="text">-->
    <!--                        <p>Tempor incididunt ut labore et dolore magna aliquat enim veniam quis nostrud exercitation ullamco laboris nis aliquip consequat duis aute irure dolor voluptate.</p>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--        <div class="testimonial-inner">-->
    <!--            <div class="client-testimonial-carousel owl-carousel owl-theme owl-nav-none owl-dots-none">-->
    <!--                <div class="testimonial-block">-->
    <!--                    <div class="text">-->
    <!--                        <p>Aspernatur aut odit aut fugit sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam estm qui dolorem ipsum quia dolor sit amet consectetur, adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam sd ipsum quaerat voluptatem.</p>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="testimonial-block">-->
    <!--                    <div class="text">-->
    <!--                        <p>Aspernatur aut odit aut fugit sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam estm qui dolorem ipsum quia dolor sit amet consectetur, adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam sd ipsum quaerat voluptatem.</p>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="testimonial-block">-->
    <!--                    <div class="text">-->
    <!--                        <p>Aspernatur aut odit aut fugit sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam estm qui dolorem ipsum quia dolor sit amet consectetur, adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam sd ipsum quaerat voluptatem.</p>-->
    <!--                    </div>-->
    <!--                </div>-->
                   
    <!--            </div>-->
    <!--            <div class="client-thumb-outer">-->
    <!--                <div class="client-thumbs-carousel owl-carousel owl-theme owl-nav-none owl-dots-none">-->
    <!--                    <div class="thumb-item">-->
    <!--                        <figure class="thumb-box"><img src="<?= Yii::$app->params['basepath'];?>/images/testimonial/testimonial-3.png" alt=""></figure>-->
    <!--                        <div class="info-box">-->
    <!--                            <h5>Robert Smith</h5>-->
    <!--                            <span class="designation">Senior Manager</span>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="thumb-item">-->
    <!--                        <figure class="thumb-box"><img src="<?= Yii::$app->params['basepath'];?>/images/testimonial/testimonial-3.png" alt=""></figure>-->
    <!--                        <div class="info-box">-->
    <!--                            <h5>Christine Eva</h5>-->
    <!--                            <span class="designation">Senior Manager</span>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="thumb-item">-->
    <!--                        <figure class="thumb-box"><img src="<?= Yii::$app->params['basepath'];?>/images/testimonial/testimonial-3.png" alt=""></figure>-->
    <!--                        <div class="info-box">-->
    <!--                            <h5>Jhon Doe</h5>-->
    <!--                            <span class="designation">Senior Manager</span>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->

    <!--<section class="fun-fact centred">-->
    <!--    <div class="auto-container">-->
    <!--        <div class="row clearfix">-->
    <!--            <div class="col-lg-3 col-md-6 col-sm-12 counter-column">-->
    <!--                <div class="counter-block-one wow slideInUp animated" data-wow-delay="00ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: slideInUp;">-->
    <!--                    <div class="count-outer count-box counted">-->
    <!--                        <span class="count-text" data-speed="1500" data-stop="254">254</span>-->
    <!--                    </div>-->
    <!--                    <p>Expert Consultants</p>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--            <div class="col-lg-3 col-md-6 col-sm-12 counter-column">-->
    <!--                <div class="counter-block-one wow slideInUp animated" data-wow-delay="00ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: slideInUp;">-->
    <!--                    <div class="count-outer count-box counted">-->
    <!--                        <span class="count-text" data-speed="1500" data-stop="930">930</span>-->
    <!--                    </div>-->
    <!--                    <p>Our Trusted Clients</p>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--            <div class="col-lg-3 col-md-6 col-sm-12 counter-column">-->
    <!--                <div class="counter-block-one wow slideInUp animated" data-wow-delay="00ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: slideInUp;">-->
    <!--                    <div class="count-outer count-box counted">-->
    <!--                        <span class="count-text" data-speed="1500" data-stop="826">826</span>-->
    <!--                    </div>-->
    <!--                    <p>Orders in Queue</p>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--            <div class="col-lg-3 col-md-6 col-sm-12 counter-column">-->
    <!--                <div class="counter-block-one wow slideInUp animated" data-wow-delay="00ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: slideInUp;">-->
    <!--                    <div class="count-outer count-box counted">-->
    <!--                        <span class="count-text" data-speed="1500" data-stop="720">720</span>-->
    <!--                    </div>-->
    <!--                    <p>Projects Delivered</p>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->
   
    
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
   $(document).ready(function(){
    $("li#home").addClass("current");
});
</script>