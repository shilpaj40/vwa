<?php
use yii\helpers\Html;

?><!--Page Title-->

<!--End Page Title-->
<section class="about-style-two about-page-2 bg-color-1 pt-50">
    <div class="auto-container">
        <div class="sec-title centred">
            <h5>Stock</h5>            
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 content-column">
                <div id="content_block_three">
                    <div class="content-box">
                        <div class="tabs-box">
                            <div class="tab-btn-box">
                                <ul class="tab-btns tab-buttons clearfix">
                                    <li class="tab-btn active-btn" data-tab="#tab-1">Active Stocks</li>
                                    <li class="tab-btn" data-tab="#tab-2">Closed Stocks</li>
                                </ul>
                            </div>
                            <div class="tabs-content">
                                <div class="tab active-tab" id="tab-1">

                                    <div class="content-inner box">
                                        <div class="box-body">
                                          <?php $stockNAme = \backend\models\Stock::find()->where(['status' => 2, 'is_closed'=>0])->groupby(['stock_name'])->all(); 
                                          $i=0;?>
                                          <?php if(count($stockNAme)>0){
                                          foreach ($stockNAme as $stocks){ $i++;?>
                                          <div class="card mt-2-2">
                                              <div class="card-header mb-4">
                                                <div class="row" >
                                                  <div class="col-md-12" >
                                                    <div class="sec-title mb-2 left">
                                                     
                                                        <h3><?= $stocks->stock_name;?></h3>
                                                       
                                                    </div>
                                                   
                                                    
                                                  </div>
                                                  
                                                </div>
                                              </div>
                                              <div class="card-body">
                                                 <!-- <button class="btn btn-primary" id="submit_data">Submit</button> -->
                                                 <table class="table table-responsive-md table-sm table-bordered" >
                                                    <thead>
                                                        <tr>
                                                            <th colspan="3" style="background: #36304a"><?= $stocks->stock_name;?></strong></th>
                                                            <th style="background: #36304a" colspan="2">Symbol : <?= $stocks->symbol;?></th>
                                                        </tr>
                                                       <tr>
                                                          <th>Date </th>
                                                          <th>Time</th>
                                                          <th>Suggestion </th>
                                                           <th>Action </th>
                                                          <th>Actual Event </th>              
                                                       </tr>
                                                    </thead>
                                                    <tbody>
                                                      <?php
                                                      $stock = \backend\models\Stock::find()->where(['status' => 2, 'is_closed'=>0, 'stock_name'=>$stocks->stock_name])->all();
                                                       foreach($stock as $list){
                                                          echo "<tr >"; 
                                                          echo "<td id='date-".$list->id."'>".date_format(date_create($list->date),'d-m-Y')."</td>";
                                                              echo "<td id='time-".$list->id."'>".date_format(date_create($list->created_at),'h:i:s')."</td>";
                                                              echo "<td id='suggestion-".$list->id."'>".$list->suggestion."</td>";
                                                              echo "<td id='action-".$list->id."'>".$list->action."</td>";
                                                              echo "<td id='actual_event-".$list->id."'>".$list->actual_event."</td>";
                                                              echo "</tr>";
                                                      }?>
                                                      
                                                    </tbody>
                                                 </table>
                                                 <!-- <span style="float:right"><button id="but_add_<?= $i?>" class="btn btn-danger">Add New Row</button></span> -->
                                              </div>
                                          </div>
                                          <?php }
                                          }
                                          else {
                                              echo "<h3>No Data</h3>";
                                          }?>
                                         
                                      </div>
                                    </div>
                                </div>
                                <div class="tab" id="tab-2">
                                    <div class="content-inner box">
                                        <div class="box-body">
                                          <?php $stockNAme = \backend\models\Stock::find()->where(['status' => 2, 'is_closed'=>1])->groupby(['stock_name'])->all(); 
                                          $i=0;?>
                                          <?php if(count($stockNAme)>0){
                                          foreach ($stockNAme as $stocks){ $i++;?>
                                          <div class="card mt-2-2">
                                              
                                              <div class="card-body">
                                                 
                                                 <table class="table table-responsive-md table-sm table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="3" style="background: #36304a"><?= $stocks->stock_name;?></strong></th>
                                                            <th colspan="2" style="background: #36304a">Symbol : <?= $stocks->symbol;?></th>
                                                        </tr>
                                                       <tr>
                                                          <th>Date </th>
                                                          <th>Time</th>
                                                          <th>Suggestion </th>
                                                           <th>Action </th>
                                                          <th>Actual Event </th>              
                                                       </tr>
                                                    </thead>
                                                    <tbody>
                                                      <?php
                                                      $stock = \backend\models\Stock::find()->where(['status' => 2, 'is_closed'=>1, 'stock_name'=>$stocks->stock_name])->all();
                                                       foreach($stock as $list){
                                                          echo "<tr >"; 
                                                          echo "<td id='date-".$list->id."'>".date_format(date_create($list->date),'d-m-Y')."</td>";
                                                              echo "<td id='time-".$list->id."'>".date_format(date_create($list->created_at),'h:i:s')."</td>";
                                                              echo "<td id='suggestion-".$list->id."'>".$list->suggestion."</td>";
                                                              echo "<td id='action-".$list->id."'>".$list->action."</td>";
                                                              echo "<td id='actual_event-".$list->id."'>".$list->actual_event."</td>";
                                                              echo "</tr>";
                                                      }?>
                                                      
                                                    </tbody>
                                                 </table>
                                                
                                              </div>
                                          </div>
                                          <?php }
                                          } else {
                                              echo "<h3>No Data</h3>";
                                          }
                                          ?>
                                         
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>
<script type="text/javascript">
    function showcalculation()
    {
       var price=$("#purchased-price").val();
       if(price!="")
       {
        var buy=parseFloat(($("#purchased-price").val()*5)/100);
        var total_buy_price=parseFloat(price-buy);
        var total_sell_price=parseFloat(price)+parseFloat(buy);
        $("#purchased-price").css("border","1px solid #ccc");
        $("#sell").show();
        $("#buy").show();
        $("#sell-price").val("Sell @ "+ total_sell_price.toFixed(2));
        $("#buy-price").val("Buy @ "+ total_buy_price.toFixed(2));
       }
       else
       {
        alert("Please enter your price");
        $("#purchased-price").css("border","1px solid #f00");
        $("#sell").hide();
        $("#buy").hide();
       }
       

    }
</script>