<html>
<head>
  <script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-
color="<color-code>"
 bolt-logo="<image path>"></script>
  <style type="text/css">
  #content_block_nine .content-box .form-group input[type='text'],#content_block_nine .content-box .form-group textarea ,#content_block_nine .content-box .form-group input[type='email']{
    position: relative;
    width: 100%;
    background: #fff;
    height: 45px;
    padding: 10px 30px;
    font-size: 16px;
    border: 2px solid #fff;
    border-radius: 30px;
    transition: all 500ms ease;
}
.disabled { cursor: not-allowed; }



  </style>
  </head>
  <!-- <body onload="submitPayuForm()"> -->


    <div class="site-login">
  <main id="page-main" role="main">
    <section class="contact-section" style="background-image: url(<?= Yii::$app->params['basepath'];?>/images/contact-1.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-xl-6 col-lg-7 col-md-7 col-md-offset-3 content-column">
                    <div id="content_block_nine">
                        <div class="content-box bg-color-white">
                            <div class="sec-title style-three centred mb-0 pb-0">
                                <h2>Subcription Request</h2>
                            </div>
                            
   
    <br/>


    <div class="form-group" style="padding: 10px;">
    <form  method="post" name="payuForm">
      
         <input type="hidden"  class="form-control" name="subcription_id" id="subscription_id"/>
      
      
      <center><div id="display"  style="color:red;"><b>Selected Subscription: <span id="script"></span> script per week for <span id="month_selected"></span> month </b></div></center>
      <br>
      <div class="error-messages" style="display:none;color:red;"></div>
        <div class="form-inner">
 
      
         <div class="row">
          <div class="form-group">

            <div class="col-md-6">
               
          <label class="control-label" for="payment-firstname">First Name </label> <span style="color:red"> *</span>
          
          <input type="text"  pattern="[A-Za-z]{1,32}" name="firstname"  minlength="2" maxlength="30" class="form-control" id="firstname" required />
        </div>
         
         <div class="col-md-6">
           <label class="control-label" for="payment-lastname">Last Name </label> <span style="color:red"> *</span> 
          
          <input type="text"  name="lastname"   pattern="[A-Za-z]{1,32}"  minlength="2" maxlength="30" class="form-control" id="lastname"  required/>
        </div>
      </div></div>

        
        <div class="row">
         
           <div class="form-group">
            <div class="col-md-6 ">
               
          <label class="control-label" for="payment-email">Email </label><span style="color:red"> *</span>
          <input type="email"  name="email" class="form-control" id="email" required="Please enter valid email id"/>
        </div>

        <div class="col-md-6">
          <label class="control-label" for="payment-mobile">Mobile Number </label><span style="color:red"> *</span> 
          <input  type="text"  id="mobile-num" minlength="10" maxlength="10"  name="phone" class="form-control"  required/>
         

         </div>

       </div>
         </div>




         <div class="row">
          <div class="form-group">
            <div class="col-md-6">
               
          <label class="control-label" for="payment-city">City</label> <span style="color:red"> *</span>
          
          <input type="text" id="city"   pattern="[A-Za-z]{1,32}" name="city"  class="form-control" required />
        </div>

            <div class="col-md-6">
               
          <label class="control-label" for="payment-amount">Amount (Rs)</label>
          
          <input type="text" id="amount"  name="amount" class="form-control disabled" readonly/>
        </div></div></div>
<br><br>


            <div class="row">
             <div class="form-group">
          
            <center>

              <button type="submit" style="width:25%;" class="theme-btn style-three" type="submit" id="submit_form" name="login-button">Submit</button></center>
         
           </div></div></div>
        
    </form>
    </div> </div>
 
                    </div>
                </div>
            </div>
        </div>
    </section>
    
  </main>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">

$("#amount").val(localStorage.getItem ("amount_val"));
$("#subscription_id").val(localStorage.getItem ("subscription_id"));



$(document).ready(function(){
  
  $("#mobile-num").on("blur", function(){
        var mobNum = $(this).val();
        var filter = /^\d*(?:\.\d{1,2})?$/;

          if (filter.test(mobNum)) {
            if(mobNum.length==10){
                //  alert("valid");
                  $(".error-messages").empty().fadeOut();
              $("#mobile-valid").removeClass("hidden");
              $("#folio-invalid").addClass("hidden");
             } else {
                //alert('Please put 10  digit mobile number');
                $(".error-messages").text("Please Enter Valid Mobile Number").fadeIn();
               $("#folio-invalid").removeClass("hidden");
               $("#mobile-valid").addClass("hidden");
               $('#mobile-num').val("");
                return false;
              }
            }
            else {
              //alert('Not a valid mobile number');
              $(".error-messages").text("Please Enter Valid Mobile Number").fadeIn();
              $("#folio-invalid").removeClass("hidden");
              $("#mobile-valid").addClass("hidden");
                $('#mobile-num').val("");
              return false;
           }
    
  });


  $("#firstname").on("blur", function() {
    if ( $(this).val().match('^[a-zA-Z]{1,32}$') ) {
        $(".error-messages").empty().fadeOut();
              $("#mobile-valid").removeClass("hidden");
              $("#folio-invalid").addClass("hidden");
    } else {
       $(".error-messages").text("Please Enter Valid First Name").fadeIn();
               $("#folio-invalid").removeClass("hidden");
               $("#mobile-valid").addClass("hidden");
               $('#firstname').val("");
                return false;
    }
});



  $("#lastname").on("blur", function() {
    if ( $(this).val().match('^[a-zA-Z]{1,32}$') ) {
        $(".error-messages").empty().fadeOut();
              $("#mobile-valid").removeClass("hidden");
              $("#folio-invalid").addClass("hidden");
    } else {
       $(".error-messages").text("Please Enter Valid Last Name").fadeIn();
               $("#folio-invalid").removeClass("hidden");
               $("#mobile-valid").addClass("hidden");
               $('#lastname').val("");
                return false;
    }
});



  $("#city").on("blur", function() {
    if ( $(this).val().match('^[a-zA-Z]{1,32}$') ) {
        $(".error-messages").empty().fadeOut();
              $("#mobile-valid").removeClass("hidden");
              $("#folio-invalid").addClass("hidden");
    } else {
       $(".error-messages").text("Please Enter Valid City Name").fadeIn();
               $("#folio-invalid").removeClass("hidden");
               $("#mobile-valid").addClass("hidden");
               $('#city').val("");
                return false;
    }
});

  
var hv = $('#subscription_id').val();
var temp=hv.split("_");
//alert(temp[0]);
$("#script").append(temp[0]);
$("#month_selected").append(temp[1]);

        });

</script>
  </body>
</html>
