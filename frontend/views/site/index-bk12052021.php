<?php

/* @var $this yii\web\View */
$this->title = 'Vishwas Soman';
?>
 <style>


ul li p::before {
  content: "\2022";
  color: black;
  font-weight: bold;
  display: inline-block; 
  width: 1em;
  margin-left: -1em;
}
</style> 

<div class="site-index">
    <!-- banner-section -->
    <section class="banner-section style-three">
        <div class="banner-carousel owl-theme owl-carousel owl-dots-none">
            <div class="slide-item">
                <div class="image-layer" style="background-image:url(<?= Yii::$app->params['basepath'];?>/images/banner/banner-4.jpg)"></div>
                <div class="auto-container">
                    <div class="content-box centred">
                        <h1>A financial doctor a day,<br />keeps market risks at bay!</h1>
                        <p>In the age of extreme volatility, global impacts and unpredictable inflation, <br />financial planning and its implementation is the only key to become wealthy in an informed manner.</p>
                        <!-- <div class="btn-box">
                            <a href="#" class="theme-btn style-three">How Can We Help</a>
                            <a href="#" class="banner-btn">Learn More</a>
                        </div> -->
                    </div>  
                </div>
            </div>
            <!-- <div class="slide-item">
                <div class="image-layer" style="background-image:url(<?= Yii::$app->params['basepath'];?>/images/banner/banner-2.jpg)"></div>
                <div class="auto-container">
                    <div class="content-box">
                        <h1>Solutions Products <br />Reinvent The Digital</h1>
                        <p>Tempor incididunt ut labore dolore magna aliqua veniam quis nostrud <br />ullamco laboris nis consequat aute irure dolor voluptate.</p>
                        <div class="btn-box">
                            <a href="index-2.html" class="theme-btn style-three">How Can We Help</a>
                            <a href="index-7.html" class="banner-btn">Learn More</a>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="slide-item">
                <div class="image-layer" style="background-image:url(<?= Yii::$app->params['basepath'];?>/images/banner/banner-3.jpg)"></div>
                <div class="auto-container">
                    <div class="content-box centred">
                        <h1>Business Assistance <br />In Right Direction</h1>
                        <p>Tempor incididunt ut labore dolore magna aliqua veniam quis nostrud <br />ullamco laboris nis consequat aute irure dolor voluptate.</p>
                        <div class="btn-box">
                            <a href="index-2.html" class="theme-btn style-three">How Can We Help</a>
                            <a href="index-7.html" class="banner-btn">Learn More</a>
                        </div>
                    </div>   
                </div>
            </div> -->
        </div>
    </section>
    <!-- banner-section end -->

    <section class="intro-section">
        <div class="auto-container">
            <div class="upper-content">
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                        <div class="single-item">
                            <div class="inner-box">
                                <div class="icon-box"><i class="flaticon-search"></i></div>
                                <h3><a href="#">Best Advisors</a></h3>
                                <p>A SEBI certified team that comprises of experts with extensive experience in the finance sector, statisticians and mathematicians</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                        <div class="single-item">
                            <div class="inner-box">
                                <div class="icon-box"><i class="flaticon-mountain"></i></div>
                                <h3><a href="#">Productive Weekends</a></h3>
                                <p>Our advisory reaches your inbox every Sunday morning. Dedicate an hour to implement our recommendations and let profits pour through the week.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                        <div class="single-item">
                            <div class="inner-box">
                                <div class="icon-box"><i class="flaticon-networking-1"></i></div>
                                <h3><a href="#">Zero Surprises</a></h3>
                                <p>We believe in complete transparency and empowerment of our subscribers. Beyond a monthly advisory fee, you pay us nothing.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="info-section pb-0">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-7 col-md-12 col-sm-12 title-column">
                    <div class="title-inner">
                        <div class="year-box">
                            <figure class="image-box"><img src="<?= Yii::$app->params['basepath'];?>/images/year-icon.png" alt=""></figure>
                            <h2>40+</h2>
                            <h3>Years</h3>
                        </div>
                        <div class="title">
                            <h2><!--Years of Experience <span>—</span> -->It is discipline, market understanding and confidence & caution that creates wealth.</h2>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 text-column">
                    <div class="text">
                    <!-- <p>
                        How often have you got lured by advertisements that read – Want to double your money, invest with us and reap high returns.</p> -->

                    <p>It is discipline, market understanding and confidence & caution that creates wealth.</p>
                   <!--  <p>As novice investors we are bound to be flummoxed by the idea of high returns and fatter wallets. But managing money isn’t that easy. If you do not manage your money correctly during your working years, you will not be able to enjoy a blissful retirement on reaching your golden years. Without falling prey to any marketing gimmicks, one must judiciously weigh the pros and cons and make an informed decision as to whether it satisfies one’s investment objectives.</p> -->
                   <p>Our team has been researching 500+ companies every week for the past 10 years to find patterns in this chaos of stock markets and validate them. A combination of these efforts and their experience has given birth to a robust, proprietary algorithm of Vishwas Wealth Advisory.</p>
                        <a href="<?= \yii\helpers\Url::to(['site/who-we-are'])?>"><i class="fas fa-arrow-right"></i><span>Read More</span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
   
    <section class="service-style-five bg-color-1 pb-50 pt-50" id="content_block_four">
    <a href="/VWA_new/site/signup">
    <div class="auto-container content-box " >
    
        <div class="tabs-content">
            <div class="row">
                <p>Limited period offer!</p>
                    <p><b>Benefit from VWA's algorithm-based stock investment advisory.</b></p>
                <div class="col-md-12">
                    
                    <ul class="clearfix">
                     <li><p>90 days FREE trial</p></li>
                     <li><p>Personal Financial Planning Tool</p></li>
                     <li><p>Weekly Buy/Sell Dynamic Advice</p></li>
                     <li><p>Access to Educational Material</p></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</a>
</section>

    <section class="service-style-two pt-50 pb-0" id="service">
        <div class="" >
            <img src="<?= Yii::$app->params['basepath'];?>images/Process.jpg" style="width:100%" />
        </div>
        <!--<div class="auto-container">-->
        <!--    <div class="sec-title light centred">-->
        <!--        <h5>WHAT WE PROVIDES</h5>-->
        <!--        <h2>Financial doctor a day<br> keeps market risks at bay! </h2>-->
        <!--        <p>How often have you got lured by advertisements that read – Want to double your money, invest with us and reap high returns.</p>-->
        <!--        <p>As novice investors we are bound to be flummoxed by the idea of high returns and fatter wallets. But managing money isn’t that easy. If you do not manage your money correctly during your working years, you will not be able to enjoy a blissful retirement on reaching your golden years. Without falling prey to any marketing gimmicks, one must judiciously weigh the pros and cons and make an informed decision as to whether it satisfies one’s investment objectives.</p>-->
                
        <!--    </div>-->
        <!--    <div class=" centred">-->
        <!--        <div class="btn-box ">-->
        <!--            <a href="#" class="btn-bg-white style-three">Read more <i class="fas fa-arrow-right"></i></a>-->
        <!--        </div>-->
        <!--    </div>-->
           
        <!--</div>-->
    </section>
  

    <!-- testimonial-section -->
    <!--<section class="testimonial-section" style="background-image: url(<?= Yii::$app->params['basepath'];?>/images/testimonial-bg.jpg);">-->
    <!--    <div class="auto-container">-->
    <!--        <div class="title-box">-->
    <!--            <div class="row clearfix">-->
    <!--                <div class="col-lg-6 col-md-12 col-sm-12 title-column">-->
    <!--                    <div class="sec-title right">-->
    <!--                        <h5>testimonials</h5>-->
    <!--                        <h2>What Our Loving <br />Clients Say</h2>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="col-lg-6 col-md-12 col-sm-12 text-column">-->
    <!--                    <div class="text">-->
    <!--                        <p>Tempor incididunt ut labore et dolore magna aliquat enim veniam quis nostrud exercitation ullamco laboris nis aliquip consequat duis aute irure dolor voluptate.</p>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--        <div class="testimonial-inner">-->
    <!--            <div class="client-testimonial-carousel owl-carousel owl-theme owl-nav-none owl-dots-none">-->
    <!--                <div class="testimonial-block">-->
    <!--                    <div class="text">-->
    <!--                        <p>Aspernatur aut odit aut fugit sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam estm qui dolorem ipsum quia dolor sit amet consectetur, adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam sd ipsum quaerat voluptatem.</p>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="testimonial-block">-->
    <!--                    <div class="text">-->
    <!--                        <p>Aspernatur aut odit aut fugit sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam estm qui dolorem ipsum quia dolor sit amet consectetur, adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam sd ipsum quaerat voluptatem.</p>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="testimonial-block">-->
    <!--                    <div class="text">-->
    <!--                        <p>Aspernatur aut odit aut fugit sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam estm qui dolorem ipsum quia dolor sit amet consectetur, adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam sd ipsum quaerat voluptatem.</p>-->
    <!--                    </div>-->
    <!--                </div>-->
                   
    <!--            </div>-->
    <!--            <div class="client-thumb-outer">-->
    <!--                <div class="client-thumbs-carousel owl-carousel owl-theme owl-nav-none owl-dots-none">-->
    <!--                    <div class="thumb-item">-->
    <!--                        <figure class="thumb-box"><img src="<?= Yii::$app->params['basepath'];?>/images/testimonial/testimonial-3.png" alt=""></figure>-->
    <!--                        <div class="info-box">-->
    <!--                            <h5>Robert Smith</h5>-->
    <!--                            <span class="designation">Senior Manager</span>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="thumb-item">-->
    <!--                        <figure class="thumb-box"><img src="<?= Yii::$app->params['basepath'];?>/images/testimonial/testimonial-3.png" alt=""></figure>-->
    <!--                        <div class="info-box">-->
    <!--                            <h5>Christine Eva</h5>-->
    <!--                            <span class="designation">Senior Manager</span>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                    <div class="thumb-item">-->
    <!--                        <figure class="thumb-box"><img src="<?= Yii::$app->params['basepath'];?>/images/testimonial/testimonial-3.png" alt=""></figure>-->
    <!--                        <div class="info-box">-->
    <!--                            <h5>Jhon Doe</h5>-->
    <!--                            <span class="designation">Senior Manager</span>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->

    <!--<section class="fun-fact centred">-->
    <!--    <div class="auto-container">-->
    <!--        <div class="row clearfix">-->
    <!--            <div class="col-lg-3 col-md-6 col-sm-12 counter-column">-->
    <!--                <div class="counter-block-one wow slideInUp animated" data-wow-delay="00ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: slideInUp;">-->
    <!--                    <div class="count-outer count-box counted">-->
    <!--                        <span class="count-text" data-speed="1500" data-stop="254">254</span>-->
    <!--                    </div>-->
    <!--                    <p>Expert Consultants</p>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--            <div class="col-lg-3 col-md-6 col-sm-12 counter-column">-->
    <!--                <div class="counter-block-one wow slideInUp animated" data-wow-delay="00ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: slideInUp;">-->
    <!--                    <div class="count-outer count-box counted">-->
    <!--                        <span class="count-text" data-speed="1500" data-stop="930">930</span>-->
    <!--                    </div>-->
    <!--                    <p>Our Trusted Clients</p>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--            <div class="col-lg-3 col-md-6 col-sm-12 counter-column">-->
    <!--                <div class="counter-block-one wow slideInUp animated" data-wow-delay="00ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: slideInUp;">-->
    <!--                    <div class="count-outer count-box counted">-->
    <!--                        <span class="count-text" data-speed="1500" data-stop="826">826</span>-->
    <!--                    </div>-->
    <!--                    <p>Orders in Queue</p>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--            <div class="col-lg-3 col-md-6 col-sm-12 counter-column">-->
    <!--                <div class="counter-block-one wow slideInUp animated" data-wow-delay="00ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: slideInUp;">-->
    <!--                    <div class="count-outer count-box counted">-->
    <!--                        <span class="count-text" data-speed="1500" data-stop="720">720</span>-->
    <!--                    </div>-->
    <!--                    <p>Projects Delivered</p>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->
   
    
</div>
