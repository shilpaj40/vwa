<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Edit Profile';
?>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>

<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">
      <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<div class="site-login">
 
        
    <!-- Login -->
  <main id="page-main" role="main">
     <div class="container">
        <a id="main-content" tabindex="-1"></a>
        <div class="row">
           <div class="col-lg-12">
              <div id="page-content">
                 <div class="content">
                    <div>
                       <div id="block-shelley-local-tasks" class="block">
                          <ul class="nav nav-tabs my-3">
                             <li class="nav-item"><a href="<?= \yii\helpers\Url::to(['site/profile', "id"=>Yii::$app->user->id])?>" class="nav-link ">View Profile<span class="visually-hidden"></span></a></li>
                            <!--  <li class="nav-item"><a href="/user/69/payment-methods" class="nav-link">Payment methods</a></li> -->
                             <li class="nav-item"><a href="<?= \yii\helpers\Url::to(['site/edit-profile', "id"=>Yii::$app->user->id])?>" class="nav-link active" >Edit Profile</a></li>
                             
                          </ul>
                       </div>
                       <div id="block-pagetitle" class="block">
                           <?php $form = ActiveForm::begin(['id' => 'signup-form','enableClientScript' => false]); ?> 

                           <div class="row">

                            <div class="col-md-8">
                               <div id="message"></div>
                               <div class="g-pa-50 ">
                                  <!-- Form -->
                                    <form class="g-py-15">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-1">
                                                  <?= $form->field($model, 'first_name', ['template' => '
                                                             <div class="row"><div class="col-md-12">{label}</div></div>
                                                             <div class="row">
                                                                 <div class="col-sm-12">
                                                                     <div class="input-group">
                                                                        
                                                                        {input}
                                                                     </div>
                                                                     {error}{hint}
                                                                 </div>
                                                              </div>'])->textInput(['class'=>'form-control g-color-black g-brd-left-none g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-pl-0 g-pr-15 g-py-15']) ?>
                                                  <p class="help-block help-block-error" id="name-error"></p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-1">
                                                  <?= $form->field($model, 'last_name', ['template' => '
                                                             <div class="row"><div class="col-md-12">{label}</div></div>
                                                             <div class="row">
                                                                 <div class="col-sm-12">
                                                                     <div class="input-group">
                                                                        
                                                                        {input}
                                                                     </div>
                                                                     {error}{hint}
                                                                 </div>
                                                              </div>'])->textInput(['class'=>'form-control g-color-black g-brd-left-none g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-pl-0 g-pr-15 g-py-15']) ?>
                                                  <p class="help-block help-block-error" id="name-error"></p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-1">
                                                    <?= $form->field($model, 'last_name', ['template' => '
                                                             <div class="row"><div class="col-md-12">{label}</div></div>
                                                             <div class="row">
                                                                 <div class="col-sm-12">
                                                                     <div class="input-group">
                                                                        
                                                                        {input}
                                                                     </div>
                                                                     {error}{hint}
                                                                 </div>
                                                              </div>'])->textInput(['class'=>'form-control g-color-black g-brd-left-none g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-pl-0 g-pr-15 g-py-15']) ?>
                                                  
                                                    <p class="help-block help-block-error" id="email-error"></p>

                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-1">
                                                  <?php echo $form->field($model, 'mobile', ['template' => '
                                                         <div class="row"><div class="col-md-12">{label}</div></div>
                                                         <div class="row">
                                                             <div class="col-sm-12">
                                                                 <div class="input-group">
                                                                    
                                                                    {input}
                                                                 </div>
                                                                 {error}{hint}
                                                             </div>
                                                          </div>'])->textInput(['class'=>'form-control g-color-black g-brd-left-none g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-pl-0 g-pr-15 g-py-15', 'readonly'=>true]) ?>
                                                    <p class="help-block help-block-error" id="email-error"></p>

                                                </div>
                                            </div>
                                             <div class="col-md-6">
                                                <div class="mb-1">
                                                  <?php echo $form->field($model, 'city', ['template' => '
                                                         <div class="row"><div class="col-md-12">{label}</div></div>
                                                         <div class="row">
                                                             <div class="col-sm-12">
                                                                 <div class="input-group">
                                                                    
                                                                    {input}
                                                                 </div>
                                                                 {error}{hint}
                                                             </div>
                                                          </div>'])->textInput(['class'=>'form-control g-color-black g-brd-left-none g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-pl-0 g-pr-15 g-py-15', 'readonly'=>true]) ?>
                                                    <p class="help-block help-block-error" id="email-error"></p>

                                                </div>
                                            </div>
                                          </div>
                                            
                                        </div>
                                        
                                        </div>
                                     
                                      
                                      


                                      <div class="g-mb-50">
                                        <button class="btn btn-md  u-btn-primary rounded text-uppercase g-py-10" type="button" onclick="return validate()">Save</button>
                                      </div>


                                    </form>
                                  <!-- End Form -->
                               </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                       </div>
                       
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
  </main>
   
       
    <!-- End Login -->
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<style type="text/css">
  .form-group {
        width: 100%;
  }
</style>
<script type="text/javascript">
   jQuery(function($) { // make a local $ , not a global that gets overridden by Someone’s script
            $( "#users-birth_date" ).datepicker({
               
                dateFormat: 'dd-mm-yy',
                onClose: function(date) { 
                if(date !="") {
                    getsign(date);
                    } 
                }
            });
        });
function getsign(ele)
{
    var dateAr = ele.split('-');
    var month= dateAr[1];
    var day=dateAr[0];
    $.ajax({
            type:'POST',
            data:{'day': day, 'month': month},
            url:"<?php echo yii\helpers\Url::to(['site/getsign']) ?>",
            success:function(data)
            {
                $("#users-starsign").val(data);
               // alert(data);
            }
        });
    return false;
}
function validate()
{
  var name=$("#users-first_name").val();
  var email=$("#users-email").val();
  var birth_date=$("#users-birth_date").val();
  var starsign=$("#users-starsign").val();
  var username=$("#users-username").val();

  //Empty errors
  $("#name-error").html("") ;
  $("#email-error").html("") ;
  $("#birth_date-error").html("") ;
  
  $("#starsign-error").html("") ;

  //Remove class
  $('#users-first_name').removeClass('error');  
  $('#users-email').removeClass('error');  
  $('#users-birth_date').removeClass('error');  
  $('#users-starsign').removeClass('error');  


  var bVal=true;
  if(name=="")
  {
    bVal=false;
    $('#users-name').addClass('error');  
    $("#name-error").html("Name cannot be blank") ;
  }
  if(email=="")
  {
    bVal=false;
    $('#users-email').addClass('error');  
    $("#email-error").html("Email cannot be blank") ;
  }
  
  if(birth_date=="" )
  {
    bVal=false;
    $('#users-birth_date').addClass('error');  
    $("#birth_date-error").html("Birth date cannot be blank") ;
  }
  if( birth_date=='0000-00-00')
  {
    bVal=false;
    $('#users-birth_date').addClass('error');  
    $("#birth_date-error").html("Birth date is invalid") ;
  }
  if(bVal==true)
  {
    
     $.ajax({
            type:'POST',
            data:{'id':<?=$model->id?>,'name': name, 'email': email, 'birth_date':birth_date, 'starsign':starsign, 'username':username },
            url:"<?php echo yii\helpers\Url::to(['site/profile-edit']) ?>",
            success: function(result) {
                json = JSON.parse(result);
                if(json.status=='success'){
                  Swal.fire({
                  icon: 'success',
                  title: 'Profile Updated',
                  text: json.message,
                 // footer: '<a href>Why do I have this issue?</a>'
                }).then(function() {
                    window.location = json.url;
                });
              }else{
                        Swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: json.message,
                 // footer: '<a href>Why do I have this issue?</a>'
                })
                        
              }
            
            },
            error: function(result){
              console.log(result);
              alert('Something went wrong');
            }
        });
  }
  return bVal;

}
</script>