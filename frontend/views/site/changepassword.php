<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Change Password';
?>
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>

<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">
      <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<div class="site-login">
 
        
    <!-- Login -->
  <main id="page-main" role="main">
     <div class="container">
        <a id="main-content" tabindex="-1"></a>
        <div class="row">
           <div class="col-lg-12">
              <div id="page-content">
                 <div class="content">
                    <div>
                       <div id="block-shelley-local-tasks" class="block">
                          <ul class="nav nav-tabs my-3">
                             <li class="nav-item"><a href="<?= \yii\helpers\Url::to(['site/profile', "id"=>Yii::$app->user->id])?>" class="nav-link active">View Profile<span class="visually-hidden"></span></a></li>
                            <!--  <li class="nav-item"><a href="/user/69/payment-methods" class="nav-link">Payment methods</a></li> -->
                             <li class="nav-item"><a href="<?= \yii\helpers\Url::to(['site/edit-profile', "id"=>Yii::$app->user->id])?>" class="nav-link" >Edit Profile</a></li>
                          </ul>
                       </div>
                       <div id="block-pagetitle " class="block mt-3 mb-3">
                        <?php if (Yii::$app->session->hasFlash('success')): ?>
                            <div class="alert alert-success alert-dismissable mt-3">
                                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                 <h4><i class="icon fa fa-check"></i>Saved!</h4>
                                 <?= Yii::$app->session->getFlash('success') ?>
                            </div>
                        <?php endif; ?>

                        <?php if (Yii::$app->session->hasFlash('error')): ?>
                            <div class="alert alert-danger alert-dismissable mt-3">
                                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                 <h4><i class="icon fa fa-close"></i>Error!</h4>
                                 <?= Yii::$app->session->getFlash('error') ?>
                            </div>
                        <?php endif; ?>
                          <?php $form = ActiveForm::begin([
                            'id'=>'changepassword-form',
                            'options'=>['class'=>'form-horizontal'],
                            'fieldConfig'=>[
                                'template'=>"{label}\n<div class=\"col-lg-4\">
                                            {input}</div>\n<div class=\"col-lg-5\">
                                            {error}</div>",
                                'labelOptions'=>['class'=>'col-lg-3 control-label'],
                            ],
                        ]); ?>
                            <?= $form->field($model,'oldpass',['inputOptions'=>[
                                'placeholder'=>'Old Password'
                            ]])->passwordInput() ?>
                           
                            <?= $form->field($model,'newpass',['inputOptions'=>[
                                'placeholder'=>'New Password'
                            ]])->passwordInput() ?>
                           
                            <?= $form->field($model,'repeatnewpass',['inputOptions'=>[
                                'placeholder'=>'Repeat New Password'
                            ]])->passwordInput() ?>
                           
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-11">
                                    <?= Html::submitButton('Change password',[
                                        'class'=>'btn btn-primary'
                                    ]) ?>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                       </div>
                       
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
  </main>
   
       
    <!-- End Login -->
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<style type="text/css">
  .form-group {
        width: 100%;
  }
</style>
