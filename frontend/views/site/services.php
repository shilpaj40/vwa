
<!--Page Title-->
<?php
Yii::$app->metaTags->register($model);
 if($model->banner->banner_image!="")
            {
                 echo "<section class='page-title centred' style='background-image: url(".$model->banner->banner_image.");'>";
            }
            else
            {
                echo "<section class='page-title centred' style='background-image: url(<?= Yii::$app->params['basepath'];?>/images/bg/page-title-2.jpg);'>";
            }
         ?>
    <div class="auto-container">
        <div class="content-box clearfix">
            <h1><?php echo $model->title; ?></h1>
            <!--<ul class="bread-crumb clearfix">-->
            <!--    <li><a href="">Home</a></li>-->
            <!--    <li>Services</li>-->
            <!--</ul>-->
        </div>
    </div>
</section>
<!--End Page Title-->
<!-- service-section -->

<section class="service-section">
    <div class="auto-container">
        <div class="sec-title centred">
            <h5>WHAT WE PROVIDE</h5>
        </div>
    </div>
    <div class="auto-container">
        <div class="sec-title centred">
           <!--  <p>You may not always have the time to curate and plan your wealth. VWA will help make your family wealth planning easier by chalking out solutions that will help you achieve your financial goals. VWA does not manage your money, your money will always be in your control. We invest experience, you invest money. An hour-and-a-half every weekend is what we want our investors to invest with us to reap maximum benefits. And once our informed investors get confident, they will better evolve to a balanced portfolio and truly paint their own masterpiece. VWA sincerely hopes to become contributors to its investor's wealth creation process.</p> -->

           <?php echo $model->body; ?>
        </div>
    </div>
</section>
<section class="service-style-five bg-color-1 pb-50 pt-50" id="content_block_four">
    <div class="auto-container content-box " >
        <div class="sec-title style-four right">
            <h5>What we offer</h5>
            <!-- <h2>Quality Services With Difference</h2> -->
        </div>
        <div class="tabs-content">
            <div class="row ulstyle">
                <?php echo $model->what_we_offer; ?>
            </div>
        </div>
    </div>
</section>
<section class="pricing-section " style="background-image: url(<?= Yii::$app->params['basepath'];?>/images/pricing-bg.jpg);">
    <div class="auto-container">
        <div class="tabs-box centred">
            <!--<div class="sec-title style-four right">-->
            <!--    <h5>What we offer</h5>-->
            <!--    <h2>Quality Services With Difference</h2>-->
            <!--</div>-->
            <div class="tabs-content">
                <div class="tab active-tab" id="tab-1">
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-6 col-sm-12 pricing-block col-md-offset-2">
                            <div class="pricing-block-one">
                                <div class="pricing-table">
                                    <div class="table-header">
                                        <h3>Free Trial</h3>
                                        <!--<div class="price-box">-->
                                            <!-- <span>recommended</span> -->
                                        <!--    <h2>Free for lifetime</h2><br/>-->
                                        <!--</div>-->
                                        <!-- <span class="text">This Plan Includes Global Relations</span> -->
                                    </div>
                                    <div class="table-content">
                                        <ul class="clearfix"> 
                                           <?php echo $model->feee_trial; ?>
                                        </ul>
                                    </div>
                                    <div class="theme-btn style-three">
                                        <a style="color:white" href="<?= \yii\helpers\Url::to(['site/signup'])?>">Signup</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 pricing-block">
                            <div class="pricing-block-one">
                                <div class="pricing-table">
                                    <div class="table-header">
                                        <h3>Premium</h3>
                                        <!--<div class="price-box">-->
                                        <!--    <span>recommended</span>-->
                                        <!--    <h2>₹ 400</h2>-->
                                        <!--    <p>/ Month</p>-->
                                        <!--</div>-->
                                    </div>
                                    <div class="table-content">
                                        <ul class="clearfix"> 
                                            <?php echo $model->premium; ?>
                                        </ul>
                                    </div>
                                    <div class="theme-btn style-three">
                                        
                                        <a style="color:white" href="<?= \yii\helpers\Url::to(['site/subscription'])?>">Subscribe</a>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cta-section">
        <div class="pattern-layer" style="background-image: url(<?= Yii::$app->params['basepath'];?>//images/pattern/shape-3.png);"></div>
        <div class="auto-container">
            <div class="inner-container clearfix">
                <div class="title pull-left col-md-12">
                    <h4 style="color:#fff"><?php echo $model->membership_text; ?></h4>
                </div>
                <!--<div class="btn-box pull-right col-md-4">-->
                <!--    <a href="">Signup</a>-->
                <!--    <a>Pre-book</a>-->
                <!--</div>-->
            </div>
        </div>
    </section>

</section>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
   $(document).ready(function(){
  
    $("li#services").addClass("current");

});
</script>

<!--  -->

