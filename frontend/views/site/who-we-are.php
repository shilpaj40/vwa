<!--Page Title--> 
<?php
Yii::$app->metaTags->register($model);
 if($model->banner->banner_image!="")
            {
                 echo "<section class='page-title centred' style='background-image: url(".$model->banner->banner_image.");'>";
            }
            else
            {
                echo "<section class='page-title centred' style='background-image: url(<?= Yii::$app->params['basepath'];?>/images/bg/page-title-2.jpg);'>";
            }
         ?>

    <div class="auto-container">
        <div class="content-box clearfix">
            <h1>About Us</h1>
            <!--<ul class="bread-crumb clearfix">-->
            <!--    <li><a href="">Home</a></li>-->
            <!--    <li>About Us</li>-->
            <!--</ul>-->
        </div>
    </div>
</section>
<!--End Page Title-->
<!-- service-section -->

<section class="service-style-one about-section pb-0">
    <div class="auto-container">
        <div class="sec-title centred">
            <h5>About Us</h5>
            <!--<h2>A financial doctor a day keeps market risks at bay!</h2>-->
        </div>

        <div class="row clearfix">
            <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                <div id="content_block_13">
                    <div class="content-box">
                        
                        <div class="text">
                           <?php echo $model->body; ?>                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                <div id="image_block_four">
                    <div class="image-box">
                        <?php if($model->image!=""){
                        echo "<figure class='image wow slideInRight animated animated animated' data-wow-delay='00ms' data-wow-duration='1500ms' style='visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: slideInRight;''><img src=".$model->image." ></figure>";
                        } else { ?>                         
                        <figure class="image wow slideInRight animated animated animated" data-wow-delay="00ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: slideInRight;"><img src="<?= Yii::$app->params['basepath'];?>images/about-3.jpg" alt=""></figure>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-md-12">
                <div class="service-details-content">
                    <div class="content-style-one mr-0">
                        <div class="sec-title left pb-0 mr-0">
                            <h5 class="pb-0 mr-0">Our Role</h5>
                        </div>
                        <?php echo $model->our_role; ?>  
                        </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="service-details-content">
                    <div class="content-style-one mr-0">
                        <div class="sec-title left pb-0 mr-0">
                            <h5 class="pb-0 mr-0">What we are not?</h5>
                        </div>                        
                        <?php echo $model->why_we_are; ?>  
                    </div>
                </div>
            </div>
        </div>              
        
    </div>
</section>
<section class="feature-style-three pt-50 pb-0" style="padding-bottom:0">
    <div class="fluid-container">
        <div class="inner-content clearfix">
            <div class="feature-block-three">
                <div class="inner-box">
                    <div class="hidden-icon"><i class="flaticon-search"></i></div>
                    <div class="inner">
                        <div class="icon-box"><i class="flaticon-search"></i></div>
                        <h3>Our Objective</h3>
                        <?php echo "<p>".$model->objective."</p>"; ?>  
                    </div>
                </div>
            </div>
            <div class="feature-block-three">
                <div class="inner-box">
                    <div class="hidden-icon"><i class="flaticon-search"></i></div>
                    <div class="inner">
                        <div class="icon-box"><i class="flaticon-search"></i></div>
                        <h3>Our Vision</h3>
                       <?php echo "<p>".$model->vision."</p>"; ?>  
                    </div>
                </div>
            </div>
            <div class="feature-block-three">
                <div class="inner-box">
                    <div class="hidden-icon"><i class="flaticon-claim"></i></div>
                    <div class="inner">
                        <div class="icon-box"><i class="flaticon-claim"></i></div>
                        <h3>Our Assurance</h3>
                        <?php echo "<p>".$model->assurance."</p>"; ?>  
                    </div>
                </div>
            </div>
           
        </div>
    </div>
</section>
<section class="service-style-two bg-color-2 cta-style-two">
        <div class="pattern-layer"> <figure class="image-box"><img src="<?= Yii::$app->params['basepath'];?>/images/shape-21.png" alt=""></figure></div>
        <div class="auto-container">
            <div class="inner-box clearfix">
                <div class="sec-title light pull-left mb-0">
                    
                    <h2>90 days Free offer!</h2>
                    <!--<h4 class="text-white">Benefit from VWA's algorithm-based stock investment advisory.</h4>-->
                    <ul class="clearfix">
                    <li><p class="text-white"><i class="fa fa-check"></i> Weekly Buy/Sell Dynamic Advice</p></li>
                     <li><p class="text-white"><i class="fa fa-check"></i> Access to Educational Material</p></li>
                    </ul>
                </div>
                <div class="btn-box pull-right">
                    <a href="<?= \yii\helpers\Url::to(['site/signup'])?>" class="theme-btn style-two">Join Now</a>
                </div>
            </div>
        </div>
    </section>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
   $(document).ready(function(){
  
    $("li#about_us").addClass("current");

});
</script>


