<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
//https://affixtheme.com/html/xmee/demo/register-4.html
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

?>

<div class="site-login">
  <main id="page-main" role="main">
    <section class="contact-section" style="background-image: url(<?= Yii::$app->params['basepath'];?>/images/contact-1.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-xl-6 col-lg-7 col-md-7 col-md-offset-3 content-column">
                    <div id="content_block_nine">
                        <div class="content-box bg-color-white">
                            <div class="sec-title style-three left">
                                <h5>focus on work</h5>
                                <h2>Let's Talk About Growing Your Business Always...</h2>
                            </div>
                           
                            <div class="form-inner">
                              <?php if (Yii::$app->session->hasFlash('success')): ?>
                                <div class="alert alert-success alert-dismissable">
                                     <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                     <?= Yii::$app->session->getFlash('success') ?>
                                </div>
                            <?php endif; ?>

                            <?php if (Yii::$app->session->hasFlash('error')): ?>
                                <div class="alert alert-danger alert-dismissable">
                                     <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                     <h4><i class="icon fa fa-close"></i>Error!</h4>
                                     <?= Yii::$app->session->getFlash('error') ?>
                                </div>
                            <?php endif; ?>
                            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                             <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= $form->field($model, 'first_name', ['template' => '
                                                       <div class="row"><div class="col-md-12">{label}</div></div>
                                                       <div class="row">
                                                           <div class="col-sm-12">
                                                               <div class="input-group">
                                                                  
                                                                  {input}
                                                               </div>
                                                               {error}{hint}
                                                           </div>
                                                        </div>'])->textInput(['class'=>'form-control ']) ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= $form->field($model, 'last_name', ['template' => '
                                                       <div class="row"><div class="col-md-12">{label}</div></div>
                                                       <div class="row">
                                                           <div class="col-sm-12">
                                                               <div class="input-group">
                                                                  
                                                                  {input}
                                                               </div>
                                                               {error}{hint}
                                                           </div>
                                                        </div>'])->textInput(['class'=>'form-control ']) ?>
                                    </div>
                                </div>
                             </div>
                              <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                      
                                       <?= $form->field($model, 'email', ['template' => '
                                                       <div class="row"><div class="col-md-12">{label}</div></div>
                                                       <div class="row">
                                                           <div class="col-sm-12">
                                                               <div class="input-group">
                                                                  
                                                                  {input}
                                                               </div>
                                                               {error}{hint}
                                                           </div>
                                                        </div>'])->input('email') ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <?= $form->field($model, 'mobile', ['template' => '
                                       <div class="row"><div class="col-md-12">{label}</div></div>
                                       <div class="row">
                                           <div class="col-sm-12">
                                               <div class="input-group">
                                                  
                                                  {input}
                                               </div>
                                               {error}{hint}
                                           </div>
                                        </div>'])->input('number') ?>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                   <?= $form->field($model, 'password')->passwordInput(['autofocus' => true, 'class'=>'form-control']) ?>
                                   
                                            <i toggle="#password" class="fa fa-fw toggle-pwdpassword field-icon fa-eye"></i>
                                    <!-- <div id="pswmeter" class="mt-1"></div>
                                    <div id="pswmeter-message" class="mt-1"></div> -->
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <?= $form->field($model, 'confirm_password')->passwordInput(['class'=>'form-control']) ?>
                                    <i toggle="#password" class="fa fa-fw toggle-cnfpassword field-icon fa-eye"></i>
                                  </div>
                                </div>
                              </div>
                              
                              
                              <div class="form-group">
                                <!-- <button type="submit" class="fxt-btn-fill">Log in</button> -->
                                <?= Html::submitButton('Register', ['class' => 'theme-btn style-three', 'name' => 'login-button']) ?>
                              </div>
                            <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
  </main>

</div>
<style type="text/css">
  .fxt-template-layout4 .fxt-content {
    padding: 15px;}
  .fxt-template-layout4 .fxt-form {
    margin-top: 0;
  }
  .fxt-template-layout4 .fxt-bg-wrap {
    padding: 15vh 0 8vh 16vw;}
</style>
