<?php
namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\session;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use app\models\PasswordForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use yii\db\Query;
use yii\helpers\Json;
use yii\web\Response;
use yii\helpers\Url;
use app\models\User;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    

                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    
    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $login = new \app\models\LoginDetails();

        if ($model->load(Yii::$app->request->post())) {
          $log = \app\models\User::find()->where(['username' => $_POST['LoginForm']['username'], 'status' => 1, 'user_type'=>2])->one();
           // print_r($_POST['LoginForm']['username']);die;
            if(empty($log)) {
                 \Yii::$app->session->setFlash('error', '<i class="fa fa-warning"></i><b> Incorrect username or password. !</b>');
                return $this->render('login', ['model' => $model]);
            }
            $login->login_user_id = $log['id'];
            $login->login_status = 1;
            $login->login_at = new \yii\db\Expression('NOW()');
            $login->user_ip_address=$_SERVER['REMOTE_ADDR'];
            $login->save(false);
            if($model->login()) {
                return $this->redirect(['stocks']);
            }
            else
            {
               \Yii::$app->session->setFlash('error', '<i class="fa fa-warning"></i><b> Incorrect username or password. !</b>');
                return $this->render('login', ['model' => $model,]);
            }
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        $log = \app\models\LoginDetails::find()->where(['login_user_id' =>Yii::$app->user->id, 'login_status' => 1])->orderBy(['login_detail_id'=>SORT_DESC])->one();
        if(!empty($log))
        {
          $log->login_status=0;
          $log->logout_at=new \yii\db\Expression('NOW()');
          $log->save(false);

        }
        
        Yii::$app->user->logout();

        return $this->goHome();
    }

    
    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) ) {
         
            $user = \app\models\User::find()->where(['username' => $_POST['SignupForm']['email'], 'status' => 1, 'user_type'=>2])->count();
            if($user>0)
            {
              Yii::$app->session->setFlash('error', 'Email already exist. Please try another one.');
            }
            else
            {
              //print_r($_POST);die;
              $user = new User();
              $user->username = $_POST['SignupForm']['email'];
              $user->first_name=$_POST['SignupForm']['first_name'];
              $user->last_name = $_POST['SignupForm']['last_name'];
              $user->email = $_POST['SignupForm']['email'];
              $user->mobile=$_POST['SignupForm']['mobile'];
              $user->password = sha1($_POST['SignupForm']['password']);
              $user->user_type=2;
              $user->subscription_start_date=date('Y-m-d');
              $user->subscription_end_date=date('Y-m-d', strtotime('+3 months'));
              $user->is_paid_user=0;
              if($user->save()){
                  
                Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
                return $this->redirect(['login']);
              }
            }
         // return $this->redirect(['login']);   
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    
    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
  public function actionResendVerificationEmail()
  {
      $model = new ResendVerificationEmailForm();
      if ($model->load(Yii::$app->request->post()) && $model->validate()) {
          if ($model->sendEmail()) {
              Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
              return $this->goHome();
          }
          Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
      }

      return $this->render('resendVerificationEmail', [
          'model' => $model
      ]);
  }
  
  
  public function actionChangepassword(){

        $model = new PasswordForm;
        $modeluser = \app\models\User::find()->where([
            'username'=>Yii::$app->user->identity->username
        ])->one();
        
        if($model->load(Yii::$app->request->post())){
            if($model->validate()){
                try{
                    $modeluser->password = sha1($_POST['PasswordForm']['newpass']);
                    if($modeluser->save()){
                         require '../../vendor/autoload.php'; 
                        $email = new \SendGrid\Mail\Mail(); 
                        $content='<div style="font:18px/23px Arial,Helvetica,sans-serif;background:#f3f3f3">
                 <div style="max-width:800px;margin-left:auto;margin-right:auto">
                    <div style="width:640px;margin-left:auto;margin-right:auto;padding:20px 0px 20px 0px; style="text-align: center;">
                        <table class="x_-665217761MsoNormalTable" border="1" cellspacing="3" cellpadding="0" style="border: solid rgb(204,204,204) 1.0pt;background: #fff;color: #000;width:98%">
                      <tbody>
                        <tr>
                          <td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                          <h2 align="center" style="text-align: center;font-family: Helvetica;">Change Password</h2>
                          <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">Your password for Shellley von Strunckel changed successfully. New login details: </span></p>                        
                          
                          </td>
                        </tr>
                       
                        <tr>
                          <td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                           <p class="MsoNormal" align="center" style="text-align: left;font-family: Helvetica;"><span style="font-size: 10.0pt;"><b>Username: </b>'.$modeluser->username.'</span></p>
                          </td>
                        </tr>
                         <tr>
                          <td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                           <p class="MsoNormal" align="center" style="text-align: left;font-family: Helvetica;"><span style="font-size: 10.0pt;"><b>Password: </b>'.$_POST['PasswordForm']['newpass'].'</span></p>
                          </td>
                        </tr>
                        <tr>
                          <td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                          <p class="MsoNormal" align="center" style="font-family: Helvetica;text-align: center;"><span style="font-size: 10.0pt;font-family: Helvetica;">Best Wishes,<br>
                          Team @<span class="x_-665217761apple-converted-space">&nbsp;</span><a href="https://u18735128.ct.sendgrid.net/ls/click?upn=lIH7flm-2BKJt5lUWrC1oQcvZs2VYe4R2Mf-2FEBv77wcynxaA5uFbxPT56VSN7ewHcSPfZuzdeYmnZpG2GsxVISqaExaOwfIM9QS94PxDD3fAuuf3O0oir-2Fj3zcJ5DG0-2Fwv-2FyBlqQJpU5SJbowKbLml8yz1osim7LSUgtJ8rEoeydkMJQZJU8txRWWqzyM9-2BEdkmzeOjW4qzsVbX1URMGf1pbMqg-2FAAEdY-2B7ISxcg4mSNHBsIUoJtYtlxB1LrxinnDXaJJzOAKuOU-2Ff-2B7h-2BKCWXE6PevVa-2BjWSuRI0eZVFLsA3FGe3MI8lEIKY6-2F-2FiIczgnNhn2TIz-2BsSvELpu-2BVanEksxKKzPMKVjDG3wzrSD9OW2SxNr8Ok9YtHYanhqNEBRJv06ADnUwMajP12Hmn3rwGUcuTlG-2Bgcuqf-2Bs7IQpKK-2B-2F02FYS0pRiaQFZR-2BDMdis7lwqtsjzIG-2BrHuSQW6bHn36H7tfeiWxxigWs9ik5LRTacBn8tLoUOdSBsOGph-2FkAg7RBV_yH1DaoHSZ7eAK-2FFWkvBK6ErrWr7xRlpQNqtWWxgD4E8WHpR35vN3elnDcb48iRBvW6c99H-2Bj9N0lO2qylWIBH2EjUQfwvG-2FoyzmFE1DZ3Lms9ruJdKa2ZmVeMkprx-2BeeC7EArf-2B2rQY6hHyUb21ktQM4Vl-2BDmQbRPvJ-2Bn5f7pq-2BdO-2FNrC8ijGEAFpZM6RrDwpx8c68ykFcy4pDeArMX9hL07OYC8-2Bv-2FVslYSp4jIG3E-3D" target="_blank">ShelleyvonStrunckel.com</a></span></p>
                          <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">Instagram:
                          <span class="x_-665217761d2edcug0"><span style="color: rgb(28,30,33);font-family: Helvetica;"><a href="https://u18735128.ct.sendgrid.net/ls/click?upn=lIH7flm-2BKJt5lUWrC1oQcoS1JJg5tZaLfz2hEd9rBUKnIU1EVgG48FN514tSKFiBgSzgaJxmMVoo0i14hnO9t-2BEQ6bhKKkrh8-2Fx4ryeJXRg-2F-2BI6Fs-2BrXIrgxAhk14nDMTQJ4QZyEB45mY5g4walXuj05p4V6LylKcABTapu6tOU-3Dr9Ur_yH1DaoHSZ7eAK-2FFWkvBK6ErrWr7xRlpQNqtWWxgD4E8WHpR35vN3elnDcb48iRBv9CTnDL2SrM3X-2FDZc7yNSzelvq5YBD43B1t-2FGHptJl-2BreKmZJQYfXqNRF2qD1epl6E8y-2BrNNf9RJFONtv265E3lK6GDBot4Gm-2Bmqar273kelIq48l-2BXNvxRPzMkHCRVwMuGD0bBBnvqJCZlBVy1vJrwFkOyXvGqfBmQZc6sfzqtk-3D" target="_blank"><span style="border: none windowtext 1.0pt;padding: 0.0cm;">vonstrunckel</span></a></span></span><span style="color: rgb(28,30,33);"></span></span></p>
                          <h1 align="center" style="margin: 0.0cm;font-family: Helvetica;text-align: center;"><span style="font-size: 10.0pt;font-weight: normal;">Facebook:<span style="color: rgb(5,5,5);"> ShelleyvonStrunckel</span></span></h1>
                          <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span class="x_-665217761d2edcug0"><span style="font-size: 10.0pt;color: rgb(28,30,33);"><a href="https://u18735128.ct.sendgrid.net/ls/click?upn=lIH7flm-2BKJt5lUWrC1oQck-2FfyP6xhDQe-2FhMR1aBw8M8tWXRSOVNoVv7dboXoMx-2FaUT1jKSJweidujLirVa0jH6IXKYO2Nc82-2BVgZ5RJEzBQZX5Ns8-2FkR-2Bw8G6sBEH4-2F6khzGNAlJmzE0-2FU9pRYMK-2BA-3D-3DgJ71_yH1DaoHSZ7eAK-2FFWkvBK6ErrWr7xRlpQNqtWWxgD4E8WHpR35vN3elnDcb48iRBvO5gVcJ1lXgrRjOoAY9TPG2jFTX9W6xJpEJJU-2BAIPt89hM2LjTYTyo1618pGCwU5TU7rljZFLAyEuAQF6UbtrktUg9bXopipL4zVqNvKTUykTXkg-2B1BWE1HeObepcuntoSCr97z1enk-2BOVFAtrJ5cdtYBMPUODJgGs7KdcRJKVTs-3D" target="_blank"><span style="color: rgb(28,30,33);text-decoration: none;">Twitter:
                          </span><span style="border: none windowtext 1.0pt;padding: 0.0cm;">vonStrunckel</span></a></span></span><span style="font-size: 10.0pt;color: rgb(28,30,33);"></span></p>
                          <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">&nbsp;</span></p>
                          </td>
                        </tr>
                      </tbody>
                    </table> 
                    </div>
                 </div>
              </div>';

                        $email->setFrom("pa@shelleyvonstrunckel.com","PA SVS");
                        $email->setSubject('Change Password for ShelleyVonStruckle.com' );
                        $email->addTo($modeluser->email);
                        $email->addContent(
                            "text/html", $content
                        );
                        $sendgrid = new \SendGrid(Yii::$app->params['SENDGRID_API_KEY']);
                        try {
                            $response = $sendgrid->send($email);
                            // $response= $response->statusCode() . "\n";
                            // $response=($response->headers());
                            // $response= $response->body() . "\n";
                        } catch (Exception $e) {
                            $response= 'Caught exception: '. $e->getMessage() ."\n";
                        }


                        Yii::$app->getSession()->setFlash(
                            'success','Password changed. Password sent on your mail'
                        );
                        return $this->redirect(['profile','id'=>$modeluser->id]);
                    }else{
                        Yii::$app->getSession()->setFlash(
                            'error','Password not changed'
                        );
                       // return $this->redirect(['index']);
                    }
                }catch(Exception $e){
                    Yii::$app->getSession()->setFlash(
                        'error',"{$e->getMessage()}"
                    );
                    return $this->render('changepassword',[
                        'model'=>$model
                    ]);
                }
            }else{
                return $this->render('changepassword',[
                    'model'=>$model
                ]);
            }
        }else{
            return $this->render('changepassword',[
                'model'=>$model
            ]);
        }
  }

  public function actionWhoWeAre()
  {
    return $this->render('who-we-are');

  }
  public function actionServices()
  {
    return $this->render('services');

  }
  public function actionFaqs()
  {
    return $this->render('faq');

  }
  public function actionContact()
  {
    return $this->render('contact');
  }
  public function actionPrivacyPolicy()
  {
    return $this->render('privacy-policy');
  }
  public function actionTermsAndCondition()
  {
    return $this->render('terms-and-condition');
  }
  public function actionResources()
  {
    //$model = \backend\models\Resources::find()->orderBy(['id'=>SORT_DESC])->all();
    return $this->render('resources');
  }
  public function actionStocks()
  {
    return $this->render('stock-calculator');
  }
}
