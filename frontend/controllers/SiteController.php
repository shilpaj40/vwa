<?php
namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\session;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use app\models\PasswordForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use yii\db\Query;
use yii\helpers\Json;
use yii\web\Response;
use yii\helpers\Url;
use app\models\User;
// use PHPMailer\PHPMailer\PHPMailer;
// use PHPMailer\PHPMailer\Exception;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    

                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $model=\backend\models\HomeBlocks::find()->orderBy(['id'=> SORT_DESC])->one();
        return $this->render('index',['model' => $model]);
    }
    
    
         public function actionSubscriptionRequest()
      {
      
     
      //print_r(Yii::$app->user->id);die;
      //echo $effectiveDate;die;
      if (Yii::$app->request->post() )
      {
        //print_r($_POST);die;
        $model=new \backend\models\Orders();

         
          $model->first_name=$_POST['firstname'];
          $model->last_name=$_POST['lastname'];
          $model->email=$_POST['email'];
          $model->phone=$_POST['phone'];
           $model->city=$_POST['city'];
           $model->amount=$_POST['amount'];
           $model->user_id=0;
           $model->created_date=date('Y-m-d H:i:s');
             $model->payment_status='0';
          $str=$_POST['subcription_id'];



          $subcription_id = (int) filter_var($str, FILTER_SANITIZE_NUMBER_INT);

      $model->subcription_id=$subcription_id;


       $sub = \backend\models\Subscriptions::find()->where(['id'=>$model->subcription_id])->one();
            //print_r($model);die;

            if($sub->one_month== $model->amount)
            {
               $model->subcription_plan=1;
            }else if($sub->twelve_month==$model->amount)
            {
                $model->subcription_plan=12;

            }else
            {
               $model->subcription_plan=3;

            }
     // echo $model->subcription_plan;die;

      $effectiveDate = date('Y-m-d H:i:s', strtotime("+".$model->subcription_plan." months", strtotime($model->created_date)));
            //echo "$effectiveDate";die;
      $model->payment_start_date=$model->created_date;
      $model->payment_end_date=$effectiveDate;
          //echo $int;die;
           $model->save(false);

           return $this->render('thankyou');

        
      } 

      return $this->render('subcription_request');
      }




    
    public function actionSuccess()
    {
      
        
            return $this->render('success');
         
    }
   
   public function actionFailure()
    {

      return $this->render('failure');
    }

    public function actionSubscription()
{
$subscription=\backend\models\Subscriptions::find()->where(['status'=>1])->orderBy(['script_per_week'=> SORT_ASC])->all();
$model=\backend\models\Services::find()->orderBy(['id'=> SORT_DESC])->one();

  // if (Yii::$app->request->post()) {
  //  // $one=$_POST['subscription']['1_month'];
  //  // $two=$_POST['subscription']['2_month'];
  //  // $two=$_POST['subscription']['2_month'];
    

  // }

return $this->render('subscription',['model' => $model,'subscription'=>$subscription]);

}

 

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $login = new \app\models\LoginDetails();

        if ($model->load(Yii::$app->request->post())) {
          $log = \app\models\User::find()->where(['username' => $_POST['LoginForm']['username'], 'status' => 1, 'user_type'=>2])->one();
           // print_r($_POST['LoginForm']['username']);die;
            if(empty($log)) {
                 \Yii::$app->session->setFlash('error', '<i class="fa fa-warning"></i><b> Incorrect username or password. !</b>');
                return $this->render('login', ['model' => $model]);
            }
            $login->login_user_id = $log['id'];
            $login->login_status = 1;
            $login->login_at = new \yii\db\Expression('NOW()');
            $login->user_ip_address=$_SERVER['REMOTE_ADDR'];
            $login->save(false);
            if($log->password===sha1($model->password))
            {
                $otp = random_int(1000, 9999);
                $log->otp = $otp;
                $log->save();
                
                require '../../vendor/autoload.php'; 
                    $email = new \SendGrid\Mail\Mail(); 
                    $content='<div style="font:18px/23px Arial,Helvetica,sans-serif;background:#f3f3f3">
                     <div style="max-width:800px;margin-left:auto;margin-right:auto">
                        <div style="width:640px;margin-left:auto;margin-right:auto;padding:20px 0px 20px 0px; style="text-align: center;">
                            <table class="x_-665217761MsoNormalTable" border="1" cellspacing="3" cellpadding="0" style="border: solid rgb(204,204,204) 1.0pt;background: #fff;color: #000;width:98%">
                          <tbody>
                            <tr>
                              <td style="border: none;padding: 2pt 22.5pt 2pt 22.5pt;">
                              <h2 align="center" style="text-align: center;font-family: Helvetica;">Welcome to vishwaswealthadvisory.com</h2>
                              <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">You are all set now.</span></p>
                              </td>
                            </tr>
                           
                            <tr>
                              <td style="border: none;padding: 2pt 22.5pt 2pt 22.5pt;">
                               <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">Use the following OTP to complete your Login procedures.</p>
                              </td>
                            </tr>
                             <tr>
                              <td style="border: none;padding: 2pt 22.5pt 2pt 22.5pt;">
                               <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;"><b>OTP - </b>'.$otp.'<b></p>
                              </td>
                            </tr>
                              <tr>
                              <td style="border: none;padding: 5pt 22.5pt 5pt 22.5pt;">
                               <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">Thank You</span></p>
                              </td>
                            </tr>
                           
                          </tbody>
                        </table> 
                        </div>
                     </div>
                  </div>';
                  $email->setFrom("info@vishwaswealthadvisory.com","Vishwas Wealth Advisory");
                $email->setSubject('Sign-in to Vishwas Wealth Advisory' );
                $email->addTo($log->email);
                 $adminEmail = \app\models\User::find()->select('email')->where(['status' => 1, 'user_type'=>1])->one();
                if($adminEmail->email!="")
                 $email->addCc($adminEmail->email);
                $email->addContent(
                    "text/html", $content
                );
                $sendgrid = new \SendGrid(Yii::$app->params['SENDGRID_API_KEY']);
                try {
                    $response = $sendgrid->send($email);
                    // $response= $response->statusCode() . "\n";
                    // $response=($response->headers());
                    // $response= $response->body() . "\n";
                } catch (Exception $e) {
                    $response= 'Caught exception: '. $e->getMessage() ."\n";
                }
                return $this->redirect(['loginotp','user'=>base64_encode(json_encode($model->attributes))]);
            }
            else{
                \Yii::$app->session->setFlash('error', '<i class="fa fa-warning"></i><b> Incorrect username or password. !</b>');
                return $this->render('login', ['model' => $model]);
            }
            /*if($model->login()) {
                
                //return $this->redirect(['stocks']);
            }
            else
            {
               \Yii::$app->session->setFlash('error', '<i class="fa fa-warning"></i><b> Incorrect username or password. !</b>');
                return $this->render('login', ['model' => $model]);
            }*/
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLoginotp($user)
    {
        $user = base64_decode($user);
        $user = json_decode($user,true);

        $model = new LoginForm;
        $model->username = $user["username"];
        $model->password = $user["password"];
        $model->rememberMe = $user["rememberMe"];
        If (\Yii::$app->request->isPost) {
            $log = \app\models\User::find()->where(['username' => $user['username'], 'status' => 1, 'user_type'=>2])->one();  
            if($log->otp==$_POST["LoginForm"]["otp"]){
                if($model->login()) {
                    return $this->redirect(['stocks']);
                }
            } else{
                $model->addError("otp","Please enter valid OTP");
            }        
        }

        return $this->render('login_otp', [
            'model' => $model,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        $log = \app\models\LoginDetails::find()->where(['login_user_id' =>Yii::$app->user->id, 'login_status' => 1])->orderBy(['login_detail_id'=>SORT_DESC])->one();
        if(!empty($log))
        {
          $log->login_status=0;
          $log->logout_at=new \yii\db\Expression('NOW()');
          $log->save(false);

        }
        
        Yii::$app->user->logout();

        return $this->goHome();
    }

    
    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        //print_r($_POST);die;
        if ($model->load(Yii::$app->request->post()) ) 
        {
          //$model->answer=$_REQEST['subscription']['answer'];
          //var_dump($request->post());die;
          
            $user = \app\models\User::find()->where(['username' => $_POST['SignupForm']['email'], 'status' => 1, 'user_type'=>2])->count();
              //$user = \app\models\User::find()->where(['username' => $_POST['SignupForm']['email'], 'status' => 1, 'user_type'=>2])->count();
            if($user>0)
            {
              Yii::$app->session->setFlash('error', 'Email already exist. Please try another one.');
            }
            else
            {
              /*$UserID = \app\models\User::find()->where(['status' => 1, 'user_type'=>2])->orderBy(['id'=>SORT_DESC])->one();                   
                $last_number= $UserID['customer_id'];
                $prefix = "VWA";
                $suffix = 1; 
                if($last_number!="" && $last_number!=null)
                {
                    $lastno= (explode("-",$last_number));
                    $suffix=$lastno[1];
                    $suffix++;
                
                }*/
                $UserCount = Yii::$app->db->createCommand('SELECT count(*) as count FROM user where MONTH(created_at)=MONTH(CURDATE()) and YEAR(created_at) = YEAR(CURDATE()) and status="1" and user_type="2"')
           ->queryOne();
           $UserCount = sprintf('%03d',($UserCount['count']+1)); 
                $alphachar = array_merge(range('A', 'Z'), range('a', 'z'));
                $upperArr = range('A', 'Z') ;                
                $fCharYear= $upperArr[(substr( date("y"), -2,1))-1];
                $sCharYear= $upperArr[(substr( date("y"), -1,1))-1];
                $yearQuarter = ceil(date("n") / 3);
                $month = ltrim(date("m"), '0'); 
                $monthCount = $upperArr[$yearQuarter-1];
            $customer_id = $fCharYear.$sCharYear.$monthCount.$month.$UserCount;
            //$prefix."-".sprintf("%04s",$suffix);
              $user = new User();
              $user->customer_id=$customer_id;
              $user->username = $_POST['SignupForm']['email'];
              $user->first_name=$_POST['SignupForm']['first_name'];
              $user->last_name = $_POST['SignupForm']['last_name'];
              $user->email = $_POST['SignupForm']['email'];
              $user->mobile=$_POST['SignupForm']['mobile'];
              $user->city=$_POST['SignupForm']['city'];
              //$user->password = sha1($_POST['SignupForm']['password']);
              $user->user_type=2;
              $user->subscription_start_date=date('Y-m-d');
              $user->created_at=date('Y-m-d H:i:s');
              $user->subscription_end_date=date('Y-m-d', strtotime('+3 months'));
              $user->is_paid_user=0;
              if($user->save()){
	              	require '../../vendor/autoload.php'; 
	              	$email = new \SendGrid\Mail\Mail(); 
	                $content='<div style="font:18px/23px Arial,Helvetica,sans-serif;background:#f3f3f3">
	                 <div style="max-width:800px;margin-left:auto;margin-right:auto">
	                    <div style="width:640px;margin-left:auto;margin-right:auto;padding:20px 0px 20px 0px; style="text-align: center;">
	                        <table class="x_-665217761MsoNormalTable" border="1" cellspacing="3" cellpadding="0" style="border: solid rgb(204,204,204) 1.0pt;background: #fff;color: #000;width:98%">
	                      <tbody>
	                        <tr>
                              <td style="border: none;padding: 2pt 22.5pt 2pt 22.5pt;">
                              <h2 align="center" style="text-align: center;font-family: Helvetica;">Welcome to vishwaswealthadvisory.com</h2>                             
                              </td>
                            </tr>
                          <tr>
                              <td style="border: none;padding: 2pt 22.5pt 2pt 22.5pt;">
                               <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">
                              Thank you for registering with us</p> </td></tr>
	                         <tr>
                              <td style="border: none;padding: 2pt 22.5pt 2pt 22.5pt;">
                               <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">
                               Your customer id is '.$customer_id.'
                               Please use the above customer id for further communication </p> </td></tr>

	                          <tr>
                              <td style="border: none;padding: 5pt 22.5pt 5pt 22.5pt;">
                               <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">Vishwas Soman <br>Thank You<br><a href="https://www.vishwaswealthadvisory.com/" target="_blank"> www.vishwaswealthadvisory.com</a></span></p>
                              </td>
                            </tr>  
	                       
	                      </tbody>
	                    </table> 
	                    </div>
	                 </div>
	              </div>';
	              $email->setFrom("info@vishwaswealthadvisory.com","Vishwas Wealth Advisory");
	            $email->setSubject('Sign-in to Vishwas Wealth Advisory' );
	            $email->addTo($user->email);
	             $adminEmail = \app\models\User::find()->select('email')->where(['status' => 1, 'user_type'=>1])->one();
                if($adminEmail->email!="")
                 $email->addCc($adminEmail->email);
	            $email->addContent(
	                "text/html", $content
	            );
	            $sendgrid = new \SendGrid(Yii::$app->params['SENDGRID_API_KEY']);
	            try {
	                $response = $sendgrid->send($email);
	                // $response= $response->statusCode() . "\n";
	                // $response=($response->headers());
	                // $response= $response->body() . "\n";
	            } catch (Exception $e) {
	                $response= 'Caught exception: '. $e->getMessage() ."\n";
	            }
                Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox.');
                //print_r($user);die;
                
        //          return $this->render('pay_u', [
        //     'user' => $user,
        // ]);
              }
            }
            return $this->redirect(['index']);
         // return $this->redirect(['login']);   
       } 

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    
    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');
            return $this->goHome();
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
  public function actionResendVerificationEmail()
  {
      $model = new ResendVerificationEmailForm();
      if ($model->load(Yii::$app->request->post()) && $model->validate()) {
          if ($model->sendEmail()) {
              Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
              return $this->goHome();
          }
          Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
      }

      return $this->render('resendVerificationEmail', [
          'model' => $model
      ]);
  }
  
  
  public function actionChangepassword(){

        $model = new PasswordForm;
        $modeluser = \app\models\User::find()->where([
            'username'=>Yii::$app->user->identity->username
        ])->one();
        
        if($model->load(Yii::$app->request->post())){
            if($model->validate()){
                try{
                    $modeluser->password = sha1($_POST['PasswordForm']['newpass']);
                    if($modeluser->save()){
                    require '../../vendor/autoload.php'; 
                    $email = new \SendGrid\Mail\Mail(); 
                        $content='<div style="font:18px/23px Arial,Helvetica,sans-serif;background:#f3f3f3">
                 <div style="max-width:800px;margin-left:auto;margin-right:auto">
                    <div style="width:640px;margin-left:auto;margin-right:auto;padding:20px 0px 20px 0px; style="text-align: center;">
                        <table class="x_-665217761MsoNormalTable" border="1" cellspacing="3" cellpadding="0" style="border: solid rgb(204,204,204) 1.0pt;background: #fff;color: #000;width:98%">
                      <tbody>
                        <tr>
                          <td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                          <h2 align="center" style="text-align: center;font-family: Helvetica;">Change Password</h2>
                          <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">Your password for VWA changed successfully. New login details: </span></p>                        
                          
                          </td>
                        </tr>
                       
                        <tr>
                          <td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                           <p class="MsoNormal" align="center" style="text-align: left;font-family: Helvetica;"><span style="font-size: 10.0pt;"><b>Username: </b>'.$modeluser->username.'</span></p>
                          </td>
                        </tr>
                         <tr>
                          <td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                           <p class="MsoNormal" align="center" style="text-align: left;font-family: Helvetica;"><span style="font-size: 10.0pt;"><b>Password: </b>'.$_POST['PasswordForm']['newpass'].'</span></p>
                          </td>
                        </tr>
                        <tr>
                          <td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                          <p class="MsoNormal" align="center" style="font-family: Helvetica;text-align: center;"><span style="font-size: 10.0pt;font-family: Helvetica;">Thank You</span></p></td>
                        </tr>
                      </tbody>
                    </table> 
                    </div>
                 </div>
              </div>';
                $email->setFrom("info@vishwaswealthadvisory.com","Vishwas Wealth Advisory");
                $email->setSubject('Change Password - Vishwas Wealth Advisory' );
                $email->addTo($modeluser->email);
                $email->addContent(
                    "text/html", $content
                );
                $sendgrid = new \SendGrid(Yii::$app->params['SENDGRID_API_KEY']);
                        try {
                            $response = $sendgrid->send($email);
                            // $response= $response->statusCode() . "\n";
                            // $response=($response->headers());
                            // $response= $response->body() . "\n";
                        } catch (Exception $e) {
                            $response= 'Caught exception: '. $e->getMessage() ."\n";
                        }


                        Yii::$app->getSession()->setFlash(
                            'success','Password changed. Password sent on your mail'
                        );
                        return $this->redirect(['profile','id'=>$modeluser->id]);
                    }else{
                        Yii::$app->getSession()->setFlash(
                            'error','Password not changed'
                        );
                       // return $this->redirect(['index']);
                    }
                }catch(Exception $e){
                    Yii::$app->getSession()->setFlash(
                        'error',"{$e->getMessage()}"
                    );
                    return $this->render('changepassword',[
                        'model'=>$model
                    ]);
                }
            }else{
                return $this->render('changepassword',[
                    'model'=>$model
                ]);
            }
        }else{
            return $this->render('changepassword',[
                'model'=>$model
            ]);
        }
  }

  public function actionWhoWeAre()
  {
    $model=\backend\models\About::find()->orderBy(['id'=> SORT_DESC])->one();
    return $this->render('who-we-are',['model' => $model]);

  }
  public function actionServices()
  {
    $model=\backend\models\Services::find()->orderBy(['id'=> SORT_DESC])->one();
    return $this->render('services',['model' => $model]);
  }
  public function actionFaqs()
  {
    $model = \backend\models\Faq::find()->where(['id'=>-1])->orderBy(['id'=>SORT_DESC])->one();

    return $this->render('faq',['model' => $model]);

  }
  public function actionContact()
  {
    $model=\backend\models\ContactUs::find()->orderBy(['id'=> SORT_DESC])->one();
    return $this->render('contact',['model' => $model]);
  }
  public function actionPrivacyPolicy()
  {
    $model=\backend\models\PrivacyPolicy::find()->orderBy(['id'=> SORT_DESC])->one();
    return $this->render('privacy-policy',['model' => $model]);
  }
  public function actionTermsAndCondition()
  {
    $model=\backend\models\Termsconditions::find()->orderBy(['id'=> SORT_DESC])->one();
    return $this->render('terms-and-condition',['model' => $model]);
  }
  public function actionResources()
  {
    $model = \backend\models\Resources::find()->where(['id'=>-2])->orderBy(['id'=>SORT_DESC])->one();
    return $this->render('resources',['model' => $model]);
  }
  public function actionStocks()
  {
    return $this->render('stock-calculator');
  }
   public function actionProfile($id)
    {
      $model=\app\models\Users::find()->where(['id'=>$id])->one();
      $log = \app\models\LoginDetails::find()->where(['login_user_id' => $id])->orderBy(['login_detail_id'=>SORT_DESC])->one();

      return $this->render('profile',['model'=>$model, 'login'=>$log]);
    }
    public function actionEditProfile($id)
    {
      $model=\backend\models\Users::find()->where(['id'=>$id])->one();

      return $this->render('edit-profile',['model'=>$model]);
    }
    public function actionProfileEdit()
    {
      $id=$_REQUEST['id'];
      $name=$_REQUEST['name'];
      $email=$_REQUEST['email'];
      $birth_date=$_REQUEST['birth_date'];
      $starsign= $_REQUEST['starsign'];
      $username=$_REQUEST['username'];

      $model=\app\models\User::find()->where(['id'=>$id, 'username'=>$username])->one();
      if($model!="" && $model!=null)
      {
        $url = Yii::$app->getUrlManager()->createUrl(["/site/profile", 'id'=>$model->id]);

        $model->first_name=$name;
        $model->email=$email;
        $model->birth_date=date_format(date_create($birth_date), 'Y-m-d');
        $model->starsign=$starsign;
        $model->updated_at=date('Y-m-d H:i:s');
        if($model->save())
        {
          return Json::encode([
              'status'=>'success','message'=>'Profile updated successfully', 'url'=>$url
          ]);
        }
        else
        {
          return Json::encode([
              'status'=>'error','message'=>'Oops! Something went wrong. Please try again'
          ]);
        }
      }
      else
      {
        return Json::encode([
              'status'=>'error','message'=>'Oops! Something went wrong. Please try again'
          ]);
      }
      
    }
  public function actionSendEmail($username,$password)
  {
    echo $username;die;
    	require '../../vendor/autoload.php'; 
          $email = new \SendGrid\Mail\Mail(); 
           $content='<div style="font:18px/23px Arial,Helvetica,sans-serif;background:#f3f3f3">
                 <div style="max-width:800px;margin-left:auto;margin-right:auto">
                    <div style="width:640px;margin-left:auto;margin-right:auto;padding:20px 0px 20px 0px; style="text-align: center;">
                        <table class="x_-665217761MsoNormalTable" border="1" cellspacing="3" cellpadding="0" style="border: solid rgb(204,204,204) 1.0pt;background: #fff;color: #000;width:98%">
                      <tbody>
                        <tr>
                          <td style="border: none;padding: 2pt 22.5pt 2pt 22.5pt;">
                          <h2 align="center" style="text-align: center;font-family: Helvetica;">Welcome to ShelleyVonStruckle.com</h2>
                          <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">You are all set now.</span></p>
                        
                          
                          </td>
                        </tr>
                       
                        <tr>
                          <td style="border: none;padding: 2pt 22.5pt 2pt 22.5pt;">
                           <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">Log in your new account.Your new account details</p>
                          </td>
                        </tr>
                         <tr>
                          <td style="border: none;padding: 2pt 22.5pt 2pt 22.5pt;">
                           <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;"><b>Username - </b>dsads<br><b>Password - </b>xsad</p>
                          </td>
                        </tr>
                          <tr>
                          <td style="border: none;padding: 5pt 22.5pt 5pt 22.5pt;">
                           <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">Thank You</span></p>
                          </td>
                        </tr>
                        
                      </tbody>
                    </table> 
                    </div>
                 </div>
              </div>';
               $email->setFrom("info@vishwaswealthadvisory.com","Vishwas Wealth Advisory");
                $email->setSubject('Sign-in to Vishwas Wealth Advisory' );
                $email->addTo($user->email);
                $email->addContent(
                    "text/html", $content
                );
                $sendgrid = new \SendGrid(Yii::$app->params['SENDGRID_API_KEY']);           
            try {
                $response = $sendgrid->send($email);
                // $response= $response->statusCode() . "\n";
                // $response=($response->headers());
                // $response= $response->body() . "\n";
            } catch (Exception $e) {
                $response= 'Caught exception: '. $e->getMessage() ."\n";
            }
            return $response;
           
  }
}
