<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [
        'css/site.css',
        'themes/atlantis/assets/css/font-awesome-all.css',
        'themes/atlantis/assets/css/flaticon.css',
        'themes/atlantis/assets/css/owl.css',
        'themes/atlantis/assets/css/jquery.fancybox.min.css',
        'themes/atlantis/assets/css/animate.css',
        'themes/atlantis/assets/css/color.css',
        'themes/atlantis/assets/css/rtl.css',
        'themes/atlantis/assets/css/style.css',
        'themes/atlantis/assets/css/responsive.css',       
    ];
    public $js = [
        //'themes/atlantis/assets/js/croppie.js',
        'themes/atlantis/assets/js/popper.min.js',
        'themes/atlantis/assets/js/bootstrap.min.js',
        'themes/atlantis/assets/js/owl.js',
        'themes/atlantis/assets/js/wow.js',
        'themes/atlantis/assets/js/validation.js',
        'themes/atlantis/assets/js/jquery.fancybox.js',
        'themes/atlantis/assets/js/appear.js',
        'themes/atlantis/assets/js/scrollbar.js',
        'themes/atlantis/assets/js/nav-tool.js',
        'themes/atlantis/assets/js/TweenMax.min.js',
        'themes/atlantis/assets/js/circle-progress.js',
        'themes/atlantis/assets/js/isotope.js',
        'themes/atlantis/assets/js/script.js',
    ];
    public $depends = [

        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        '\rmrevin\yii\fontawesome\AssetBundle',

    ];
}
