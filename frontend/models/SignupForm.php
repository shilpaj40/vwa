<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $confirm_password;
    public $birth_date;
    public $first_name;
    public $last_name;
    public $mobile;
    public $city;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'first_name', 'last_name'], 'trim'],

           ['email','email','message'=>'Email ID is not valid'],

            // ['mobile', 'number', 'max' => 10, 'min' => 0, 'tooBig' => 'Please enter a valid Phone number.', 'tooSmall' => 'Please enter a valid Phone number.'],
            //['mobile','match', 'pattern'=>'/^[1][34578][0-9]{9}$/'],
            // [['mobile'],'number','min' => 10],
           // [['mobile'],'match','pattern'=>'/^[1][358][0-9]{9}$/'],
            ['mobile', 'match', 'pattern' => '/^\d{10}$/', 'message' => 'Please enter a valid Phone number.'],

            [['username', 'first_name', 'last_name', 'email', 'mobile','password','confirm_password'], 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

           

   ['first_name', 'match','pattern' => '^[a-zA-Z]$','message' => 'First Name can only contain characters'],
   ['last_name', 'match','pattern' => '^[a-zA-Z]$','message' => 'Last Name can only contain characters'],

    
            ['confirm_password', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],

            [['password', 'confirm_password'], 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->email;
        echo $this->email;die;
        $user->first_name=$this->first_name;
        $user->email = $this->email;
        $user->mobile=$this->mobile;
        $user->city=$this->city;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
       
        return $user->save(); //&& $this->sendEmail($user);

    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
}
