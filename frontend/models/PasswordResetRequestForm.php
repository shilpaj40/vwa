<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use yii\helpers\Html;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\app\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE, 'user_type' => 2],
                'message' => 'There is no user with this email address.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if (!$user) {
            return false;
        }
        
        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }//'passwordResetToken-html', ['user' => $user]
       
        $resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
         require '../../vendor/autoload.php'; // If you're using Composer (recommended)
        // Comment out the above line if not using Composer
        // require("<PATH TO>/sendgrid-php.php");
        // If not using Composer, uncomment the above line and
        // download sendgrid-php.zip from the latest release here,
        // replacing <PATH TO> with the path to the sendgrid-php.php file,
        // which is included in the download:
        // https://github.com/sendgrid/sendgrid-php/releases
       // $content= "Hello ".Html::encode($user->first_name).",<br />Follow the link below to reset your password:<br />".Html::a(Html::encode($resetLink), $resetLink);

        $content='<div style="font:18px/23px Arial,Helvetica,sans-serif;background:#f3f3f3">
                 <div style="max-width:800px;margin-left:auto;margin-right:auto">
                    <div style="width:640px;margin-left:auto;margin-right:auto;padding:20px 0px 20px 0px; style="text-align: center;">
                        <table class="x_-665217761MsoNormalTable" border="1" cellspacing="3" cellpadding="0" style="border: solid rgb(204,204,204) 1.0pt;background: #fff;color: #000;">
                      <tbody>
                        <tr>
                          <td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                          <h2 align="center" style="text-align: center;font-family: Helvetica;">Password Reset</h2>
                          <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">Seems like you forgot your password for VWA. If this is true click below to reset your password </span></p>
                        
                          
                          </td>
                        </tr>
                        <tr>
                          <td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                           <p align="center" style="text-align: center;"><a href="'.$resetLink.'" target="_blank" style="display: inline-flex;min-height: 50px;align-items: center;justify-content: center;padding: 0rem 4.0rem;background: #e6a34b;border-radius: 0;border: 2px solid #e6a34b;color:#fff;font-size: 1rem;line-height: 3;font-family: Helvetica;text-decoration:none">Reset Password</a></p>
                          </td>
                        </tr>
                        <tr>
                          <td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                           <p class="MsoNormal" align="center" style="text-align: center;font-family: Helvetica;"><span style="font-size: 10.0pt;">If you did not request a password reset, please ignore this email or reply to let us know.</span></p>
                          </td>
                        </tr>
                        <tr>
                          <td style="border: none;padding: 5.25pt 22.5pt 5.5pt 22.5pt;">
                          <p class="MsoNormal" align="center" style="font-family: Helvetica;text-align: center;"><span style="font-size: 10.0pt;font-family: Helvetica;">Thank You</span></p>
                          </td>
                        </tr>
                      </tbody>
                    </table> 
                    </div>
                 </div>
              </div>';
                $email = new \SendGrid\Mail\Mail(); 
                $email->setFrom("info@vishwaswealthadvisory.com","Vishwas Wealth Advisory");
                $email->setSubject('Forget Password - Vishwas Wealth Advisory' );
                $email->addTo($this->email);
                $email->addContent(
                    "text/html", $content
                );
                $sendgrid = new \SendGrid(Yii::$app->params['SENDGRID_API_KEY']);
            try {
            $response = $sendgrid->send($email);
            // $response= $response->statusCode() . "\n";
            // $response=($response->headers());
            // $response= $response->body() . "\n";
        } catch (Exception $e) {
            $response= 'Caught exception: '. $e->getMessage() ."\n";
        }
        return $response;
       
     }
}
