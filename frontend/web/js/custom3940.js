/**
 * @file
 */
(() => {
  if (document.getElementById('block-claro-content').innerHTML.indexOf('physical-good') == -1) {
    var tabs = document.getElementsByClassName('tabs__link');
    for (var i=0; i<tabs.length; i++) {
      if (tabs[i].textContent == 'Shipments') {
        tabs[i].remove();
      }
    }
  }
})();
