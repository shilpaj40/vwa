<!-- Sidebar -->
<div class="sidebar sidebar-style-2">           
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-2">
                    <img src="../assets/img/profile.jpg" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                        <span>
                            Hizrian
                            <span class="user-level">Administrator</span>
                            <span class="caret"></span>
                        </span>
                    </a>
                    <div class="clearfix"></div>

                    <div class="collapse in" id="collapseExample">
                        <ul class="nav">
                            <li>
                                <a href="#profile">
                                    <span class="link-collapse">My Profile</span>
                                </a>
                            </li>
                            <li>
                                <a href="#edit">
                                    <span class="link-collapse">Edit Profile</span>
                                </a>
                            </li>
                            <li>
                                <a href="#settings">
                                    <span class="link-collapse">Settings</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="nav nav-primary">
                <li class="nav-item active">
                    <a data-toggle="collapse" href="#dashboard" class="collapsed" aria-expanded="false">
                        <i class="fa fa-home"></i>
                        <p>Dashboard</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="dashboard">
                        <ul class="nav nav-collapse">
                            <li>
                                <a href="">
                                    <span class="sub-item">Dashboard 1</span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="sub-item">Dashboard 2</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <!-- <li class="nav-section">
                    <span class="sidebar-mini-icon">
                        <i class="fa fa-ellipsis-h"></i>
                    </span>
                    <h4 class="text-section">Services</h4>
                </li> -->
                <li class="nav-item">
                    <a data-toggle="collapse" href="#base">
                        <i class="fa fa-database"></i>
                        <p>Services</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="base">
                        <ul class="nav nav-collapse">
                            <li>
                                <a href="<?= \yii\helpers\Url::to(['shop-act-registration/'])?>">
                                    <span class="sub-item">Shop Act (GUMASTA) Registration</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= \yii\helpers\Url::to(['udyog-adhar/'])?>">
                                    <span class="sub-item">Udyog Adhar</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="sub-item">Goods & Service Tax Registration</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="sub-item">Food Licence</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="sub-item">Maharashtra Labour Welfare Fund</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="sub-item">Digital Signature</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="sub-item">Provident Fund</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="sub-item">ESIC</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="sub-item">OPC Pvt ltd</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="sub-item">Pvt Ltd</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="sub-item">Trust Registration</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="sub-item">PAN Card</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="sub-item">TAN Number</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="sub-item">Income Tax Return</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="sub-item">ESIC</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="sub-item">ETDS</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                
            </ul>
        </div>
    </div>
</div>
<!-- End Sidebar -->