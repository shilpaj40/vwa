<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="MobileOptimized" content="width" />
    <meta name="HandheldFriendly" content="true" />
    <link rel="shortcut icon" href="<?= Yii::$app->params['basepath'];?>images/favicon.ico" type="image/vnd.microsoft.icon" />
    <link rel="canonical" href="" />
    <link rel="shortlink" href="" />
    <link rel="revision" href="" />
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="boxed_wrapper ltr">
<?php $this->beginBody() ?>
    <!-- Preloader -->
    <div class="loader-wrap">
        <!--<div class="preloader"><div class="preloader-close">Preloader Close</div></div>-->
        <div class="layer layer-one"><span class="overlay"></span></div>
        <div class="layer layer-two"><span class="overlay"></span></div>        
        <div class="layer layer-three"><span class="overlay"></span></div>        
    </div>
    <!-- page-direction -->
   
    <!-- page-direction end -->
     <!-- main header -->
    <header class="main-header style-one style-three">
       
        <div class="header-lower">
            <div class="outer-box">
                <div class="auto-container">
                    <div class="main-box clearfix">
                        <div class="logo-box pull-left">
                             <?php
                $logodetails = \backend\models\HomeBlocks::find()->select(['logo','copyright'])->orderBy(['id'=> SORT_DESC])->one();

                if($logodetails->logo!="")
                {
                    echo "<figure class='logo'><a href='".yii\helpers\Url::to(['site/index'])."'><img src=".$logodetails->logo."></a></figure>";
                }
                else
                {
                    echo "<figure class='logo'><a href='".yii\helpers\Url::to(['site/index'])."><img src=".Yii::$app->params['logo']."></a></figure>";
                }
                ?>
                            
                        </div>
                        <div class="menu-area pull-right clearfix">
                            <!--Mobile Navigation Toggler-->
                            <div class="mobile-nav-toggler">
                                <i class="icon-bar"></i>
                                <i class="icon-bar"></i>
                                <i class="icon-bar"></i>
                            </div>
                            <nav class="main-menu navbar-expand-md navbar-light">
                                <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                    <ul class="navigation clearfix">
                                        <li  id="home" ><a href="<?= yii\helpers\Url::to(['site/index']);?>">Home</a></li>
                                        <li id="about_us"><a  href="<?= yii\helpers\Url::to(['site/who-we-are']);?>">About Us</a></li>
                                        <li id="services"><a  href="<?= yii\helpers\Url::to(['site/services']);?>">Services</a></li>
                                        <li id="resources"><a  href="<?= yii\helpers\Url::to(['site/resources']);?>">Resources</a></li> 
                                        
                                        <li id="FAQ"><a   href="<?= yii\helpers\Url::to(['site/faqs']);?>">FAQ's</a></li>                          
                                        <li id="Contact"><a   href="<?= yii\helpers\Url::to(['site/contact']);?>">Contact</a></li>
                                    </ul>
                                </div>
                            </nav>
                            <div class="menu-right-content clearfix">
                               
                                <!--<div class="nav-btn nav-toggler navSidebar-button clearfix">-->
                                <!--    <i class="fas fa-align-right"></i>-->
                                <!--</div>-->
                                <?php  if(Yii::$app->user->isGuest) {?>
                                <div class="btn-box">
                                    <?php /*
                                    <a href="<?= \yii\helpers\Url::to(['site/login'])?>" class="theme-btn style-three">Login</a>
                                    */?>
                                    <a href="<?= \yii\helpers\Url::to(['site/signup'])?>" class="theme-btn style-three">Sign Up</a>
                                    
                                </div>
                                <?php }?>
                                <?php if(!Yii::$app->user->isGuest) {?>
                                <div class="dropdown">
                                  <button class="dropbtn"><?= \Yii::$app->user->identity->username?><!-- <img src="http://localhost:81/vishwas/frontend/web/images/logo.jpg" /> --></button>
                                  <div class="dropdown-content">
                                    <a href="<?= \yii\helpers\Url::to(['site/profile?id='.Yii::$app->user->identity->id])?>">My Profile</a>
                                   <!--  <a href="#">Settings</a> -->
                                    <a  href="<?= \yii\helpers\Url::to(['site/resources'])?>">Resources</a>
                                    <a  href="<?= \yii\helpers\Url::to(['site/stocks'])?>">Stock</a>
                                    <a data-method="post" href="<?= \yii\helpers\Url::to(['site/logout'])?>">Logout</a>
                                  </div>
                                </div>
                            <?php }?>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--sticky Header-->
        <div class="sticky-header">
            <div class="auto-container">
                <div class="outer-box clearfix">
                    <div class="logo-box pull-left">
                        <figure class="logo"><a href="<?= yii\helpers\Url::to(['site/index']);?>"><img src="assets/images/small-logo-3.png" alt=""></a></figure>
                    </div>
                    <div class="menu-area pull-right">
                        <nav class="main-menu clearfix">
                            <!--Keep This Empty / Menu will come through Javascript-->
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- main-header end -->

    <!-- Mobile Menu  -->
    <div class="mobile-menu">
        <div class="menu-backdrop"></div>
        <div class="close-btn"><i class="fas fa-times"></i></div>
        
        <nav class="menu-box">
            <div class="nav-logo"><a href="index-2.html"><img src="assets/images/mobile-logo.png" alt="" title=""></a></div>
            <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
            
            
        </nav>
    </div><!-- End Mobile Menu -->
   


<div class="dialog-off-canvas-main-canvas" data-off-canvas-main-canvas>


    <div id="content">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
       
        <?= $content ?>
    </div>
</div>
<footer class="main-footer">
        <!--<div class="footer-top">-->
        <!--    <div class="auto-container">-->
        <!--        <div class="widget-section">-->
        <!--            <div class="row clearfix">-->
        <!--                <div class="col-lg-3 col-md-6 col-sm-12 footer-column">-->
        <!--                    <div class="footer-widget logo-widget">-->
        <!--                        <figure class="footer-logo"><a href=""><img src="<?= Yii::$app->params['logo'];?>" alt=""></a></figure>-->
        <!--                        <div class="text">-->
        <!--                            <p>As novice investors, we are bound to be flummoxed by the idea of high returns and fatter wallets, but before falling prey to any such marketing gimmicks, it is important to understand what the product is all about.</p>-->
        <!--                        </div>-->
        <!--                        <ul class="info-list clearfix">-->
        <!--                            <li><i class="fas fa-map-marker-alt"></i>838 Andy Street, Madison, NJ 08003</li>-->
        <!--                            <li><i class="fas fa-envelope"></i>Email <a href="mailto:support@my-domain.com">support@my-domain.com</a></li>-->
        <!--                            <li><i class="fas fa-headphones"></i>Support <a href="tel:01005200369">0100 5200 369</a></li>-->
        <!--                        </ul>-->
        <!--                        <ul class="social-links clearfix">-->
        <!--                            <li><a href=""><i class="fab fa-twitter"></i></a></li>-->
        <!--                            <li><a href=""><i class="fab fa-facebook-f"></i></a></li>-->
        <!--                            <li><a href=""><i class="fab fa-instagram"></i></a></li>-->
        <!--                            <li><a href=""><i class="fab fa-linkedin-in"></i></a></li>-->
        <!--                            <li><a href=""><i class="fab fa-pinterest-p"></i></a></li>-->
        <!--                        </ul>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--                <div class="col-lg-3 col-md-6 col-sm-12 footer-column">-->
        <!--                    <div class="footer-widget links-widget ml-70">-->
        <!--                        <div class="widget-title">-->
        <!--                            <h4>Useful Links</h4>-->
        <!--                        </div>-->
        <!--                        <div class="widget-content">-->
        <!--                            <ul class="list clearfix">-->
        <!--                                <li><a href="<?= yii\helpers\Url::to(['site/who-we-are']);?>">About Us</a></li>-->
        <!--                                <li><a href="">What We Offers</a></li>-->
        <!--                                <li><a href="">Testimonials</a></li>-->
        <!--                                <li><a href="">Our Projectss</a></li>-->
        <!--                                <li><a href="">Latest News</a></li>-->
        <!--                                <li><a href="">Privacy Policy</a></li>-->
        <!--                                <li><a href="">Contact Us</a></li>-->
        <!--                            </ul>-->
        <!--                        </div>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--                <div class="col-lg-3 col-md-6 col-sm-12 footer-column">-->
        <!--                    <div class="footer-widget links-widget">-->
        <!--                        <div class="widget-title">-->
        <!--                            <h4>What We Do</h4>-->
        <!--                        </div>-->
        <!--                        <div class="widget-content">-->
        <!--                            <ul class="list clearfix">-->
        <!--                                <li><a href="">Financial Advice</a></li>-->
        <!--                                <li><a href="">Business Planning</a></li>-->
        <!--                                <li><a href="">Startup Help</a></li>-->
        <!--                                <li><a href="">Investment Strategy</a></li>-->
        <!--                                <li><a href="">Management Services</a></li>-->
        <!--                                <li><a href="">Market Research</a></li>-->
        <!--                                <li><a href="">SEO Optimization</a></li>-->
        <!--                            </ul>-->
        <!--                        </div>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--                <div class="col-lg-3 col-md-6 col-sm-12 footer-column">-->
        <!--                    <div class="footer-widget newsletter-widget">-->
        <!--                        <div class="widget-title">-->
        <!--                            <h4>Newsletter</h4>-->
        <!--                        </div>-->
        <!--                        <div class="widget-content">-->
        <!--                            <div class="text">-->
        <!--                                <p>Get in your inbox the latest News</p>-->
        <!--                            </div>-->
        <!--                            <form action="#" method="post" class="newsletter-form">-->
        <!--                                <div class="form-group">-->
        <!--                                    <i class="far fa-user"></i>-->
        <!--                                    <input type="text" name="name" placeholder="Your Name" required="">-->
        <!--                                </div>-->
        <!--                                <div class="form-group">-->
        <!--                                    <i class="far fa-envelope"></i>-->
        <!--                                    <input type="email" name="email" placeholder="Email address" required="">-->
        <!--                                </div>-->
        <!--                                <div class="form-group message-btn">-->
        <!--                                    <button class="theme-btn style-one" type="submit">subscribe</button>-->
        <!--                                </div>-->
        <!--                            </form>-->
        <!--                        </div>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--    </div>-->
        <!--</div>-->
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="row">
                    <div class="col-md-6 text-left"><p><a href="<?= \yii\helpers\Url::to(['site/privacy-policy'])?>">Privacy Policy</a> | <a href="<?= \yii\helpers\Url::to(['site/terms-and-condition'])?>">Terms & Condition</a></p></div>
                    <div class="col-md-6 text-right">
                        <?php if($logodetails->copyright!=""){
                            echo "<p>".$logodetails->copyright."</p>";
                        }
                        else
                        {
                             echo "<p>© 2021 <a href='#''>Vishwas Wealth Advisory LLP/Pvt. Ltd. </a> All rights reserved.</p>";
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </footer>

<!-- sidebar cart item -->
    <div class="xs-sidebar-group info-group info-sidebar">
        <div class="xs-overlay xs-bg-black"></div>
        <div class="xs-sidebar-widget">
            <div class="sidebar-widget-container">
                <div class="widget-heading">
                    <a href="#" class="close-side-widget">X</a>
                </div>
                <div class="sidebar-textwidget">
                <div class="sidebar-info-contents">
                    <div class="content-inner">
                        <div class="upper-box">
                            <div class="logo">
                                <a href=""><img src="<?= Yii::$app->params['logo'];?>" alt="" /></a>
                            </div>
                            <div class="text">
                                <!--<p>Exercitation ullamco laboris nis aliquip sed conseqrure dolorn repreh deris ptate velit ecepteur duis.</p>-->
                            </div>
                        </div>
                        <div class="side-menu-box">
                            <div class="side-menu">
                                <nav class="menu-box">
                                    <div class="menu-outer">
                                        
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <div class="info-box">
                            <h3>Get in touch</h3>
                            <ul class="info-list clearfix">
                                <li><i class="fas fa-map-marker-alt"></i>838 Andy Street, Madison, NJ</li>
                                <li><i class="fas fa-envelope"></i><a href="mailto:support@my-domain.com">support@my-domain.com</a></li>
                                <li><i class="fas fa-headphones-alt"></i><a href="tel:101005200369">+1  0100 5200 369</a></li>
                                <li><i class="far fa-clock"></i>Monday to Friday: 9am - 6pm</li>
                            </ul>
                            <form action="#" method="post" class="subscribe-form">
                                <div class="form-group">
                                    <input type="email" name="email" placeholder="Email address" required="">
                                    <button type="submit" class="theme-btn style-one">subscribe now</button>
                                </div>
                            </form>
                            <ul class="social-links clearfix">
                                <li><a href=""><i class="fab fa-twitter"></i></a></li>
                                <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href=""><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href=""><i class="fab fa-google-plus-g"></i></a></li>
                                <li><a href=""><i class="fab fa-pinterest-p"></i></a></li>
                                <li><a href=""><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!--  <div class="nb-form">
        <p class="title">Send a message</p>
        <img src="https://lh3.googleusercontent.com/-LvTWzTOL4c0/V2yhfueroyI/AAAAAAAAGZM/Ebwt4EO4YlIc03tw8wVsGrgoOFGgAsu4wCEw/w140-h140-p/43bf8578-86b8-4c1c-86a6-a556af8fba13" alt="" class="user-icon">
        <form>
          <input type="text" name="cpname" placeholder="Name:" required>
          <input type="email" name="cpemail" placeholder="Email:" required>
          <input type="tel" name="cpphone" placeholder="Phone:" required>
          <textarea name="cpmessage" placeholder="Message:"></textarea>
          <input type="submit" value="Send message">
        </form>
    </div> -->
    <!-- END sidebar widget item -->
<?php $this->endBody() ?>

<script type="text/javascript">
     /*-------------------------------------
    Toggle Class
    -------------------------------------*/
    $(".toggle-password").on('click', function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        
        //var input = ;
        if ($("#loginform-password").attr("type") == "password") {
            $("#loginform-password").attr("type", "text");
        } else {
            $("#loginform-password").attr("type", "password");
        }
    });
    /*-------------------------------------
    Toggle Class
    -------------------------------------*/
    $(".toggle-pwdpassword").on('click', function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        
        //var input = ;
        if ($("#signupform-password").attr("type") == "password") {
            $("#signupform-password").attr("type", "text");
        } else {
            $("#signupform-password").attr("type", "password");
        }
        
        if ($("#loginform-password").attr("type") == "password") {
            $("#loginform-password").attr("type", "text");
        } else {
            $("#loginform-password").attr("type", "password");
        }
    });
    $(".toggle-cnfpassword").on('click', function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        
        //var input = ;
        if ($("#signupform-confirm_password").attr("type") == "password") {
            $("#signupform-confirm_password").attr("type", "text");
        } else {
            $("#signupform-confirm_password").attr("type", "password");
        }
        if ($("#loginform-confirm_password").attr("type") == "password") {
            $("#loginform-confirm_password").attr("type", "text");
        } else {
            $("#loginform-confirm_password").attr("type", "password");
        }
    });
</script> 
</body>
</html>
<?php $this->endPage() ?>
