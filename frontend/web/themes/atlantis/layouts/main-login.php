<?php
use backend\assets\AppAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

dmstr\web\AdminLteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>
    <link href="<?= Yii::$app->params['basepath'];?>themes/atlantis/assets/css/login.css" rel="stylesheet">

    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>

    <?= $content ?>

<?php $this->endBody() ?>
<script type="text/javascript">
     /*-------------------------------------
    Toggle Class
    -------------------------------------*/
    $(".toggle-password").on('click', function () {
        alert("D");
        $(this).toggleClass("fa-eye fa-eye-slash");
        
        //var input = ;
        if ($("#loginform-password").attr("type") == "password") {
            $("#loginform-password").attr("type", "text");
        } else {
            $("#loginform-password").attr("type", "password");
        }
    });
    /*-------------------------------------
    Toggle Class
    -------------------------------------*/
    $(".toggle-pwdpassword").on('click', function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        
        //var input = ;
        if ($("#signupform-password").attr("type") == "password") {
            $("#signupform-password").attr("type", "text");
        } else {
            $("#signupform-password").attr("type", "password");
        }
    });
    $(".toggle-cnfpassword").on('click', function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        
        //var input = ;
        if ($("#signupform-confirm_password").attr("type") == "password") {
            $("#signupform-confirm_password").attr("type", "text");
        } else {
            $("#signupform-confirm_password").attr("type", "password");
        }
    });
</script> 
</body>
</html>
<?php $this->endPage() ?>
