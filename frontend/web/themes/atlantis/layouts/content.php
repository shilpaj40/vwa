<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<div class="content-wrapper">
    <div class="main-panel">
        <div class="content">
            <div class="page-inner">
                <!-- <div class="page-header">
                    <?php if (isset($this->blocks['content-header'])) { ?>
                        <h4 class="page-title"><?= $this->blocks['content-header'] ?></h4>
                    <?php } else { ?>
                        <h4 class="page-title">
                            <?php
                            if ($this->title !== null) {
                                echo \yii\helpers\Html::encode($this->title);
                            } else {
                                echo \yii\helpers\Inflector::camel2words(
                                    \yii\helpers\Inflector::id2camel($this->context->module->id)
                                );
                                echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                            } ?>
                        </h4>
                    <?php } ?>
                    <?=
                        Breadcrumbs::widget(
                            [
                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                            ]
                    ) ?>
                </div> -->
                <div class="row">
                    <div class="col-md-12">
                        <?= Alert::widget() ?>
                         <?= $content ?>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    
</div>



