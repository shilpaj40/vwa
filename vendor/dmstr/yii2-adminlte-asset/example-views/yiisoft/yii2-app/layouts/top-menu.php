<?= Yii::$app->Permission->getMenus(); ?> 
<?php
use yii\bootstrap4\NavBar;
use kartik\nav\NavX;

//NavBar::begin([
//    'brandLabel' => 'My Company',
//    'brandUrl' => Yii::$app->homeUrl,
//    'options' => [
//        'class' => 'navbar-inverse navbar-fixed-top',
//    ],
//]);
/*$menuItems = [
    [
        'label' => Yii::t('app', 'Logout') . '(' . Yii::$app->user->identity->username . ')',
        'url' => ['/site/logout'],
        'linkOptions' => ['data-method' => 'post']
    ]
];
echo Nav::widget([
    'options' => ['class' => 'nav navbar-nav navbar-right'],
    'items' => $menuItems,
]);

$menuItemsMain = [
    [
        'label' => '<i class="fa fa-laptop"></i> ' . Yii::t('app', 'Design'),
        'url' => ['#'],
        'class' => 'text-orange',
        'active' => false,
        'linkOptions'=> ['class'=>'text-logo-color'],
        'items' => [
            
            [
                'label' => '<i class="fa fa-angle-double-right"></i> ' . Yii::t('app', 'Menu'),
                'url' => ['/menu/creator'],
                'linkOptions'=> ['class'=>'text-logo-color'],
            ],
            [
                'label' => '<i class="fa fa-angle-double-right"></i> ' . Yii::t('app', 'Banner'),
                'url' => ['/banner/index'],
                
                'linkOptions'=> ['class'=>'text-logo-color'],
            ],
            [
                'label' => '<i class="fa fa-angle-double-right"></i> ' . Yii::t('app', 'Carousel'),
                'url' => ['/carousel-item/index'],
                
                'linkOptions'=> ['class'=>'text-logo-color'],
            ],
           
        ],
    ],
    
   
    [
        'label' => '<i class="fa fa-th"></i> ' . Yii::t('app', 'Content'),
        'url' => ['#'],
        'active' => false,
        'linkOptions'=> ['class'=>'text-logo-color'],
        'items' => [
            [
                'label' => '<i class="fa fa-angle-double-right"></i> ' . Yii::t('app', 'Starsign'),
                'url' => ['/starsign/index'],
                'linkOptions'=> ['class'=>'text-logo-color'],
            ],
            [
                'label' => '<i class="fa fa-angle-double-right"></i> ' . Yii::t('app', 'Celebrity Profile'),
                'url' => ['/celebrity-profile/index'],
                'linkOptions'=> ['class'=>'text-logo-color'],
            ],
             [
                'label' => '<i class="fa fa-angle-double-right"></i> ' . Yii::t('app', 'Home'),
                'url' => ['#'],
                'active' => false,
                'linkOptions'=> ['class'=>'text-logo-color'],
                'items' => [
                    [
                        'label' => '<i class="fa fa-angle-double-right"></i> ' . Yii::t('app', 'Starsign'),
                        'url' => ['/starsign/index'],
                        'linkOptions'=> ['class'=>'text-logo-color'],
                    ],
                ]
            ],
            // [
            //     'label' => '<i class="fa fa-user-md"></i> ' . Yii::t('app', 'Comment'),
            //     'url' => ['/blog/default/blog-comment'],
            //     'linkOptions'=> ['class'=>'text-logo-color'],
            // ],
            // [
            //     'label' => '<i class="fa fa-user-md"></i> ' . Yii::t('app', 'Tag'),
            //     'url' => ['/blog/default/blog-tag'],
            //     'linkOptions'=> ['class'=>'text-logo-color'],
            // ],
        ],
    ],
    [
        'label' => '<i class="fa fa-cog"></i> ' . Yii::t('app', 'System'),
        'url' => ['#'],
        'active' => false,
        'linkOptions'=> ['class'=>'text-logo-color'],
        'items' => [
            [
                'label' => '<i class="fa fa-user"></i> ' . Yii::t('app', 'User'),
                'url' => ['/user'],
                'linkOptions'=> ['class'=>'text-logo-color'],
            ],
            [
                'label' => '<i class="fa fa-lock"></i> ' . Yii::t('app', 'Role'),
                'url' => ['/role'],
                'linkOptions'=> ['class'=>'text-logo-color'],
            ],
        ],
    ],
];
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-left'],
    'items' => $menuItemsMain,
    'encodeLabels' => false,
]);


//NavBar::end();*/

// Usage with bootstrap nav pills.
/*
echo NavX::widget([
    'options'=>['class'=>'nav nav-pills'],
    'items' => [
       // ['label' => 'Action', 'url' => '#'],
        
        ['label' => 'Users', 'url' => yii\helpers\Url::to(['/users']), 'linkOptions'=> ['class'=>'text-logo-color']],
        ['label' => 'Stocks', 'url' => yii\helpers\Url::to(['/stock']), 'linkOptions'=> ['class'=>'text-logo-color']],
       
       ['label' => 'Web Pages','linkOptions'=> ['class'=>'text-logo-color no-arrow'], 'active'=>false, 'items' => [
           ['label' => 'Home Blocks', 'url' => yii\helpers\Url::to(['/homeblocks']),'linkOptions'=> ['class'=>'text-logo-color'] ],
            ['label' => 'About Us', 'url' => yii\helpers\Url::to(['/about']),'linkOptions'=> ['class'=>'text-logo-color'] ],
             ['label' => 'Service', 'url' => yii\helpers\Url::to(['/services']),'linkOptions'=> ['class'=>'text-logo-color'] ],
            ['label' => 'Resources', 'url' => yii\helpers\Url::to(['/resources']), 'linkOptions'=> ['class'=>'text-logo-color']],
             ['label' => 'FAQ', 'url' => yii\helpers\Url::to(['/faq']), 'linkOptions'=> ['class'=>'text-logo-color']],
            ['label' => 'Contact', 'url' => yii\helpers\Url::to(['/contactus']), 'linkOptions'=> ['class'=>'text-logo-color']],
             ['label' => 'Privacy Policy', 'url' => yii\helpers\Url::to(['/privacypolicy']), 'linkOptions'=> ['class'=>'text-logo-color']],
              ['label' => 'Terms & Conditions', 'url' => yii\helpers\Url::to(['/termsconditions']), 'linkOptions'=> ['class'=>'text-logo-color']],
            
        ]],
        ['label' => 'Design','linkOptions'=> ['class'=>'text-logo-color no-arrow'], 'active'=>false, 'items' => [
          
            ['label' => 'Banner', 'url' => yii\helpers\Url::to(['/banner/index']), 'linkOptions'=> ['class'=>'text-logo-color']],
            ['label' => 'Carousel Item', 'url' => yii\helpers\Url::to(['/carouselitem']), 'linkOptions'=> ['class'=>'text-logo-color']],

            
        ]],
        
    ]
]);*/