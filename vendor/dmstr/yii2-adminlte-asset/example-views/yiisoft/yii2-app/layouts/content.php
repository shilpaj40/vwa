<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<div class="content-wrapper" style="min-height:300px!important"> 
    <div class="container">

        <section class="content-header">
            <h1><?= $this->title ?></h1>
            <?=
            Breadcrumbs::widget(
                [
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]
            ) ?>
        </section>

        <section class="content">
            <?= Alert::widget() ?>
            <?= $content ?>
        </section>
    </div>
</div>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; 2020-2021 <a href="#">Xceed Imagination</a>.</strong> All rights
    reserved.
</footer>
   

<aside class="sidebar control-sidebar control-sidebar-dark" id="userProfile">
    <div class="text-center p-4">
        <figure class="avatar avatar-state-success avatar-lg mb-4">
            
            <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="rounded-circle" alt="User Image"/>
        </figure>
        <h4 class="text-primary"><?= \Yii::$app->user->identity->username?></h4>
       
        <a class="d-block btn btn-success text-center" href="<?= yii\helpers\Url::to(["site/changepassword", "id" =>Yii::$app->user->id])?>"><i class="fa fa-key"></i> Change Password</a><br />
        <a class=" d-block  btn btn-success text-center" href="<?= yii\helpers\Url::to(["site/viewprofile", "id" =>Yii::$app->user->id])?>"><i class="fa fa-pencil-square-o"></i> Edit Profile</a><br />
         <a data-method="post" href="<?= yii\helpers\Url::to(["site/logout"])?>" class=" d-block  btn btn-danger text-center"><i class="fa fa-sign-out"></i> Logout</a>
      <div class="row">
        <div class="col-md-6 g-mb-30 g-mb-0--md">
          <div class="g-bg-gray-light-v4 g-color-black text-center g-rounded-10 g-py-25">
            <i class="d-block display-2 g-line-height-1 fa fa-user g-font-size-2 g-mb-10"></i>
            <span class="g-font-weight-700 text-uppercase g-font-size-12">User</span>
          </div>
        </div>
      </div>
    </div>
    
  </aside>
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
