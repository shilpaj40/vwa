<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>
<header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="" style="width:100%"><!--container-->
        <div class="navbar-header">
            <?= Html::a('<img src="'.Yii::$app->params['logo'].'" />', '#', ['class' => 'navbar-brand logo']) ?> 
          <button type="button" class="navbar-toggle collapsed text-logo-color" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
            <div class="navbar-custom-menu">
                 <?= $this->render('//layouts/top-menu.php') ?>
            </div>
          
        </div>
        <!-- /.navbar-collapse -->
        <div class="navbar-custom-menu notifications">
          <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
              <a href="#" data-toggle="control-sidebar" data-sidebar-open="#userProfile">
                <?php echo "<img src='".Yii::$app->params['image']."/images/user.jpg' class='user-image rounded-circle' alt='User Image'></a>"; ?>
            </li>
          </ul>
        </div>
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>

