<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>

<div class="login-box">
    <div class="login-logo">
        <a href="#"><img src="<?= Yii::$app->params['logo'];?>" /></a>
    </div>
    <!-- /.login-logo -->
    <div id="om-lightbox-case-study-optin-bar" class="boston-clearfix boston-optin-bar om-clearfix"><span class="boston-bar boston-yellow-bar om-bar om-red-bar"></span><span class="boston-bar boston-black-bar om-bar om-blue-bar"></span><span class="boston-bar boston-yellow-bar om-bar om-red-bar"></span><span class="boston-bar boston-black-bar om-bar om-blue-bar"></span></div>
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('username')]) ?>

        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

        <div class="row">
            <!-- <div class="col-xs-8">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </div> -->
            <!-- /.col -->
            <div class="col-xs-12">
                <?= Html::submitButton('Sign in', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
            <!-- /.col -->
        </div>


        <?php ActiveForm::end(); ?>
           
        <center><br> <?= Html::a(
                                    'I forgot my password',
                                    ['site/request-password-reset'],
                                    ['class' => ' ']
                                ) ?></center><br>

    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
 <?php if(\Yii::$app->session->hasFlash('loginError')) : ?>
        <div class="alert alert-danger alert-dismissible" style="margin-top: 5%;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <?php echo \Yii::$app->session->getFlash('loginError'); ?>
        </div>
       <?php endif; ?>
<style type="text/css">
    .login-page, .register-page {
    height: auto;
    background: #d2d6de;
    display: block;
    position: fixed;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    background: url(http://localhost:81/shelly/backend/web/images/bg.png) transparent;
    background-size: cover;
    z-index: -1;
}
    .btn-primary {
    background-color: #e7a54f!important;
    border-color: #e7a54f!important;
}
 .btn-primary:hover {
    background-color: #fff!important;
    border-color: #e7a54f!important;
    color:#e7a54f;
}
    .boston-optin-bar {
    margin-bottom: 20px;
}
.boston-red-bar {
    background-color: #b81f1f;
}
.boston-bar {
    float: left;
    width: 25%;
    height: 11px;
    display: block;
}
.boston-green-bar {
    background-color: #abb92e;
}
.boston-yellow-bar {
    background-color: #e7a54f;
}
 .boston-black-bar {
    background-color: #191919;
}
a:hover, a:active, a:focus {
   opacity: 0.8;color:#e7a54f;
}
a{
    color:#e7a54f;
}
</style>