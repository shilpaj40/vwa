<?php namespace pceuropa\menu\controllers;

use Yii;
use yii\web\Response;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

use pceuropa\menu\models\Model;
use pceuropa\menu\models\Search;

class CreatorController extends \yii\web\Controller {

	public function beforeAction($action) {
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}

	public function actionIndex(){

		$searchModel = new Search();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionCreate(){
		$model = new Model();
		$r = Yii::$app->request;
	
//		 if ($r->isAjax) {
//           echo "<pre>";
//           print_r($_POST);
//           echo "</pre>";
//           exit;
//			$m->menu = $r->post('menu');
//			\Yii::$app->response->format = Response::FORMAT_JSON;
//			return ['success' => $m->save(), 'url' => Url::to(['update'])];
//		} else {
//			//return $this->render('create');}
//            
//            return $this->render('create', [
//                'model' => $m,
//            ]);
        //}

//           echo "<pre>";
//           print_r($_POST);
//            print_r($model);
//           echo "</pre>";
//           exit;   
           
        if ($model->load(Yii::$app->request->post()) ) {
           $model->name =   $_POST['Model']['name'];
           $model->description =   $_POST['Model']['description'];
           $model->save(false);
            return $this->redirect(['update', 'id' => $model->menu_id,['model'=>$model]]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }      
  
	}

	public function actionView($id){
		//$m = Model::findModel($id);
        //$mod = new Model();
        $m = $this->findModel($id);
		$r = Yii::$app->request;
	
		 if ($r->isAjax) {
			\Yii::$app->response->format = Response::FORMAT_JSON;

			switch (true) {
				case $r->post('get'): return ['success' => true, 'menu' => $m->menu];
				default: return ['success' => false];
			}
		}

		return $this->render('view', ['model' => $m]);
	}

	public function actionUpdate($id){
		//$m = Model::findModel($id);
        $m = $this->findModel($id);//print_r($m);die;
		$r = Yii::$app->request;
	
		 if ($r->isAjax) {
			\Yii::$app->response->format = Response::FORMAT_JSON;

			switch (true) {
				case $r->isGet : return ['success' => true, 'menu' => $m->menu];
				case $r->post('update'): 
					$m->menu = $r->post('menu');
					return $m->save() ? ['success' => true] : ['success' => false]; 
				default: return ['success' => false];
			}
		}
	
		return $this->render('update', ['model' => $m]);
	}

	public function actionDelete($id){
		$this->findModel($id)->delete();
		return $this->redirect(Yii::$app->request->referrer);
	}
    
	public function findModel($id){
	    if (($model = Model::find()->where(['menu_id' => $id])->one()) !== null) {
	        return $model;
	    } else {
	        return (object) [
					'menu' => '{"left" : [{"label": "wrong id of menu"}], "right": []}',
				  ];
	    }
	}

}
