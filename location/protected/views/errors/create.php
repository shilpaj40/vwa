<?php 
/* @var $this ErrorsController */
/* @var $model Errors */

$this->breadcrumbs=array(
	'Errors'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Errors', 'url'=>array('index')),
	array('label'=>'Manage Errors', 'url'=>array('admin')),
);
?>

<h1>Create Errors</h1>

<?php  $this->renderPartial('_form', array('model'=>$model)); ?>