<?php 
/* @var $this UserLocationController */
/* @var $model UserLocation */
/* @var $form CActiveForm */
?>

<div class="form">

<?php  $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-location-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php  echo $form->errorSummary($model); ?>

	<div class="row">
		<?php  echo $form->labelEx($model,'user_id'); ?>
		<?php  echo $form->textField($model,'user_id'); ?>
		<?php  echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php  echo $form->labelEx($model,'latitude'); ?>
		<?php  echo $form->textField($model,'latitude'); ?>
		<?php  echo $form->error($model,'latitude'); ?>
	</div>

	<div class="row">
		<?php  echo $form->labelEx($model,'longitude'); ?>
		<?php  echo $form->textField($model,'longitude'); ?>
		<?php  echo $form->error($model,'longitude'); ?>
	</div>

	<div class="row">
		<?php  echo $form->labelEx($model,'address'); ?>
		<?php  echo $form->textField($model,'address',array('size'=>60,'maxlength'=>500)); ?>
		<?php  echo $form->error($model,'address'); ?>
	</div>

	<div class="row">
		<?php  echo $form->labelEx($model,'city'); ?>
		<?php  echo $form->textField($model,'city',array('size'=>60,'maxlength'=>100)); ?>
		<?php  echo $form->error($model,'city'); ?>
	</div>

	<div class="row">
		<?php  echo $form->labelEx($model,'battery'); ?>
		<?php  echo $form->textField($model,'battery',array('size'=>10,'maxlength'=>10)); ?>
		<?php  echo $form->error($model,'battery'); ?>
	</div>

	<div class="row buttons">
		<?php  echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php  $this->endWidget(); ?>

</div><!-- form -->