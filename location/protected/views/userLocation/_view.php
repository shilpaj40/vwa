<?php 
/* @var $this UserLocationController */
/* @var $data UserLocation */
?>

<div class="view">

	<b><?php  echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php  echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php  echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php  echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php  echo CHtml::encode($data->getAttributeLabel('latitude')); ?>:</b>
	<?php  echo CHtml::encode($data->latitude); ?>
	<br />

	<b><?php  echo CHtml::encode($data->getAttributeLabel('longitude')); ?>:</b>
	<?php  echo CHtml::encode($data->longitude); ?>
	<br />

	<b><?php  echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
	<?php  echo CHtml::encode($data->address); ?>
	<br />

	<b><?php  echo CHtml::encode($data->getAttributeLabel('city')); ?>:</b>
	<?php  echo CHtml::encode($data->city); ?>
	<br />

	<b><?php  echo CHtml::encode($data->getAttributeLabel('battery')); ?>:</b>
	<?php  echo CHtml::encode($data->battery); ?>
	<br />


</div>