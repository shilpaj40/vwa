<body class="bg-transperent ">
	<head>
    <meta charset="UTF-8">
    <title>Contact Management</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="fonts/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="css/adminLTE/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <link href="css/adminLTE/AdminLTE.css" rel="stylesheet" type="text/css" />
	<link href="css/custom.css" rel="stylesheet" type="text/css" />
	<link href="css/adminLTE/iCheck/flat/_all.css" rel="stylesheet" />
	<link href="fonts/ionicons/css/ionicons.min.css" rel="stylesheet" />
	<link href="css/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />		
	<link href="css/form.css" rel="stylesheet" />	
	
    </head>
	<div id="header">
		<div id="mainMbmenu">
			<center>
				<p style="font-size: 22px;color: rgb(65, 145, 71);"><b>Contact Management & Document Store</b></p>
			</center>
		</div>	
	</div>
	<section class="content">

                    <div class="error-page">
                        <h2 class="headline text-info"> 404</h2>
                        <div class="error-content" style="margin-top:8%;">
                            <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
                            <p>
                                We could not find the page you were looking for.
                            </p>
           
                        </div><!-- /.error-content -->
                    </div><!-- /.error-page -->

                </section><!-- /.content -->


	<div id="footer" class="navbar-fixed-bottom footer_new">
        <div class="container">
            <center>
			<br/>
			<span class="footer-text">
				Copyright © 2015 All Rights Reserved with Deven Infotech
			</span>
                </center>
        </div>
    </div>
</body>
<style>
body > .header {
position: relative;
max-height: 100px;
z-index: 1030;
}
body > .header {
position: absolute;
top: 0;
left: 0;
right: 0;
z-index: 1030;
}
</style>
