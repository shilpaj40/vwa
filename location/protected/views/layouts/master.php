

<body class="skin-blue skin-blue wysihtml5-supported fixed">

		 <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="breadcrumb">
                            <div class="row">
                                <div class="col-lg-5">
                                    <h4>JVS Comatsco Industries Private Limited</h4>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <!-- <div class="pull-right">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger panel-collapse">
                                            <i class="fa fa-caret-up"></i>
                                        </button>
                                    </div>
                                    
                                </div> -->
                             <h4 class="margin-none">
                                    <i class="fa fa-th fa-fw"></i> <?php echo $this->pageTitle;?>
                                </h4>

                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <?php  echo $content; ?>
						                <?php  $this->widget('Flashes'); ?>
                                    </div>
                                    <!-- /.col-lg-6 (nested) -->
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->
     </div>
