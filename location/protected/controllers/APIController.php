<?php
header('Access-Control-Allow-Origin: *');
class APIController extends Controller
{
    // {{{ *** Members ***
    /**
     * Key which has to be in HTTP USERNAME and PASSWORD headers 
     */
    Const APPLICATION_ID = 'ASCCPE';

    private $format = 'json';
    // }}} 
    // {{{ filters
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array();
    } 

    // }}} 
    // {{{ *** Actions ***
    // {{{ actionIndex
    public function actionIndex()
    {
        echo CJSON::encode(array(1, 2, 3));
    }

    
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return MedicalInsuranceProvider the loaded model
     * @throws CHttpException
     */
    public function loadModel($model,$id)
    {
        switch($model)
        {
            // Find respective model
            case 'Profile': // {{{ 
                $model = UserMaster::model()->find(array("condition"=>"id='".$id."'"));        
                break; // }}}
            case 'Feed': // {{{ 
                $model = FeedMaster::model()->find(array("condition"=>"id='".$id."'"));        
                break; // }}}
            default: // {{{ 
                $this->_sendResponse(501, sprintf('Error: Mode <b>update</b> is not implemented for model <b>%s</b>',$model) );
                exit; // }}} 
        }
        if(is_null($model))
            $this->_sendResponse(400, sprintf("Error: Didn't find any model <b>%s</b> with ID <b>%s</b>.",$model, $rowid) );
        return $model;
    }
     // }}} 
    // {{{ *** Actions ***
    // {{{ actiongetAddressBookList
    
    
    /**
     * Sends the API response 
     * 
     * @param int $status 
     * @param string $body 
     * @param string $content_type 
     * @access private
     * @return void
     */
    private function _sendResponse($status = 200, $body = '', $content_type = 'text/html')
    {
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);

        header("Access-Control-Allow-Origin: *");

        // set the status
        header($status_header);
        // set the content type
        header('Content-type: ' . $content_type);

        // pages with body are easy
        if($body != '')
        {
            // send the body
            echo $body;
            exit;
        }
        // we need to create the body if none is passed
        else
        {
            // create some body messages
            $message = '';

            // this is purely optional, but makes the pages a little nicer to read
            // for your users.  Since you won't likely send a lot of different status codes,
            // this also shouldn't be too ponderous to maintain
            switch($status)
            {
                case 200:
                    $message = 'OK';
                    break;
                case 401:
                    $message = 'You must be authorized to view this page.';
                    break;
                case 409:
                    $message = 'Conflict';
                    break;    
                case 404:
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 500:
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501:
                    $message = 'The requested method is not implemented.';
                    break;
            }

            // servers don't always have a signature turned on (this is an apache directive "ServerSignature On")
            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];

            // this should be templatized in a real-world solution
            $body = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
                        <html>
                            <head>
                                <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
                                <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
                            </head>
                            <body>
                                <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
                                <p>' . $message . '</p>
                                <hr />
                                <address>' . $signature . '</address>
                            </body>
                        </html>';

            echo $body;
            exit;
        }
    } 

  

    public function actiongetUsersLocations()
    {
        $user_id="";
       if(isset($_POST['user_id']))
            {
                $user_id = $_POST['user_id'];
            }
       
        $address = array();    
        
         $query="SELECT u.driver_name as name,
                    date_format(c.created_at, '%d-%m-%Y %H:%i:%s') as created_at, c.latitude, c.longitude, c.address, c.city,  '#f00' as color,
                    (CASE WHEN u.status=1 THEN 'Reach loading point'
                    WHEN u.status=2 THEN 'Loading started'
                    WHEN u.status=3 THEN 'Loading completed'
                    WHEN u.status=4 THEN 'Trip started'
                    WHEN u.status=5 THEN 'Reach Unloading point'
                    WHEN u.status=6 THEN 'Unloading started'
                    WHEN u.status=7 THEN 'Unloading completed'
                    WHEN u.status=8 THEN 'Trip end'
                    WHEN u.status=9 THEN 'Trip pause'
                    WHEN u.status=10 THEN 'trip resume'
                    ELSE
                        'Trip not started yet'
                    END) as session_color FROM user_location c, drivers u
                    WHERE c.user_id=u.id ";
        if($user_id!="" && $user_id!='0')
            $query.=" and c.user_id IN (".$user_id.")";
        
        $query .= " ORDER BY c.user_id";
        $models=Yii::app()->db->createCommand($query)->queryAll();
        for($i=0;$i<count($models);$i++)
        {
            $address[] = array(
                $models[$i]['name']."<br />".$models[$i]['address']."<br />".$models[$i]['city']."<br />".$models[$i]['created_at']."<br/>". $models[$i]['session_color'],
                $models[$i]['latitude'],
                $models[$i]['longitude'],
                $models[$i]['color'],
                $models[$i]['session_color'],
            );
        }
       
        $this->_sendResponse(200, CJSON::encode($address));
    }

   
    
    /**
     * Gets the message for a status code
     * 
     * @param mixed $status 
     * @access private
     * @return string
     */
    private function _getStatusCodeMessage($status)
    {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported'
        );

        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    // }}} 
    // {{{ _getObjectEncoded
    /**
     * Returns the json or xml encoded array
     * 
     * @param mixed $model 
     * @param mixed $array Data to be encoded
     * @access private
     * @return void
     */
    private function _getObjectEncoded($model, $array)
    {
        if(isset($_GET['format']))
            $this->format = $_GET['format'];

        if($this->format=='json')
        {
            return CJSON::encode($array);
        }
        elseif($this->format=='xml')
        {
            $result = '<?xml version="1.0">';
            $result .= "\n<$model>\n";
            foreach($array as $key=>$value)
                $result .= "    <$key>".utf8_encode($value)."</$key>\n"; 
            $result .= '</'.$model.'>';
            return $result;
        }
        else
        {
            return;
        }
    } // }}} 
    // }}} End Other Methods
}